<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 19.05.15
 * Time: 7:13
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class ResponsiveSlidesAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'bower_components/ResponsiveSlides.js/responsiveslides.min.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}