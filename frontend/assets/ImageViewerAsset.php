<?php
/**
 * Created by PhpStorm.
 * User: iavGipostroy
 * Date: 19.05.2015
 * Time: 9:33
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class ImageViewerAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/box.js',
    ];

    public $css = [
        'css/box.css',
    ];

    public $depend = [
        'yii\web\JqueryAsset',
    ];
}