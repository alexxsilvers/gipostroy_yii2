<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 19.05.15
 * Time: 6:37
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class ChosenAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'bower_components/chosen/public/chosen.jquery.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}