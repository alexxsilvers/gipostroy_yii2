<?php
/**
 * Created by PhpStorm.
 * User: iavGipostroy
 * Date: 19.05.2015
 * Time: 9:35
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class JUIAsset extends AssetBundle {
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'bower_components/jquery-ui/ui/jquery.ui.core.js',
        'bower_components/jquery-ui/ui/jquery.ui.widget.js',
        'bower_components/jquery-ui/ui/jquery.ui.mouse.js',
        'bower_components/jquery-ui/ui/jquery.ui.datepicker.js',
        'bower_components/jquery-ui/ui/jquery.ui.slider.js',
        'bower_components/jquery.cookie/jquery.cookie.js',
        'bower_components/jQuery-Mask-Plugin/jquery.mask.js',
    ];

    public $depend = [
        'yii\web\JqueryAsset',
    ];
}