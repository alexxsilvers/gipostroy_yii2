<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 19.05.15
 * Time: 6:33
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class FoundationAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'bower_components/foundation/js/foundation.min.js',
        'bower_components/foundation/js/foundation/foundation.tab.js',
        'bower_components/foundation/js/foundation/foundation.dropdown.js',
    ];
}