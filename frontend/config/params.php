<?php
return [
    'adminEmail' => 'admin@example.com',
    'gipoEmail' => 'info@gipostroy.ru',
    'upload_dir' => '../web/uploaded_files',
    'allowed_tags' => '<noindex></noindex><a></a><b></b><h1></h1><h2></h2><h3></h3><br><br/><ul><li></ul></li><ol></ol><p></p><div></div>',
    'good_fields' => [
        '`ct_catalog`.`ct_id`', '`ct_catalog`.`ct_name`', '`ct_catalog`.`ct_newest`', '`ct_catalog`.`ct_sale`', '`ct_catalog`.`ct_hit`',
        '`ct_catalog`.`edm`', '`ct_catalog`.`ct_sale_price`', '`ct_catalog`.`ct_pic`', '`ct_catalog`.`ct_url`', '`ct_catalog`.`ct_price`',
        '`ct_catalog`.`ct_status`', '`ct_catalog`.`ct_create_time`', '`ct_catalog`.`ct_balance`', '`ct_catalog`.`ct_balance_storage`',
        '`ct_catalog`.`ct_newart`', '`ct_catalog`.`ct_mn_id`', '`ct_catalog`.`ct_fl_amount`', '`ct_catalog`.`ct_mn_id`', '`ct_catalog`.`ct_cable_cut`',
        '`ct_catalog`.`ct_covrolin_rezka`', '`ct_catalog`.`ct_fl_dlina`', '`ct_catalog`.`ct_covrolin_width`', '`ct_catalog`.`ct_linoleum_rezka`',
        '`ct_catalog`.`ct_linoleum_width`', '`ct_catalog`.`ct_fl_area`', '`ct_catalog`.`ct_vin_area`'
    ],
    'section_filters' => require(__DIR__ . '/sectionFiltersConfig.php'),
    'subsection_filters' => require(__DIR__ . '/subsectionFiltersConfig.php')
];
