<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'Гипострой.ру',
    'name' => 'Гипострой.ру',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'cart' => [
            'class' => 'yz\shoppingcart\ShoppingCart',
            'cartId' => 'gipostroy_cart',
        ],
        'user' => [
            'identityClass' => 'common\models\Users',
            'enableAutoLogin' => true,
            'loginUrl' => ['site/index'],
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '&nbsp;',
            'thousandSeparator' => ' ',
        ],
        'i18n' => [
            'translations' => [
                'front*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'sourceLanguage' => 'ru-RU',
                    //'fileMap' => [
                    //    'cart' => 'cart.php',
                    //],
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                'top-sales' => 'site/top',
                'logout' => 'ajax/logout',
                'kompleksnoye-snabzhenie' => 'site/kompleks',
                'map' => 'site/map',
                'catalog/item/<id:[\w-]+>' => 'catalog/item',
                'proizvoditeli' => 'site/proizvoditeli',
                'proizvoditeli/<name>' => 'site/show',

                'page/article/<name>' => 'site/article',

                ['class' => 'app\components\SeoPageRule'],
                ['class' => 'app\components\TagPageRule'],

                'catalog/section/<name>/<manuf>'=>'catalogmanufacter/section',
                'catalog/subsection/<name>/<manuf>'=>'catalogmanufacter/subsection',

                '<controler>/<action>/<name:[\w-]+>' => '<controler>/<action>',
                'catalog/<action>/<name>/<manuf>'=>'catalogmanufacter/<action>',
            ],
        ],
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl' => '@web/assets',
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,   // do not publish the bundle
                    'js' => [
                        'bower_components/jquery/dist/jquery.min.js',
                    ]
                ],
            ],
        ],
        'request' => [
            'baseUrl' => ''
        ]
    ],
    'params' => $params,
];
