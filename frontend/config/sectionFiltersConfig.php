<?php
/**
 * Конфигурация для фильтров во втором уровне
 * Пример реализации
      74 => [ // id 2 уровня
           'ct_vin_class' => [ // Поле из таблицы ct_catalog, по которому будем искать значения фильтра
                'param' => 'doska_obreznaya_width', // GET-параметр
                'name' => 'Толщина доски', // Название фильтра
                'withManufacter' => true, // ТОЛЬКО для поля коллекции, если хотим имитацию умного фильтра
                'labelAsUrl' => true, // Если label должен быть ссылкой (можно выбрать ИЛИ отображение ссылкой ИЛИ изображением (type = wide))
                'labelOptions' => [
                     'route' => 'lumber/doskaobrezwidth', // Указываем руот для ссылки
                     'param' => 'width', // Указываем параметр ссылки
                     'replace' => [ // Можно настроить замену симоволов
                            '/',
                            '-',
                      ],
                     'strtolower' => true, // Применить функцию strtolower
                ],
                'type' => 'default', // Указываем тип фильтра (default или wide(изображения))
                'wideImages' => [ // Изобрание для каждого значения фильтра
                     1 => '<br/><img src="/img/laminatfasca/l1.jpg" style="width:80px;height:80px;"/>',
                     2 => '<br/><img src="/img/laminatfasca/l2.jpg" style="width:80px;height:80px;"/>',
                     3 => '<br/><img src="/img/laminatfasca/l3.jpg" style="width:80px;height:80px;"/>',
                     4 => '<br/><img src="/img/laminatfasca/l4.jpg" style="width:80px;height:80px;"/>',
                     5 => '<br/><img src="/img/laminatfasca/l5.jpg" style="width:80px;height:80px;"/>',
                ],
                'replaceValuesModel' => [ // Можно указать модель для замены значений (Только для type == default и )
                    'name' => CableType::className(),
                    'keyField' => 'cabt_id',
                    'valueField' => 'cabt_name',
                ],
                'replaceValues' => [ // Можно настроить замену значений
                    2 => 'Нет',
                    1 => 'Да',
                ],
                'values' => [], // Массив для значений фильтра
           ],
      ],
 */
use common\models\CableType;

return [
    127 => [
        'ct_cable_type' => [
            'param' => 'cable_type',
            'name' => 'Тип кабеля',
            'type' => 'default',
            'replaceValuesModel' => [
                'name' => CableType::className(),
                'keyField' => 'cabt_id',
                'valueField' => 'cabt_name',
            ],
            'values' => [],
        ],
        'ct_cable_conductor' => [
            'param' => 'cable_conductor',
            'name' => 'Количество жил',
            'type' => 'default',
            'values' => [],
        ],
    ],
    176 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    184 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    177 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    180 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    183 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    179 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    178 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    181 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    182 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    123 => [
        'ct_aut_polus' => [
            'param' => 'ct_aut_polus',
            'name' => 'Количество полюсов',
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
        'ct_aut_tok' => [
            'param' => 'ct_aut_tok',
            'name' => 'Номинальный ток',
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
    ],
    190 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_cable_cut' => [
            'param' => 'detSvetType',
            'name' => 'Тип',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    192 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_cable_cut' => [
            'param' => 'detSvetType',
            'name' => 'Тип',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    193 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_cable_cut' => [
            'param' => 'detSvetType',
            'name' => 'Тип',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    185 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_colvo' => [
            'param' => 'ct_lamp_colvo',
            'name' => 'Кол-во ламп',
            'type' => 'default',
            'values' => [],
        ],
        'ct_ker_color' => [
            'param' => 'ct_ker_color',
            'name' => 'Цвет арматуры',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorPlafon',
            'name' => 'Цвет плафона',
            'type' => 'default',
            'values' => [],
        ],
        'ct_cable_cut' => [
            'param' => 'detSvetType',
            'name' => 'Тип',
            'type' => 'default',
            'values' => [],
        ],
        'ct_tema' => [
            'param' => 'svetStyle',
            'name' => 'Стиль',
            'type' => 'default',
            'values' => [],
        ],
    ],
    50 => [
        'ct_vin_zamok' => [
            'param' => 'plintus_furnitura',
            'name' => 'Тип',
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
    ],
    49 => [
        'ct_fl_collection' => [
            'param' => 'collectionkr',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_zamok' => [
            'param' => 'parketWidth',
            'name' => 'Толщина',
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'parketPolos',
            'name' => 'Количество полос',
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
    ],
    194 => [
        'ct_pil_razm' => [
            'param' => 'powerVodonagrev',
            'name' => 'Мощность',
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lamp_power' => [
            'param' => 'naprVodonagrev',
            'name' => 'Напряжение',
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'obiemVodonagrev',
            'name' => 'Объем',
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
        'ct_pil_dost' => [
            'param' => 'podachaVodonagrev',
            'name' => 'Способ подачи воды',
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
        'ct_pl_collection' => [
            'param' => 'ustanovkaVodonagrev',
            'name' => 'Установка',
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
    ],
    72 => [
        'ct_pil_sort' => [
            'param' => 'vagonka_class',
            'name' => 'Класс',
            'labelAsUrl' => true,
            'labelOptions' => [
                'route' => 'seo/sectionseo',
                'sectionParam' => 'name',
                'sectionParamValue' => 'vagonka',
                'param' => 'pageName',
            ],
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
    ],
    82 => [
        'ct_pil_sort' => [
            'param' => 'fanera_sort',
            'name' => 'Сорт',
            'labelAsUrl' => true,
            'labelOptions' => [
                'route' => 'seo/sectionseo',
                'sectionParam' => 'name',
                'sectionParamValue' => 'fanera',
                'param' => 'pageName',
                'replace' => [
                    '/',
                    '-',
                ],
            ],
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'fanera_list',
            'name' => 'Толщина листа',
            'labelAsUrl' => true,
            'labelOptions' => [
                'route' => 'seo/sectionseo',
                'sectionParam' => 'name',
                'sectionParamValue' => 'fanera',
                'param' => 'pageName',
            ],
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],

        'ct_vin_ves' => [
            'param' => 'fanera_mark',
            'name' => 'Марка',
            'labelAsUrl' => true,
            'labelOptions' => [
                'route' => 'seo/sectionseo',
                'sectionParam' => 'name',
                'sectionParamValue' => 'fanera',
                'param' => 'pageName',
                'replaceParam' => [
                    'ФК' => 'fk',
                    'ФСФ' => 'fsf',
                ],
            ],
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
    ],
    74 => [
        'ct_vin_class' => [
            'param' => 'doska_obreznaya_width',
            'name' => 'Толщина доски',
            'labelAsUrl' => true,
            'labelOptions' => [
                'route' => 'seo/sectionseo',
                'sectionParam' => 'name',
                'sectionParamValue' => 'doska-obreznaya',
                'param' => 'pageName',
            ],
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
    ],
    76 => [
        'ct_vin_class' => [
            'param' => 'imitation_brus_width',
            'name' => 'Ширина бруса',
            'labelAsUrl' => true,
            'labelOptions' => [
                'route' => 'seo/sectionseo',
                'sectionParam' => 'name',
                'sectionParamValue' => 'imitaciya-brusa',
                'param' => 'pageName',
            ],
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
    ],
    219 => [
        'ct_vin_colvo' => [
            'param' => 'qtyMoyka',
            'name' => 'Количество чаш',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'colorMoyka',
            'name' => 'Цвет',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_razm' => [
            'param' => 'materialMoyka',
            'name' => 'Материал',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_zamok' => [
            'param' => 'installMoyka',
            'name' => 'Установка',
            'type' => 'default',
            'values' => [],
        ],
    ],
    221 => [
        'ct_ker_type' => [
            'param' => 'polotenceForma',
            'name' => 'Форма',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_zamok' => [
            'param' => 'polotenceType',
            'name' => 'Тип',
            'type' => 'default',
            'values' => [],
        ],
    ],
    154 => [
        'ct_vin_zamok' => [
            'param' => 'vinilType',
            'name' => 'Тип замка',
            'type' => 'default',
            'values' => [],
        ],
    ],
    261 => [
        'ct_ker_type' => [
            'param' => 'pissuarMaterial',
            'name' => 'Материал',
            'type' => 'default',
            'values' => [],
        ],
    ],
    46 => [
        'ct_fl_collection' => [
            'param' => 'laminatCollection',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_lf_id' => [
            'param' => 'laminatFasca',
            'name' => 'Фаска',
            'type' => 'wide',
            'wideImages' => [
                'Без фаски' => 'Без фаски<br/><img src="/img/laminatfasca/l1.jpg" style="width:80px;height:80px;"/>',
                'Микрофаска' => 'Микрофаска<br/><img src="/img/laminatfasca/l2.jpg" style="width:80px;height:80px;"/>',
                'Фаска 2х' => 'Фаска 2х<br/><img src="/img/laminatfasca/l3.jpg" style="width:80px;height:80px;"/>',
                'Фаска 4х' => 'Фаска 4х<br/><img src="/img/laminatfasca/l4.jpg" style="width:80px;height:80px;"/>',
                'U-фаска' => 'U-фаска<br/><img src="/img/laminatfasca/l5.jpg" style="width:80px;height:80px;"/>',
            ],
            'values' => [],
        ],
        'ct_laminatolshina' => [
            'param' => 'laminatThick',
            'name' => 'Толщина',
            'type' => 'default',
            'values' => [],
        ],
        'ct_laminatclass' => [
            'param' => 'laminatClass',
            'name' => 'Класс',
            'type' => 'default',
            'values' => [],
        ],
        'ct_vin_class' => [
            'param' => 'laminatWater',
            'name' => 'Водостойкость',
            'type' => 'default',
            'values' => [],
        ],
    ],
    45 => [
        'ct_fl_collection' => [
            'param' => 'covrolinCollection',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_covrolin_width' => [
            'param' => 'covrolinWidth',
            'name' => 'Ширина',
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
        'ct_covrolin_rezka' => [
            'param' => 'covrolinRezka',
            'name' => 'Резка',
            'withManufacter' => false,
            'type' => 'default',
            'replaceValues' => [
                2 => 'Нет',
                1 => 'Да',
            ],
            'values' => [],
        ],
        'ct_vin_area' => [
            'param' => 'covrolinTypeModel',
            'name' => 'Тип основы',
            'withManufacter' => false,
            'type' => 'default',
            'values' => [],
        ],
        'ct_covrolin_shade' => [
            'param' => 'covrolinShade',
            'name' => 'Цвет декора',
            'withManufacter' => false,
            'type' => 'wide',
            'wideImages' => [
                1 => '<div style="text-align:center;">Бежевый</div><div style="display: inline-flex;border-radius:3px 3px 3px 3px;border:1px solid #d7d7d7;width:90px;height:90px;background-color:#d2b48c"></div>',
                2 => '<div style="text-align:center;">Коричневый</div><div style="display: inline-flex;border-radius:3px 3px 3px 3px;border:1px solid #d7d7d7;width:90px;height:90px;background-color:#964b00"></div>',
                3 => '<div style="text-align:center;">Серый</div><div style="display: inline-flex;border-radius:3px 3px 3px 3px;border:1px solid #d7d7d7;width:90px;height:90px;background-color:#808080"></div>',
                4 => '<div style="text-align:center;">Зеленый</div><div style="display: inline-flex;border-radius:3px 3px 3px 3px;border:1px solid #d7d7d7;width:90px;height:90px;background-color:#008000"></div>',
                5 => '<div style="text-align:center;">Фиолетовый</div><div style="display: inline-flex;border-radius:3px 3px 3px 3px;border:1px solid #d7d7d7;width:90px;height:90px;background-color:#8b00ff"></div>',
                6 => '<div style="text-align:center;">Черный</div><div style="display: inline-flex;border-radius:3px 3px 3px 3px;border:1px solid #d7d7d7;width:90px;height:90px;background-color:#000000"></div>',
                7 => '<div style="text-align:center;">Белый</div><div style="display: inline-flex;border-radius:3px 3px 3px 3px;border:1px solid #d7d7d7;width:90px;height:90px;background-color:#FFFFFF"></div>',
                8 => '<div style="text-align:center;">Синий</div><div style="display: inline-flex;border-radius:3px 3px 3px 3px;border:1px solid #d7d7d7;width:90px;height:90px;background-color:#206fd2"></div>',
                10 => '<div style="text-align:center;">Красный</div><div style="display: inline-flex;border-radius:3px 3px 3px 3px;border:1px solid #d7d7d7;width:90px;height:90px;background-color:#ff0000"></div>',
                11 => '<div style="text-align:center;">Желтый</div><div style="display: inline-flex;border-radius:3px 3px 3px 3px;border:1px solid #d7d7d7;width:90px;height:90px;background-color:#FFFF00"></div>',
            ],
            'values' => [],
        ],
    ],
    47 => [
        'ct_fl_collection' => [
            'param' => 'linoleumCollection',
            'name' => 'Коллекция',
            'withManufacter' => true,
            'type' => 'default',
            'values' => [],
        ],
        'ct_linoleum_width' => [
            'param' => 'linoleumWidth',
            'name' => 'Ширина',
            'type' => 'default',
            'values' => [],
        ],
        'ct_linoleum_rezka' => [
            'param' => 'linoleumRezka',
            'name' => 'Резка',
            'type' => 'default',
            'replaceValues' => [
                2 => 'Нет',
                1 => 'Да',
            ],
            'values' => [],
        ],
        'ct_vin_area' => [
            'param' => 'linoleumOsnova',
            'name' => 'Тип основы',
            'type' => 'default',
            'values' => [],
        ],
    ],
];