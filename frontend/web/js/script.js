(function() {
    $(window).load(function() {
        return $('.catalog_block .menu .drop').css({
            'min-height': $('.catalog_block .menu').outerHeight() + 1
        });
    });

    $('.tab-childs-show').click(function(){
        var li = $(this).parent().prev();
        li.children().show();
        $(this).remove();
    });

    $(document).ready(function() {
        var menuDrops = $('.drop');
        menuDrops.each(function(){
            var leng = $(this).children('ul').length;
            if(leng==1) {
                $(this).css('width', '258px');
            }
            if(leng==2) {
                $(this).css('width', '516px');
            }
            if(leng==3) {
                $(this).css('width', '773px');
            }
            if(leng==4) {
                $(this).css('width', '1030px');
            }
        });

        var divs = $('.myId');
        divs.each(function(){
            $(this).attr('style', 'min-height:370px;');
        });

        $('.getMoreAdditional').click(function() { //Показываем больше "Товаров из этой категории" или прячем их
            var additionalGoods = $('.addGood');
            if($(this).hasClass('clicked')) {
                $(this).removeClass('clicked');
                additionalGoods.fadeOut();
                $(this).text('Показать');
            } else {
                $(this).addClass('clicked');
                additionalGoods.fadeIn();
                $(this).text('Скрыть');
            }
        });

        $('.getOtherPprz').click(function() { //Показываем больше "Товаров из этой категории" или прячем их
            var additionalGoods = $('.otherPprz');
            if($(this).hasClass('clicked')) {
                $(this).removeClass('clicked');
                additionalGoods.fadeOut();
                $(this).text('Показать');
            } else {
                $(this).addClass('clicked');
                additionalGoods.fadeIn();
                $(this).text('Скрыть');
            }
        });

        var cart, empty_cart, siderRange;
        var button = $('#basketButton');
        if ($('.catalog_block').hasClass('show')) {
            $('.catalog_block .menu').css({
                'display': 'block'
            });
            $('.menuCustom').css({
                'display': 'block'
            });
        } else {
            $('.catalog_block .menu').hide();
            $('.menuCustom').hide();
        }
        $('#catalog').click(function() {
            $('.catalog_block .menu').slideToggle();
            $('.menuCustom').slideToggle();
            return $('.catalog_block').toggleClass('show');
        });
        $('#add_new_address').click(function(e) {
            e.preventDefault();
            $('.page.dash .addresses').addClass('hide');
            $('.shipmentMapBox').fadeIn();
            return $('.page.dash .new_address').removeClass('hide');
        });
        $('.new_address .back').click(function(e) {
            e.preventDefault();
            $('.page.dash .addresses').removeClass('hide');
            $('.shipmentMapBox').fadeOut();
            return $('.page.dash .new_address').addClass('hide');
        });
        $('.how_to_get a').click(function(e) {
            e.preventDefault();
            return $(this).parent().toggleClass('show');
        });
        $('.datepicker').datepicker({
            showOn: 'button',
            buttonImage: '/images/icons/calendar.png',
            buttonImageOnly: true,
            language: 'ru',
            dateFormat: 'yy-mm-dd'
        });
        var st = $('#filter_date_start').val();
        var en = $('#filter_date_end').val();
        $('#filter_date_start').datepicker('setDate', st);
        $('#filter_date_end').datepicker('setDate', en);
        $(document).foundation();
        $('.order .expand-control').click(function(e) {
            var order;
            e.preventDefault();
            order = $(this).parent();
            $(this).toggleClass('expanded');
            order.find('.order-header').slideToggle(250, function() {
                return order.find('.order-header .order-info a, .order-footer .order-info a').fadeToggle(350);
            });
            return setTimeout(function() {
                return order.find('.order-body').slideToggle(350);
            }, 200);
        });
        $('#callback_phone, #Order_or_phone ,#ur_phone, #one_click_call_phone, #dash_phone, #fiz_phone, .mask-phone').mask('+7 (000) 000 00 00');


        window.open_modal = function(modal) {
            $('.modal').removeClass('open').fadeOut(150);
            $('.modals').addClass('open').fadeIn(250);
            $('.modals').find('.' + modal).addClass('open').fadeIn(150);
            return $('.modals').find('.' + modal + ' .chosen-select-modal').chosen({
                disable_search_threshold: 10
            });
        };
        window.close_modal = function() {
            $('.modal').removeClass('open').fadeOut(250);
            return $('.modals').removeClass('open').fadeOut(350);
        };
        $('.modal .cross').click(function(e) {
            e.preventDefault();
            return close_modal();
        });

        $('.add-from-good-card').click(function(){
            var sendData = {
                id: $(this).data('id'),
                quantity: $('.qty-input').val(),
                _csrf: yii.getCsrfToken()
            };
            $.ajax({
                type: 'post',
                url: '/basket/add',
                dataType: 'json',
                data: sendData,
                cache    : false,
                success:function(responce) {
                    if(responce.success) {
                        button.removeClass('button-big').addClass('button-orange').addClass('basket-non-empty');
                        cart = '<img src="/images/icons/cart.png" alt="' + responce.itemsCountString + ' ' + responce.totalPrice + ' р">' +
                        '<span><span class="qty">' + responce.itemsCountString + '</span> <span class="sum">' + responce.totalPrice + ' р</span></span>';
                        button.html(cart);
                    }
                },
                error: function(){
                }
            });
        });

        $('.add_to_cart').click(function() { //Добавляем товар в корзину из любого места кроме карточки товара
            var sendData = {
                id: $(this).data('id'),
                _csrf: yii.getCsrfToken()
            };
            $.ajax({
                type: 'post',
                url: '/basket/add',
                dataType: 'json',
                data: sendData,
                cache    : false,
                success:function(responce) {
                    if(responce.success) {
                        button.removeClass('button-big').addClass('button-orange').addClass('basket-non-empty');
                        cart = '<img src="/images/icons/cart.png" alt="' + responce.itemsCountString + ' ' + responce.totalPrice + ' р">' +
                        '<span><span class="qty">' + responce.itemsCountString + '</span> <span class="sum">' + responce.totalPrice + ' р</span></span>';
                        button.html(cart);
                    }
                },
                error: function(){
                }
            });
        });

        $('.mass_add_to_cart').click(function() { //Добавляем товар в корзину из любого места кроме карточки товара
            var sendData = {
                ids: $(this).data('ids'),
                _csrf: yii.getCsrfToken()
            };
            $.ajax({
                type: 'post',
                url: '/basket/massadd',
                dataType: 'json',
                data: sendData,
                cache    : false,
                success:function(responce) {
                    if(responce.success) {
                        button.removeClass('button-big').addClass('button-orange').addClass('basket-non-empty');
                        cart = '<img src="/images/icons/cart.png" alt="' + responce.itemsCountString + ' ' + responce.totalPrice + ' р">' +
                        '<span><span class="qty">' + responce.itemsCountString + '</span> <span class="sum">' + responce.totalPrice + ' р</span></span>';
                        button.html(cart);
                    }
                },
                error: function(){
                }
            });
        });

        $('.qtyRemove').click(function(){
            var qtyInput = $(this).next('.qty-input');
            var nowValue = parseFloat(qtyInput.val());
            var step = parseFloat(qtyInput.data('step'));
            var min = parseFloat(qtyInput.data('min'));

            if((nowValue - step) >= min) {
                qtyInput.val(nowValue - step);
            }
        });

        $('.qtyAdd').click(function(){
            var qtyInput = $(this).prev('.qty-input');
            var nowValue = parseFloat(qtyInput.val());
            var step = parseFloat(qtyInput.data('step'));

            qtyInput.val(nowValue + step);
        });

        button.click(function(){
            if(!$(this).hasClass('opened')) {
                var obj = $(this);
                obj.empty().addClass('opened').addClass('cart-preview').removeClass('button')
                    .removeClass('button-orange').removeClass('basket-non-empty');
                $.ajax({
                    type: 'POST',
                    url: '/basket/preview',
                    dataType: 'html',
                    success:function(data) {
                        obj.html(data);
                    }
                });
            }
        });

        $(document).on('click', '.pre-cart-close', function(){
            if(button.hasClass('opened')) {
                var preCart = $('#preCart');
                var count = parseInt(preCart.attr('data-count'));
                var itemsString = preCart.attr('data-items');
                var price = preCart.attr('data-price');
                button.removeClass('opened').removeClass('cart-preview').addClass('button');

                if(count > 0) {
                    button.addClass('basket-non-empty').addClass('button-orange').html('<img src="/images/icons/cart.png" alt="' + itemsString + ' ' + price + ' р"><span><span class="qty">' + itemsString + '</span> <span class="sum">' + price + ' р</span></span>');
                } else {
                    button.addClass('button').addClass('button-big').html('<img src="/images/icons/cart_empty.png" alt="Корзина пуста"><span class="empty_cart">Корзина пуста</span>');
                }
            }
        });


        $(document).on('click', '.preCartDelete', function(){ // Удаляем товар из предпросмотра корзины
            var goodId = $(this).data('id');
            var parent = $(this).parent('.preCartGood');
            var sendData = {
                id: goodId,
                _csrf: yii.getCsrfToken()
            };

            $.ajax({
                type: "post",
                url: "/basket/remove",
                dataType: "json",
                data: sendData,
                success: function(responce){
                    if(responce.success) {
                        var preCart = $('#preCart');
                        if(responce.itemsCount > 0) {
                            parent.remove();
                            $('.preCartSummary').text('В корзине ' + responce.itemsCountString + ' - ' + responce.totalPrice + ' руб.');
                            preCart.attr('data-price', responce.totalPrice);
                            preCart.attr('data-items', responce.itemsCountString);
                            preCart.attr('data-count', responce.itemsCount);
                        } else {
                            preCart.attr('data-count', 0);
                            preCart.html('<div class="preCartEmpty">Ваша корзина пуста. Воспользуйтесь каталогом для добавления товаров.</div>');
                            $('.preCartButtons').remove();
                        }
                    }
                }
            })
        });

        $('.checkout-form form').hide().css('visibility', 'visible');
        $('.checkout-form form.' + $('.for-who .input-radio:checked').val()).show();
        $('.view-options a').click(function(e) {
            e.preventDefault();
            $('.view-options a').removeClass('active');
            $(this).addClass('active');
            if ($(this).hasClass('list')) {
                return $('.result ul').attr({
                    'class': ''
                }).addClass('list');
            } else if ($(this).hasClass('plate')) {
                return $('.result ul').attr({
                    'class': ''
                }).addClass('plate');
            }
        });
        siderRange = {
            min: $('.slider .gipo-slider').data('min'),
            max: $('.slider .gipo-slider').data('max')
        };
        $('.gipo-range .legend .min').html(siderRange.min);
        $('.gipo-range .legend .middle').html((siderRange.max - siderRange.min) / 2 + siderRange.min);
        $('.gipo-range .legend .max').html(siderRange.max);
        $(".gipo-range .slider .gipo-slider").slider({
            range: true,
            step: 10,
            min: siderRange.min,
            max: siderRange.max,
            values: [siderRange.min, siderRange.max],
            slide: function(event, ui) {
                $('#range-min').val(ui.values[0]);
                return $('#range-max').val(ui.values[1]);
            }
        });
        $('.chosen-select-mod').chosen({
            disable_search_threshold: 10
        });
        if (!$('#range-min').val()) {
            $('#range-min').val(siderRange.min);
        } else {
            $(".gipo-range .slider .gipo-slider").slider('values', 0, $('#range-min').val());
        }
        if (!$('#range-max').val()) {
            $('#range-max').val(siderRange.max);
        } else {
            $(".gipo-range .slider .gipo-slider").slider('values', 1, $('#range-max').val());
        }
        $('#range-min').change(function() {
            return $(".gipo-range .slider .gipo-slider").slider('values', 0, $('#range-min').val());
        });
        $('#range-max').change(function() {
            return $(".gipo-range .slider .gipo-slider").slider('values', 1, $('#range-max').val());
        });
        return $('.chosen-select').chosen({
            disable_search_threshold: 10,
            no_results_text: "Не найдено:"
        });
    });

}).call(this);

(function() { //Для планшетников
    $(document).ready(function() {
        return $('.tablet .menu a').click(function(e) {
            if($(this).hasClass('oneClickState')) {
                window.location = $(this).attr('href');
            } else {
                $('.tablet .menu a').removeClass('oneClickState');
                $(this).addClass('oneClickState');
            }

            if ($(this).parent().find('> ul').length) {
                e.preventDefault();
            }
            if (!$(this).parent().hasClass('hover')) {
                $(this).parent().parent().find('.hover').removeClass('hover');
                return $(this).parent().addClass('hover');
            }
        });
    });

}).call(this);


function sendReview() // Отправляем отзыв на info
{
    var review = $('.review');
    $.ajax({
        type: 'post',
        url: '/ajax/review',
        data: review.serialize(),
        dataType: 'json',
        success:function(data) {
            var errBlock = review.find('.error-review');
            errBlock.show().empty();
            review.find('input').removeClass('error-input');
            review.find('textarea').removeClass('error-input');
            if(data.error == 'false') {
                review.find('textarea').val('');
                review.find('input').val('');
                open_modal('review-accepted');
            } else {
                $.each(data.messages, function(i, val){
                    review.find("input[name='ReviewForm["+i+"]']").addClass('error-input');
                    review.find("textarea[name='ReviewForm["+i+"]']").addClass('error-input');
                    errBlock.append(val+"<br/>");
                });
            }
        },
        error: function(){
            var errBlock = review.find('.error-review');
            errBlock.show().empty().append('Произошла ошибка при отправке отзыва, обратитесь к администратору.');
        }
    });
}

function sendCallback() // Обратный звонок
{
    var callback = $(".callback");
    $.ajax({
        type: 'post',
        url: '/ajax/callback',
        data: callback.serialize(),
        dataType: 'json',
        success:function(data) {
            var errBlock = callback.find('.error-callback');
            errBlock.show().empty();
            callback.find('input').removeClass('error-input');
            callback.find('textarea').removeClass('error-input');
            callback.find('select').removeClass('error-input');
            if(data.error == 'false') {
                callback.find('textarea').val('');
                callback.find('input').val('');
                callback.find('select').val('');
                open_modal('callback-accepted');
            } else {
                $.each(data.messages, function(i, val){
                    callback.find("input[name='CallbackForm["+i+"]']").addClass('error-input');
                    $("textarea[name='CallbackForm["+i+"]']").addClass('error-input');
                    $("select[name='CallbackForm["+i+"]']").addClass('error-input');
                    errBlock.append(val+"<br/>");
                });
            }
        },
        error: function(){
            var errBlock = callback.find('.error-callback');
            errBlock.show().empty().append('Произошла ошибка при отправке заявки, обратитесь к администратору.');
        }
    });
}

function sendProposal() // Отправляем заявку
{
    var proposal = $('.proposal');
    $.ajax({
        type: 'post',
        url: '/ajax/proposal',
        data: proposal.serialize(),
        dataType: 'json',
        success:function(data) {
            var errBlock = proposal.find('.error-proposal');
            errBlock.show().empty();
            proposal.find('input').removeClass('error-input');
            proposal.find('textarea').removeClass('error-input');
            if(data.error == 'false') {
                proposal.find('textarea').val('');
                proposal.find('input').val('');
                $('.dropzone-previews').empty();
                $('.dz-message').show();
                open_modal('proposal-accepted');
            } else {
                $.each(data.messages, function(i, val){
                    $("input[name='ProposalForm["+i+"]']").addClass('error-input');
                    errBlock.append(val+"<br/>");
                });
            }
        },
        error: function(){
            var errBlock = proposal.find('.error-proposal');
            errBlock.show().empty().append('Произошла ошибка при отправке заявки, обратитесь к администратору.');
        }
    });
}

function signin() //Авторизация
{
    var signin = $('.signin');
    $.ajax({
        type: 'post',
        url: '/ajax/signin',
        data: signin.serialize(),
        dataType: 'json',
        success:function(data) {
            var errBlock = signin.find('.error-modal');
            errBlock.show().empty();
            signin.find('input').removeClass('error-input');
            if(data.error == 'true') {
                $.each(data.messages, function(i, val){
                    $("input[name='SigninForm["+i+"]']").addClass('error-input');
                    errBlock.append(val+"<br/>");
                });
            }
        },
        error: function(){
            var errBlock = signin.find('.error-proposal');
            errBlock.show().empty().append('Произошла ошибка при отправке заявки, обратитесь к администратору.');
        }
    });
}

function sendOneClickCall() //Заказ в 1 клик
{
    $('.good-id-oneclickcall').val($('.art').data('id'))
    var oneclickcall = $('.oneclickcall');
    var sendButton = $('.one-click-call-send');
    var loaderImg = $('.one-click-call-loading');

    sendButton.hide();
    loaderImg.show();

    $.ajax({
        type: 'post',
        url: '/ajax/oneclickcall',
        data: oneclickcall.serialize(),
        dataType: 'json',
        success:function(data) {
            var errBlock = oneclickcall.find('.error-modal');
            errBlock.show().empty();
            oneclickcall.find('input').removeClass('error-input');
            if(data.error == 'true') {
                sendButton.show();
                loaderImg.hide();
                $.each(data.messages, function(i, val){
                    $("input[name='OneClickCallForm["+i+"]']").addClass('error-input');
                    errBlock.append(val+"<br/>");
                });
            } else {
                open_modal('callback-accepted');
                oneclickcall.find('textarea').val('');
                oneclickcall.find('input').val('');
                sendButton.show();
                loaderImg.hide();
                $('.onec_id').show().html(data.message);
            }
        },
        error: function(){
            var errBlock = oneclickcall.find('.error-oneclickcall');
            errBlock.show().empty().append('Произошла ошибка при создании заказа, обратитесь к администратору.');
        }
    });
}

function getUrlVar() {
    var urlVar = window.location.search; // получаем параметры из урла
    var arrayVar = []; // массив для хранения переменных
    var valueAndKey = []; // массив для временного хранения значения и имени переменной
    var resultArray = []; // массив для хранения переменных
    arrayVar = (urlVar.substr(1)).split('&'); // разбираем урл на параметры
    if(arrayVar[0]=="") return false; // если нет переменных в урле
    for (i = 0; i < arrayVar.length; i ++) { // перебираем все переменные из урла
        valueAndKey = arrayVar[i].split('='); // пишем в массив имя переменной и ее значение
        if(valueAndKey[0]!='psize') {
            resultArray[valueAndKey[0]] = valueAndKey[1]; // пишем в итоговый массив имя переменной и ее значение
        }
    }
    return resultArray; // возвращаем результат
}

$(document).on('change','#pSize',function(){
    var value = $(this).val();
    var parameters = getUrlVar();
    if(!parameters) {
        location = window.location.href+"?psize="+value;
    } else {
        var href = window.location.href;
        href = href.split("?");
        if(Object.keys(parameters).length==0) {
            location = href[0]+"?psize="+value;
        } else {
            var getStr = '';
            var keys = Object.keys(parameters);
            for(var i = 0; i <= keys.length; i++){
                if(keys[i]!=undefined){
                    getStr += keys[i]+"="+parameters[keys[i]]+"&";
                }
            }

            location = href[0]+"?"+getStr+"psize="+value;
        }
    }
});

$(document).on('click', '.changeDisplayGoods', function(){
    console.log($.cookie('display_goods_gipostroy'));
    $.cookie('display_goods_gipostroy', null, {
        path: '/'
    });
    if($(this).hasClass('list')) {
        $.cookie('display_goods_gipostroy', 'list', {
            path: '/'
        });
    } else if($(this).hasClass('plate')) {
        $.cookie('display_goods_gipostroy', 'plate', {
            path: '/'
        });
    }
});

//Автокомплит для поиска
$(document).on('click', 'body', function(){
    $('#show_search').fadeOut();
});
$(document).on("keyup", "#search_input", function(){
    var updBlock = $('#show_search');
    var word = $(this).val();
    var cat = $('#search_cat').val();
    var loadImg = $('#searchLoader');
    if(word.length >= 3) {
        updBlock.fadeIn();
        loadImg.show();
        $.ajax({
            url: "/search/autocomplete",
            data: "term="+word+"&cat="+cat,
            dataType: "html",
            success: function(responce){
                loadImg.hide();
                updBlock.empty().append(responce);
            }
        })
    } else {
        updBlock.html('<img src="/images/ajax-loader.gif" id="searchLoader">');
    }
});

function printDiv(div) // Функция для печати
{
    var divToPrint=document.getElementById(div);
    var newWin=window.open('','Print-Window');
    newWin.document.open();
    newWin.document.write('<html><head>    <link rel="stylesheet" href="/styles/style.css"><body   onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
    newWin.document.close();
    setTimeout(function(){newWin.close();},10);
}
