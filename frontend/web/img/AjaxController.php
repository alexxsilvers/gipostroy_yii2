﻿<?php

class AjaxController extends ExtendedController
{


	public function actionAddToBasket() {
        //Собираем статистику по товарам, которые пользователь положил в корзину
        if(isset($_POST['id'])) {
            $good = Catalog::model()->findByPk($_POST['id']);
            if($good !== null){
                $model = new GoodsStatistycs();
                $model->good_id = $good->ct_id;
                $model->time = new CDbExpression('NOW()');
                $model->ct_co = $good->ct_co_id;
                $model->ct_subco = $good->ct_subco_id;
                $model->price = $good->ct_price;
                $model->costed = 1;
                $model->save(false);
            }
        }

        $good = Catalog::model()->find('ct_id=:id', array(':id'=>$_POST['id']));
        $polArr = array(45, 46, 47, 51, 142, 146);
        $cableArr = array(37, 38, 40, 41, 42, 43, 44, 45, 46, 47);
        if(in_array($good->ct_co_id, $polArr)) {
            echo 'Выберите количество:&nbsp;&nbsp;&nbsp;&nbsp; <strong class="minus" onclick="down(); "></strong><input style="width: 40px;font-size: 12px;text-align:center;margin: 0 7px 0 7px;"
            type="text" value="10" id="colvo"><strong class="plus" onclick="up(); "></strong><a id="sendToBasket"></a>
                <span id="minQuantity" style="display:none;">Минимальный объем заказа 10м<sup>2</sup></span>

	       <script>
	       function up() {
                $("#colvo").val(parseInt($("#colvo").val())+1);
	       };
	       function down() {
	            $("#colvo").val(parseInt($("#colvo").val())-1);
	       }

	       $("#sendToBasket").click(function(){
                var id = '.$_POST['id'].';
                var colvo = $("#colvo").val();
                if(colvo >= 10) {
                    $.ajax({
                        type: "post",
                        url: "/ajax/sendToBasket/",
                        data: "id=" + id + "&colvo="+colvo,
                        success: function(html){
                            $("#noticetext").html("");
                            $("#noticetext").append("<span>"+html+"</span>");
                            setTimeout(\'$("#noticeholder").fadeOut()\', 300);
                        }
                    });
                    $.ajax({
                        type: "post",
                        url: "/ajax/updateBasket/",
                        data: "id=" + id + "&colvo="+colvo,
                        success: function(html){
                            $("#basket").html("");
                            $("#basket").append(html);
                        }
                    });
                } else {
                    $("#minQuantity").show();
	            }
	       });

	       </script>
	       ';
        } elseif(in_array($good->ct_subco_id, $cableArr) && $good->ct_cable_cut <= 10 && $good->ct_cable_cut != '') {
            echo 'Выберите количество:&nbsp;&nbsp;&nbsp;&nbsp; <strong class="minus" onclick="down(); "></strong><input style="width: 40px;font-size: 12px;text-align:center;margin: 0 7px 0 7px;"
            type="text" value="100" id="colvo"><strong class="plus" onclick="up(); "></strong><a id="sendToBasket"></a>
                <span id="minQuantity" style="display:none;">Минимальный объем заказа от 100 м.</span>

	       <script>
           function up() {
                $("#colvo").val(parseInt($("#colvo").val())+1);
	       };
	       function down() {
	            $("#colvo").val(parseInt($("#colvo").val())-1);
	       }

	       $("#sendToBasket").click(function(){
                var id = '.$_POST['id'].';
                var colvo = $("#colvo").val();
                if(colvo >= 100) {
                    $.ajax({
                        type: "post",
                        url: "/ajax/sendToBasket/",
                        data: "id=" + id + "&colvo="+colvo,
                        success: function(html){
                            $("#noticetext").html("");
                            $("#noticetext").append("<span>"+html+"</span>");
                            setTimeout(\'$("#noticeholder").fadeOut()\', 300);
                        }
                    });
                    $.ajax({
                        type: "post",
                        url: "/ajax/updateBasket/",
                        data: "id=" + id + "&colvo="+colvo,
                        success: function(html){
                            $("#basket").html("");
                            $("#basket").append(html);
                        }
                    });
                } else {
                    $("#minQuantity").show();
	            }
	       });

	       </script>
	       ';
        } else {
            echo 'Выберите количество:&nbsp;&nbsp;&nbsp;&nbsp; <strong class="minus" onclick="down(); "></strong><input style="width: 40px;font-size: 12px;text-align:center;margin: 0 7px 0 7px;"
            type="text" value="1" id="colvo"><strong class="plus" onclick="up(); "></strong><a id="sendToBasket"></a>

	       <script>
           function up() {
                $("#colvo").val(parseInt($("#colvo").val())+1);
	       };
	       function down() {
	            $("#colvo").val(parseInt($("#colvo").val())-1);
	       }
	       $("#sendToBasket").click(function(){

	       	var id = '.$_POST['id'].';
	        var colvo = $("#colvo").val();

	       	$.ajax({
				type: "post",
				url: "/ajax/sendToBasket/",
				data: "id=" + id + "&colvo="+colvo,
				success: function(html){
					$("#noticetext").html("");
					$("#noticetext").append("<span>"+html+"</span>");
					setTimeout(\'$("#noticeholder").fadeOut()\', 300);
				}
			});

			$.ajax({
				type: "post",
				url: "/ajax/updateBasket/",
				data: "id=" + id + "&colvo="+colvo,
				success: function(html){
					$("#basket").html("");
					$("#basket").append(html);
				}
			});

	       });

	       </script>
	       ';
        }
    }



	
	public function actionUpdateBasket() {
		$this->renderPartial("updateBasket");
	}
	    

	
    public function actionSendToBasket() {
       if(sizeof(Yii::app()->session['cart']) > 0) {
            $stop = 0;
            $newSession = Array();

            foreach(Yii::app()->session['cart'] as $val) {
                if($_POST['id'] == $val['id']){
                    $stop = 1;
                    $colvo = $val['colvo'] + $_POST['colvo'];
                    $newSession[] = array('id' => $val['id'], 'colvo' => $colvo);
                } else {
                    $newSession[] = $val;
                }
            }

            if($stop == 1){
                $session = new CHttpSession;
                $session->open();
                $session['cart'] = $newSession;
                    if($_POST['colvo'] > 1) {
                        echo 'Товары добавлены в корзину';
                    } else {
                        echo 'Товар добавлен в корзину';
                    }
                exit;
            }

            if($stop == 0){
                $session = new CHttpSession;
                $session->open();
                $cart = Yii::app()->session['cart'];
                $cart[] = array('id' => $_POST['id'], 'colvo' => $_POST['colvo']);
                $session['cart'] = $cart;
            }

       } else {
            $session = new CHttpSession;
            $session->open();
            $cart = Array();
            $cart[0]['id'] = $_POST['id'];
            $cart[0]['colvo'] = $_POST['colvo'];
            $session['cart'] = $cart;
       }

        if($_POST['colvo'] > 1) {
            echo 'Товары добавлены в корзину';
        } else {
            echo 'Товар добавлен в корзину';
        }
        exit;
    }



	public function actionQq() {
		$message = new YiiMailMessage();
		$message->view = 'qq';
		$message->setBody(array('lawyer' => $lawyer), 'text/html');
		$message->addTo('info@gipostroy.ru');
		$message->setSubject('Вы получили новый заказ на обратный звонок gipostroy.ru');
		$message->from = "call@gipostroy.ru";
		Yii::app()->mail->send($message);
		echo 'Ваше сообщение отправлено!';
	}




}

