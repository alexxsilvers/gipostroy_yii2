<?php
namespace app\components;

use common\models\Seo;
use yii\web\UrlRule;

class TagPageRule extends UrlRule
{
    public function init()
    {
        if ($this->name === null) {
            $this->name = __CLASS__;
        }
    }

    public function createUrl($manager, $route, $params)
    {
        if ($route === 'seo/sectiontag') {
            if (isset($params['section'], $params['tag'])) {
                $str = 'catalog/section/' . $params['section'] . '/' . $params['tag'];
                if(isset($params['page'], $params['psize'])) {
                    $str .= '?page=' . $params['page'] . '&' . 'psize=' . $params['psize'];
                }
                return $str;
            }
        }

        if ($route === 'seo/subsectiontag') {
            if (isset($params['subsection'], $params['tag'])) {
                $str = 'catalog/subsection/' . $params['subsection'] . '/' . $params['tag'];
                if(isset($params['page'], $params['psize'])) {
                    $str .= '?page=' . $params['page'] . '&' . 'psize=' . $params['psize'];
                }
                return $str;
            }
        }

        return false;
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = explode('/', $request->getPathInfo());
        if($pathInfo[1] == 'section' && isset($pathInfo[3])) {
            $find = Seo::find()->where(['url' => $pathInfo[3], 'entity_type' => 'tag', 'subcat_id' => 0])->with(['section'])->asArray()->all();
            if($find !== null) {
                foreach($find as $f) {
                    if($f['section']['prz_url'] == $pathInfo[2]){
                        return ['seo/sectiontag', ['section' => $pathInfo[2], 'tag' => $pathInfo[3]]];
                    }
                }
            }
        } elseif($pathInfo[1] == 'subsection' && isset($pathInfo[3])) {
            $find = Seo::find()->where(['url' => $pathInfo[3], 'entity_type' => 'tag'])->with(['subsection'])->asArray()->all();
            if($find !== null) {
                foreach($find as $f) {
                    if($f['subsection']['pprz_url'] == $pathInfo[2]){
                        return ['seo/subsectiontag', ['section' => $pathInfo[2], 'tag' => $pathInfo[3]]];
                    }
                }
            }
        }

        return false;
    }
}