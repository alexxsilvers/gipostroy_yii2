<?php
namespace app\components;

use common\models\Seo;
use yii\web\UrlRule;

class SeoPageRule extends UrlRule
{
    public function init()
    {
        if ($this->name === null) {
            $this->name = __CLASS__;
        }
    }

    public function createUrl($manager, $route, $params)
    {
        if ($route === 'seo/sectionseo') {
            if (isset($params['name'], $params['pageName'])) {
                $str = 'catalog/section/' . $params['name'] . '/' . $params['pageName'];
                if(isset($params['page'], $params['psize'])) {
                    $str .= '?page=' . $params['page'] . '&' . 'psize=' . $params['psize'];
                }
                return $str;
            }
        }

        if ($route === 'seo/subsectionseo') {
            if (isset($params['name'], $params['pageName'])) {
                $str = 'catalog/subsection/' . $params['name'] . '/' . $params['pageName'];
                if(isset($params['page'], $params['psize'])) {
                    $str .= '?page=' . $params['page'] . '&' . 'psize=' . $params['psize'];
                }
                return $str;
            }
        }

        return false;
    }

    public function parseRequest($manager, $request)
    {
        $pathInfo = explode('/', $request->getPathInfo());
        if($pathInfo[1] == 'section' && isset($pathInfo[3])) {
            $find = Seo::find()->where(['url' => $pathInfo[3], 'entity_type' => 'level3', 'subcat_id' => 0])->with(['section'])->asArray()->all();
            if($find !== null && $find['section']['prz_url'] == $pathInfo[2]) {
                foreach($find as $f) {
                    if($f['section']['prz_url'] == $pathInfo[2]) {
                        return ['seo/sectionseo', ['name' => $pathInfo[2], 'pageName' => $pathInfo[3]]];
                    }
                }
            }
        } elseif($pathInfo[1] == 'subsection' && isset($pathInfo[3])) {
            $find = Seo::find()->where(['url' => $pathInfo[3], 'entity_type' => 'level4'])->with(['subsection'])->asArray()->all();
            if($find !== null) {
                foreach($find as $f) {
                    if($f['subsection']['pprz_url'] == $pathInfo[2]) {
                        return ['seo/subsectionseo', ['name' => $pathInfo[2], 'pageName' => $pathInfo[3]]];
                    }
                }
            }
        }

        return false;
    }
}