<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

/* @var $route string */
/* @var $items array() */
?>

<ul class="nav">
    <?php foreach($items as $item => $r) { ?>
    <li <?= in_array($route, $r) ? 'class="active"' : '' ?>>
        <?= Html::a($item, [$r[0]]) ?>
    </li>
    <?php } ?>
</ul>
