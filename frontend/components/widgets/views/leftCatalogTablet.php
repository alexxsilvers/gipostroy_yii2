<?php
/* @var $this yii\web\View */
/* @var $menuArray array() */
/* @var $reviews boolean */
/* @var $show string */
/* @var $showKompleks boolean */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="catalog_block tablet <?= $show ?>">
    <?= Html::a('Каталог', '#', ['id' => 'catalog', 'class' => 'catalog']); ?>
    <ul class="menu">
    <?php foreach($menuArray as $razdel) { ?>
        <li>
            <?= Html::a($razdel['name'], ['catalog/category', 'name' => $razdel['url']]); ?>
            <ul>
                <?php if(!empty($razdel['cat']['column1'])) { ?>
                    <?php foreach($razdel['cat']['column1'] as $section) { ?>
                    <li>
                        <?= Html::a((isset($section['subcat'])) ? $section['name'] . '<span>▶</span>' : $section['name'], ['catalog/section', 'name' => $section['url']]); ?>
                        <?php if(isset($section['subcat'])) { ?>
                        <ul>
                            <?php foreach($section['subcat'] as $subsection) { ?>
                            <li>
                                <?= Html::a($subsection['name'], ['catalog/subsection', 'name' => $subsection['url']]); ?>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </li>
                    <?php } ?>
                <?php } ?>
                <?php if(!empty($razdel['cat']['column2'])) { ?>
                    <?php foreach($razdel['cat']['column2'] as $section) { ?>
                        <li>
                            <?= Html::a((isset($section['subcat'])) ? $section['name'] . '<span>▶</span>' : $section['name'], ['catalog/section', 'name' => $section['url']]); ?>
                            <?php if(isset($section['subcat'])) { ?>
                                <ul>
                                    <?php foreach($section['subcat'] as $subsection) { ?>
                                        <li>
                                            <?= Html::a($subsection['name'], ['catalog/subsection', 'name' => $subsection['url']]); ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                    <?php } ?>
                <?php } ?>
                <?php if(!empty($razdel['cat']['column3'])) { ?>
                    <?php foreach($razdel['cat']['column3'] as $section) { ?>
                        <li>
                            <?= Html::a((isset($section['subcat'])) ? $section['name'] . '<span>▶</span>' : $section['name'], ['catalog/section', 'name' => $section['url']]); ?>
                            <?php if(isset($section['subcat'])) { ?>
                                <ul>
                                    <?php foreach($section['subcat'] as $subsection) { ?>
                                        <li>
                                            <?= Html::a($subsection['name'], ['catalog/subsection', 'name' => $subsection['url']]); ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                    <?php } ?>
                <?php } ?>
                <?php if(!empty($razdel['cat']['column4'])) { ?>
                    <?php foreach($razdel['cat']['column4'] as $section) { ?>
                        <li>
                            <?= Html::a((isset($section['subcat'])) ? $section['name'] . '<span>▶</span>' : $section['name'], ['catalog/section', 'name' => $section['url']]); ?>
                            <?php if(isset($section['subcat'])) { ?>
                                <ul>
                                    <?php foreach($section['subcat'] as $subsection) { ?>
                                        <li>
                                            <?= Html::a($subsection['name'], ['catalog/subsection', 'name' => $subsection['url']]); ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </li>
    <?php } ?>
    </ul>
</div>