<?php
/* @var $this yii\web\View */
/* @var $prices array() */
/* @var $manufacter array() */
/* @var $other array() */

$this->registerJs("
    $('.manufacter').on(\"change\",function(){
        var manufacters = $('.manufacter');
        var collectionFilter = $('#ct_fl_collectionFilter');
        var collections = collectionFilter.find('li');

        collections.hide();
        var counter = 0;
        var i = 0;

        manufacters.each(function(){
            if($(this).is(\":checked\")) {
                counter++;
                var mn = $(this).data('url');
                collections.hide();
                if(collectionFilter.find(\"li[data-manufacter='\" + mn + \"']\").show()) {
                    i++;
                }
            }
        });

        if(i > 0) {
            collections.parent().addClass('open');
        } else {
            collections.parent().removeClass('open');
        }

        if(counter==0) {
            collections.show();
        }
    });
");
?>

<form method="get">
    <div class="row">
        <div class="small-3 columns">
            <table class="range-input">
                <tr>
                    <td>Цена</td>
                    <td><label for="range-min">от</label></td>
                    <td><input name="minPrice" type="number" value="<?= (Yii::$app->request->get('minPrice') > 0) ? Yii::$app->request->get('minPrice') : $prices['min'] ?>" id="range-min"></td>
                    <td><label for="range-max">до</label></td>
                    <td><input name="maxPrice" type="number" value="<?= (Yii::$app->request->get('maxPrice') > 0) ? Yii::$app->request->get('maxPrice') : $prices['max'] ?>" id="range-max"></td>
                </tr>
            </table>
            <div class="gipo-range">
                <div class="slider">
                    <div class="gipo-slider" data-min="<?= $prices['min'] ?>" data-max="<?= $prices['max'] ?>"></div>
                </div>
                <div class="legend">
                    <p class="min"></p><p class="middle"></p><p class="max"></p>
                </div>
            </div>
        </div>
        <div class="small-9 columns">
            <div class="filters-block">
                <?php if(sizeof($manufacter)>1) { ?>
                    <div class="select-drop filter-block">
                        <a href="#" data-dropdown="manufacterFilter" class="select-dropdown-link">Производитель</a>
                        <ul id="manufacterFilter" class="select-dropdown" data-dropdown-content>
                            <?php $manufacterGet = Yii::$app->request->get('manufacter') ?>
                            <?php asort($manufacter) ?>
                            <?php foreach($manufacter as $k=>$v) { ?>
                               <li>
                                    <input <?= (is_array($manufacterGet) && in_array($k,  $manufacterGet)) ? ' checked="checked" ' : '' ?>
                                        name="manufacter[]"
                                        value="<?= $k ?>"
                                        data-url="<?= $v['url'] ?>"
                                        type="checkbox"
                                        class="manufacter"
                                        id="manuf<?= str_replace(array(' ','.',','), '_', $k) ?>"
                                    >
                                    <label for="manuf<?= str_replace(array(' ','.',','), '_', $k) ?>">
                                        <?= $v['name'] ?>
                                    </label>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
                <?php if(!empty($other)) { ?>
                <?php foreach($other as $field=>$filter) { ?>
                    <?php if(sizeof($filter['values']) > 1) { ?>
                    <div class="select-drop filter-block">
                        <a href="#" data-dropdown="<?= $field ?>Filter" class="select-dropdown-link"><?= $filter['name'] ?></a>
                        <ul id="<?= $field ?>Filter" class="select-dropdown <?= ($filter['type'] == 'wide') ? 'dropdown-wide' : '' ?>" data-dropdown-content>
                            <?php $getParam = Yii::$app->request->get($filter['param']) ?>
                            <?php ksort($filter['values']) ?>
                            <?php foreach($filter['values'] as $k=>$v) { ?>
                                    <?php if(is_array($v) && $filter['withManufacter']) { ?>
                                        <li data-manufacter="<?= $v['manufacter'] ?>" class="collectionFilter">
                                            <input <?= (isset($getParam) && in_array($k,  $getParam)) ? ' checked="checked" ' : '' ?>
                                                name="<?= $filter['param'] ?>[]"
                                                value="<?= $k ?>"
                                                type="checkbox"
                                                id="<?= $field . '_' . str_replace(array(' ','.',','), '_', $k) ?>"
                                                >
                                            <label for="<?= $field . '_' . str_replace(array(' ','.',','), '_', $k) ?>">
                                                <?= $v['collection'] ?>
                                            </label>
                                        </li>
                                    <?php } else { ?>
                                        <li>
                                            <input <?= (is_array($getParam) && in_array($k,  $getParam)) ? ' checked="checked" ' : '' ?>
                                                name="<?= $filter['param'] ?>[]"
                                                value="<?= $k ?>"
                                                type="checkbox"
                                                id="<?= $field . '_' . str_replace(array(' ','.',','), '_', $k) ?>"
                                            >
                                            <label for="<?= $field . '_' . str_replace(array(' ','.',','), '_', $k) ?>">
                                                <?= $v ?>
                                            </label>
                                        </li>
                                    <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>
                <?php } ?>
                <?php } ?>
                <div class="clear"></div>
            </div>
        </div>
        <div class="small-3 columns">
            <input type="submit" class="button button-wide button-filter" value="Подобрать">
        </div>
    </div>
</form>