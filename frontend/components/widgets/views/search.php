<?php
/* @var $this yii\web\View */
/* @var $search string */
/* @var $catId integer */
/* @var $razdelArray array() */

use yii\helpers\Html;
?>

<?= Html::beginForm('/search', 'get', ['name' => '#', 'class' => 'search row', 'id' => 'searchFormS']); ?>
<div class="small-3 columns">
	<select id="search_cat" name="category" class="chosen-select chosen-dark" data-placeholder="Категории">
		<?= Html::renderSelectOptions($catId, $razdelArray); ?>
	</select>
</div>
<div class="small-7 columns">
	<?= Html::input('search', 'search', $search, [
        'autocomplete' => 'off',
        'id' => 'search_input',
        'onkeypress' => '"usl(this.value, event)"',
        'placeholder' => 'Введите наименование товара или артикул',
        'class' => 'input-search'
    ]); ?>
</div>
<div class="small-2 columns">
	<?= Html::submitInput('Найти', [
        'id' => 'search-button',
        'class' => 'button button-orange',
    ]); ?>
</div>
<?= Html::endForm(); ?>

<div id="show_search">
	<?= Html::img('/images/ajax-loader.gif', ['id' => 'searchLoader']); ?>
</div>