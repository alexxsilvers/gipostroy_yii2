<?php
/* @var $this yii\web\View */
/* @var $menuArray array() */
/* @var $reviews boolean */
/* @var $show string */
/* @var $showKompleks boolean */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="catalog_block <?= $show ?>">
    <?= Html::a('Каталог', '#', ['id' => 'catalog', 'class' => 'catalog']) ?>
    <ul class="menu">
    <?php foreach($menuArray as $rz_id => $razdel) { ?>
        <li class="razdel-right-arrow">
            <?php if($rz_id == 25) { ?>
                <?= Html::a(
                    Html::img('/images/label_discount.png', ['class' => 'left-catalog-discount']) . $razdel['name'],
                    [
                        'catalog/category',
                        'name' => $razdel['url']
                    ]
                ); ?>
            <?php } else { ?>
                <?= Html::img('/images/categories/icons/' . $razdel['icon'],
                    [
                        'class' => 'left-catalog-icons',
                    ]
                ); ?>
                <?= Html::a($razdel['name'],
                    [
                        'catalog/category',
                        'name' => $razdel['url']
                    ]
                ); ?>
            <?php } ?>
            <div class="drop">
                <?php if(!empty($razdel['cat']['column1'])) { ?>
                <ul>
                    <?php foreach($razdel['cat']['column1'] as $section) { ?>
                    <li class="mainMenu">
                        <?= Html::a(
                            $section['name'],
                            [
                                'catalog/section',
                                'name' => $section['url']
                            ],
                            [
                                'class' => 'level2-main-menu'
                            ]
                        ); ?>
                        <?php if(isset($section['subcat'])) { ?>
                        <ul>
                            <?php foreach($section['subcat'] as $subsection) { ?>
                            <li>
                                <?= Html::a(
                                    $subsection['name'],
                                    [
                                        'catalog/subsection',
                                        'name' => $subsection['url']
                                    ],
                                    [
                                        'class' => 'level3-main-menu'
                                    ]
                                ); ?>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>

                <?php if(!empty($razdel['cat']['column2'])) { ?>
                <ul>
                    <?php foreach($razdel['cat']['column2'] as $section) { ?>
                    <li class="mainMenu">
                        <?= Html::a(
                            $section['name'],
                            [
                                'catalog/section',
                                'name' => $section['url']
                            ],
                            [
                                'class' => 'level2-main-menu'
                            ]
                        ); ?>
                        <?php if(isset($section['subcat'])) { ?>
                        <ul>
                            <?php foreach($section['subcat'] as $subsection) { ?>
                            <li>
                                <?= Html::a(
                                    $subsection['name'],
                                    [
                                        'catalog/subsection',
                                        'name' => $subsection['url']
                                    ],
                                    [
                                        'class' => 'level3-main-menu'
                                    ]
                                ); ?>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>

                <?php if(!empty($razdel['cat']['column3'])) { ?>
                <ul>
                    <?php foreach($razdel['cat']['column3'] as $section) { ?>
                    <li class="mainMenu">
                        <?= Html::a(
                            $section['name'],
                            [
                                'catalog/section',
                                'name' => $section['url']
                            ],
                            [
                                'class' => 'level2-main-menu'
                            ]
                        ); ?>
                        <?php if(isset($section['subcat'])) { ?>
                        <ul>
                            <?php foreach($section['subcat'] as $subsection) { ?>
                            <li>
                                <?= Html::a(
                                    $subsection['name'],
                                    [
                                        'catalog/subsection',
                                        'name' => $subsection['url']
                                    ],
                                    [
                                        'class' => 'level3-main-menu'
                                    ]
                                ); ?>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>

                <?php if(!empty($razdel['cat']['column4'])) { ?>
                <ul>
                    <?php foreach($razdel['cat']['column4'] as $section) { ?>
                    <li class="mainMenu">
                        <?= Html::a(
                            $section['name'],
                            [
                                'catalog/section',
                                'name' => $section['url']
                            ],
                            [
                                'class' => 'level2-main-menu'
                            ]
                        ); ?>
                        <?php if(isset($section['subcat'])) { ?>
                        <ul>
                            <?php foreach($section['subcat'] as $subsection) { ?>
                            <li>
                                <?= Html::a(
                                    $subsection['name'],
                                    [
                                        'catalog/subsection',
                                        'name' => $subsection['url']
                                    ],
                                    [
                                        'class' => 'level3-main-menu'
                                    ]
                                ); ?>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </div>
        </li>
    <?php } ?>
    </ul>
</div>

<?php if($reviews) { ?>
    <noindex>
        <?= Html::a(Html::img('/images/ym.png', ['alt' => 'Читать отзывы']),
            Url::to('http://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2508/*http://market.yandex.ru/shop/110733/reviews', true),
            [
                'class' => 'read_reviews',
                'target' => '_blank'
            ]
        ); ?>
    </noindex>
<?php } ?>

<?php if($showKompleks) { ?>
    <div class="catalog_block show">
        <ul class="menuCustom">
            <li>
                <?= Html::a(
                    'Комплексное снабжение и комплектация строительных объектов',
                    ['site/kompleks'],
                    [
                        'class' => 'kompleks'
                    ]
                ); ?>
            </li>
        </ul>
    </div>
<?php } ?>