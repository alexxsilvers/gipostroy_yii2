<?php
namespace frontend\components\widgets;

use common\models\Razdel;
use yii\base\Widget;
use Yii;

class SearchBlockWidget extends Widget
{
    private $search;
    private $catId;
    private $razdelArray;

    public function init()
    {
        parent::init();
        $this->search = Yii::$app->request->get('search', '');
        $this->catId = Yii::$app->request->get('category', 0);
        $this->razdelArray = Razdel::getDb()->cache(function($db){
            return Razdel::find()->addSelect('rz_name')->show()->orderBy('rz_sorti ASC')->indexBy('rz_id')->asArray()->column();
        }, 1000);
        $this->razdelArray = ['Выбрать раздел для поиска'] + $this->razdelArray;
    }

    public function run()
    {
        return $this->render('search', [
            'search' => $this->search,
            'catId' => $this->catId,
            'razdelArray' => $this->razdelArray,
        ]);
    }
}