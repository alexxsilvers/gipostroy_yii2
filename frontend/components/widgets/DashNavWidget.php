<?php
namespace frontend\components\widgets;

use yii\base\Widget;
use Yii;

class DashNavWidget extends Widget
{
    private $route;
    private $items = [
        'Информация' => ['info', 'edit', 'changepassword'],
        'Адреса доставки' => ['shipment'],
        'Просмотренные товары' => ['viewed'],
        'Мои заказы' => ['orders'],
    ];

    public function init()
    {
        parent::init();

        $this->route = Yii::$app->controller->action->id;
    }

    public function run()
    {
        return $this->render('dashNav', [
            'route' => $this->route,
            'items' => $this->items,
        ]);
    }
}