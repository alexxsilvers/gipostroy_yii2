<?php

namespace frontend\components\widgets;


use common\models\Goods;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class FiltersWidget extends Widget
{
    public $sectionId;
    public $condition = null;
    public $sectionUrl;
    public $manufacterId = null;
    public $type = 'section';

    private $pricesArray = [
        'min' => 0,
        'max' => 0,
    ];
    private $replaceValues = [];
    private $manufacterArray = [];
    private $otherFiltersArray = [];
    private $values = [
        'ct_id',
        'ct_mn_id',
        'ct_price'
    ];

    public function init()
    {
        parent::init();

        if($this->type === 'section') {
            $this->otherFiltersArray = (isset(Yii::$app->params['section_filters'][$this->sectionId])) ? Yii::$app->params['section_filters'][$this->sectionId] : [];
        } elseif($this->type === 'subsection') {
            $this->otherFiltersArray = (isset(Yii::$app->params['subsection_filters'][$this->sectionId])) ? Yii::$app->params['subsection_filters'][$this->sectionId] : [];
        }

        $this->values = (!empty($this->otherFiltersArray)) ? ArrayHelper::merge($this->values, array_keys($this->otherFiltersArray)) : $this->values;
        $sectionId = $this->sectionId;

        $goods = Goods::getDb()->cache(function($db) use ($sectionId) {
            $returnQuery = Goods::find()
                ->addSelect($this->values)
                ->joinWith([
                    $this->type . 'Goods' => function ($query) use ($sectionId) { /* @var $query \yii\db\ActiveQuery */
                        $query->andWhere([
                            ($this->type === 'section') ? 'cat_id' : 'subcat_id' => $sectionId
                        ]);
                    }
                ])
                ->with(['manufacter'])
                ->show()
                ->noDeleted()
                ->asArray();
            if(!is_null($this->manufacterId)) {
                $returnQuery->andWhere(['ct_mn_id' => $this->manufacterId]);
            }
            if(!is_null($this->condition)) {
                $returnQuery->andWhere($this->condition);
            }

            return $returnQuery->all();
        }, 600);

        foreach($goods as $good) {
            if(!isset($this->manufacterArray[$good['ct_mn_id']]) && !empty($good['ct_mn_id'])) { // Производитель
                $this->manufacterArray[$good['ct_mn_id']] = [
                    'name' => Html::a(
                        $good['manufacter']['mn_name'],
                        ['catalogmanufacter/' . $this->type, 'name' => $this->sectionUrl, 'manuf' => $good['manufacter']['mn_url']]
                    ),
                    'url' => $good['manufacter']['mn_url']
                ];
            }

            if($this->pricesArray['min'] == 0) {
                $this->pricesArray['min'] = $good['ct_price'];
            } else {
                if($good['ct_price'] < $this->pricesArray['min']) {
                    $this->pricesArray['min'] = $good['ct_price'];
                }
            }
            if($this->pricesArray['max'] == 0) {
                $this->pricesArray['max'] = $good['ct_price'];
            } else {
                if($good['ct_price'] > $this->pricesArray['max']) {
                    $this->pricesArray['max'] = $good['ct_price'];
                }
            }

            if(!empty($this->otherFiltersArray)) {
                foreach($this->otherFiltersArray as $field => $filter) {
                    if(isset($filter['replaceValuesModel']) && !isset($this->replaceValues[$field])) {
                        $this->replaceValues[$field] = $filter['replaceValuesModel']['name']::find()
                            ->select($filter['replaceValuesModel']['valueField'])->asArray()->indexBy($filter['replaceValuesModel']['keyField'])->column();
                    }
                    if(!empty($good[$field]) && !isset($this->otherFiltersArray[$field]['values'][$good[$field]])) {
                        if(isset($this->otherFiltersArray[$field]['withManufacter']) && $this->otherFiltersArray[$field]['withManufacter']) {
                            $this->otherFiltersArray[$field]['values'][$good[$field]] = [
                                'collection' => $good[$field],
                                'manufacter' => $good['manufacter']['mn_url'],
                            ];
                        } elseif($this->otherFiltersArray[$field]['type'] == 'wide') {
                            $this->otherFiltersArray[$field]['values'][$good[$field]] = $this->otherFiltersArray[$field]['wideImages'][$good[$field]];
                        } else {
                            if(isset($this->otherFiltersArray[$field]['labelAsUrl']) && $this->otherFiltersArray[$field]['labelAsUrl']) {
                                $param = $good[$field];

                                if(!empty($this->otherFiltersArray[$field]['labelOptions']['replace'])) {
                                    $param = str_replace($this->otherFiltersArray[$field]['labelOptions']['replace'][0], $this->otherFiltersArray[$field]['labelOptions']['replace'][1], $good[$field]);
                                }

                                if(($this->otherFiltersArray[$field]['labelOptions']['strtolower'])) {
                                    $param = strtolower($param);
                                }

                                if(isset($this->otherFiltersArray[$field]['labelOptions']['add'])) {
                                    $param = $param . $this->otherFiltersArray[$field]['labelOptions']['add'];
                                }

                                if(isset($this->otherFiltersArray[$field]['labelOptions']['replaceParam'][$good[$field]])) {
                                    $param = $this->otherFiltersArray[$field]['labelOptions']['replaceParam'][$good[$field]];
                                }

                                if(isset($this->otherFiltersArray[$field]['labelOptions']['sectionParam'])) {
                                    $this->otherFiltersArray[$field]['values'][$good[$field]] = Html::a(
                                        $good[$field],
                                        [
                                            $this->otherFiltersArray[$field]['labelOptions']['route'],
                                            $this->otherFiltersArray[$field]['labelOptions']['sectionParam'] => $this->otherFiltersArray[$field]['labelOptions']['sectionParamValue'],
                                            $this->otherFiltersArray[$field]['labelOptions']['param'] => $param,
                                        ]
                                    );
                                } else {
                                    $this->otherFiltersArray[$field]['values'][$good[$field]] = Html::a(
                                        $good[$field],
                                        [
                                            $this->otherFiltersArray[$field]['labelOptions']['route'],
                                            $this->otherFiltersArray[$field]['labelOptions']['param'] => $param,
                                        ]
                                    );
                                }
                            } else {
                                if(isset($filter['replaceValuesModel'])) {
                                    $value = isset($this->replaceValues[$field][$good[$field]]) ? $this->replaceValues[$field][$good[$field]] : $good[$field];
                                } else {
                                    $value = (isset($this->otherFiltersArray[$field]['replaceValues'])) ? $this->otherFiltersArray[$field]['replaceValues'][$good[$field]] : $good[$field];
                                }
                                $this->otherFiltersArray[$field]['values'][$good[$field]] = $value;
                            }
                        }
                    }
                }
            }
        }
    }

    public function run()
    {
        return $this->render('filters', [
            'prices' => $this->pricesArray,
            'manufacter' => $this->manufacterArray,
            'other' => $this->otherFiltersArray,
        ]);
    }
}