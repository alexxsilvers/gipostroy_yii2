<?php

namespace frontend\components\widgets;

use common\models\Podrazdel;
use common\models\Podrazdel3lvl;
use common\models\Razdel;
use frontend\components\MobileDetect;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class LeftCatalogWidget extends Widget {
    public $reviews = true;
    public $show = 'show';
    public $showKompleks = true;
    private $menuArray = array();
    private $isTablet;

    public function init()
    {
        parent::init();
        $detect = new MobileDetect();
        $this->isTablet = $detect->isTablet();

        $getRazdels = Razdel::getDb()->cache(function($db){ // 1 уровень
            return Razdel::find()->show()->asArray()->orderBy('rz_sorti ASC')->all();
        }, 1000);
        foreach($getRazdels as $razdel) {
            $this->menuArray[$razdel['rz_id']] = array(
                'name' => $razdel['rz_name'],
                'icon' => $razdel['rz_icon'],
                'url' => $razdel['rz_url'],
                'cat' => array(
                    'column1' => array(),
                    'column2' => array(),
                    'column3' => array(),
                    'column4' => array(),
                ),
            );
        }

        $getSubsections = Podrazdel3lvl::getDb()->cache(function($db){ //3 уровень
            return Podrazdel3lvl::find()->show()->asArray()->orderBy('pprz_sorti ASC')->all();
        }, 1000);
        $subsectionsArr = array();
        foreach($getSubsections as $subsection) {
            $subsectionsArr[$subsection['pprz_prz_id']][] = array(
                'name' => $subsection['pprz_name'],
                'url' => $subsection['pprz_url'],
            );
        }

        $getSections = Podrazdel::getDb()->cache(function($db){ // 2 уровень
            return Podrazdel::find()->show()->asArray()->orderBy('prz_sorti ASC')->all();
        }, 1000);
        foreach($getSections as $section) {
            if(isset($this->menuArray[$section['prz_rz_id']]) && $section['column_number']<=4) {
                $this->menuArray[$section['prz_rz_id']]['cat']['column'.$section['column_number']][] = array(
                    'name' => $section['prz_name'],
                    'url' => $section['prz_url'],
                    'sort' => $section['prz_sorti'],
                    'subcat' => (isset($subsectionsArr[$section['prz_id']])) ? $subsectionsArr[$section['prz_id']] : null
                );
            }
        }
    }

    public function run()
    {
        if($this->isTablet) {
            return $this->render('leftCatalogTablet', [
                'menuArray' => $this->menuArray,
                'reviews' => $this->reviews,
                'show' => $this->show,
                'showKompleks' => $this->showKompleks,
            ]);
        } else {
            return $this->render('leftCatalog', [
                'menuArray' => $this->menuArray,
                'reviews' => $this->reviews,
                'show' => $this->show,
                'showKompleks' => $this->showKompleks,
            ]);
        }
    }
}