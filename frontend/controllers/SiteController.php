<?php
namespace frontend\controllers;

use common\models\GoodCat;
use common\models\Goods;
use common\models\GoodSubcat;
use common\models\Manufacter;
use common\models\Razdel;
use common\models\RRCache;
use common\models\Sitepages;
use Yii;
use yii\filters\PageCache;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => 'yii\web\ErrorAction',
        ];
    }

    public function actionIndex()
    {
        $goodsIds = RRCache::getItemsToMain(4);
        $goodsFromDb = Goods::getDb()->cache(function($db) use ($goodsIds){
            return Goods::find()->where(['ct_id' => $goodsIds])->indexBy('ct_id')->all();
        }, 600);
        $goods = [];
        foreach ($goodsIds as $k => $v) {
            $goods[$k] = $goodsFromDb[$v];
        }
        $razdelImages = Razdel::getDb()->cache(function($db){
            return Razdel::find()->show()->notSale()->notNY()->asArray()->orderBy('rz_sorti ASC')->all();
        }, 100);
        $title = 'Гипострой.ру - интернет-магазин стройматериалов в Москве и области. Гипермаркет
            строительных материалов';
        $description = 'Гипострой.ру - интернет-магазин строительных материалов. ☎ +7 (495) 545-44-547 Стройматериалы по
            доступной цене! Продажа стройматериалов с доставкой по Москве и области!';
        $keywords = '';
        return $this->render('index', [
            'title' => $title,
            'keywords' => $keywords,
            'description' => $description,
            'goods' => $goods,
            'razdelImages' => $razdelImages,
        ]);
    }

    public function actionArticle($name)
    {
        $oldPages = ['help', 'grafik-raboty', 'legal-information'];
        if(in_array($name, $oldPages)) {
            $this->redirect(['site/article', 'name' => 'about'], 301);
        }

        $model = Sitepages::getDb()->cache(function($db) use ($name){
            return Sitepages::findOne(['sp_url' => $name]);
        }, 1000);

        if($model === null) {
            throw new NotFoundHttpException;
        }

        if($name == 'delivery') {
            return $this->render('delivery', [
                'model' => $model,
            ]);
        } elseif($name == 'contacts') {
            return $this->render('contacts', [
                'model' => $model,
            ]);
        } else {
            return $this->render('default', [
                'model' => $model,
            ]);
        }
    }

    public function actionTop()
    {
        $rrGoods = RRCache::getItemsToMain();
        $positions = Goods::getDb()->cache(function($db) use ($rrGoods){
            return Goods::find()
                ->addSelect(Yii::$app->params['good_fields'])
                ->with(['manufacter', 'unit'])
                ->show()
                ->where(['in', 'ct_id', $rrGoods])
                ->noDeleted()
                ->indexBy('ct_id')
                ->all();
        }, 1000);
        foreach($rrGoods as $k => $rrId) {
            if(isset($positions[$rrId])) {
                $rrGoods[$k] = $positions[$rrId];
            } else {
                unset($rrGoods[$k]);
            }
        }

        $title = 'Интернет-гипермаркет Гипострой — лидер продаж стройматериалов в Москве.';
        $description = 'Лидеры продаж в  интернет-гипермаркете стройматериалов Гипострой. Продажа популярных
            строительных материалов по доступным ценам с доставкой по Москве и области. Заказывать у нас  просто и выгодно!';
        $keywords = '';

        return $this->render('top', [
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'positions' => $rrGoods,
        ]);
    }

    public function actionKompleks()
    {
        $title = 'Комплектация и снабжение строительных объектов материалами, Москва и область. Оптовая продажа, комплексная поставка стройматериалов';
        $description = 'Комплексное снабжение и комплектация строительных объектов стройматериалами в Москве. Оптовая продажа и поставка строительных материалов,
            сантехники  по лучшим ценам в компании Гипострой.';
        $keywords = '';

        return $this->render('kompleks', [
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
        ]);
    }

    public function actionMap()
    {
        $map = Razdel::getDb()->cache(function($db){
            return Razdel::find()
                ->with(['sections.subsections'])
                ->show()
                ->orderBy([
                    'rz_sorti' => SORT_ASC
                ])
                ->asArray()
                ->all();
        }, 1000);
        $staticPages = Sitepages::getDb()->cache(function($db){
            return Sitepages::find()->show()->orderBy(['sp_sorti' => SORT_ASC])->asArray()->all();
        }, 1000);
        $title = 'Карта сайта интернет-гипермаркета Гипострой.ру';
        $description = 'Карта сайта интернет-гипермаркета Гипострой.ру. Заказывайте качественные строительные материалы у нас! Недорого!';
        $keywords = '';

        return $this->render('map', [
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'map' => $map,
            'staticPages' => $staticPages,
        ]);
    }

    public function actionProizvoditeli()
    {
        $manufacters = Manufacter::getDb()->cache(function($db){
            return Manufacter::find()
                ->addSelect(['mn_name', 'mn_url', 'LEFT(mn_name, 1) as alphabet'])
                ->show()
                ->orderBy([
                    'mn_name' => SORT_ASC
                ])
                ->asArray()
                ->all();
        }, 1000);
        $manufactersArray = [];
        foreach($manufacters as $manufacter) {
            $manufactersArray[$manufacter['alphabet']][] = $manufacter;
        }

        $enAlphabet = range('A','Z');
        $ruAlphabet = array('А','Б','В','Г','Д','Е','Ё','Ж','З','И','К','Л','М','Н','О','П','Р','С','Т','У',
            'Ф','Х','Ц','Ч','Щ','Ш','Ь','Ы','Ъ','Э','Ю','Я');

        $title = 'Производители';
        $description = '';
        $keywords = '';

        return $this->render('manufacters', [
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'manufacters' => $manufactersArray,
            'enAlphabet' => $enAlphabet,
            'ruAlphabet' => $ruAlphabet,
        ]);
    }

    /**
     * Показываем информацию о выбранном производителе
     * @param $name
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionShow($name)
    {
        $model = Manufacter::find()
            ->where(['mn_url' => $name])
            ->show()
            ->one();
        if($model === null) {
            throw new NotFoundHttpException;
        }

        $subsections = GoodSubcat::find()
            ->select('subcat_id', 'distinct')
            ->where(['manufacter_id' => $model['mn_id']])
            ->with(['subsection.section'])
            ->asArray()
            ->all();

        $catsArray = [];
        foreach($subsections as $subsection) {
            if(!empty($subsection['subsection']['pprz_prz_id'])) {
                $section = $subsection['subsection']['section'];
                if(!isset($catsArray[$section['prz_id']]['subsections'])) {
                    $catsArray[$section['prz_id']] = $section;
                    $catsArray[$section['prz_id']]['subsections'] = [];
                }
                $catsArray[$section['prz_id']]['subsections'][] = $subsection;
            }
        }

        $lvl2 = GoodCat::find()
            ->select('cat_id', 'distinct')
            ->where(['manufacter_id' => $model['mn_id']])
            ->with(['section'])
            ->indexBy('cat_id')
            ->asArray()
            ->all();
        foreach($lvl2 as $key => $value) {
            if(isset($catsArray[$value['cat_id']])) {
                unset($lvl2[$key]);
            }
        }

        return $this->render('manufacter', [
            'model' => $model,
            'sections' => $catsArray,
            'lvl2' => $lvl2,
        ]);
    }
}
