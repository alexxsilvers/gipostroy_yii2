<?php
namespace frontend\controllers;

use common\models\Goods;
use common\models\Orders;
use common\models\UserInfo;
use common\models\Users;
use common\models\UserShipment;
use common\models\UserViewedGoods;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Dash controller
 */
class DashController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['info', 'edit', 'changepassword', 'shipment', 'sms', 'email', 'viewed', 'orders'],
                'rules' => [
                    [
                        'actions' => ['info', 'edit', 'changepassword', 'shipment', 'sms', 'email', 'viewed', 'orders'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionInfo()
    {
        $user = Users::find()->where(['id' => Yii::$app->user->id])->with(['info', 'viewedGoods', 'orders'])->one();
        if($user === null) {
            throw new NotFoundHttpException;
        }



        $title = 'Личный кабинет - Информация';
        $description = '';
        $keywords = '';
        return $this->render('info', [
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'user' => $user,
        ]);
    }

    public function actionEdit()
    {
        $userInfo = UserInfo::find()->where(['user_id' => Yii::$app->user->id])->one();
        if($userInfo === null) {
            throw new NotFoundHttpException;
        }

        if($userInfo->load(Yii::$app->request->post()) && $userInfo->validate()) {
            $userInfo->save();
            $this->redirect(['info']);
        }

        $title = 'Личный кабинет - Редактирование профиля';
        $description = '';
        $keywords = '';
        return $this->render('edit', [
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'userInfo' => $userInfo
        ]);
    }

    public function actionShipment()
    {
        $shipments = UserShipment::find()->where(['user_id' => Yii::$app->user->id])->asArray()->all();
        $model = new UserShipment();

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            $this->redirect(['shipment']);
        }

        $title = 'Личный кабинет - Адреса доставки';
        $description = '';
        $keywords = '';
        return $this->render('shipment', [
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'shipments' => $shipments,
            'model' => $model,
        ]);
    }

    public function actionViewed()
    {
        $goodIds = UserViewedGoods::find()->addSelect('good_id')->where(['user_id' => Yii::$app->user->id])->asArray()->column();
        $positions = Goods::getDb()->cache(function($db) use ($goodIds) {
            return Goods::find()
                ->addSelect(Yii::$app->params['good_fields'])
                ->with(['manufacter', 'unit'])
                ->where(
                    ['in', 'ct_id', $goodIds]
                )
                ->indexBy('ct_id')
                ->orderBy('ct_count_index DESC')
                ->all();
        });

        $dataProvider = new ArrayDataProvider([
            'allModels' => $positions,
            'sort' => [
                'enableMultiSort' => true,
                'attributes' => [
                    'price' => [
                        'asc' => ['ct_price' => SORT_ASC],
                        'desc' => ['ct_price' => SORT_DESC],
                        'label' => 'Цене',
                    ],
                    'name' => [
                        'asc' => ['ct_name' => SORT_ASC],
                        'desc' => ['ct_name' => SORT_DESC],
                        'label' => 'Названию',
                    ],
                    'art' => [
                        'asc' => ['ct_newart' => SORT_ASC],
                        'desc' => ['ct_newart' => SORT_DESC],
                        'label' => 'Артикулу',
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => Goods::getPageSize(),
                'totalCount' => sizeof($positions),
                'pageSizeParam' => 'psize',
            ],
        ]);

        $title = 'Личный кабинет - Просмотренные товары';
        $description = '';
        $keywords = '';
        return $this->render('viewed', [
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOrders()
    {
        $orders = Orders::find()->where(['or_user_id' => Yii::$app->user->id])
            ->with(['positions.good.unit'])
            ->orderBy(['or_id' => SORT_DESC])->asArray()
            ->all();

        /*echo '<pre>';
        echo var_dump($orders);
        echo '</pre>';*/

        $dataProvider = new ArrayDataProvider([
            'allModels' => $orders,
            'pagination' => [
                'pageSize' => 10,
                'totalCount' => sizeof($orders),
                'pageSizeParam' => 'psize',
            ],
        ]);

        $title = 'Личный кабинет - Заказы';
        $description = '';
        $keywords = '';
        return $this->render('orders', [
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'dataProvider' => $dataProvider,
        ]);
    }
}
