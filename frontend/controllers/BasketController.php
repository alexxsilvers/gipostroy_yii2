<?php
/**
 * Created by PhpStorm.
 * User: iavGipostroy
 * Date: 28.05.2015
 * Time: 10:27
 */

namespace frontend\controllers;


use bupy7\ajaxfilter\AjaxFilter;
use common\models\Cart;
use common\models\Goods;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class BasketController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => AjaxFilter::className(),
                'actions' => [
                    'add',
                    'massadd',
                    'remove',
                    'preview',
                ]
            ],
        ];
    }

    public function actionOrder()
    {
        $cart = Cart::getCart();
        if(!$cart->reCalc()) {
            throw new BadRequestHttpException;
        }

        $title = 'Корзина';
        $description = '';
        $keywords = '';
        return $this->render('order', [
            'title' => $title,
            'description' => $description,
            'keywords' => $keywords,
            'cart' => $cart,
        ]);
    }

    public function actionPreview()
    {
        $cart = Cart::getCart();
        if ($cart !== null) {
            $cart->reCalc(true);
        }

        return $this->renderPartial('preview', [
            'cart' => $cart,
        ]);
    }

    /**
     * Add a good to cart
     * @return array
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionAdd()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(Yii::$app->request->post('id') === null) {
            throw new BadRequestHttpException;
        }
        /* @var $good Goods */
        $good = Goods::findOne(Yii::$app->request->post('id'));
        if($good === null) {
            throw new NotFoundHttpException;
        }

        $quantity = (Yii::$app->request->post('quantity') !== null) ? Yii::$app->request->post('quantity') : $good->restrictions()['min'];
        $cart = Cart::getCart(true);
        if(isset($cart->items[$good->ct_id])) {
            $cart->items[$good->ct_id] = [
                'quantity' => $quantity + $cart->items[$good->ct_id]['quantity'],
            ];
        } else {
            $cart->items[$good->ct_id] = [
                'quantity' => $quantity,
            ];
        }
        if($cart->reCalc()) {
            return [
                'success' => true,
                'itemsCount' => $cart->items_count,
                'itemsCountString' => $cart->items_count_string,
                'totalPrice' => $cart->total_price,
            ];
        } else {
            return [
                'success' => false,
                'message' => 'Не удалось добавить товар в корзину',
            ];
        }
    }

    /**
     * Add a certain amount of goods to the cart
     * @return array
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionMassadd()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(Yii::$app->request->post('ids') === null) {
            throw new BadRequestHttpException;
        }
        $goods = Goods::find()->where(['in', 'ct_id', Yii::$app->request->post('ids')])->all();
        if($goods === null) {
            throw new NotFoundHttpException;
        }

        $cart = Cart::getCart(true);
        /* @var $good Goods */
        foreach($goods as $good) {
            $quantity = $good->restrictions()['min'];
            if(isset($cart->items[$good->ct_id])) {
                $cart->items[$good->ct_id] = [
                    'quantity' => $quantity + $cart->items[$good->ct_id]['quantity'],
                ];
            } else {
                $cart->items[$good->ct_id] = [
                    'quantity' => $quantity,
                ];
            }
        }

        if($cart->reCalc()) {
            return [
                'success' => true,
                'itemsCount' => $cart->items_count,
                'itemsCountString' => $cart->items_count_string,
                'totalPrice' => $cart->total_price,
            ];
        } else {
            return [
                'success' => false,
                'message' => 'Не удалось добавить товары в корзину',
            ];
        }
    }

    public function actionRemove()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->post('id');
        if($id === null) {
            throw new BadRequestHttpException;
        }
        $cart = Cart::getCart();
        if($cart === null || !isset($cart->items[$id])) {
            throw new NotFoundHttpException;
        }

        unset($cart->items[$id]);
        if($cart->reCalc()) {
            return [
                'success' => true,
                'itemsCount' => $cart->items_count,     
                'itemsCountString' => $cart->items_count_string,
                'totalPrice' => $cart->total_price,
            ];
        } else {
            return [
                'success' => false,
                'message' => 'Не удалось удалить товар из корзины',
            ];
        }
    }
}