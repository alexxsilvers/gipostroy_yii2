<?php
namespace frontend\controllers;

use common\models\Goods;
use common\models\Manufacter;
use common\models\Podrazdel;
use common\models\Podrazdel3lvl;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Catalog controller
 */
class CatalogmanufacterController extends Controller
{
    public function actionSection($name, $manuf)
    {
        $model = Podrazdel::find()->where(['prz_url' => $name])
            ->with(['subsections', 'category'])
            ->show()->one();
        if($model === null){
            throw new NotFoundHttpException;
        }

        $manufacter = Manufacter::find()->where(['mn_url' => $manuf])->show()->with(['seo'])->one();
        if($manufacter === null) {
            throw new NotFoundHttpException;
        }

        $otherSections = Podrazdel::find()->select(['prz_name', 'prz_url', 'prz_pic'])
            ->where(['prz_rz_id' => $model['category']['rz_id']])->show()->asArray()->all();

        $compareIds = $this->getCompareIds();

        $sectionId = $model->prz_id;
        $manufacterId = $manufacter->mn_id;
        $positions = Goods::find()
                ->addSelect(Yii::$app->params['good_fields'])
                ->joinWith([
                    'sectionGoods' => function ($query) use ($sectionId) { /* @var $query ActiveQuery */
                        $query->andWhere([
                            'cat_id' => $sectionId
                        ]);
                    }
                ])
                ->where(['ct_mn_id' => $manufacterId])
                ->with(['manufacter', 'unit'])
                ->show()
                ->noDeleted()
                ->sectionFiltersHandle($sectionId)
                ->indexBy('ct_id')
                ->orderBy('ct_count_index DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $positions,
            'pagination' => [
                'pageSize' => Goods::getPageSize(),
                'totalCount' => $positions->count(),
                'pageSizeParam' => 'psize',
            ],
            'sort' => [
                'enableMultiSort' => true,
                'attributes' => [
                    'price' => [
                        'asc' => ['ct_price' => SORT_ASC],
                        'desc' => ['ct_price' => SORT_DESC],
                        'label' => 'Цене',
                    ],
                    'name' => [
                        'asc' => ['ct_name' => SORT_ASC],
                        'desc' => ['ct_name' => SORT_DESC],
                        'label' => 'Названию',
                    ],
                    'art' => [
                        'asc' => ['ct_newart' => SORT_ASC],
                        'desc' => ['ct_newart' => SORT_DESC],
                        'label' => 'Артикулу',
                    ],
                ],
            ],
        ]);

        return $this->render('section', [
            'model' => $model,
            'manufacter' => $manufacter,
            'dataProvider' => $dataProvider,
            'otherSections' => $otherSections,
            'compareIds' => $compareIds
        ]);
    }

    public function actionSubsection($name, $manuf)
    {
        $model = Podrazdel3lvl::find()->where(['pprz_url' => $name])
            ->with(['section.category'])
            ->show()->one();
        if($model === null){
            throw new NotFoundHttpException;
        }

        $manufacter = Manufacter::find()->where(['mn_url' => $manuf])->show()->with(['seo'])->one();
        if($manufacter === null) {
            throw new NotFoundHttpException;
        }

        $otherSubsections = Podrazdel3lvl::find()->select(['pprz_name', 'pprz_url', 'pprz_pic'])
            ->where(['pprz_prz_id' => $model['section']['prz_id']])->show()->asArray()->all();

        $compareIds = $this->getCompareIds();

        $subsectionId = $model->pprz_id;
        $manufacterId = $manufacter->mn_id;
        $positions = Goods::find()
                ->addSelect(Yii::$app->params['good_fields'])
                ->joinWith([
                    'subsectionGoods' => function ($query) use ($subsectionId) { /* @var $query ActiveQuery */
                        $query->andWhere([
                            'subcat_id' => $subsectionId
                        ]);
                    }
                ])
                ->where(['ct_mn_id' => $manufacterId])
                ->with(['manufacter', 'unit'])
                ->show()
                ->noDeleted()
                ->subsectionFiltersHandle($subsectionId)
                ->indexBy('ct_id')
                ->orderBy('ct_count_index DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $positions,
            'pagination' => [
                'pageSize' => Goods::getPageSize(),
                'totalCount' => $positions->count(),
                'pageSizeParam' => 'psize',
            ],
            'sort' => [
                'enableMultiSort' => true,
                'attributes' => [
                    'price' => [
                        'asc' => ['ct_price' => SORT_ASC],
                        'desc' => ['ct_price' => SORT_DESC],
                        'label' => 'Цене',
                    ],
                    'name' => [
                        'asc' => ['ct_name' => SORT_ASC],
                        'desc' => ['ct_name' => SORT_DESC],
                        'label' => 'Названию',
                    ],
                    'art' => [
                        'asc' => ['ct_newart' => SORT_ASC],
                        'desc' => ['ct_newart' => SORT_DESC],
                        'label' => 'Артикулу',
                    ],
                ],
            ],
        ]);

        return $this->render('subsection', [
            'model' => $model,
            'manufacter' => $manufacter,
            'dataProvider' => $dataProvider,
            'otherSubections' => $otherSubsections,
            'compareIds' => $compareIds
        ]);
    }

    protected function getCompareIds()
    {
        return isset(Yii::$app->request->cookies['gipostroy_compare']) ?  Json::decode(Yii::$app->request->cookies['gipostroy_compare']->value) : null;
    }
}
