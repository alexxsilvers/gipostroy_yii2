<?php
namespace frontend\controllers;

use bupy7\ajaxfilter\AjaxFilter;
use frontend\models\CallbackForm;
use frontend\models\ProposalForm;
use frontend\models\OneClickCallForm;
use frontend\models\ReviewForm;
use frontend\models\SigninForm;
use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * Ajax controller
 */
class AjaxController extends Controller
{
    public function behaviors()
    {
        return [
            [
                'class' => AjaxFilter::className(),
                'actions' => [
                    'proposal',
                    'upload',
                    'review',
                    'callback',
                    'signin',
                    'oneclickcall',
                ]
            ],
        ];
    }

    public function actionProposal()
    {
        $model = new ProposalForm();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->send()) {
                echo Json::encode([
                    'error' => 'false',
                ]);
            } else {
                echo Json::encode([
                    'error' => 'true',
                    'messages' => [
                        ['Произошла ошибка при отправке заявки, обратитесь к администратору.'],
                    ]
                ]);
            }

        } else {
            echo Json::encode([
                'error' => 'true',
                'messages' => $model->getErrors(),
            ]);
        }
    }

    public function actionCallback()
    {
        $model = new CallbackForm();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->send()) {
                echo Json::encode([
                    'error' => 'false',
                ]);
            } else {
                echo Json::encode([
                    'error' => 'true',
                    'messages' => [
                        ['Произошла ошибка при отправке заявки, обратитесь к администратору.'],
                    ]
                ]);
            }

        } else {
            echo Json::encode([
                'error' => 'true',
                'messages' => $model->getErrors(),
            ]);
        }
    }

    public function actionReview()
    {
        $model = new ReviewForm();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->send()) {
                echo Json::encode([
                    'error' => 'false',
                ]);
            } else {
                echo Json::encode([
                    'error' => 'true',
                    'messages' => [
                        ['Произошла ошибка при отправке отзыва, обратитесь к администратору.'],
                    ]
                ]);
            }

        } else {
            echo Json::encode([
                'error' => 'true',
                'messages' => $model->getErrors(),
            ]);
        }
    }

    public function actionOneclickcall()
    {
        $model = new OneClickCallForm();
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($model->createOrder()) {
                echo Json::encode([
                    'error' => 'false',
                    'message' => 'Заказ успешно оформлен! Номер заказа ' . $model->order_id,
                ]);
            } else {
                echo Json::encode([
                    'error' => 'true',
                    'messages' => [
                        ['Произошла ошибка при отправке отзыва, обратитесь к администратору.'],
                    ]
                ]);
            }

        } else {
            echo Json::encode([
                'error' => 'true',
                'messages' => $model->getErrors(),
            ]);
        }
    }

    public function actionSignin()
    {
        if(Yii::$app->user->isGuest) {
            $model = new SigninForm();
            if($model->load(Yii::$app->request->post()) && $model->login()) {
                $this->redirect(['dash/info']);
            } else {
                echo Json::encode([
                    'error' => 'true',
                    'message' => $model->getErrors(),
                ]);
            }
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionUpload()
    {
        $fileName = 'file';
        $uploadPath = Yii::$app->params['upload_dir'];

        if (isset($_FILES[$fileName])) {
            $file = UploadedFile::getInstanceByName($fileName);
            if ($file->saveAs($uploadPath . '/' . $file->name)) {
                //Now save file data to database
                echo Json::encode($file);
            }
        }

        return false;
    }
}
