<?php
namespace frontend\controllers;

use common\models\Goods;
use common\models\Podrazdel;
use common\models\Podrazdel3lvl;
use common\models\Razdel;
use common\models\UserViewedGoods;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Catalog controller
 */
class CatalogController extends Controller
{
    public function actionCategory($name)
    {
        $model = Razdel::find()->where(['rz_url' => $name])->show()->with(['sections'])->one();
        if($model === null){
            throw new NotFoundHttpException;
        }

        $sectionsHasCharakters = [];
        $childSections = $model['sections'];
        foreach($childSections as $section) {
            if(isset($section['prz_charakters'])) {
                $sectionsHasCharakters[] = $section['prz_id'];
            }
        }

        $categoryRing = Razdel::getRing($model->rz_id);

        $compareIds = $this->getCompareIds();

        $positions = Goods::find()
            ->addSelect(Yii::$app->params['good_fields'])
            ->joinWith([
                'sectionGoods' => function ($query) use ($childSections) { /* @var $query ActiveQuery */
                    $query->andWhere([
                        'cat_id' => array_keys($childSections)
                    ]);
                }
            ])
            ->with(['manufacter', 'unit'])
            ->show()
            ->noDeleted()
            ->indexBy('ct_id')
            ->orderBy(['ct_count_index' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $positions,
            'pagination' => [
                'pageSize' => Goods::getPageSize(),
                'totalCount' => $positions->count(),
                'pageSizeParam' => 'psize',
            ],
            'sort' => [
                'enableMultiSort' => true,
                'attributes' => [
                    'price' => [
                        'asc' => ['ct_price' => SORT_ASC],
                        'desc' => ['ct_price' => SORT_DESC],
                        'label' => 'Цене',
                    ],
                    'name' => [
                        'asc' => ['ct_name' => SORT_ASC],
                        'desc' => ['ct_name' => SORT_DESC],
                        'label' => 'Названию',
                    ],
                    'art' => [
                        'asc' => ['ct_newart' => SORT_ASC],
                        'desc' => ['ct_newart' => SORT_DESC],
                        'label' => 'Артикулу',
                    ],
                ],
            ],
        ]);

        return $this->render('category', [
            'model' => $model,
            'compareIds' => $compareIds,
            'dataProvider' => $dataProvider,
            'sectionsHasCharakters' => $sectionsHasCharakters,
            'categoryRing' => $categoryRing,
        ]);
    }

    public function actionSection($name)
    {
        $model = Podrazdel::find()->where(['prz_url' => $name])
            ->with(['subsections', 'category', 'seoTags', 'seo3lvl'])
            ->show()->one();
        if($model === null){
            throw new NotFoundHttpException;
        }

        $otherSections = Podrazdel::find()->select(['prz_name', 'prz_url', 'prz_pic'])
            ->where(['prz_rz_id' => $model['category']['rz_id']])->show()->asArray()->all();

        $compareIds = $this->getCompareIds();

        $sectionId = $model->prz_id;
        $positions = Goods::find()
                ->addSelect(Yii::$app->params['good_fields'])
                ->joinWith([
                    'sectionGoods' => function ($query) use ($sectionId) { /* @var $query ActiveQuery */
                        $query->andWhere([
                            'cat_id' => $sectionId
                        ]);
                    }
                ])
                ->with(['manufacter', 'unit'])
                ->show()
                ->noDeleted()
                ->sectionFiltersHandle($sectionId)
                ->indexBy('ct_id')
                ->orderBy(['ct_count_index' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $positions,
            'pagination' => [
                'pageSize' => Goods::getPageSize(),
                'totalCount' => $positions->count(),
                'pageSizeParam' => 'psize',
            ],
            'sort' => [
                'enableMultiSort' => true,
                'attributes' => [
                    'price' => [
                        'asc' => ['ct_price' => SORT_ASC],
                        'desc' => ['ct_price' => SORT_DESC],
                        'label' => 'Цене',
                    ],
                    'name' => [
                        'asc' => ['ct_name' => SORT_ASC],
                        'desc' => ['ct_name' => SORT_DESC],
                        'label' => 'Названию',
                    ],
                    'art' => [
                        'asc' => ['ct_newart' => SORT_ASC],
                        'desc' => ['ct_newart' => SORT_DESC],
                        'label' => 'Артикулу',
                    ],
                ],
            ],
        ]);

        return $this->render('section', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'otherSections' => $otherSections,
            'compareIds' => $compareIds
        ]);
    }

    public function actionSubsection($name)
    {
        $model = Podrazdel3lvl::find()->where(['pprz_url' => $name])
            ->with(['section.category', 'seoTags', 'seo4lvl'])
            ->show()->one();
        if($model === null){
            throw new NotFoundHttpException;
        }

        $otherSubsections = Podrazdel3lvl::find()->select(['pprz_name', 'pprz_url', 'pprz_pic'])
            ->where(['pprz_prz_id' => $model['section']['prz_id']])->show()->asArray()->all();

        $compareIds = $this->getCompareIds();
        $subsectionId = $model->pprz_id;
        $positions = Goods::find()
            ->addSelect(Yii::$app->params['good_fields'])
            ->joinWith([
                'subsectionGoods' => function ($query) use ($subsectionId) { /* @var $query ActiveQuery */
                    $query->andWhere([
                        'subcat_id' => $subsectionId
                    ]);
                }
            ])
            ->with(['manufacter', 'unit'])
            ->show()
            ->noDeleted()
            ->subsectionFiltersHandle($subsectionId)
            ->indexBy('ct_id')
            ->orderBy('ct_count_index DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $positions,
            'pagination' => [
                'pageSize' => Goods::getPageSize(),
                'totalCount' => $positions->count(),
                'pageSizeParam' => 'psize',
            ],
            'sort' => [
                'enableMultiSort' => true,
                'attributes' => [
                    'price' => [
                        'asc' => ['ct_price' => SORT_ASC],
                        'desc' => ['ct_price' => SORT_DESC],
                        'label' => 'Цене',
                    ],
                    'name' => [
                        'asc' => ['ct_name' => SORT_ASC],
                        'desc' => ['ct_name' => SORT_DESC],
                        'label' => 'Названию',
                    ],
                    'art' => [
                        'asc' => ['ct_newart' => SORT_ASC],
                        'desc' => ['ct_newart' => SORT_DESC],
                        'label' => 'Артикулу',
                    ],
                ],
            ],
        ]);

        return $this->render('subsection', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'otherSubections' => $otherSubsections,
            'compareIds' => $compareIds
        ]);
    }

    public function actionItem($id)
    {
        /* @var $item Goods */
        $item = Goods::find()->where(['ct_url' => $id])->with([
                'reviews',
                'section',
                'subsection',
                'morePhotos',
                'unit',
                'manufacter'
            ])->one();
        if($item === null) {
            throw new NotFoundHttpException;
        }

        $this->setViewed($item->ct_id);
        $compareIds = $this->getCompareIds();
        $reviews = $item->reviews;
        $section = $item->section;
        $subsection = $item->subsection;
        $morePhotos = $item->morePhotos;
        $unit = $item->unit['name'];
        $manufacter = $item->manufacter;
        $tabs = $item->getGoodsForTabs();

        return $this->render('item', [
            'item' => $item,
            'unit' => $unit,
            'tabs' => $tabs,
            'reviews' => $reviews,
            'section' => $section,
            'subsection' => $subsection,
            'compareIds' => $compareIds,
            'morePhotos' => $morePhotos,
            'manufacter' => $manufacter,
        ]);
    }

    protected function setViewed($id)
    {
        if(Yii::$app->user->isGuest) {
            $session = Yii::$app->session;
            if($session->has('viewed_goods')) {
                $goods = $session->get('viewed_goods');
                if(!in_array($id, $goods)) {
                    $goods[] = $id;
                    $session->set('viewed_goods', $goods);
                }
            } else {
                $session->set('viewed_goods', [$id]);
            }
        } else {
            $findGood = UserViewedGoods::find()->where([
                'user_id' => Yii::$app->user->id,
                'good_id' => $id,
            ])->one();
            if(is_null($findGood)) {
                $model = new UserViewedGoods();
                $model->user_id = Yii::$app->user->id;
                $model->good_id = $id;
                $model->save(false);
            }
        }
    }

    protected function getCompareIds()
    {
        return isset(Yii::$app->request->cookies['gipostroy_compare']) ?  Json::decode(Yii::$app->request->cookies['gipostroy_compare']->value) : [];
    }
}
