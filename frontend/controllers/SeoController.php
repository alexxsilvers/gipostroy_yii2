<?php
namespace frontend\controllers;


use common\models\Goods;
use common\models\Podrazdel;
use common\models\Podrazdel3lvl;
use common\models\Seo;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SeoController extends Controller
{
    public function actionSectiontag($section, $tag)
    {
        $model = Podrazdel::find()->where(['prz_url' => $section])
            ->with(['subsections', 'category'])
            ->show()->one();
        if($model === null){
            throw new NotFoundHttpException;
        }

        $tag = Seo::find()->where(['cat_id' => $model->prz_id, 'url' => $tag, 'entity_type' => 'tag'])->one();
        if($tag === null){
            throw new NotFoundHttpException;
        }

        $otherSections = Podrazdel::find()->select(['prz_name', 'prz_url', 'prz_pic'])
            ->where(['prz_rz_id' => $model['category']['rz_id']])->show()->asArray()->all();

        $compareIds = isset(Yii::$app->request->cookies['gipostroy_compare']) ?  Json::decode(Yii::$app->request->cookies['gipostroy_compare']->value) : null;

        $sectionId = $model->prz_id;
        $positions = Goods::find()
                ->addSelect(Yii::$app->params['good_fields'])
                ->joinWith([
                    'sectionGoods' => function ($query) use ($sectionId, $tag) { /* @var $query ActiveQuery */
                        $query->andWhere([
                            'cat_id' => $sectionId
                        ]);
                    }
                ])
                ->andWhere($tag->condition)
                ->with(['manufacter', 'unit'])
                ->show()
                ->noDeleted()
                ->sectionFiltersHandle($sectionId)
                ->indexBy('ct_id')
                ->orderBy(['ct_count_index' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $positions,
            'pagination' => [
                'pageSize' => Goods::getPageSize(),
                'totalCount' => $positions->count(),
                'pageSizeParam' => 'psize',
            ],
            'sort' => [
                'enableMultiSort' => true,
                'attributes' => [
                    'price' => [
                        'asc' => ['ct_price' => SORT_ASC],
                        'desc' => ['ct_price' => SORT_DESC],
                        'label' => 'Цене',
                    ],
                    'name' => [
                        'asc' => ['ct_name' => SORT_ASC],
                        'desc' => ['ct_name' => SORT_DESC],
                        'label' => 'Названию',
                    ],
                    'art' => [
                        'asc' => ['ct_newart' => SORT_ASC],
                        'desc' => ['ct_newart' => SORT_DESC],
                        'label' => 'Артикулу',
                    ],
                ],
            ],
        ]);

        return $this->render('sectiontag', [
            'model' => $model,
            'tag' => $tag,
            'dataProvider' => $dataProvider,
            'otherSections' => $otherSections,
            'compareIds' => $compareIds
        ]);
    }

    public function actionSectionseo($name, $pageName)
    {
        $model = Podrazdel::find()->where(['prz_url' => $name])
            ->with(['subsections', 'category'])
            ->show()->one();
        if($model === null){
            throw new NotFoundHttpException;
        }

        $page = Seo::find()->where(['cat_id' => $model->prz_id, 'url' => $pageName, 'entity_type' => 'level3'])->one();
        if($page === null){
            throw new NotFoundHttpException;
        }

        $otherSections = Podrazdel::find()->select(['prz_name', 'prz_url', 'prz_pic'])
            ->where(['prz_rz_id' => $model['category']['rz_id']])->show()->asArray()->all();

        $compareIds = isset(Yii::$app->request->cookies['gipostroy_compare']) ?  Json::decode(Yii::$app->request->cookies['gipostroy_compare']->value) : null;

        $sectionId = $model->prz_id;
        $positions = Goods::find()
                ->addSelect(Yii::$app->params['good_fields'])
                ->joinWith([
                    'sectionGoods' => function ($query) use ($sectionId, $page) { /* @var $query ActiveQuery */
                        $query->andWhere([
                            'cat_id' => $sectionId
                        ]);
                    }
                ])
                ->andWhere($page->condition)
                ->with(['manufacter', 'unit'])
                ->show()
                ->noDeleted()
                ->sectionFiltersHandle($sectionId)
                ->indexBy('ct_id')
                ->orderBy(['ct_count_index' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $positions,
            'pagination' => [
                'pageSize' => Goods::getPageSize(),
                'totalCount' => $positions->count(),
                'pageSizeParam' => 'psize',
            ],
            'sort' => [
                'enableMultiSort' => true,
                'attributes' => [
                    'price' => [
                        'asc' => ['ct_price' => SORT_ASC],
                        'desc' => ['ct_price' => SORT_DESC],
                        'label' => 'Цене',
                    ],
                    'name' => [
                        'asc' => ['ct_name' => SORT_ASC],
                        'desc' => ['ct_name' => SORT_DESC],
                        'label' => 'Названию',
                    ],
                    'art' => [
                        'asc' => ['ct_newart' => SORT_ASC],
                        'desc' => ['ct_newart' => SORT_DESC],
                        'label' => 'Артикулу',
                    ],
                ],
            ],
        ]);

        return $this->render('sectionseo', [
            'model' => $model,
            'page' => $page,
            'dataProvider' => $dataProvider,
            'otherSections' => $otherSections,
            'compareIds' => $compareIds
        ]);
    }

    public function actionSubsectionseo($name, $pageName)
    {
        $model = Podrazdel3lvl::find()->where(['pprz_url' => $name])
            ->with(['section.category'])
            ->show()->one();
        if($model === null){
            throw new NotFoundHttpException;
        }

        $page = Seo::find()->where(['subcat_id' => $model->pprz_id, 'cat_id' => $model['section']['prz_id'], 'url' => $pageName, 'entity_type' => 'level4'])->one();
        if($page === null){
            throw new NotFoundHttpException;
        }

        $otherSubsections = Podrazdel3lvl::find()->select(['pprz_name', 'pprz_url', 'pprz_pic'])
            ->where(['pprz_prz_id' => $model['section']['prz_id']])->show()->asArray()->all();

        $compareIds = isset(Yii::$app->request->cookies['gipostroy_compare']) ?  Json::decode(Yii::$app->request->cookies['gipostroy_compare']->value) : null;

        $subsectionId = $model->pprz_id;
        $positions = Goods::find()
                ->addSelect(Yii::$app->params['good_fields'])
                ->joinWith([
                    'subsectionGoods' => function ($query) use ($subsectionId, $page) { /* @var $query ActiveQuery */
                        $query->andWhere([
                            'subcat_id' => $subsectionId
                        ]);
                    }
                ])
                ->andWhere($page->condition)
                ->with(['manufacter', 'unit'])
                ->show()
                ->noDeleted()
                ->subsectionFiltersHandle($subsectionId)
                ->indexBy('ct_id')
                ->orderBy('ct_count_index DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $positions,
            'pagination' => [
                'pageSize' => Goods::getPageSize(),
                'totalCount' => $positions->count(),
                'pageSizeParam' => 'psize',
            ],
            'sort' => [
                'enableMultiSort' => true,
                'attributes' => [
                    'price' => [
                        'asc' => ['ct_price' => SORT_ASC],
                        'desc' => ['ct_price' => SORT_DESC],
                        'label' => 'Цене',
                    ],
                    'name' => [
                        'asc' => ['ct_name' => SORT_ASC],
                        'desc' => ['ct_name' => SORT_DESC],
                        'label' => 'Названию',
                    ],
                    'art' => [
                        'asc' => ['ct_newart' => SORT_ASC],
                        'desc' => ['ct_newart' => SORT_DESC],
                        'label' => 'Артикулу',
                    ],
                ],
            ],
        ]);

        return $this->render('subsectionseo', [
            'model' => $model,
            'page' => $page,
            'dataProvider' => $dataProvider,
            'otherSubections' => $otherSubsections,
            'compareIds' => $compareIds
        ]);
    }

    public function actionSubsectiontag($subsection, $tag)
    {
        $model = Podrazdel3lvl::find()->where(['pprz_url' => $subsection])
            ->with(['section.category', 'seoTags', 'seo4lvl'])
            ->show()->one();
        if($model === null){
            throw new NotFoundHttpException;
        }

        $tag = Seo::find()->where(['subcat_id' => $model->pprz_id, 'cat_id' => $model['section']['prz_id'], 'url' => $tag, 'entity_type' => 'tag'])->one();
        if($tag === null){
            throw new NotFoundHttpException;
        }

        $otherSubsections = Podrazdel3lvl::find()->select(['pprz_name', 'pprz_url', 'pprz_pic'])
            ->where(['pprz_prz_id' => $model['section']['prz_id']])->show()->asArray()->all();

        $compareIds = isset(Yii::$app->request->cookies['gipostroy_compare']) ?  Json::decode(Yii::$app->request->cookies['gipostroy_compare']->value) : null;

        $subsectionId = $model->pprz_id;
        $positions = Goods::find()
                ->addSelect(Yii::$app->params['good_fields'])
                ->joinWith([
                    'subsectionGoods' => function ($query) use ($subsectionId, $tag) { /* @var $query ActiveQuery */
                        $query->andWhere([
                            'subcat_id' => $subsectionId
                        ]);
                    }
                ])
                ->andWhere($tag->condition)
                ->with(['manufacter', 'unit'])
                ->show()
                ->noDeleted()
                ->subsectionFiltersHandle($subsectionId)
                ->indexBy('ct_id')
                ->orderBy('ct_count_index DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $positions,
            'pagination' => [
                'pageSize' => Goods::getPageSize(),
                'totalCount' => $positions->count(),
                'pageSizeParam' => 'psize',
            ],
            'sort' => [
                'enableMultiSort' => true,
                'attributes' => [
                    'price' => [
                        'asc' => ['ct_price' => SORT_ASC],
                        'desc' => ['ct_price' => SORT_DESC],
                        'label' => 'Цене',
                    ],
                    'name' => [
                        'asc' => ['ct_name' => SORT_ASC],
                        'desc' => ['ct_name' => SORT_DESC],
                        'label' => 'Названию',
                    ],
                    'art' => [
                        'asc' => ['ct_newart' => SORT_ASC],
                        'desc' => ['ct_newart' => SORT_DESC],
                        'label' => 'Артикулу',
                    ],
                ],
            ],
        ]);

        return $this->render('subsection', [
            'model' => $model,
            'tag' => $tag,
            'dataProvider' => $dataProvider,
            'otherSubections' => $otherSubsections,
            'compareIds' => $compareIds
        ]);
    }
}