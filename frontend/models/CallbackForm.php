<?php

namespace frontend\models;


use Yii;
use yii\base\Model;

class CallbackForm extends Model
{
    public $name;
    public $phone;
    public $razdel;
    public $comment;

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'trim'],

            ['phone', 'required'],
            ['phone', 'match',
                'pattern' => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/',
                'message' => 'Номер телефона введен некорректно.'
            ],

            [['razdel', 'comment'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Номер телефона',
            'razdel' => 'Раздел',
            'comment' => 'Комментарий',
        ];
    }

    public function send()
    {
        $message = Yii::$app->mailer->compose(['html' => 'callback-html', 'text' => 'callback-text'], [
            'model' => $this
        ]);
        $message->setFrom([Yii::$app->params['gipoEmail'] => Yii::$app->name . ' robot']);
        $message->setTo(Yii::$app->params['gipoEmail']);
        $message->setSubject('Новый запрос на обратный звонок');
        if($message->send()) {
            return true;
        }

        return false;
    }
}