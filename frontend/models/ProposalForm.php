<?php

namespace frontend\models;


use Yii;
use yii\base\Model;

class ProposalForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $companyName;
    public $inn;
    public $attach;
    public $comment;

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'trim'],

            ['email', 'email'],
            ['email', 'required'],
            ['email', 'trim'],

            ['phone', 'required'],
            ['phone', 'match',
                'pattern' => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/',
                'message' => 'Номер телефона введен некорректно.'
            ],

            ['inn', 'trim'],
            ['inn', 'safe'],

            [['comment', 'attach', 'companyName'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'Email',
            'phone' => 'Номер телефона',
            'companyName' => 'Название компании',
            'inn' => 'ИНН',
            'attach' => 'Файл',
            'comment' => 'Комментарий',
        ];
    }

    public function send()
    {
        $message = Yii::$app->mailer->compose(['html' => 'proposal-html', 'text' => 'proposal-text'], [
            'model' => $this
        ]);
        $message->setFrom([Yii::$app->params['gipoEmail'] => Yii::$app->name . ' robot']);
        $message->setTo(Yii::$app->params['gipoEmail']);
        $message->setSubject('Новая заявка с сайта');
        if(!empty($this->attach) && file_exists(Yii::$app->params['upload_dir'] . '/' . $this->attach)) {
            $message->attach(Yii::$app->params['upload_dir'] . '/' . $this->attach);
        }
        if($message->send()) {
            return true;
        }

        return false;
    }
}