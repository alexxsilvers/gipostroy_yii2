<?php

namespace frontend\models;



use common\models\Goods;
use common\models\OrderItem;
use common\models\Orders;
use Yii;
use yii\base\Model;

/**
 * @property string $name
 * @property string $fname
 * @property string $phone
 * @property integer $good_id
 * @property Goods $good
 */
class OneClickCallForm extends Model
{
    public $name;
    public $fname;
    public $phone;
    public $good_id;
    public $good = null;
    public $order_id;

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'trim'],

            ['fname', 'required'],
            ['fname', 'trim'],

            ['phone', 'required'],
            ['phone', 'match',
                'pattern' => '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/',
                'message' => 'Номер телефона введен некорректно.'
            ],

            ['good_id', 'required'],
            ['good_id', 'integer'],

            [['good', 'order_id'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'fname' => 'Фамилия',
            'phone' => 'Номер телефона',
            'good_id' => 'Товар',
        ];
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->good = Goods::findOne($this->good_id);
            if($this->good === null) {
                $this->addError('good_id', 'Произошла ошибка');
            }
            return true;
        }
        return false;
    }

    public function createOrder()
    {
        $order = new Orders();
        $order->or_ordersum = round($this->good->ct_price*$this->good->restrictions()['min']);
        $order->or_order_type = 1; //B2C (Business to Client)
        $order->or_delivery_type = 1; //Доставка
        $order->or_surname = $this->fname;
        $order->or_firstname = $this->name;
        $order->or_phone = $this->phone;
        $order->oneclickcall = 1;
        if($order->validate() && $order->save()) {
            $orderPosition = new OrderItem();
            $orderPosition->or_id = $order->or_id;
            $orderPosition->ct_id = $this->good_id;
            $orderPosition->colvo = round($this->good->restrictions()['min'], 2);
            $orderPosition->price =$this->good->ct_price;
            if($orderPosition->save()) {
                $this->order_id = $order->onec_id;
                return true;
            }
        }

        return false;
    }
}