<?php

namespace frontend\models;


use Yii;
use yii\base\Model;

class ReviewForm extends Model
{
    public $tema;
    public $comment;

    public function rules()
    {
        return [
            ['tema', 'required'],
            ['tema', 'trim'],

            ['comment', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'tema' => 'Тема',
            'comment' => 'Отзыв',
        ];
    }

    public function send()
    {
        $message = Yii::$app->mailer->compose(['html' => 'review-html', 'text' => 'review-text'], [
            'model' => $this
        ]);
        $message->setFrom([Yii::$app->params['gipoEmail'] => Yii::$app->name . ' robot']);
        $message->setTo(Yii::$app->params['gipoEmail']);
        $message->setSubject('Новый отзыв о сайте');
        if($message->send()) {
            return true;
        }

        return false;
    }
}