<?php

namespace frontend\models;


use common\models\Users;
use Yii;
use yii\base\Model;

class SigninForm extends Model
{
    public $email;
    public $password;
    public $remember = true;

    private $_user = null;

    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],

            ['password', 'required'],
            ['password', 'validatePassword'],

            ['remember', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Электронная почта',
            'password' => 'Пароль',
            'remember' => 'Запомнить меня',
        ];
    }

    public function validatePassword()
    {
        $user = $this->getUser();
        if($user == null || !$user->validatePassword($this->password)) {
            $this->addError('password', 'Не правильный пароль');
        }
    }

    public function login()
    {
        if($this->validate()) {
            /* @var $user Users */
            $user = $this->getUser();
            Yii::$app->user->login($user, $this->remember ? 3600*24*30 : 0);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Finds user by email
     * @return Users|null
     */
    private function getUser()
    {
        if($this->_user === null) {
            $this->_user = Users::find()->where(['email' => $this->email])->one();
        }
        return $this->_user;
    }
}