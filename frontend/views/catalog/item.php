<?php
/* @var $this yii\web\View */
/* @var $item Goods */
/* @var $compareIds [] */
/* @var $section [] */
/* @var $subsection [] */
/* @var $morePhotos [] */
/* @var $unit string */
/* @var $tabs [] */
/* @var $manufacter Manufacter */
/* @var $reviews [] */

use common\models\Goods;
use common\models\Manufacter;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
use yii\helpers\Html;

$this->title = $item->getTitle();
$this->registerMetaTag([
    'name' => 'description',
    'content' => $item->getDescription(),
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $item->getKeywords(),
]);
$this->registerJs("
    $('a.fancybox').magnificPopup({
        type:'image',
        gallery:{enabled:true}
    });
");
\frontend\assets\ImageViewerAsset::register($this);

$this->params['breadcrumbs'][] = Html::a($section->category['rz_name'], ['catalog/category', 'name' => $section->category['rz_url']]);
$this->params['breadcrumbs'][] = Html::a($section['prz_name'], ['catalog/section', 'name' => $section['prz_url']]);
if($subsection !== null) {
    $this->params['breadcrumbs'][] = Html::a($subsection['pprz_name'], ['catalog/subsection', 'name' => $subsection['pprz_url']]);
}
?>

<section id="main">
    <div class="wrap product-info">
        <aside>
            <?= LeftCatalogWidget::widget(['reviews' => false]) ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="breadcrumb">
                <?= yii\widgets\Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'encodeLabels' => false,
                    'options' => [
                        'class' => 'nav'
                    ],
                ]) ?>

                <div style="float:right;margin-top: 5px;">
                    <div class="addthis_sharing_toolbox"></div>
                </div>
            </div>

            <div class="page product">
                <header class="product-header">
                    <h1 style="font-size:20px;color:black;">
                        <?= $item->ct_name ?>
                    </h1>
                </header>

                <div style="display: none;">
                    <?= $this->render('_itemSchema', [
                        'price' => $item->getGoodPriceInteger(),
                        'name' => $item->ct_name,
                        'pic' => $item->getImageSrc(),
                        'text' => $item->ct_text,
                        'reviews' => $reviews,
                    ]) ?>
                </div>

                <div class="row">
                    <div class="small-6 columns">
                        <div class="product-gallery row">
                            <div class="small-9 columns">
                                <div class="main-image">
                                    <a href="#" class="image">
                                        <noindex>
                                            <?= Html::a(
                                                Html::img($item->getImageSrc(), [
                                                    'class' => 'fancybox',
                                                    'rel' => 'group',
                                                    'alt' => $item->ct_name,
                                                    'title' => $item->ct_name,
                                                    'style' => 'width:320px;height:320px;'
                                                ]),
                                                $item->getImageSrc(),
                                                [
                                                    'class' => 'fancybox',
                                                    'rel' => 'group',
                                                    'alt' => $item->ct_name,
                                                    'title' => $item->ct_name,
                                                ]
                                            ) ?>
                                        </noindex>
                                    </a>

                                    <?php if($item->ct_sale == 1) { ?>
                                        <div class="good-discount-good" style="top:0;"></div>
                                    <?php } elseif($item->ct_hit == 1) { ?>
                                        <div class="good-hit-good" style="top:0;"></div>
                                    <?php } elseif($item->isNewGood()) { ?>
                                        <div class="good-new-good" style="top:0;"></div>
                                    <?php } ?>

                                    <?php //Шильдики для раздела ламината, производители: Balterio, Egger, Kaindl, Quick-Step ?>
                                    <?php if(in_array($item->ct_mn_id, [297,130,132,140]) && $section['prz_id'] == 46) { ?>
                                        <div id="eplf_id_info<?= $item->ct_id ?>" class="eplf_id_info" style="text-decoration: none;">Данный производитель входит в Европейскую Ассоциациию Производителей Ламинированных полов</div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?= $this->render('_itemMorePhotos', [
                                'photos' => $morePhotos,
                                'name' => $item->ct_name,
                            ]) ?>
                        </div>
                    </div>
                    <div class="small-6 columns">
                        <div class="row">

                            <!-- Блок корзины START -->
                            <div class="small-6 columns">
                                <div class="shop-controls panel2">
                                    <p class="art" data-id="<?= $item->ct_id ?>" data-js="<?= $item->ct_newart ?>">Артикул <?= $item->ct_newart ?></p>
                                    <?= $item->getGoodBalanceBlock($unit) ?>

                                    <div class="row text-center qty-box" style="margin:0">
                                        <div class="small-7 columns" style="margin-left:-13px;">
                                            <?php if(!isset($item->ct_price[5])) { ?>
                                                <p class="price"><?= $item->ct_price ?> р.</p>
                                            <?php } elseif(!isset($item->ct_price[6])) { ?>
                                                <p class="price" style="font-size: 26px;"><?= $item->ct_price ?> р.</p>
                                            <?php } else { ?>
                                                <p class="price" style="font-size: 22px;"><?= $item->ct_price ?> р.</p>
                                            <?php } ?>

                                            <?php if($item->ct_sale == 1 && !empty($item->ct_sale_price)) { ?>
                                                <span class="old-price-item">
                                                    <?= $item->ct_sale_price ?> р.
                                                </span>
                                            <?php } ?>
                                        </div>
                                        <div class="small-5 columns product-quantity">
                                            <div class="qty" style="margin-left:-10px;width:100px;">
                                                <a href="" class="control remove qtyRemove">-</a>
                                                <input style="width:50px;" class="qty-input" type="number" min="<?= $item->restrictions()['min'] ?>"
                                                       data-step="<?= $item->restrictions()['step'] ?>"
                                                       data-min="<?= $item->restrictions()['min'] ?>"
                                                       value="<?= $item->restrictions()['min'] ?>"
                                                    <?= ($item->restrictions()['disabled'] === true)?' disabled="disabled"':' ' ?> title="Кол-во"/>
                                                <a href="" class="control add qtyAdd">+</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="buttons">
                                        <?= Html::a(
                                            'В корзину' . Html::img('/images/icons/cart_small.png', ['width' => 24, 'height' => 24, 'style' => 'margin-top:-2px;margin-bottom:-2px;margin-left:10px']),
                                            null,
                                            [
                                                'data-id' => $item->ct_id,
                                                'class' => 'button button-wide button-orange add-from-good-card',
                                            ]
                                        ) ?>
                                        <?= Html::a(
                                            'Купить в 1 клик',
                                            null,
                                            [
                                                'onClick' => "open_modal('oneclickcall'); return false;",
                                                'class' => 'button button-wide button-gray-shop',
                                            ]
                                        ) ?>
                                        <div class="compare-good">
                                            <?php if(in_array($item->ct_id, $compareIds)) { ?>
                                                <span class="compare-cat-remove comp<?= $item->ct_id ?>" data-id="<?= $item->ct_id ?>"><span>Убрать из сравнения</span></span>
                                            <?php } else { ?>
                                                <span class="compare-cat-add comp<?= $item->ct_id ?>" data-id="<?= $item->ct_id ?>"><span>Добавить к сравнению</span></span>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Блок корзины END -->

                            <!-- Блок доставки START -->
                            <div class="small-6 columns" style="padding-right:0;padding-left:0;">
                                <div class="info" style="height:256px;min-height:256px;">
                                    <span style="font-size:16px;color:#000;">Доставка</span>
                                    <?php if($item->ct_status == 1) { ?>
                                        <p><?= $item->getGoodShipping() ?></p>
                                    <?php } elseif($item->ct_status == 2) { ?>
                                        <p>Возможная дата доставки -<br><strong>по согласованию</strong></p>
                                    <?php } else { ?>
                                        <p>Возможная дата доставки -<br><strong>по согласованию</strong></p>
                                    <?php } ?>
                                    <?php if($section['prz_id'] != 46) { ?>
                                        <p>Самовывоз со склада -<br><strong>бесплатно!</strong></p>
                                    <?php } ?>

                                    <p>
                                        <strong>
                                            <?= $item->calculateDelivery($section['prz_id'], $subsection['pprz_id']) ?>
                                        </strong>
                                    </p>

                                    <p>
                                        <?= Html::a('Подробнее о доставке', ['page/article', 'name' => 'delivery'], ['rel' => 'nofollow']) ?>
                                        <?= Html::a('Способы оплаты', ['page/article', 'name' => 'delivery', 'active' => 'pay'], [
                                            'rel' => 'nofollow',
                                            'style' => 'margin-top:7px;',
                                            'onclick' => "that=this;ga('send', 'event', 'test', 'GOOD_GO2PAYMENTWAYS');ga('gipoEc.send', 'event', 'test', 'GOOD_GO2PAYMENTWAYS');setTimeout(function() { location.href=that.href }, 200);return false;"
                                        ]) ?>
                                        <?= Html::a('Возврат и обмен', ['page/article', 'name' => 'vozvrat-i-obmen'], [
                                            'rel' => 'nofollow',
                                            'style' => 'margin-top:7px;'
                                        ]) ?>
                                    </p>
                                </div>
                            </div>
                            <!-- Блок доставки END -->

                            <div style="margin-top: 10px;" class="small-12 columns text-left require">
                                <span style="font-size:14px;color:#464646">
                                <?php if(in_array($section['prz_id'], [45,46,47])) { ?>
                                    Минимальный объем заказа: от <?= $item->restrictions()['min'] ?> <?= $unit ?><br/>
                                <?php } else { ?>
                                    <?php if(!empty($item->ct_fl_min)) { ?>
                                        Минимальный объем заказа: от <?= $item->ct_fl_min ?> <?= $item->ct_izmer2 ?><br/>
                                    <?php } ?>
                                <?php } ?>
                                    <?php if(!empty($item->ct_fl_min_all)) { ?>
                                        Минимальная сумма общего заказа: от <?= $item->ct_fl_min_all ?> <?= $item->ct_izmer3 ?><br/>
                                    <?php } ?>
                                    <?php if(!empty($item->ct_warning)) { ?>
                                        Внимание! <?= $item->ct_warning ?>
                                    <?php } ?>
                                </span>
                            </div>

                            <?php if(!empty($section['prz_vin'])) { ?>
                            <div style="margin-top: 10px;" class="good_consultant small-12 columns text-left require user-help man-help">
                                <span class="help-header">
                                    <?= $section['prz_vin'] ?>
                                </span>
                                <span class="help-subheader">
                                    Свяжитесь с нами и наш специалист сделает все за Вас!
                                </span>
                                <div class="help-contacts">
                                    <div class="help-phone">
                                        <?= Html::img('/images/icons/phone.png') ?>
                                        <?= Html::a('Заказать звонок', null, [
                                            'onclick' => "open_modal('callback');ga('send', 'event', '#gpstr033 callback', 'open form');ga('gipoEc.send', 'event', '#gpstr033 callback', 'open form'); return false;"
                                        ]) ?>
                                    </div>
                                    <div class="help-email">
                                        <?= Html::img('/images/icons/email.png') ?>
                                        <?= Html::a('info@gipostroy.ru', 'mailto:info@gipostroy.ru', [
                                            'onclick' => "ga('send', 'event', '#gpstr033 email', 'click email');ga('gipoEc.send', 'event', '#gpstr033 email', 'click email'); return false;"
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="wrap product-tabs tabbed tabbed2" style="width:inherit;">
                <dl class="tabs" data-tab>
                    <dd class="active"><a href="#product-features">Информация о товаре</a></dd>
                    <dd><a href="#product-comments">Отзывы</a></dd>
                </dl>
                <div class="tabs-content">
                    <div class="content active" id="product-features">
                        <div class="row">
                            <div class="product-charakters-table small-6 columns">
                                <span class="myCustomHeader" style="font-size:17px;">Характеристики</span>
                                <table class="charakters-table" style="margin-top:10px;">
                                <?php foreach($item->getCharakters($manufacter, $section, $subsection) as $charName => $charValue) { ?>
                                    <tr>
                                        <td class="nm">
                                            <?= $charName ?>:
                                        </td>
                                        <td>
                                            <?= $charValue ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </table>
                            </div>
                            <div class="small-6 columns" style="text-align:justify;">
                                <?php if(isset($item['ct_text'][20])) { ?>
                                    <span class="myCustomHeader" style="font-size: 17px;">Описание</span>
                                    <p style="margin-top:5px;"><?= strip_tags($item['ct_text'], Yii::$app->params['allowed_tags']) ?></p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="content" id="product-comments">
                        <script type="text/javascript" src="//dev.mneniya.pro/js/gipostroyru/mneniyafeed.js"></script>
                        <div id="mneniyapro_feed"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
</section>

<?= $this->render('_itemTabs', [
    'tabs' => $tabs,
]) ?>
