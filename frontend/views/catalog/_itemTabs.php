<?php
/* @var $this yii\web\View */
/* @var $tabGood Goods */
/* @var $tabs [] */

use common\models\Goods;
use yii\helpers\Html;
?>

<div class="wrap shop-tabs tabbed tabbed2">
    <dl class="tabs" data-tab>
        <?php if(isset($tabs['first_block']['tabs']['recommendedTab']) && sizeof($tabs['first_block']['tabs']['recommendedTab']['goods']) > 0) { ?>
            <dd <?= ($tabs['first_block']['activeTab'] == $tabs['first_block']['tabs']['recommendedTab']['id']) ? 'class="active"' : '' ?>><a href="#<?= $tabs['first_block']['tabs']['recommendedTab']['id'] ?>" style="font-size:15px;"><?= $tabs['first_block']['tabs']['recommendedTab']['name'] ?></a></dd>
        <?php } ?>
        <?php if(isset($tabs['first_block']['tabs']['availableTab']) && sizeof($tabs['first_block']['tabs']['availableTab']['goods']) > 0) { ?>
            <dd <?= ($tabs['first_block']['activeTab'] == $tabs['first_block']['tabs']['availableTab']['id']) ? 'class="active"' : '' ?>><a href="#<?= $tabs['first_block']['tabs']['availableTab']['id'] ?>" style="font-size:15px;"><?= $tabs['first_block']['tabs']['availableTab']['name'] ?></a></dd>
        <?php } ?>
        <?php if(isset($tabs['first_block']['tabs']['accessTab']) && sizeof($tabs['first_block']['tabs']['accessTab']['goods']) > 0) { ?>
            <dd <?= ($tabs['first_block']['activeTab'] == $tabs['first_block']['tabs']['accessTab']['id']) ? 'class="active"' : '' ?>><a href="#<?= $tabs['first_block']['tabs']['accessTab']['id'] ?>" style="font-size:15px;"><?= $tabs['first_block']['tabs']['accessTab']['name'] ?></a></dd>
        <?php } ?>
    </dl>
    <div class="tabs-content">
        <?php if(isset($tabs['first_block']['tabs']['recommendedTab']) && sizeof($tabs['first_block']['tabs']['recommendedTab']['goods']) > 0) { ?>
        <div class="good-card-tab-content content <?= ($tabs['first_block']['activeTab'] == $tabs['first_block']['tabs']['recommendedTab']['id']) ? 'active' : '' ?>" id="<?= $tabs['first_block']['tabs']['recommendedTab']['id'] ?>">
            <div class="products">
                <ul class="small-block-grid-5">
                    <?php foreach($tabs['first_block']['tabs']['recommendedTab']['goods'] as $tabGood) { ?>
                        <li class="tab-childs">
                            <div class="product text-center" style="height:295px;">
                                <?php if($tabGood->ct_sale == 1) { ?>
                                    <div class="good-discount"></div>
                                <?php } elseif($tabGood->ct_hit == 1) { ?>
                                    <div class="good-hit"></div>
                                <?php } elseif($tabGood->isNewGood()) { ?>
                                    <div class="good-new"></div>
                                <?php } ?>
                                <div class="widget-product">
                                    <?= Html::a(
                                        Html::img($tabGood->getImageSrc(), ['style'=>'height:212px;']),
                                        ['catalog/item', 'id' => $tabGood->ct_url],
                                        [
                                            'title' => $tabGood->ct_name,
                                            'onmousedown' => "try { rrApi.recomMouseDown(" . $tabGood->ct_id .", 'ItemToItems') } catch (e) {}",
                                        ]
                                    ) ?>
                                </div>
                                <div class="desq text-center">
                                    <?= Html::a(
                                        $tabGood->ct_name,
                                        ['catalog/item', 'id' => $tabGood->ct_url],
                                        [
                                            'title' => $tabGood->ct_name,
                                            'onmousedown' => "try { rrApi.recomMouseDown(" . $tabGood->ct_id .", 'ItemToItems') } catch (e) {}",
                                        ]
                                    )?>
                                </div>
                                <footer>
                                    <div class="price">
                                        <?= $tabGood->getGoodPriceInteger() ?>
                                    </div>
                                    <?= Html::a(
                                        'В корзину ' . Html::img('/images/icons/cart_small.png', ['width' => 24, 'height' => 24]),
                                        null,
                                        [
                                            'class' => 'button button-orange add_to_cart',
                                            'data-id' => $tabGood->ct_id,
                                            'onmousedown' => "try { rrApi.recomAddToCart(" . $tabGood->ct_id .", 'ItemToItems') } catch (e) {}",
                                        ]
                                    ) ?>
                                </footer>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <?php if(sizeof($tabs['first_block']['tabs']['recommendedTab']['goods']) > 5) { ?>
                    <footer class="small-12 columns text-right" style="margin-top: 10px;">
                        <?= Html::a('Показать все', null, ['class' => 'tab-childs-show']) ?>
                    </footer>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
        <?php if(isset($tabs['first_block']['tabs']['availableTab']) && sizeof($tabs['first_block']['tabs']['availableTab']['goods']) > 0) { ?>
        <div class="good-card-tab-content content <?= ($tabs['first_block']['activeTab'] == $tabs['first_block']['tabs']['availableTab']['id']) ? 'active' : '' ?>" id="<?= $tabs['first_block']['tabs']['availableTab']['id'] ?>">
            <div class="products">
                <ul class="small-block-grid-5">
                    <?php foreach($tabs['first_block']['tabs']['availableTab']['goods'] as $tabGood) { ?>
                        <li class="tab-childs">
                            <div class="product text-center" style="height:295px;">
                                <?php if($tabGood->ct_sale == 1) { ?>
                                    <div class="good-discount"></div>
                                <?php } elseif($tabGood->ct_hit == 1) { ?>
                                    <div class="good-hit"></div>
                                <?php } elseif($tabGood->isNewGood()) { ?>
                                    <div class="good-new"></div>
                                <?php } ?>
                                <div class="widget-product">
                                    <?= Html::a(
                                        Html::img($tabGood->getImageSrc(), ['style'=>'height:212px;']),
                                        ['catalog/item', 'id' => $tabGood->ct_url],
                                        [
                                            'title' => $tabGood->ct_name,
                                            'onmousedown' => "try { rrApi.recomMouseDown(" . $tabGood->ct_id .", 'ItemToItems') } catch (e) {}",
                                        ]
                                    ) ?>
                                </div>
                                <div class="desq text-center">
                                    <?= Html::a(
                                        $tabGood->ct_name,
                                        ['catalog/item', 'id' => $tabGood->ct_url],
                                        [
                                            'title' => $tabGood->ct_name,
                                            'onmousedown' => "try { rrApi.recomMouseDown(" . $tabGood->ct_id .", 'ItemToItems') } catch (e) {}",
                                        ]
                                    )?>
                                </div>
                                <footer>
                                    <div class="price">
                                        <?= $tabGood->getGoodPriceInteger() ?>
                                    </div>
                                    <?= Html::a(
                                        'В корзину ' . Html::img('/images/icons/cart_small.png', ['width' => 24, 'height' => 24]),
                                        null,
                                        [
                                            'class' => 'button button-orange add_to_cart',
                                            'data-id' => $tabGood->ct_id,
                                            'onmousedown' => "try { rrApi.recomAddToCart(" . $tabGood->ct_id .", 'ItemToItems') } catch (e) {}",
                                        ]
                                    ) ?>
                                </footer>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <?php if(sizeof($tabs['first_block']['tabs']['availableTab']['goods']) > 5) { ?>
                    <footer class="small-12 columns text-right" style="margin-top: 10px;">
                        <?= Html::a('Показать все', null, ['class' => 'tab-childs-show']) ?>
                    </footer>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
        <?php if(isset($tabs['first_block']['tabs']['accessTab']) && sizeof($tabs['first_block']['tabs']['accessTab']['goods']) > 0) { ?>
        <div class="good-card-tab-content content <?= ($tabs['first_block']['activeTab'] == $tabs['first_block']['tabs']['accessTab']['id']) ? 'active' : '' ?>" id="<?= $tabs['first_block']['tabs']['accessTab']['id'] ?>">
            <div class="products">
                <ul class="small-block-grid-5">
                    <?php foreach($tabs['first_block']['tabs']['accessTab']['goods'] as $tabGood) { ?>
                        <li class="tab-childs">
                            <div class="product text-center" style="height:295px;">
                                <?php if($tabGood->ct_sale == 1) { ?>
                                    <div class="good-discount"></div>
                                <?php } elseif($tabGood->ct_hit == 1) { ?>
                                    <div class="good-hit"></div>
                                <?php } elseif($tabGood->isNewGood()) { ?>
                                    <div class="good-new"></div>
                                <?php } ?>
                                <div class="widget-product">
                                    <?= Html::a(
                                        Html::img($tabGood->getImageSrc(), ['style'=>'height:212px;']),
                                        ['catalog/item', 'id' => $tabGood->ct_url],
                                        [
                                            'title' => $tabGood->ct_name,
                                            'onmousedown' => "try { rrApi.recomMouseDown(" . $tabGood->ct_id .", 'ItemToItems') } catch (e) {}",
                                        ]
                                    ) ?>
                                </div>
                                <div class="desq text-center">
                                    <?= Html::a(
                                        $tabGood->ct_name,
                                        ['catalog/item', 'id' => $tabGood->ct_url],
                                        [
                                            'title' => $tabGood->ct_name,
                                            'onmousedown' => "try { rrApi.recomMouseDown(" . $tabGood->ct_id .", 'ItemToItems') } catch (e) {}",
                                        ]
                                    )?>
                                </div>
                                <footer>
                                    <div class="price">
                                        <?= $tabGood->getGoodPriceInteger() ?>
                                    </div>
                                    <?= Html::a(
                                        'В корзину ' . Html::img('/images/icons/cart_small.png', ['width' => 24, 'height' => 24]),
                                        null,
                                        [
                                            'class' => 'button button-orange add_to_cart',
                                            'data-id' => $tabGood->ct_id,
                                            'onmousedown' => "try { rrApi.recomAddToCart(" . $tabGood->ct_id .", 'ItemToItems') } catch (e) {}",
                                        ]
                                    ) ?>
                                </footer>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <?php if(sizeof($tabs['first_block']['tabs']['accessTab']['goods']) > 5) { ?>
                    <footer class="small-12 columns text-right" style="margin-top: 10px;">
                        <?= Html::a('Показать все', null, ['class' => 'tab-childs-show']) ?>
                    </footer>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
    </div>
</div>

<div class="clear" style="height:20px;"></div>

<div class="wrap shop-tabs tabbed tabbed2">
    <dl class="tabs" data-tab>
        <?php if(isset($tabs['second_block']['tabs']['collectionTab']) && sizeof($tabs['second_block']['tabs']['collectionTab']['goods']) > 0) { ?>
            <dd <?= ($tabs['second_block']['activeTab'] == $tabs['second_block']['tabs']['collectionTab']['id']) ? 'class="active"' : '' ?>><a href="#<?= $tabs['second_block']['tabs']['collectionTab']['id'] ?>" style="font-size:15px;"><?= $tabs['second_block']['tabs']['collectionTab']['name'] ?></a></dd>
        <?php } ?>
        <?php if(isset($tabs['second_block']['tabs']['viewedTab']) && sizeof($tabs['second_block']['tabs']['viewedTab']['goods']) > 0) { ?>
            <dd <?= ($tabs['second_block']['activeTab'] == $tabs['second_block']['tabs']['viewedTab']['id']) ? 'class="active"' : '' ?>><a href="#<?= $tabs['second_block']['tabs']['viewedTab']['id'] ?>" style="font-size:15px;"><?= $tabs['second_block']['tabs']['viewedTab']['name'] ?></a></dd>
        <?php } ?>
        <?php if(isset($tabs['second_block']['tabs']['additionalTab']) && sizeof($tabs['second_block']['tabs']['additionalTab']['goods']) > 0) { ?>
            <dd <?= ($tabs['second_block']['activeTab'] == $tabs['second_block']['tabs']['additionalTab']['id']) ? 'class="active"' : '' ?>><a href="#<?= $tabs['second_block']['tabs']['additionalTab']['id'] ?>" style="font-size:15px;"><?= $tabs['second_block']['tabs']['additionalTab']['name'] ?></a></dd>
        <?php } ?>
    </dl>
    <div class="tabs-content">
        <?php if(isset($tabs['second_block']['tabs']['collectionTab']) && sizeof($tabs['second_block']['tabs']['collectionTab']['goods']) > 0) { ?>
        <div class="good-card-tab-content content <?= ($tabs['second_block']['activeTab'] == $tabs['second_block']['tabs']['collectionTab']['id']) ? 'active' : '' ?>" id="<?= $tabs['second_block']['tabs']['collectionTab']['id'] ?>">
            <div class="products">
                <ul class="small-block-grid-5">
                    <?php foreach($tabs['second_block']['tabs']['collectionTab']['goods'] as $tabGood) { ?>
                        <li class="tab-childs">
                            <div class="product text-center" style="height:295px;">
                                <?php if($tabGood->ct_sale == 1) { ?>
                                    <div class="good-discount"></div>
                                <?php } elseif($tabGood->ct_hit == 1) { ?>
                                    <div class="good-hit"></div>
                                <?php } elseif($tabGood->isNewGood()) { ?>
                                    <div class="good-new"></div>
                                <?php } ?>
                                <div class="widget-product">
                                    <?= Html::a(
                                        Html::img($tabGood->getImageSrc(), ['style'=>'height:212px;']),
                                        ['catalog/item', 'id' => $tabGood->ct_url],
                                        [
                                            'title' => $tabGood->ct_name,
                                        ]
                                    ) ?>
                                </div>
                                <div class="desq text-center">
                                    <?= Html::a(
                                        $tabGood->ct_name,
                                        ['catalog/item', 'id' => $tabGood->ct_url],
                                        [
                                            'title' => $tabGood->ct_name,
                                        ]
                                    )?>
                                </div>
                                <footer>
                                    <div class="price">
                                        <?= $tabGood->getGoodPriceInteger() ?>
                                    </div>
                                    <?= Html::a(
                                        'В корзину ' . Html::img('/images/icons/cart_small.png', ['width' => 24, 'height' => 24]),
                                        null,
                                        [
                                            'class' => 'button button-orange add_to_cart',
                                            'data-id' => $tabGood->ct_id,
                                        ]
                                    ) ?>
                                </footer>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <?php if(sizeof($tabs['second_block']['tabs']['collectionTab']['goods']) > 5) { ?>
                    <footer class="small-12 columns text-right" style="margin-top: 10px;">
                        <?= Html::a('Показать все', null, ['class' => 'tab-childs-show']) ?>
                    </footer>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
        <?php if(isset($tabs['second_block']['tabs']['viewedTab']) && sizeof($tabs['second_block']['tabs']['viewedTab']['goods']) > 0) { ?>
        <div class="good-card-tab-content content <?= ($tabs['second_block']['activeTab'] == $tabs['second_block']['tabs']['viewedTab']['id']) ? 'active' : '' ?>" id="<?= $tabs['second_block']['tabs']['viewedTab']['id'] ?>">
            <div class="products">
                <ul class="small-block-grid-5">
                    <?php foreach($tabs['second_block']['tabs']['viewedTab']['goods'] as $tabGood) { ?>
                        <li class="tab-childs">
                            <div class="product text-center" style="height:295px;">
                                <?php if($tabGood->ct_sale == 1) { ?>
                                    <div class="good-discount"></div>
                                <?php } elseif($tabGood->ct_hit == 1) { ?>
                                    <div class="good-hit"></div>
                                <?php } elseif($tabGood->isNewGood()) { ?>
                                    <div class="good-new"></div>
                                <?php } ?>
                                <div class="widget-product">
                                    <?= Html::a(
                                        Html::img($tabGood->getImageSrc(), ['style'=>'height:212px;']),
                                        ['catalog/item', 'id' => $tabGood->ct_url],
                                        [
                                            'title' => $tabGood->ct_name,
                                        ]
                                    ) ?>
                                </div>
                                <div class="desq text-center">
                                    <?= Html::a(
                                        $tabGood->ct_name,
                                        ['catalog/item', 'id' => $tabGood->ct_url],
                                        [
                                            'title' => $tabGood->ct_name,
                                        ]
                                    )?>
                                </div>
                                <footer>
                                    <div class="price">
                                        <?= $tabGood->getGoodPriceInteger() ?>
                                    </div>
                                    <?= Html::a(
                                        'В корзину ' . Html::img('/images/icons/cart_small.png', ['width' => 24, 'height' => 24]),
                                        null,
                                        [
                                            'class' => 'button button-orange add_to_cart',
                                            'data-id' => $tabGood->ct_id,
                                        ]
                                    ) ?>
                                </footer>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <?php if(sizeof($tabs['second_block']['tabs']['viewedTab']['goods']) > 5) { ?>
                    <footer class="small-12 columns text-right" style="margin-top: 10px;">
                        <?= Html::a('Показать все', null, ['class' => 'tab-childs-show']) ?>
                    </footer>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
        <?php if(isset($tabs['second_block']['tabs']['additionalTab']) && sizeof($tabs['second_block']['tabs']['additionalTab']['goods']) > 0) { ?>
        <div class="good-card-tab-content content <?= ($tabs['second_block']['activeTab'] == $tabs['second_block']['tabs']['additionalTab']['id']) ? 'active' : '' ?>" id="<?= $tabs['second_block']['tabs']['additionalTab']['id'] ?>">
            <div class="products">
                <ul class="small-block-grid-5">
                    <?php foreach($tabs['second_block']['tabs']['additionalTab']['goods'] as $tabGood) { ?>
                        <li class="tab-childs">
                            <div class="product text-center" style="height:295px;">
                                <?php if($tabGood->ct_sale == 1) { ?>
                                    <div class="good-discount"></div>
                                <?php } elseif($tabGood->ct_hit == 1) { ?>
                                    <div class="good-hit"></div>
                                <?php } elseif($tabGood->isNewGood()) { ?>
                                    <div class="good-new"></div>
                                <?php } ?>
                                <div class="widget-product">
                                    <?= Html::a(
                                        Html::img($tabGood->getImageSrc(), ['style'=>'height:212px;']),
                                        ['catalog/item', 'id' => $tabGood->ct_url],
                                        [
                                            'title' => $tabGood->ct_name,
                                        ]
                                    ) ?>
                                </div>
                                <div class="desq text-center">
                                    <?= Html::a(
                                        $tabGood->ct_name,
                                        ['catalog/item', 'id' => $tabGood->ct_url],
                                        [
                                            'title' => $tabGood->ct_name,
                                        ]
                                    )?>
                                </div>
                                <footer>
                                    <div class="price">
                                        <?= $tabGood->getGoodPriceInteger() ?>
                                    </div>
                                    <?= Html::a(
                                        'В корзину ' . Html::img('/images/icons/cart_small.png', ['width' => 24, 'height' => 24]),
                                        null,
                                        [
                                            'class' => 'button button-orange add_to_cart',
                                            'data-id' => $tabGood->ct_id,
                                        ]
                                    ) ?>
                                </footer>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <?php if(sizeof($tabs['second_block']['tabs']['additionalTab']['goods']) > 5) { ?>
                    <footer class="small-12 columns text-right" style="margin-top: 10px;">
                        <?= Html::a('Показать все', null, ['class' => 'tab-childs-show']) ?>
                    </footer>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
    </div>
</div>