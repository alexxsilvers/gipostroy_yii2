<?php
/* @var $price integer */
/* @var $name string */
/* @var $pic string */
/* @var $text string */
/* @var $reviews [] */

$allReviews = 0;
$totalRate = 0;
foreach($reviews as $review) {
    $allReviews++;
    $totalRate += $review['rate'];
}
?>

<?php if(sizeof($reviews) > 0) { ?>
    <div itemscope itemtype="http://schema.org/Product">
        <span itemprop="name"><?= $name ?></span>
        <img src=<?= $pic ?>" alt="<?= $name ?>" />
        <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
            Оценка <span itemprop="ratingValue"><?= round($totalRate / $allReviews, 1, PHP_ROUND_HALF_UP) ?></span>/5
            Основана <span itemprop="reviewCount"><?= $allReviews ?></span> отзывах пользователей
        </div>
        <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
            <span itemprop="priceCurrency" content="RUB">RUB</span>
            <span itemprop="price" content="<?= $price ?>"><?= $price ?></span>
        </div>
        <span itemprop="description">
            <?= $text ?>
        </span>
        <?php foreach($reviews as $r) { ?>
            <div itemprop="review" itemscope itemtype="http://schema.org/Review">
                <span itemprop="name"><?= $r['email'] ?></span> -
                by <span itemprop="author"><?= $r['name'] ?></span>,
                <meta itemprop="datePublished" content="<?= $r['date_review'] ?>"><?= $r['date_review'] ?>
                <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                    <meta itemprop="worstRating" content = "1">
                    <span itemprop="ratingValue"><?= $r['rate'] ?></span>/
                    <span itemprop="bestRating">5</span>
                </div>
                <span itemprop="description">
                    <?= $r['text'] ?>
                </span>
            </div>
        <?php } ?>
    </div>
<?php } ?>