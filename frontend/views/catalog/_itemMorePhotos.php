<?php
/* @var $this yii\web\View */
/* @var $name string */
/* @var $photos [] */

$i = 0;
use yii\helpers\Html;
?>

<?php if(sizeof($photos) > 0) { ?>
    <div class="small-3 columns">
        <div class="thumbnails">
            <?php foreach($photos as $photo) { ?>
                <?php if($i <= 3) { ?>
                    <noindex>
                        <?= Html::a(
                            Html::img('http://www.gipostroy.ru/pdir/more/' . $photo['photo'], [
                                'alt' => $name,
                                'title' => $name,
                                'style' => 'width:100px;height:90px;margin-bottom:8px;'
                            ]),
                            'http://www.gipostroy.ru/pdir/more/' . $photo['photo'],
                            [
                                'class' => 'fancybox',
                                'rel' => 'group',
                            ]
                        ) ?>
                    </noindex>
                <?php } ?>
                <?php $i++; ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>