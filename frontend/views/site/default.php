<?php
/* @var $this yii\web\View */
/* @var $model Sitepages */
use common\models\Sitepages;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
use yii\helpers\Html;

$this->title = $model->sp_title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->sp_description,
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->sp_keywords,
]);

$this->params['breadcrumbs'][] = Html::a($model->sp_name, ['site/article', 'name' => $model->sp_url]);
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="breadcrumb">
                <?= yii\widgets\Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'encodeLabels' => false,
                    'options' => [
                        'class' => 'nav'
                    ],
                ]) ?>
            </div>

            <?=$model['sp_text']?>
        </div>
    </div>
</section>