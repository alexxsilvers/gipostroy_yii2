<?php
/* @var $this yii\web\View */
/* @var $model Sitepages */
use common\models\Sitepages;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
use yii\helpers\Html;

$this->title = $model->sp_title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->sp_description,
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->sp_keywords,
]);

$this->params['breadcrumbs'][] = Html::a($model->sp_name, ['site/article', 'name' => $model->sp_url]);
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="breadcrumb">
                <?= yii\widgets\Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'encodeLabels' => false,
                    'options' => [
                        'class' => 'nav'
                    ],
                ]) ?>
            </div>
            <div class="page contacts">
                <header>
                    <h1>Контактные данные интернет-гипермаркета Гипострой.ру</h1>
                    <dl class="tabs" data-tab>
                        <dd <?= (Yii::$app->request->get('city', 'moscow') == 'moscow') ? 'class="active"' : '' ?>><a href="#ds">Москва</a></dd>
                        <dd <?= (Yii::$app->request->get('city') == 'spb') ? 'class="active"' : '' ?>><a href="#saintp">Санкт-Петербург</a></dd>
                        <dd <?= (Yii::$app->request->get('city') == 'nn') ? 'class="active"' : '' ?>><a href="#nn">Нижний Новгород</a></dd>
                    </dl>
                </header>
                <div class="tabs-content">
                    <div class="content <?= (Yii::$app->request->get('city', 'moscow') == 'moscow') ? 'active' : '' ?>" id="ds">
                        <div class="information">
                            <p>
                                <strong>Заказы через сайт</strong> принимаются 24 часа в сутки 365 дней в году.
                            </p>
                            <p>
                                <strong>Заказы через операторов</strong> принимаются в рабочее время магазина: Пн-Пт с 09:00-18-00
                            </p>
                            <p>
                                Время работы <strong>пункта выдачи товаров</strong>: Пн-Пт с 10:00-19:00
                            </p>
                            <a href="#" onClick="window.print();" class="button button-print">Распечатать страницу</a>
                        </div>
                        <div class="immersed">
                            <div class="phones">
                                <h3>Телефоны</h3>
                                <div class="row">
                                    <div class="small-6 columns msk">
                                        <?= Html::img('/images/moscow.png', [
                                            'alt' => 'Звонок из Москвы +7 (495) 545-44-54',
                                            'title' => 'Звонок из Москвы +7 (495) 545-44-54',
                                        ]) ?>
                                        <p>Для звонков из Москвы <a href="tel:+74955454454">+7 (495) 545-44-54</a></p>
                                    </div>
                                    <div class="small-6 columns russia">
                                        <?= Html::img('/images/russia.png', [
                                            'alt' => 'Звонок по россии +7 (800) 555-05-72',
                                            'title' => 'Звонок по россии +7 (800) 555-05-72',
                                        ]) ?>
                                        <p>Бесплатный звонок по России <a href="tel:+78005550572">+7 (800) 555-05-72</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="emails">
                                <h3>Электронная почта</h3>
                                <div class="email">
                                    <p>
                                        Общие вопросы
                                        <a href="mailto:info@gipostroy.ru">info@gipostroy.ru</a>
                                    </p>
                                </div>
                                <div class="email">
                                    <p>
                                        По вопросам доставки заказов
                                        <a href="mailto:dostavka@gipostroy.ru">dostavka@gipostroy.ru</a>
                                    </p>
                                </div>
                                <div class="email">
                                    <p>
                                        Пожелания и претензии по обслуживанию
                                        <a href="mailto:sev@gipostroy.ru">sev@gipostroy.ru</a>
                                    </p>
                                </div>
                                <div class="email">
                                    <p>
                                        По вопросам рекламы
                                        <a href="mailto:adv@gipostroy.ru">adv@gipostroy.ru</a>
                                    </p>
                                    <div class="hint">
                                        <?= Html::img('/images/hint.png') ?>
                                        <div class="hint-body" style="width: 200px;">
                                            Предложения в области SEO, SMO, SMM не рассматриваются
                                        </div>
                                    </div>
                                </div>
                                <div class="email">
                                    <p>
                                        Для поставщиков
                                        <a href="mailto:zakupka@gipostroy.ru">zakupka@gipostroy.ru</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <h3>Адрес офиса</h3>
                        <div class="map">
                            <?= Html::img('/images/maps/moscow_office.jpg', ['width' => 960, 'height' => 300]) ?>
                            <div class="how_to_get show" style="max-width: 300px;">
                                <a href="#">Как добраться?</a>
                                <div class="content">
                                    <p>
                                        Первый вагон из центра, из стеклянных дверей налево и еще раз налево, перейти дорогу. Идти в сторону института МАТИ, следующее здание бизнес-центр Молодёжный. При себе иметь паспорт или водительское удостоверение.
                                    </p>
                                </div>
                            </div>
                            <div class="address stem stem-left" style="top: 90px; left: 390px;">
                                <header>
                                    Адрес офиса:
                                </header>
                                <p>
                                    Россия, 121552, г. Москва, ул. Оршанская. д. 5, б/ц “Молодежный” (метро “Молодежная”, 3 мин. пешком)
                                </p>
                                <p>
                                    Телефон: <a href="tel:+74955454454">+7 (495) 545-44-54</a>
                                </p>
                            </div>
                        </div>
                        <h3>Адрес пункта выдачи товаров</h3>
                        <div class="map">
                            <?= Html::img('/images/maps/moscow_point_of_delivery.jpg', ['width' => 960, 'height' => 300]) ?>
                            <div class="how_to_get show" style="max-width: 250px;">
                                <a href="#">Как добраться?</a>
                                <div class="content">
                                    <p>
                                        Последний вагон из центра, из стеклянных дверей налево, подняться по ступенькам и повернуть направо. Идти прямо до второго светофора, затем снова направо. (Цокольный этаж, рядом с магазином Магнит, с правой стороны)  При себе иметь паспорт или водительское удостоверение.
                                    </p>
                                </div>
                            </div>
                            <div class="address stem stem-left" style="top: 98px;left: 338px;">
                                <header>
                                    Адрес пункта выдачи товаров:
                                </header>
                                <p>
                                    Россия, г. Москва, ул. Покрышкина. д. 8, корпус 3, “ТЦ на Покрышкина” (метро “Юго-западная”)
                                </p>
                                <p>
                                    Телефон: <a href="tel:+74955454454">+7 (495) 545-44-54</a> доб. 120
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="content <?= (Yii::$app->request->get('city') == 'spb') ? 'active' : '' ?>" id="saintp">
                        <div class="information">
                            <p>
                                <strong>Заказы через сайт</strong> принимаются 24 часа в сутки 365 дней в году.
                            </p>
                            <p>
                                <strong>Заказы через операторов</strong> принимаются в рабочее время магазина: Пн-Пт с 09:00-18-00
                            </p>
                            <a href="#" onClick="printDiv('saintp')" class="button button-print ri">Распечатать страницу</a>
                            <div class="rules">
                                <h3>Порядок получения заказов в СПБ</h3>
                                <div class="hint">
                                    <?= Html::img('/images/hint.png') ?>
                                    <div class="hint-body" style="width: 480px;">
                                        <header>Порядок получения заказов в пунктах самовывоза:</header>
                                        <p>- сотрудники пункта самовывоза проверяют комплектность заказа и выдают его покупателю, консультации по товару не производятся</p>
                                        <p>- хранение товара осуществляется в течении 7 календарных дней</p>
                                        <p>- при получении клиентом заказа и последующем возврате в интернет-магазин стоимость доставки оплачивается в полном объеме</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="immersed">
                            <div class="phones">
                                <h3>Телефоны</h3>
                                <div class="row">
                                    <div class="small-6 columns spb">
                                        <?= Html::img('/images/saintpetersburg.png') ?>
                                        <p>Для звонков из Санкт-Петербурга <a href="tel:+78124253261">+7 (812) 425-32-61</a></p>
                                    </div>
                                    <div class="small-6 columns russia">
                                        <?= Html::img('/images/russia.png') ?>
                                        <p>Бесплатный звонок по России <a href="tel:+78005550572">+7 (800) 555-05-72</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="emails">
                                <h3>Электронная почта</h3>
                                <div class="email">
                                    <p>
                                        Общие вопросы
                                        <a href="mailto:info@gipostroy.ru">info@gipostroy.ru</a>
                                    </p>
                                </div>
                                <div class="email">
                                    <p>
                                        По вопросам доставки заказов
                                        <a href="mailto:dostavka@gipostroy.ru">dostavka@gipostroy.ru</a>
                                    </p>
                                </div>
                                <div class="email">
                                    <p>
                                        Пожелания и претензии по обслуживанию
                                        <a href="mailto:sev@gipostroy.ru">sev@gipostroy.ru</a>
                                    </p>
                                </div>
                                <div class="email">
                                    <p>
                                        По вопросам рекламы
                                        <a href="mailto:adv@gipostroy.ru">adv@gipostroy.ru</a>
                                    </p>
                                    <div class="hint">
                                        <?= Html::img('/images/hint.png') ?>
                                        <div class="hint-body" style="width: 200px;">
                                            Предложения в области SEO, SMO, SMM не рассматриваются
                                        </div>
                                    </div>
                                </div>
                                <div class="email">
                                    <p>
                                        Для поставщиков
                                        <a href="mailto:zakupka@gipostroy.ru">zakupka@gipostroy.ru</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <h3>Адрес пункта выдачи товаров</h3>
                        <div class="map">
                            <?= Html::img('/images/maps/saintpetersburg_point_of_delivery_1.jpg', ['width' => 960, 'height' => 300]) ?>
                            <div class="how_to_get">
                                <a href="#">Как добраться?</a>
                                <div class="content">
                                    <p>
                                        Выйдя из метро, поворачиваем направо до пешеходного перехода, переходим Московский проспект. Поворачиваем налево и идемдо Лиговского проспекта, далее направо до пересечения с ул. Ломаная и снова направо (ориентир Ай-Си-Ай-Си-Ай Банк, должен быть слева).Проходим прямо по улице примерно 300м. до здания КПП (послепересечения с ул. Коли Томчака) с адресом - ул. Ломаная д. 5. Проходимчерез КПП, предъявив удостоверение личности. На территории проходимпрямо до 4-х этажного здания. Заходим. Поднявшись на 3 этаж, поворачиваемналево и проходим до двери с надписью “Спб-Логистики”. <br>Вы на месте!
                                    </p>
                                </div>
                            </div>
                            <div class="address stem stem-bottom stem-right" style="top: 20px;left: 539px;">
                                <header>
                                    Адрес пункта выдачи товаров:
                                </header>
                                <p>
                                    Россия, г. Санкт-Петербург, м. Московские ворота, ул. Ломаная д. 5
                                </p>
                                <p><strong>Время работы</strong>: с 11 до 20 часов ежедневно</p>
                                <p>
                                    Телефон: <a href="tel:+78123097148">+7 (812) 309-71-48</a> доб. 621
                                </p>
                            </div>
                        </div>
                        <div class="map">
                            <?= Html::img('/images/maps/saintpetersburg_point_of_delivery_2.jpg', ['width' => 960, 'height' => 300]) ?>
                            <div class="how_to_get">
                                <a href="#">Как добраться?</a>
                                <div class="content">
                                    <p>
                                        Выходите из метро в сторону ул. Илюшина, идите прямо, перед пешеходным переходом поворачиваете направо, далее опять же идете прямо около 700 метров по ул. Илюшина, как ориентир проходите мимо магазина “Пятерочка”, который должен находится справа от вас,
                                        доходите до перекрестка, немного сворачиваете напрво чтобы перейти на противоположную сторону по пешеходному переходу. Перешли дорогу, поворачиваете направо и идете к Торговому Комплексу “Горизонт”, который должен находиться с левой стороны от вас, 2-й этаж, павильон 37 и вы на мсете!
                                    </p>
                                </div>
                            </div>
                            <div class="address stem stem-top stem-middle" style="top: 90px; left: 445px; width: 270px;">
                                <header>
                                    Адрес пункта выдачи товаров:
                                </header>
                                <p>
                                    Россия, г. Санкт-Петербург, м. Площадь Восстания, Лиговский проспект  д. 50, к. 9
                                </p>
                                <p><strong>Время работы</strong>: с 11 до 20 часов ежедневно</p>
                                <p>
                                    Телефон: <a href="tel:+78123092846">+7 (812) 309-28-46</a> доб. 622
                                </p>
                            </div>
                        </div>
                        <div class="map">
                            <?= Html::img('/images/maps/saintpetersburg_point_of_delivery_3.jpg', ['width' => 960, 'height' => 300]) ?>
                            <div class="how_to_get">
                                <a href="#">Как добраться?</a>
                                <div class="content">
                                    <p>
                                        Выходя на Лиговский проспект. поворачиваете налево и идете прямо по Лиговскому проспекту около 800 метров, ориентир магазин “ТехноШок”. Перед магазином “ТехноШок” поворачиваете налево, далее первый поворот опять же налево, проходите немного вперед, сразу направо и немного прямо. Вторая дверь (если считать справа налево), на двери надпись “Самовывоз Грастин”. (Корпус 9 находится напротив корпуса 6).
                                    </p>
                                </div>
                            </div>
                            <div class="address stem stem-left stem-middle" style="top: 10px; left: 470px;">
                                <header>
                                    Адрес пункта выдачи товаров:
                                </header>
                                <p>
                                    Россия, г. Санкт-Петербург, м. Комендантский проспект, пр. Авиаконструкторов  д. 2, 2 этаж, павильон 37
                                </p>
                                <p><strong>Время работы</strong>: с 11 до 20 часов в Пн-Пт</p>
                                <p>
                                    Телефон: <a href="tel:+78123092846">+7 (812) 309-28-46</a> доб. 623
                                </p>
                            </div>
                        </div>
                        <div class="map">
                            <?= Html::img('/images/maps/saintpetersburg_point_of_delivery_4.jpg', ['width' => 960, 'height' => 300]) ?>
                            <div class="how_to_get" style="z-index: 1000;">
                                <a href="#">Как добраться?</a>
                                <div class="content">
                                    <p>
                                        Выходим из метро, переходим проспект Большевиков на противоположную сторону и идем вправо вдоль торговых центров.
                                        За рестораном «Розарио Васаби» (в конце торгового центра) поворачиваем налево и спускаемся по ступенькам вниз, опять поворачиваем налево и заходим в третий слева павильон.
                                    </p>
                                </div>
                            </div>
                            <div class="address stem stem-left stem-middle" style="top: 20px; left: 235px; width: 290px;">
                                <header>
                                    Адрес пункта выдачи товаров:
                                </header>
                                <p>
                                    Россия, г. Санкт-Петербург, м. Улица Дыбенко, пр Большевиков д. 17, литер Ч ТЦ Лорд - розовый павильон
                                </p>
                                <p><strong>Время работы</strong>: с 11 до 20 часов ежедневно</p>
                                <p>
                                    Телефон: <a href="tel:+78123097148">+7 (812) 309-71-48</a> доб. 624
                                </p>
                            </div>
                        </div>
                        <div class="map">
                            <?= Html::img('/images/maps/saintpetersburg_point_of_delivery_5.jpg', ['width' => 960, 'height' => 300]) ?>
                            <div class="how_to_get" style="z-index: 1000;">
                                <a href="#">Как добраться?</a>
                                <div class="content">
                                    <p>
                                        Выйдя из метро через дорогу напротив вы увидите ТЦ Шувалово. Переходите по пешеходному переходу, заходите в ТЦ, на эскалаторе поднимаетесь на 2 этаж, затем направо, наш пункт выдачи заказов расположен в павильон С-9.
                                    </p>
                                </div>
                            </div>
                            <div class="address stem stem-bottom stem-middle" style="top: 20px; left: 207px; width: 290px;">
                                <header>
                                    Адрес пункта выдачи товаров:
                                </header>
                                <p>
                                    Россия, г. Санкт-Петербург, м. Проспект Просвещения, ТЦ Шувалово, пр. Энгельса, д. 139, павильон С-9
                                </p>
                                <p><strong>Время работы</strong>: с 11 до 20 часов ежедневно</p>
                                <p>
                                    Телефон: <a href="tel:+78123097148">+7 (812) 309-71-48</a> доб. 625
                                </p>
                            </div>
                        </div>
                        <div class="map">
                            <?= Html::img('/images/maps/saintpetersburg_point_of_delivery_6.jpg', ['width' => 960, 'height' => 300]) ?>
                            <div class="how_to_get" style="z-index: 1000;">
                                <a href="#">Как добраться?</a>
                                <div class="content">
                                    <p>
                                        Выходя из метро переходим дорогу на противоположную сторону по пешеходному переходу,затем направо и немного прямо по левой стороне увидите БЦ Рубикон. Войдя в главный вход с правой стороны увидите лифт на котором поднимаемся на 4 этаж и по левую сторону офис №2, Вы на месте!
                                    </p>
                                </div>
                            </div>
                            <div class="address stem stem-bottom stem-middle" style="top: 10px; left: 487px; width: 290px;">
                                <header>
                                    Адрес пункта выдачи товаров:
                                </header>
                                <p>
                                    Санкт-Петербург, м. Гражданский проспект, БЦ Рубикон, Гражданский проспект, дом 119, литер А,  на 4 этаже, офис 2
                                </p>
                                <p><strong>Время работы</strong>: с 11 до 20 часов ежедневно</p>
                                <p>
                                    Телефон: <a href="tel:+78123097148">+7 (812) 309-71-48</a> доб. 626
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="content <?= (Yii::$app->request->get('city') == 'nn') ? 'active' : '' ?>" id="nn">
                        <div class="information">
                            <p>
                                <strong>Заказы через сайт</strong> принимаются 24 часа в сутки 365 дней в году.
                            </p>
                            <p>
                                <strong>Заказы через операторов</strong> принимаются в рабочее время магазина: Пн-Пт с 09:00-18-00
                            </p>
                            <a href="#" onClick="printDiv('nn')" class="button button-print ri">Распечатать страницу</a>
                            <div class="rules">
                                <h3>Порядок получения заказов в Нижнем Новгороде</h3>
                                <div class="hint">
                                    <?= Html::img('/images/hint.png') ?>
                                    <div class="hint-body" style="width: 480px;">
                                        <header>Порядок получения заказов в пунктах самовывоза:</header>
                                        <p>- сотрудники пункта самовывоза проверяют комплектность заказа и выдают его покупателю, консультации по товару не производятся</p>
                                        <p>- хранение товара осуществляется в течении 7 календарных дней</p>
                                        <p>- при получении клиентом заказа и последующем возврате в интернет-магазин стоимость доставки оплачивается в полном объеме</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="immersed">
                            <div class="phones">
                                <h3>Телефоны</h3>
                                <div class="row">
                                    <div class="small-6 columns nn">
                                        <?= Html::img('/images/nn.png') ?>
                                        <p>Для звонков из Нижнего Новгорода <a href="tel:+78314290388">+7 (831) 429-03-88</a></p>
                                    </div>
                                    <div class="small-6 columns russia">
                                        <?= Html::img('/images/russia.png') ?>
                                        <p>Бесплатный звонок по России <a href="tel:+78005550572">+7 (800) 555-05-72</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="emails">
                                <h3>Электронная почта</h3>
                                <div class="email">
                                    <p>
                                        Общие вопросы
                                        <a href="mailto:info@gipostroy.ru">info@gipostroy.ru</a>
                                    </p>
                                </div>
                                <div class="email">
                                    <p>
                                        По вопросам доставки заказов
                                        <a href="mailto:dostavka@gipostroy.ru">dostavka@gipostroy.ru</a>
                                    </p>
                                </div>
                                <div class="email">
                                    <p>
                                        Пожелания и претензии по обслуживанию
                                        <a href="mailto:sev@gipostroy.ru">sev@gipostroy.ru</a>
                                    </p>
                                </div>
                                <div class="email">
                                    <p>
                                        По вопросам рекламы
                                        <a href="mailto:adv@gipostroy.ru">adv@gipostroy.ru</a>
                                    </p>
                                    <div class="hint">
                                        <?= Html::img('/images/hint.png') ?>
                                        <div class="hint-body" style="width: 200px;">
                                            Предложения в области SEO, SMO, SMM не рассматриваются
                                        </div>
                                    </div>
                                </div>
                                <div class="email">
                                    <p>
                                        Для поставщиков
                                        <a href="mailto:zakupka@gipostroy.ru">zakupka@gipostroy.ru</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <h3>Адрес пункта выдачи товаров</h3>
                        <div class="map">
                            <?= Html::img('/images/maps/nn_addr.jpg', ['width' => 960, 'height' => 300]) ?>
                            <!--<div class="how_to_get">
                                <a href="#">Как добраться?</a>
                                <div class="content">
                                    <p>
                                        Выйдя из метро, поворачиваем направо до пешеходного перехода, переходим Московский проспект. Поворачиваем налево и идемдо Лиговского проспекта, далее направо до пересечения с ул. Ломаная и снова направо (ориентир Ай-Си-Ай-Си-Ай Банк, должен быть слева).Проходим прямо по улице примерно 300м. до здания КПП (послепересечения с ул. Коли Томчака) с адресом - ул. Ломаная д. 5. Проходимчерез КПП, предъявив удостоверение личности. На территории проходимпрямо до 4-х этажного здания. Заходим. Поднявшись на 3 этаж, поворачиваемналево и проходим до двери с надписью “Спб-Логистики”. <br>Вы на месте!
                                    </p>
                                </div>
                            </div>-->
                            <div class="address stem stem-left stem-middle" style="top: 60px;left: 539px;">
                                <header>
                                    Адрес пункта выдачи товаров:
                                </header>
                                <p>
                                    Россия, г. Нижний Новгород, м. Московская, ул. Гордеевская д. 7, 1 этаж, оф 103
                                </p>
                                <p><strong>Время работы</strong>: с 12 до 20 часов по будням</p>
                                <p>Суббота - с 12 до 16 часов</p>
                                <p>
                                    Телефон: <a href="tel:+78314290388">+7 (831) 429-03-88</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <footer>
                    <div>
                        <p class="details text-left">
                            <span>Реквизиты нашей компании:</span><br>
                            Общество с ограниченной ответственностью “Гипострой.ру” (ООО ”Гипострой.ру”)<br>
                            Место нахождения: Россия, 121552, г. Москва, ул. Оршанская, д. 5<br>
                            ИНН 7731660453<br>
                            КПП 733101001<br>
                            ОГРН 1107746925263<br>
                            ОКВЭД 74.7<br>
                            ОКПО 68936919<br>
                            р/с 40702810438040025368<br>
                            в Московский банк Сбербанка России (ОАО)<br>
                            к/с 30101810400000000225<br>
                            БИК 044525225<br>
                            <br>
                            <span>Генеральный директор:</span> Милюхин Дмитрий Валерьевич
                        </p>
                    </div>
                </footer>
            </div>

        </div>

</section>