<?php
/* @var $this yii\web\View */
/* @var $model Manufacter */
/* @var $sections array() */
/* @var $lvl2 array() */

use common\models\Manufacter;
use yii\helpers\Html;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;

$this->title = $model->getManufactersPageTitle();
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->getManufactersPageDescription()
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->getManufactersPageKeywords()
]);

$this->params['breadcrumbs'][] = Html::a('Производители', ['site/proizvoditeli']);
$this->params['breadcrumbs'][] = Html::a($model->mn_name, ['site/show', 'name' => $model->mn_url]);
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="breadcrumb">
                <?= yii\widgets\Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'encodeLabels' => false,
                    'options' => [
                        'class' => 'nav'
                    ],
                ]) ?>
            </div>

            <div class="page text-left">
                <header>
                    <?= $model->getManufactersPageHeader() ?>
                </header>

                <?php if(!empty($sections) && !empty($lvl2)) { ?>
                    <div class="manufacter_cats">
                        <header>
                            <h2>Категории, в которых представлен данный производитель</h2>
                        </header>
                        <?php if(!empty($sections)) { ?>
                            <?php foreach($sections as $section) { ?>
                                <div>
                                    <?= Html::a(
                                        $section['prz_name'],
                                        ['catalogmanufacter/section', 'name' => $section['prz_url'], 'manuf' => $model->mn_url],
                                        [
                                            'class' => 'mnCat'
                                        ]
                                    ) ?>
                                    <?php if(!empty($section['subsections'])) { ?>
                                        <br/>
                                        <?php foreach($section['subsections'] as $subsection) { ?>
                                            <?= Html::a(
                                                $subsection['subsection']['pprz_name'],
                                                ['catalogmanufacter/subsection', 'name' => $subsection['subsection']['pprz_url'], 'manuf' => $model->mn_url],
                                                [
                                                    'class' => 'mnSubcat',
                                                ]
                                            ) ?>
                                            <br/>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <?php if(!empty($lvl2)) { ?>
                            <?php foreach($lvl2 as $section) { ?>
                                <div>
                                    <?= Html::a(
                                        $section['section']['prz_name'],
                                        ['catalogmanufacter/section', 'name' => $section['section']['prz_url'], 'manuf' => $model->mn_url],
                                        [
                                            'class' => 'mnCat'
                                        ]
                                    ) ?>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                <?php } ?>

                <div class="manufacter_desc" id="non-selectable">
                    <?= Html::img($model->getImageSrc(), [
                        'class' => 'image manufacter-image',
                        'alt' => $model->mn_name,
                        'title' => $model->mn_name,
                    ]) ?>
                    <?= $model->getManufactersPageText() ?>
                </div>
            </div>
        </div>
    </div>
</section>