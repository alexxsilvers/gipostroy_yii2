<?php
/* @var $this yii\web\View */
/* @var $model Sitepages */
use common\models\Sitepages;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
use yii\helpers\Html;

$this->title = $model->sp_title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->sp_description,
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->sp_keywords,
]);

$this->params['breadcrumbs'][] = Html::a($model->sp_name, ['site/article', 'name' => $model->sp_url]);
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="breadcrumb">
                <?= yii\widgets\Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'encodeLabels' => false,
                    'options' => [
                        'class' => 'nav'
                    ],
                ]) ?>
            </div>

            <div class="page delivery payment">
                <header>
                    <h1 class="delivery-header">
                        Условия доставки товаров и оплаты в интернет-гипермаркете Гипострой.ру
                    </h1>
                    <dl class="tabs" data-tab>
                        <?php if(Yii::$app->request->get('active') !== null) { ?>
                            <dd><a href="#delivery">Доставка</a></dd>
                            <dd class="active"><a href="#payment">Оплата</a></dd>
                        <?php } else { ?>
                            <dd class="active"><a href="#delivery">Доставка</a></dd>
                            <dd><a href="#payment">Оплата</a></dd>
                        <?php } ?>
                    </dl>
                </header>
                <div class="tabs-content">
                    <?php if(Yii::$app->request->get('active') === null) { ?>
                        <div class="content active" id="delivery">
                    <?php } else { ?>
                        <div class="content" id="delivery">
                    <?php } ?>
                            <div class="tabbed">
                                <dl class="tabs" data-tab>
                                    <dd class="active"><a href="#msk">Москва и Московская область</a></dd>
                                    <dd><a href="#spb">Cанкт-Петербург и Ленинградская область</a></dd>
                                    <dd><a href="#nn">Нижний Новгород</a></dd>
                                    <dd><a href="#region">Доставка по России</a></dd>
                                </dl>
                                <div class="tabs-content">
                                    <div class="content active" id="msk">
                                        <h3>Условия доставки товаров интернет-гипермаркета Гипострой.ру</h3>
                                        <p>Доставка товаров осуществляется в пределах двух рабочих дней с момента согласования заказа с
                                            менеджером компании (за исключением товаров, поставляемых под заказ: сроки поставки таких
                                            товаров оговариваются индивидуально). Товар общим весом до 10 кг доставляется до дверей
                                            (за исключением группы товаров: гипсокартон, фанера, листовые материалы, профиль, рейки, брус, утеплитель).
                                            Товар свыше 10 кг доставляется до подъезда. В стоимость доставки товара до подъезда не
                                            входит разгрузка из автомобиля и подъем на этаж.</p>
                                        <p>Также у нас возможен самовывоз по предварительной заявке. <strong><?= Html::a('Адреса пунктов самовывоза.', ['site/article', 'name' => 'contacts']) ?></strong></p>
                                        <p>Товары с габаритами более 2,5 м., объемом более 0,5 м<sup>3</sup> и массой более 50 кг на пункты самовывоза не доставляются.</p>
                                        <h3>Стоимость доставки</h3>
                                        <table class="table">
                                            <tr>
                                                <td>Вес груза</td>
                                                <td>До 5 кг</td>
                                                <td>До 25 кг</td>
                                                <td>До 500 кг</td>
                                                <td>До 1500 кг</td>
                                                <td>До 3000 кг</td>
                                                <td>До 5000 кг</td>
                                                <td>До 10 000 кг</td>
                                                <td>До 20 000 кг</td>
                                            </tr>
                                            <tr>
                                                <td>Объем груза</td>
                                                <td>До 0,15 м<sup>3</sup></td>
                                                <td>До 0,5 м<sup>3</sup></td>
                                                <td>До 3 м<sup>3</sup></td>
                                                <td>До 10 м<sup>3</sup></td>
                                                <td>До 15 м<sup>3</sup></td>
                                                <td>До 30 м<sup>3</sup></td>
                                                <td>До 45 м<sup>3</sup></td>
                                                <td>До 80 м<sup>3</sup></td>
                                            </tr>
                                            <tr>
                                                <td>Габариты груза</td>
                                                <td>До 0,35 м</td>
                                                <td>До 1 м</td>
                                                <td>До 3 м</td>
                                                <td>До 5 м</td>
                                                <td>До 5 м</td>
                                                <td>До 5 м</td>
                                                <td>До 6,5 м</td>
                                                <td>До 12 м</td>
                                            </tr>
                                            <tr>
                                                <td>Цена</td>
                                                <td>300 руб</td>
                                                <td>500 руб</td>
                                                <td>1000 руб</td>
                                                <td>2000 руб</td>
                                                <td>3000 руб</td>
                                                <td>4500 руб</td>
                                                <td>7000 руб</td>
                                                <td>10 000 руб</td>
                                            </tr>
                                            <tr>
                                                <td>Въезд в ТТК (добавляется к основному тарифу)</td>
                                                <td></td>
                                                <td></td>
                                                <td>500 руб</td>
                                                <td>500 руб</td>
                                                <td>500 руб</td>
                                                <td>500 руб</td>
                                                <td>1000 руб</td>
                                                <td>1000 руб</td>
                                            </tr>
                                            <tr>
                                                <td>Въезд в Садовое Кольцо (добавляется к основному тарифу)</td>
                                                <td></td>
                                                <td></td>
                                                <td>1000 руб</td>
                                                <td>1000 руб</td>
                                                <td>1000 руб</td>
                                                <td>1000 руб</td>
                                                <td>1500 руб</td>
                                                <td>2000 руб</td>
                                            </tr>
                                            <tr>
                                                <td>За пределами МКАД</td>
                                                <td>10 руб/км</td>
                                                <td>15 руб/км</td>
                                                <td>30 руб/км</td>
                                                <td>30 руб/км</td>
                                                <td>40 руб/км</td>
                                                <td>45 руб/км</td>
                                                <td>50 руб/км</td>
                                                <td>60 руб/км</td>
                                            </tr>
                                            <tr>
                                                <td>Доставка до транспортной компании</td>
                                                <td>300 руб</td>
                                                <td>500 руб</td>
                                                <td>1000 руб</td>
                                                <td>2000 руб</td>
                                                <td>3000 руб</td>
                                                <td>4500 руб</td>
                                                <td>7000 руб</td>
                                                <td>10 000 руб</td>
                                            </tr>
                                            <tr>
                                                <td>Нормативное время при выгрузке товара</td>
                                                <td>30 мин</td>
                                                <td>30 мин</td>
                                                <td>30 мин</td>
                                                <td>60 мин</td>
                                                <td>60 мин</td>
                                                <td>1,5 часа</td>
                                                <td>2 часа</td>
                                                <td>4 часа</td>
                                            </tr>
                                            <tr>
                                                <td>Стоимость простоя транспорта при выгрузке товара (час)</td>
                                                <td>250 руб</td>
                                                <td>250 руб</td>
                                                <td>300 руб</td>
                                                <td>300 руб</td>
                                                <td>350 руб</td>
                                                <td>500 руб</td>
                                                <td>500 руб</td>
                                                <td>1000 руб</td>
                                            </tr>
                                            <tr>
                                                <td>Стоимость растентовки автомобиля при погрузке/выгрузке</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>1000 руб</td>
                                                <td>1000 руб</td>
                                            </tr>
                                        </table>
                                        <h3>Стоимость разгрузки и подъема на этаж</h3>
                                        <p>Учитывая большое количество факторов, влияющих на стоимость (общий объем покупки, вес и габариты товаров, наличие лифта и другие), услуга расчитывается индивидуально в каждом конкретном случае. Точную стоимость услуги уточняйте у менеджера.</p>
                                        <p>В случае отказа от товара заказчик обязан оплатить транспортные расходы в размере равному стоимости доставки, в соответствии со статьей 497 пункт 3 Гражданского кодекса РФ. “Покупатель до передачи товара вправе отказаться от исполнения договора розничной купли-продажи при условии возмещения продавцу необходимых расходов, понесенных в связи с совершением действий по выполнению договора”.</p>
                                    </div>
                                    <div class="content" id="spb">
                                        <h3>Условия доставки товаров интернет-гипермаркета Гипострой.ру</h3>
                                        <p>Доставка товаров осуществляется в пределах двух рабочих дней с момента согласования заказа
                                            с менеджером компании (за исключением товаров, поставляемых под заказ: сроки поставки
                                            таких товаров оговариваются индивидуально).</p>
                                        <p>Также у нас возможен самовывоз по предварительной заявке. <strong><?= Html::a('Адреса пунктов самовывоза.', ['site/article', 'name' => 'contacts', 'city' => 'spb']) ?></strong></p>
                                        <h3>Стоимость доставки</h3>
                                        <table class="table">
                                            <tr>
                                                <td>Вес груза</td>
                                                <td>До 5 кг</td>
                                                <td>До 10 кг</td>
                                                <td>До 15 кг</td>
                                                <td>До 20 кг</td>
                                                <td>До 25 кг</td>
                                            </tr>
                                            <tr>
                                                <td>Стоимость доставки</td>
                                                <td>290 руб.</td>
                                                <td>370 руб.</td>
                                                <td>470 руб.</td>
                                                <td>580 руб.</td>
                                                <td>730 руб.</td>
                                            </tr>
                                        </table>
                                        <p>
                                            <strong>Минимальная стоимость заказа 3000 руб.</strong><br>
                                            <strong>Максимальный объем груза для доставки 0.3 м<sup>3</sup>.</strong><br>
                                            <strong>Максимальный общий вес груза для доставки 25 кг.</strong><br>
                                            <strong>Максимальное время ожидания курьера составляет 15 минут.</strong><br>
                                            <strong>Доставка легкобьющихся товаров не осуществляется.</strong><br>
                                            В случае доставки за пределы КАД дополнительно оплачивается 50 руб. за каждые 5 км.<br>
                                            Заказы весом более 15 кг доставляются до подъезда, подъем на этаж оговаривается отдельно.
                                        </p>
                                    </div>
                                    <div class="content" id="nn">
                                        <h3>Условия доставки товаров интернет-гипермаркета Гипострой.ру</h3>
                                        <p>Доставка товаров осуществляется в пределах двух рабочих дней с момента согласования
                                            заказа с менеджером компании (за исключением товаров, поставляемых под заказ: сроки
                                            поставки таких товаров оговариваются индивидуально).</p>
                                        <p>Также у нас возможен самовывоз по предварительной заявке. <strong><?= Html::a('Адреса пунктов самовывоза.', ['site/article', 'name' => 'contacts', 'city' => 'nn']) ?></strong></p>
                                        <h3>Стоимость доставки</h3>
                                        <table class="table">
                                            <tr>
                                                <td>Вес груза</td>
                                                <td>До 5 кг</td>
                                                <td>До 10 кг</td>
                                                <td>До 15 кг</td>
                                                <td>До 20 кг</td>
                                                <td>До 25 кг</td>
                                            </tr>
                                            <tr>
                                                <td>Стоимость доставки</td>
                                                <td>290 руб.</td>
                                                <td>380 руб.</td>
                                                <td>470 руб.</td>
                                                <td>600 руб.</td>
                                                <td>750 руб.</td>
                                            </tr>
                                        </table>
                                        <p>
                                            <strong>Минимальная стоимость заказа 3000 руб.</strong><br>
                                            <strong>Максимальный объем груза для доставки 0.3 м<sup>3</sup>.</strong><br>
                                            <strong>Максимальный общий вес груза для доставки 25 кг.</strong><br>
                                            <strong>Максимальное время ожидания курьера составляет 15 минут.</strong><br>
                                            <strong>Доставка легкобьющихся товаров не осуществляется.</strong><br>
                                            Заказы весом более 15 кг доставляются до подъезда, подъем на этаж оговаривается отдельно.
                                        </p>
                                    </div>
                                    <div class="content" id="region">
                                        <h3>Условия доставки товаров интернет-гипермаркета Гипострой.ру</h3>
                                        <p>Доставка товаров осуществляется в пределах двух рабочих дней с момента согласования заказа с менеджером компании (за исключением товаров, поставляемых под заказ: сроки поставки таких товаров оговариваются индивидуально). Доставка товара свыше 25 кг производится до подъезда.</p>
                                        <p>Также у нас возможен самовывоз по <strong>предварительной заявке</strong></p>
                                        <h3>Транспортной компанией до пункта выдачи в вашем городе</h3>
                                        <table class="table">
                                            <tr>
                                                <td>Транспортная компания</td>
                                                <td>Способы оплаты заказа</td>
                                                <td>Ограничения по габаритам и весу</td>
                                                <td>Предоплата</td>
                                            </tr>
                                            <tr>
                                                <td>ПЭК<br>Деловые линии<br>Автотрейдинг</td>
                                                <td>Безналичный расчет<br>(банковский перевод)</td>
                                                <td>Без ограничений</td>
                                                <td>Требуется 100% предоплата заказа</td>
                                            </tr>
                                            <tr>
                                                <td>Рейл Континент</td>
                                                <td>Безналичный расчет<br/>Наличный расчёт</td>
                                                <td>Без ограничений</td>
                                                <td>Требуется 100% предоплата заказа</td>
                                            </tr>
                                        </table>
                                        <p>
                                            <strong>*Доставка легкобьющихся товаров не осуществляется</strong><br>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if(Yii::$app->request->get('active') == 'pay') { ?>
                            <div class="content active" id="payment">
                        <?php } else { ?>
                            <div class="content" id="payment">
                        <?php } ?>
                                <div class="immersed out">
                                    <h4 class="text-center">Уважаемые покупатели! В нашем интернет-магазине вы можете оплатить товар следующими способами</h4>
                                    <div class="row method text-center">
                                        <div class="small-4 columns">
                                            <?= Html::img('/images/labels/cash.png') ?>
                                            <h4>Наличными</h4>
                                            <p>Оплата наличными производится или курьеру при доставке товара или на пункте самовывоза.</p>
                                        </div>
                                        <div class="small-4 columns">
                                            <?= Html::img('/images/labels/card.png') ?>
                                            <h4>Банковской картой</h4>
                                            <p>Оплата производится через терминал курьеру при доставке товара или на пункте самовывоза. В случае возврата товара денежные средства перечисляются на ту же карту, с которой был произведен  платеж.</p>
                                        </div>
                                        <div class="small-4 columns">
                                            <?= Html::img('/images/labels/cashless.png') ?>
                                            <h4>Безналичным расчетом</h4>
                                            <p>Оплата товара безналично возможна как юридическим, так и физическим лицам. Наши менеджеры высылают вам на электронную почту счет. После зачисления денежных средств на наш расчетный счет мы примем заказ к исполнению. Для сокращения сроков доставки вы можете выслать нам платежку по электронной почте.</p>
                                        </div>
                                    </div>
                                    <p>Товар находящийся в статусе “Под заказ” продается при условии не менее 50% предоплаты.</p>
                                    <p>Товар, который режется (ковролин, линолеум) продается при условии 100% предоплаты.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>