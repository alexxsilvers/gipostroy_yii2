<?php
/* @var $this yii\web\View */
/* @var $title string */
/* @var $description string */
/* @var $keywords string */
/* @var $map array() */
/* @var $staticPages array() */

use yii\helpers\Html;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
\frontend\assets\ResponsiveSlidesAsset::register($this);

$this->title = $title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $keywords
]);

$this->params['breadcrumbs'][] = Html::a('Карта сайта', 'map');
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="breadcrumb">
                <?= yii\widgets\Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'encodeLabels' => false,
                    'options' => [
                        'class' => 'nav'
                    ],
                ]) ?>
            </div>

            <div class="page catalog">
                <header>
                    <h1>
                        Карта сайта
                    </h1>
                </header>

                <div class="result">
                    <ul class="plate">
                        <?php foreach($staticPages as $page) { ?>
                            <div class="map-level1">
                                <?= Html::a($page['sp_name'], ['site/article', 'name' => $page['sp_url']]) ?>
                            </div>
                        <?php } ?>
                        <div class="map-level1">
                            <?= Html::a('Производители', 'proizvoditeli') ?>
                        </div>
                        <?php foreach($map as $razdel) { ?>
                            <div class="map-level1">
                                <?= Html::a($razdel['rz_name'], ['catalog/category', 'name' => $razdel['rz_url']]) ?>
                            </div>
                                <?php if(!empty($razdel['sections'])) { ?>
                                    <?php foreach($razdel['sections'] as $section) { ?>
                                        <div class="map-level2">
                                            <?= Html::a($section['prz_name'], ['catalog/section', 'name' => $section['prz_url']]) ?>
                                        </div>
                                        <?php if(!empty($section['subsections'])) { ?>
                                            <?php foreach($section['subsections'] as $subsection) { ?>
                                                <div class="map-level3">
                                                    <?= Html::a($subsection['pprz_name'], ['catalog/subsection', 'name' => $subsection['pprz_url']]) ?>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>