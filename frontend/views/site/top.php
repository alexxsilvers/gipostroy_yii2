<?php
/* @var $this yii\web\View */
/* @var $positions array() */
/* @var $good Goods */
/* @var $title string */
/* @var $description string */
/* @var $keywords string */
use common\models\Goods;
use yii\helpers\Html;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
\frontend\assets\ResponsiveSlidesAsset::register($this);

$this->title = $title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $keywords
]);

$this->params['breadcrumbs'][] = Html::a('Лидеры продаж', 'top-sales');
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="breadcrumb">
                <?= yii\widgets\Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'encodeLabels' => false,
                    'options' => [
                        'class' => 'nav'
                    ],
                ]) ?>
            </div>

            <div class="page catalog">
                <div class="filter"></div>
                <div class="result">
                    <ul class="plate">
                    <?php foreach($positions as $good) { ?>
                        <li>
                            <div class="product">
                                <?php if($good->ct_sale == 1) { ?>
                                    <div class="good-discount"></div>
                                <?php } elseif($good->ct_hit == 1) { ?>
                                    <div class="good-hit"></div>
                                <?php } elseif($good->isNewGood()) { ?>
                                    <div class="good-new"></div>
                                <?php } ?>
                                <div class="product-image">
                                    <?= Html::a(
                                        Html::img($good->getImageSrc(), [
                                            'alt' => $good['ct_name'],
                                            'title' => $good['ct_name'],
                                            'class' => 'image',
                                        ]),
                                        [
                                            'catalog/item',
                                            'id' => $good['ct_url']
                                        ],
                                        [
                                            'title' => $good['ct_name'],
                                        ]
                                    ) ?>
                                </div>
                                <div class="product-description">
                                    <?= Html::a(
                                        $good->ct_name,
                                        ['catalog/item', 'id' => $good->ct_url],
                                        ['title' => $good->ct_name]
                                    ) ?>
                                    <table class="params">
                                        <tr>
                                            <td>Артикул</td>
                                            <td><?= $good->ct_newart ?></td>
                                        </tr>
                                        <?php if(!empty($good['manufacter']['mn_name'])) { ?>
                                            <tr>
                                                <td>Производитель</td>
                                                <td><?= $good['manufacter']['mn_name'] ?></td>
                                            </tr>
                                        <?php }?>
                                        <?php if(!empty($good['manufacter']['mn_country'])) { ?>
                                            <tr>
                                                <td>Страна</td>
                                                <td><?= $good['manufacter']['mn_country'] ?></td>
                                            </tr>
                                        <?php }?>
                                    </table>
                                </div>
                                <footer>
                                    <div class="available">
                                        <?= $good->getGoodBalance($good['unit']['name']) ?>
                                    </div>
                                    <div class="price">
                                        <?= $good->getGoodPriceBlock() ?>
                                    </div>
                                    <div class="buttons">
                                        <?= $good->getGoodBasketButton() ?>
                                    </div>
                                    <div class="shipping">
                                        <?= $good->getGoodShipping(); ?>
                                    </div>
                                </footer>
                            </div>
                        </li>
                    <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>