<?php
/* @var $this yii\web\View */
/* @var $title string */
/* @var $description string */
/* @var $keywords string */
/* @var $goods array() */
/* @var $razdelImages array() */
use common\models\Goods;
use yii\helpers\Html;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
\frontend\assets\ResponsiveSlidesAsset::register($this);

$this->title = $title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $keywords
]);
$this->registerJs("
    $('#content .rslides').responsiveSlides({
        pager: true
    });
");
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="banner">
                <ul class="rslides">
                    <li>
                        <?= Html::a(Html::img('/images/banner/banner_1.jpg'), ['catalog/section', 'name' => 'smesiteli']) ?>
                    </li>
                    <li>
                        <?= Html::a(Html::img('/images/banner/banner_2.jpg'), ['catalog/section', 'name' => 'suhie-smesi']) ?>
                    </li>
                    <li>
                        <?= Html::a(Html::img('/images/banner/banner_3.jpg'), ['catalog/section', 'name' => 'osveschenie']) ?>
                    </li>
                </ul>
            </div>

            <div class="page home with_shadow">

                <div class="lead row">
                    <header class="small-12 columns">
                        <span class="myCustomHeader">Лидеры продаж</span>
                    </header>
                    <div class="products row">
                        <?php /* @var $good Goods */ ?>
                        <?php foreach($goods as $good) { ?>
                            <div class="small-3 columns">
                                <div class="product" style="height:295px;">
                                    <?php if($good->ct_sale == 1) { ?>
                                        <div class="good-discount"></div>
                                    <?php } elseif($good->ct_hit == 1) { ?>
                                        <div class="good-hit"></div>
                                    <?php } elseif($good->isNewGood()) { ?>
                                        <div class="good-new"></div>
                                    <?php } ?>
                                    <div class="widget-product">
                                        <?= Html::a(
                                            Html::img($good->getImageSrc(), ['alt' => $good->ct_name, 'title' => $good->ct_name, 'style' => 'height:212px;']),
                                            [
                                                'catalog/item',
                                                'id' => $good->ct_url,
                                            ],
                                            [
                                                'title' => $good->ct_name,
                                                'onmousedown' => "try { rrApi.recomMouseDown('" . $good->ct_id . "', 'ItemsToMain') } catch (e) {}"
                                            ]
                                        ) ?>
                                    </div>
                                    <div style="clear:both;"></div>
                                    <div class="desq text-center">
                                        <?= Html::a($good->ct_name, ['catalog/item', 'id' => $good->ct_url], [
                                            'title' => $good->ct_name,
                                            'onmousedown' => "try { rrApi.recomMouseDown('" . $good->ct_id . "', 'ItemsToMain') } catch (e) {}"
                                        ]) ?>
                                    </div>
                                    <footer>
                                        <div class="price">
                                            <?= $good->getGoodPriceInteger() ?>
                                        </div>
                                            <?= $good->getGoodBasketButton() ?>
                                    </footer>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <footer class="small-12 columns text-right" style="margin-top: 10px;">
                        <?= Html::a('Показать все', 'top-sales') ?>
                    </footer>
                </div>
                <div class="immersed">
                    <div class="categories text-center row">
                        <?php foreach($razdelImages as $razdel) { ?>
                            <div class="category small-3 columns end">
                                <?= Html::a(
                                    Html::img('/images/categories/' . $razdel['rz_pic'], [
                                        'alt' => $razdel['rz_name'],
                                        'title' => $razdel['rz_name'],
                                    ]) . '<span>' . $razdel['rz_name'] . '</span>',
                                    [
                                        'catalog/category',
                                        'name' => $razdel['rz_url'],
                                    ]
                                ) ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="company row">
                    <div class="logo small-4 columns text-center">
                        <?= Html::img('/images/logo_big.png', [
                            'alt' => 'Интернет-магазин стройматериалов «Гипострой.ру»',
                            'title' => 'Интернет-магазин стройматериалов «Гипострой.ру»',
                        ]) ?>
                    </div>
                    <div class="small-7 columns end">
                        <header>
                            <h1>Интернет магазин строительных материалов Гипострой.ру</h1>
                        </header>
                        <p>
                            Мы хорошо знаем, что такое ремонт и строительство. Мы знаем, как сложно представив облик
                            будущего жилища, воплотить в жизнь все мечты. Чтобы сложное стало легким мы разработали
                            удобный каталог, системы поиска товаров и создали качественное описание к каждой позиции
                            нашего интернет-гипермаркета. Теперь вам проще искать, проще выбирать и проще покупать.
                            Покупать все необходимое в одном месте!
                        </p>
                    </div>
                </div>
                <div class="benefits row">
                    <header class="small-12 columns text-center">
                        <h3>Наши преимущества</h3>
                    </header>
                    <div class="small-6 columns">
                        <figure>
                            <?= Html::img('/images/labels/assortment.png', [
                                'alt' => 'Большой ассортимент стройматериалов',
                                'title' => 'Большой ассортимент стройматериалов',
                            ]) ?>
                            <figcaption>
                                <h4>Большой ассортимент</h4>
                                <p>
                                    Мы собрали в одном интернет-магазине всех известных производителей товаров для
                                    строительства и ремонта. В настоящий момент на сайте более 50000 товаров и их
                                    ассортимент постоянно растет.
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="small-6 columns">
                        <figure>
                            <?= Html::img('/images/labels/price.png', [
                                'alt' => 'Самая низкая стоимость стройматериалов в Москве',
                                'title' => 'Самая низкая стоимость стройматериалов в Москве',
                            ]) ?>
                            <figcaption>
                                <h4>Низкая стоимость</h4>
                                <p>
                                    Наши цены способны порадовать даже самого экономного покупателя. Они регулярно
                                    корректируются, чтобы оправдать ваши ожидания.
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="small-6 columns">
                        <figure>
                            <?= Html::img('/images/labels/delivery.png', [
                                'alt' => 'Быстрая доставка стройматериалов по Москве и области',
                                'title' => 'Быстрая доставка стройматериалов по Москве и области',
                            ]) ?>
                            <figcaption>
                                <h4>Быстрая доставка</h4>
                                <p>
                                    Мы быстро доставим ваши покупки вне зависимости от адреса доставки, объема
                                    или массы заказа. С нами ремонт - это быстро.
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="small-6 columns">
                        <figure>
                            <?= Html::img('/images/labels/consult.png', [
                                'alt' => 'Квалифицированные консультации в интернет-магазине «Гипострой.ру»',
                                'title' => 'Квалифицированные консультации в интернет-магазине «Гипострой.ру»',
                            ]) ?>
                            <figcaption>
                                <h4>Квалифицированные консультации</h4>
                                <p>
                                    Наши менеджеры - профессионалы своего дела. Они всегда окажут квалифицированную
                                    помощь в выборе товара и ответят на все ваши вопросы.
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>