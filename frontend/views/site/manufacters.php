<?php
/* @var $this yii\web\View */
/* @var $title string */
/* @var $description string */
/* @var $keywords string */
/* @var $manufacters array() */
/* @var $enAlphabet array() */
/* @var $ruAlphabet array() */

use yii\helpers\Html;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;

$this->title = $title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $keywords
]);

$this->params['breadcrumbs'][] = Html::a('Производители', 'proizvoditeli');
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="breadcrumb">
                <?= yii\widgets\Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'encodeLabels' => false,
                    'options' => [
                        'class' => 'nav'
                    ],
                ]) ?>
            </div>

            <div class="page text-left">
                <header>
                    <h1>
                        Производители
                    </h1>
                </header>
                <div class="alphabet">
                    <?php foreach($enAlphabet as $alph) { ?>
                        <?php if(isset($manufacters[$alph]) && !empty($manufacters[$alph])) { ?>
                            <a href="#<?=$alph?>"><?=$alph?></a>
                        <?php } else { ?>
                            <?=$alph?>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="alphabet">
                    <?php foreach($ruAlphabet as $alph) { ?>
                        <?php if(isset($manufacters[$alph]) && !empty($manufacters[$alph])) { ?>
                            <a href="#<?=$alph?>"><?=$alph?></a>
                        <?php } else { ?>
                            <?=$alph?>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="manufacters">
                    <?php foreach($manufacters as $k=>$manufacter) { ?>
                        <header>
                            <span id="<?=$k?>"><?=$k?></span>
                        </header>
                        <div>
                            <?php foreach($manufacter as $mn) { ?>
                                <?= Html::a($mn['mn_name'], ['site/show', 'name' => $mn['mn_url']]) ?>&nbsp;
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
