<?php
/* @var $this yii\web\View */
/* @var $title string */
/* @var $description string */
/* @var $keywords string */
/* @var $dataProvider ArrayDataProvider */

use common\models\Goods;
use frontend\components\widgets\DashNavWidget;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
use yii\widgets\LinkPager;

$this->title = $title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $keywords
]);
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="page dash">
                <header>
                    <h3>Личный кабинет</h3>
                </header>
                <?= DashNavWidget::widget() ?>
            </div>

            <div class="page catalog">
                <div class="sort">
                    <div class="row">
                        <div class="small-6 columns">
                            Сортировать по
                            <?= $dataProvider->sort->link('price') ?>
                            <?= $dataProvider->sort->link('name') ?>
                            <?= $dataProvider->sort->link('art') ?>
                        </div>
                        <div class="small-6 columns text-right view-options">
                            Показать в виде: <a href="#" class="changeDisplayGoods list<?= (Goods::getDisplayGoods()=='list') ? ' active' : '' ?>"></a><a href="#" class="changeDisplayGoods plate<?= (Goods::getDisplayGoods()=='list') ? '' : ' active' ?>"></a>
                        </div>
                    </div>
                </div>

                <?php if($dataProvider->totalCount > 24) { ?>
                    <div class="listing-options row">
                        <div class="small-6 small-offset-3 columns text-center pagination">
                            <?= LinkPager::widget([
                                'pagination' => $dataProvider->pagination,
                                'nextPageLabel' => 'Следующая',
                                'prevPageLabel' => 'Предыдущая',
                                'maxButtonCount' => 9,
                            ]) ?>
                        </div>
                        <div class="small-2 columns show-by text-right">
                            <label>
                                Показать по:
                                <select id="pSize" class="chosen-select show-by-select" name="psize">
                                    <option value="24" <?= (Goods::getPageSize() == 24) ? 'selected' : '' ?>>24</option>
                                    <option value="48" <?= (Goods::getPageSize() == 48) ? 'selected' : '' ?>>48</option>
                                    <option value="72" <?= (Goods::getPageSize() == 72) ? 'selected' : '' ?>>72</option>
                                    <option value="96" <?= (Goods::getPageSize() == 96) ? 'selected' : '' ?>>96</option>
                                    <option value="all" <?= (Goods::getPageSize() == 'all') ? 'selected' : '' ?>>Все</option>
                                </select>
                            </label>
                        </div>
                    </div>
                <?php } ?>

                <div class="result">
                    <ul class="<?= Goods::getDisplayGoods() ?>">
                        <?php /* @var $good Goods */ ?>
                        <?php foreach($dataProvider->getModels() as $good) { ?>
                            <li>
                                <div class="product">
                                    <?php if($good->ct_sale == 1) { ?>
                                        <div class="good-discount"></div>
                                    <?php } elseif($good->ct_hit == 1) { ?>
                                        <div class="good-hit"></div>
                                    <?php } elseif($good->isNewGood()) { ?>
                                        <div class="good-new"></div>
                                    <?php } ?>
                                    <div class="product-image">
                                        <?= Html::a(
                                            Html::img($good->getImageSrc(), [
                                                'alt' => $good['ct_name'],
                                                'title' => $good['ct_name'],
                                                'class' => 'image',
                                            ]),
                                            [
                                                'catalog/item',
                                                'id' => $good['ct_url']
                                            ],
                                            [
                                                'title' => $good['ct_name'],
                                            ]
                                        ) ?>
                                    </div>
                                    <div class="product-description">
                                        <?= Html::a(
                                            $good->ct_name,
                                            ['catalog/item', 'id' => $good->ct_url],
                                            ['title' => $good->ct_name]
                                        ) ?>
                                        <table class="params">
                                            <tr>
                                                <td>Артикул</td>
                                                <td><?= $good->ct_newart ?></td>
                                            </tr>
                                            <?php if(!empty($good['manufacter']['mn_name'])) { ?>
                                                <tr>
                                                    <td>Производитель</td>
                                                    <td><?= $good['manufacter']['mn_name'] ?></td>
                                                </tr>
                                            <?php }?>
                                            <?php if(!empty($good['manufacter']['mn_country'])) { ?>
                                                <tr>
                                                    <td>Страна</td>
                                                    <td><?= $good['manufacter']['mn_country'] ?></td>
                                                </tr>
                                            <?php }?>
                                        </table>
                                    </div>
                                    <footer>
                                        <div class="available">
                                            <?= $good->getGoodBalance($good['unit']['name']) ?>
                                        </div>
                                        <div class="price">
                                            <?= $good->getGoodPriceBlock() ?>
                                        </div>
                                        <div class="buttons">
                                            <?= $good->getGoodBasketButton() ?>
                                        </div>
                                        <div class="shipping">
                                            <?= $good->getGoodShipping(); ?>
                                        </div>
                                    </footer>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>

                <?php if($dataProvider->totalCount > 24) { ?>
                    <div class="listing-options row">
                        <div class="small-6 small-offset-3 columns text-center pagination">
                            <?= LinkPager::widget([
                                'pagination' => $dataProvider->pagination,
                                'nextPageLabel' => 'Следующая',
                                'prevPageLabel' => 'Предыдущая',
                                'maxButtonCount' => 9,
                            ]) ?>
                        </div>
                        <div class="small-2 columns show-by text-right">
                            <label>
                                Показать по:
                                <select id="pSize" class="chosen-select show-by-select" name="psize">
                                    <option value="24" <?= (Goods::getPageSize() == 24) ? 'selected' : '' ?>>24</option>
                                    <option value="48" <?= (Goods::getPageSize() == 48) ? 'selected' : '' ?>>48</option>
                                    <option value="72" <?= (Goods::getPageSize() == 72) ? 'selected' : '' ?>>72</option>
                                    <option value="96" <?= (Goods::getPageSize() == 96) ? 'selected' : '' ?>>96</option>
                                    <option value="all" <?= (Goods::getPageSize() == 'all') ? 'selected' : '' ?>>Все</option>
                                </select>
                            </label>
                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</section>