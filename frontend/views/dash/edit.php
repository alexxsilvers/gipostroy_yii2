<?php
/* @var $this yii\web\View */
/* @var $title string */
/* @var $description string */
/* @var $keywords string */
/* @var $userInfo UserInfo */

use common\models\UserInfo;
use frontend\components\widgets\DashNavWidget;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $keywords
]);
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="page dash address">
                <header>
                    <h3>Личный кабинет</h3>
                </header>
                <?= DashNavWidget::widget() ?>

                <div class="account panel">
                <?php $form = ActiveForm::begin([
                    'options' => [
                        'class' => 'gipo-form'
                    ]
                ]) ?>

                    <h4>Редактирование профиля</h4>
                    <?= $form->field($userInfo, 'fname')->input('text') ?>
                    <?= $form->field($userInfo, 'name')->input('text') ?>
                    <?= $form->field($userInfo, 'lname')->input('text') ?>
                    <?= $form->field($userInfo, 'region')->input('text') ?>
                    <?= $form->field($userInfo, 'phone')->input('phone', [
                        'id' => 'dash_phone',
                        'placeholder' => '+7 (___) ___ __ __'
                    ]) ?>
                    <div class="row" style="margin-top:20px;">
                        <div class="small-5 columns">
                            <?= Html::submitInput('Сохранить', ['class' => 'button button-blue']) ?>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>