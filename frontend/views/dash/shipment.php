<?php
/* @var $this yii\web\View */
/* @var $title string */
/* @var $description string */
/* @var $keywords string */
/* @var $shipments array() */
/* @var $model UserShipment */

use common\models\UserShipment;
use frontend\components\widgets\DashNavWidget;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = $title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $keywords
]);
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="page dash address">
                <header>
                    <h3>Личный кабинет</h3>
                </header>
                <?= DashNavWidget::widget() ?>

                <div class="addresses">
                    <?php if(sizeof($shipments) > 0) { ?>
                        <?php foreach($shipments as $shipment) { ?>
                            <div class="address">
                                <p class="name">
                                    <?= $shipment['fname'] ?> <?= $shipment['name'] ?> <?= $shipment['lname'] ?>
                                </p>
                                <p class="addr">
                                    <?php
                                    $addr = '';
                                    $addr .= $shipment['addr_string'];
                                    if(!empty($shipment['apartment'])) {
                                        $addr .= ', кв. ' . $shipment['apartment'];
                                    }
                                    if(!empty($shipment['floor'])) {
                                        $addr .= ', ' . $shipment['floor'] . ' этаж';
                                    }
                                    ?>
                                    <?= $addr ?>
                                </p>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <a href="#" id="add_new_address" class="button button-blue">+ Добавить</a>
                </div>

                <?php $form = ActiveForm::begin([
                    'options' => [
                        'class' => 'new_address hide gipo-form'
                    ]
                ]) ?>

                <h3>Новый адрес</h3>
                <?= $form->field($model, 'city')->input('text') ?>
                <?= $form->field($model, 'street')->input('text') ?>
                <?= $form->field($model, 'house')->input('text') ?>
                <?= $form->field($model, 'apartment')->input('text') ?>
                <?= $form->field($model, 'floor')->input('text') ?>
                <?= $form->field($model, 'comment')->textarea() ?>
                <div class="row" style="margin-top:20px;">
                    <div class="small-7 columns">
                        <a href="#" class="back">Вернуться к списку адресов</a>
                    </div>
                    <div class="small-5 columns">
                        <?= Html::submitInput('Сохранить', ['class' => 'button button-blue']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>

                <div id="map" class="shipmentMapBox"></div>
            </div>
        </div>
    </div>
</section>