<?php
/* @var $this yii\web\View */
/* @var $title string */
/* @var $description string */
/* @var $keywords string */
/* @var $dataProvider ArrayDataProvider */

use frontend\components\widgets\DashNavWidget;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
use yii\helpers\Json;
use yii\widgets\LinkPager;

$this->title = $title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $keywords
]);
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="page dash">
                <header>
                    <h3>Личный кабинет</h3>
                </header>
                <?= DashNavWidget::widget() ?>

                <?php if($dataProvider->totalCount > 10) { ?>
                    <div class="listing-options row">
                        <div class="small-6 small-offset-3 columns text-center pagination">
                            <?= LinkPager::widget([
                                'pagination' => $dataProvider->pagination,
                                'nextPageLabel' => 'Следующая',
                                'prevPageLabel' => 'Предыдущая',
                                'maxButtonCount' => 9,
                            ]) ?>
                        </div>
                    </div>
                <?php } ?>

                <div class="orders">
                    <?php foreach($dataProvider->getModels() as $order) { ?>
                        <div class="order">
                            <a href="#" class="expand-control"></a>
                            <header class="order-header row">
                                <div class="small-4 columns order-info">
                                    <a>Заказ <?= $order['onec_id'] ?> от <?= Yii::$app->formatter->asDate($order['or_ordertime'], 'dd MMMM yyyy') ?></a>
                                </div>
                                <div class="small-1 columns text-center product-price">Цена</div>
                                <div class="small-1 columns text-center product-quantity">Количество</div>
                                <div class="small-2 columns text-center product-sum">Цена с учетом количества</div>
                                <div class="small-3 columns text-center order-repeat">
                                </div>
                                <div class="small-1 columns">
                                </div>
                            </header>
                            <div class="order-body">
                                <?php foreach($order['positions'] as $item) { ?>
                                    <div class="product row">
                                        <div class="small-4 columns product-name">
                                            <?= Html::a(
                                                $item['good']['ct_name'],
                                                ['catalog/item', 'id' => $item['good']['ct_url']]
                                            ) ?>
                                            <span>Арт. <?= $item['good']['ct_newart'] ?></span>
                                        </div>
                                        <div class="small-1 columns text-center product-price">
                                            <?= $item['price'] ?> р.
                                        </div>
                                        <div class="small-1 columns text-center product-quantity">
                                            <?= $item['colvo'] ?> <?= $item['good']['unit']['name'] ?>
                                        </div>
                                        <div class="small-2 columns text-center product-sum">
                                            <?= round($item['price'] * $item['colvo'], 0, PHP_ROUND_HALF_UP) ?> р.
                                        </div>
                                        <div class="small-1 small-offset-3 columns">
                                            <?= Html::a(
                                                '',
                                                null,
                                                [
                                                    'class' => 'basket active add_to_cart',
                                                    'data-id' => $item['good']['ct_id'],
                                                ]
                                            ) ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <footer class="order-footer row">
                                <div class="small-4 columns order-info">
                                    <a>Заказ <?= $order['onec_id'] ?> от <?= Yii::$app->formatter->asDate($order['or_ordertime'], 'dd MMMM yyyy') ?></a>
                                </div>
                                <div class="small-1 small-offset-1 columns text-center order-sum">
                                    Итого
                                </div>
                                <div class="small-2 columns text-center order-sum">
                                    <?= $order['or_ordersum'] ?> р.
                                </div>
                                <div class="small-4 columns text-center order-repeat">
                                    <?= Html::a(
                                        'Повторить весь заказ',
                                        null,
                                        [
                                            'class' => 'button button-gray-shop mass_add_to_cart',
                                            'data-ids' => Json::encode(array_keys($order['positions'])),
                                        ]
                                    ) ?>
                                </div>
                            </footer>
                        </div>
                    <?php } ?>
                </div>

                <?php if($dataProvider->totalCount > 10) { ?>
                    <div class="listing-options row">
                        <div class="small-6 small-offset-3 columns text-center pagination">
                            <?= LinkPager::widget([
                                'pagination' => $dataProvider->pagination,
                                'nextPageLabel' => 'Следующая',
                                'prevPageLabel' => 'Предыдущая',
                                'maxButtonCount' => 9,
                            ]) ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>