<?php
/* @var $this yii\web\View */
/* @var $title string */
/* @var $description string */
/* @var $keywords string */
/* @var $user Users */

use common\models\Users;
use frontend\components\widgets\DashNavWidget;
use yii\helpers\Html;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;

$this->title = $title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $description
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $keywords
]);
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>

        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="page dash">
                <header>
                    <h3>Личный кабинет</h3>
                </header>
                <?= DashNavWidget::widget() ?>

                <div class="account panel">
                    <div class="panel-body">
                        <table>
                            <tr>
                                <td>
                                    Логин (электронная почта)
                                </td>
                                <td>
                                    <?= $user->email ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Мой регион
                                </td>
                                <td>
                                    <?= $user['info']['region'] ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h3>Персональные данные</h3>
                                </td>
                            </tr>
                            <tr>
                                <td>ФИО</td>
                                <td><?= $user['info']['fname'] ?> <?= $user['info']['name'] ?> <?= $user['info']['lname'] ?></td>
                            </tr>
                            <tr>
                                <td>Контактная информация</td>
                                <td>
                                    <?php if(!empty($user['info']['phone'])) { ?>
                                        <?= $user['info']['phone'] ?>  (телефон)<br>
                                    <?php } ?>

                                    <?php if(!empty($user->email)) { ?>
                                        <?= $user->email ?>  (почта)
                                    <?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>Мои товары</h4>
                                    <ul>
                                        <li>
                                            <?= Html::a(
                                                'просмотренные товары (' . sizeof($user['viewedGoods']) . ')',
                                                ['dash/viewed']
                                            ) ?>
                                        </li>
                                        <li>
                                            <?= Html::a(
                                                'мои заказы (' . sizeof($user['orders']) . ')',
                                                ['dash/orders']
                                            ) ?>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                        <div class="subscribe">
                            <p>Подписка на акции, новости, спецпредложения</p>
                            <form action="#">
                                <div class="sms">
                                    <?= Html::input('checkbox', 'null', $user['info']['sms_send'], [
                                        'id' => 'subscribe_by_sms',
                                        'class' => 'input-checkbox',
                                        'onclick' => 'sms();',
                                        'checked' => ($user['info']['sms_send'] == 1) ? 'checked' : '',
                                    ]) ?>
                                    <label for="subscribe_by_sms">SMS</label>
                                </div>
                                <div class="email">
                                    <?= Html::input('checkbox', 'null', $user['info']['email_send'], [
                                        'id' => 'subscribe_by_email',
                                        'class' => 'input-checkbox',
                                        'onclick' => 'email();',
                                        'checked' => ($user['info']['email_send'] == 1) ? 'checked' : '',
                                    ]) ?>
                                    <label for="subscribe_by_email">E-mail</label>
                                </div>
                            </form>
                        </div>
                        <div class="change">
                            <?= Html::a(
                                'Изменить мои данные',
                                ['dash/edit']
                            ) ?>
                            <?= Html::a(
                                'Изменить мой пароль',
                                ['dash/changepassword']
                            ) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>