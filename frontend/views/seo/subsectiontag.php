<?php
/* @var $this yii\web\View */
/* @var $model Podrazdel3lvl */
/* @var $otherSubections Podrazdel3lvl */
/* @var $compareIds array() */
/* @var $tag Seo */
/* @var $dataProvider yii\data\ActiveDataProvider */

use common\models\Goods;
use common\models\Podrazdel3lvl;
use common\models\Seo;
use frontend\components\widgets\FiltersWidget;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = $tag->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $tag->description,
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $tag->keywords,
]);

$this->params['breadcrumbs'][] = Html::a($model['section']['category']['rz_name'], ['catalog/category', 'name' => $model['section']['category']['rz_url']]);
$this->params['breadcrumbs'][] = Html::a($model['section']['prz_name'], ['catalog/section', 'name' => $model['section']['prz_url']]);
$this->params['breadcrumbs'][] = Html::a($model->pprz_name, ['catalog/subsection', 'name' => $model->pprz_url]);
$this->params['breadcrumbs'][] = Html::a($tag->type, ['seo/subsectiontag', 'subsection' => $model->pprz_url, 'tag' => $tag->url]);
?>

<section id="main">
    <div class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget() ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>

            <div class="breadcrumb">
                <?= yii\widgets\Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'encodeLabels' => false,
                    'options' => [
                        'class' => 'nav'
                    ],
                ]) ?>
            </div>

            <div class="page catalog">
                <header>
                    <h2 class="catHeader">
                        <?= $tag->header ?>
                    </h2>
                </header>

                <div class="filter">
                    <?= FiltersWidget::widget([
                        'sectionId' => $model->pprz_id,
                        'sectionUrl' => $model->pprz_url,
                        'condition' => $tag->condition,
                        'type' => 'subsection',
                    ]); ?>
                </div>

                <div class="sort">
                    <div class="row">
                        <div class="small-6 columns">
                            Сортировать по
                            <?= $dataProvider->sort->link('price') ?>
                            <?= $dataProvider->sort->link('name') ?>
                            <?= $dataProvider->sort->link('art') ?>
                        </div>
                        <div class="small-6 columns text-right view-options">
                            Показать в виде: <a href="#" class="changeDisplayGoods list<?= (Goods::getDisplayGoods()=='list') ? ' active' : '' ?>"></a><a href="#" class="changeDisplayGoods plate<?= (Goods::getDisplayGoods()=='list') ? '' : ' active' ?>"></a>
                        </div>
                    </div>
                </div>

                <?php if($dataProvider->totalCount > 24) { ?>
                    <div class="listing-options row">
                        <div class="small-6 small-offset-3 columns text-center pagination">
                            <?= LinkPager::widget([
                                'pagination' => $dataProvider->pagination,
                                'nextPageLabel' => 'Следующая',
                                'prevPageLabel' => 'Предыдущая',
                                'maxButtonCount' => 9,
                            ]) ?>
                        </div>
                        <div class="small-2 columns show-by text-right">
                            <label>
                                Показать по:
                                <select id="pSize" class="chosen-select show-by-select" name="psize">
                                    <option value="24" <?= (Goods::getPageSize() == 24) ? 'selected' : '' ?>>24</option>
                                    <option value="48" <?= (Goods::getPageSize() == 48) ? 'selected' : '' ?>>48</option>
                                    <option value="72" <?= (Goods::getPageSize() == 72) ? 'selected' : '' ?>>72</option>
                                    <option value="96" <?= (Goods::getPageSize() == 96) ? 'selected' : '' ?>>96</option>
                                    <option value="all" <?= (Goods::getPageSize() == 'all') ? 'selected' : '' ?>>Все</option>
                                </select>
                            </label>
                        </div>
                    </div>
                <?php } ?>

                <div class="result">
                    <ul class="<?= Goods::getDisplayGoods() ?>">
                        <?php /* @var $good Goods */ ?>
                        <?php foreach($dataProvider->getModels() as $good) { ?>
                            <li>
                                <div class="product">
                                    <?php if($good->ct_sale == 1) { ?>
                                        <div class="good-discount"></div>
                                    <?php } elseif($good->ct_hit == 1) { ?>
                                        <div class="good-hit"></div>
                                    <?php } elseif($good->isNewGood()) { ?>
                                        <div class="good-new"></div>
                                    <?php } ?>
                                    <div class="product-image">
                                        <?= Html::a(
                                            Html::img($good->getImageSrc(), [
                                                'alt' => $good['ct_name'],
                                                'title' => $good['ct_name'],
                                                'class' => 'image',
                                            ]),
                                            [
                                                'catalog/item',
                                                'id' => $good['ct_url']
                                            ],
                                            [
                                                'title' => $good['ct_name'],
                                            ]
                                        ) ?>
                                    </div>
                                    <div class="product-description">
                                        <?= Html::a(
                                            $good->ct_name,
                                            ['catalog/item', 'id' => $good->ct_url],
                                            ['title' => $good->ct_name]
                                        ) ?>
                                        <table class="params">
                                            <tr>
                                                <td>Артикул</td>
                                                <td><?= $good->ct_newart ?></td>
                                            </tr>
                                            <?php if(!empty($good['manufacter']['mn_name'])) { ?>
                                                <tr>
                                                    <td>Производитель</td>
                                                    <td><?= $good['manufacter']['mn_name'] ?></td>
                                                </tr>
                                            <?php }?>
                                            <?php if(!empty($good['manufacter']['mn_country'])) { ?>
                                                <tr>
                                                    <td>Страна</td>
                                                    <td><?= $good['manufacter']['mn_country'] ?></td>
                                                </tr>
                                            <?php }?>
                                        </table>
                                    </div>
                                    <footer>
                                        <div class="available">
                                            <?= $good->getGoodBalance($good['unit']['name']) ?>
                                        </div>
                                        <div class="price">
                                            <?= $good->getGoodPriceBlock() ?>
                                        </div>
                                        <div class="buttons">
                                            <?= $good->getGoodBasketButton() ?>
                                        </div>
                                        <div class="shipping">
                                            <?= $good->getGoodShipping(); ?>
                                        </div>
                                        <?= $good->getGoodsCompareBlock(null, $compareIds) ?>
                                    </footer>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>

                <?php if($dataProvider->totalCount > 24) { ?>
                    <div class="listing-options row">
                        <div class="small-6 small-offset-3 columns text-center pagination">
                            <?= LinkPager::widget([
                                'pagination' => $dataProvider->pagination,
                                'nextPageLabel' => 'Следующая',
                                'prevPageLabel' => 'Предыдущая',
                                'maxButtonCount' => 9,
                            ]) ?>
                        </div>
                        <div class="small-2 columns show-by text-right">
                            <label>
                                Показать по:
                                <select id="pSize" class="chosen-select show-by-select" name="psize">
                                    <option value="24" <?= (Goods::getPageSize() == 24) ? 'selected' : '' ?>>24</option>
                                    <option value="48" <?= (Goods::getPageSize() == 48) ? 'selected' : '' ?>>48</option>
                                    <option value="72" <?= (Goods::getPageSize() == 72) ? 'selected' : '' ?>>72</option>
                                    <option value="96" <?= (Goods::getPageSize() == 96) ? 'selected' : '' ?>>96</option>
                                    <option value="all" <?= (Goods::getPageSize() == 'all') ? 'selected' : '' ?>>Все</option>
                                </select>
                            </label>
                        </div>
                    </div>
                <?php } ?>

                <div class="clr">
                    <div class="wrap shop-tabs tabbed tabbed2" style="width: 960px;">
                        <dl class="tabs" data-tab>
                            <dd class="active"><a href="#shop-otherPprz" style="font-size:15px;">Другие разделы этой категории</a></dd>
                        </dl>
                        <div class="tabs-content" style="background: #f9f9f9">
                            <div class="content active" id="shop-otherPprz">
                                <div class="products">
                                    <ul class="small-block-grid-4">
                                        <?php foreach($otherSubections as $k => $otherSubsection) { ?>
                                            <li <?= ($k >= 4) ? ' class="otherPprz" style="display:none;" ' : '' ?>>
                                                <div class="text-center tab-category-images">
                                                    <?php if(in_array($_SERVER['SERVER_NAME'],array('develop.gipostroy.ru', 'gipo.loc', 'gipo2yii.loc', 'yiishop.loc'))) {
                                                        $src = 'http://www.gipostroy.ru/img/catalog/' . $otherSubsection['pprz_pic'];
                                                    } elseif(file_exists($_SERVER['DOCUMENT_ROOT'].'/img/catalog/' . $otherSubsection['pprz_pic'])) {
                                                        $src = '/img/catalog/' . $otherSubsection['pprz_pic'];
                                                    } else {
                                                        $src = '/images/nophoto.jpg';
                                                    } ?>
                                                    <?= Html::a(
                                                        Html::img($src, ['alt' => $otherSubsection['pprz_name'], 'title' => $otherSubsection['pprz_name']]),
                                                        ['catalog/subsection', 'name' => $otherSubsection['pprz_url']]
                                                    ) ?>

                                                    <?= Html::a(
                                                        '<span>' . $otherSubsection['pprz_name'] . '</span>',
                                                        ['catalog/subsection', 'name' => $otherSubsection['pprz_url']]
                                                    ) ?>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                    <?php if(sizeof($otherSubections)>4) { ?>
                                        <footer class="small-12 columns text-right" style="margin-top: 10px;">
                                            <a class="getOtherPprz">Показать все</a>
                                        </footer>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="immersed" id="non-selectable">
                    <div class="description" style="text-align:justify">
                        <?php if(Yii::$app->request->get('page') <= 1) { ?>
                            <?= $tag->getText() ?>
                        <?php } ?>
                        <p>
                            Информация об условиях и способах оплаты товаров из каталога “<?= $model->pprz_name . ' ' . $tag->type ?>” доступна на
                            странице “<noindex><?= Html::a('Оплата', ['page/article', 'name' => 'delivery'], ['rel' => 'nofollow']) ?></noindex>”.
                        </p>
                        <p>
                            О стоимости. сроках и условиях доставки по Москве (в пределах МКАД) и в
                            регионы России читайте на странице “<noindex><?= Html::a('Доставка', ['page/article', 'name' => 'delivery'], ['rel' => 'nofollow']) ?></noindex>”.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>