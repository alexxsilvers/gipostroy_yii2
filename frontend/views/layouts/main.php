<?php
use common\models\Links;
use common\models\Razdel;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Sitepages;
use frontend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$mainMenu = Sitepages::getDb()->cache(function($db){
    return Sitepages::find()->show()->asArray()->orderBy('sp_sorti ASC')->all();
}, 1000);
$razdelArray = Razdel::getDb()->cache(function($db){
    return Razdel::find()->show()->asArray()->orderBy('rz_sorti ASC')->all();
}, 1000);
$route = Yii::$app->controller->route;
$cart = \common\models\Cart::getCart(true);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=1200, maximum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <header id="header">
        <div class="top_row">
            <div class="wrap">
                <?php if($route == 'site/index') { ?>
                    <span id="logo">
                    <?= Html::img('/images/logo.png', [
                        'alt' => 'Интекрнет-гипермаркет Гипострой.ру',
                        'title' => 'Интекрнет-гипермаркет Гипострой.ру'
                    ]) ?>
                </span>
                <?php } else { ?>
                    <?= Html::a(
                        Html::img('/images/logo.png', ['alt' => 'Интекрнет-гипермаркет Гипострой.ру','title' => 'Интекрнет-гипермаркет Гипострой.ру']),
                        ['site/index'],
                        ['id' => 'logo']
                    ) ?>
                <?php } ?>
                <div class="links container_12">
                    <noindex>
                        <ul class="nav">
                            <?php foreach($mainMenu as $page) { ?>
                                <li>
                                    <?= Html::a($page['sp_name'], ['page/article', 'name' => $page['sp_url']], ['rel' => 'nofollow']) ?>
                                </li>
                            <?php } ?>
                            <li>
                                <?= Html::a('Мобильная версия', ['site/gotomobile', 'url' => str_replace('/','-',$_SERVER['REQUEST_URI'])]) ?>
                            </li>
                            <?php if(!Yii::$app->user->isGuest) { ?>
                                <li>
                                    <?= Html::a('Выйти из личного кабинета', ['ajax/logout'])?>
                                </li>
                            <?php } ?>
                        </ul>
                    </noindex>
                </div>
            </div>
        </div>

        <div class="bottom_row">
            <div class="wrap">
                <div class="container right">
                    <div class="row collapse">
                        <div class="small-3 columns">
                            <div class="phone">
                                <?= Html::a('+7 (495) <div class="call_phone_1">545-44-54</div>', 'tel:+74955454454') ?>
                                <?= Html::a('+7 (812) 425-32-61', 'tel:+78124253261') ?>
                                <span>
                                    Консультация Пн-Пт 9.00-18.00
                                </span>
                            </div>
                        </div>
                        <div class="small-3 columns">
                            <div class="phone">
                                <?= Html::a('+7 (800) 555-05-72', 'tel:+78005550572') ?>
                                <span>
                                    Бесплатный звонок по России
                                </span>
                            </div>
                        </div>
                        <div class="small-6 columns">
                            <div class="callback">
                                <?= Html::a('Заказать звонок', null, [
                                    'onclick' => "
                                                open_modal('callback');
                                                ga('send', 'event', 'CallMe', 'OpenCallback', 'OpenCallback');
                                                ga('gipoEc.send', 'event', 'CallMe', 'OpenCallback', 'OpenCallback');
                                            ",
                                    'class' => 'button button-orange',
                                    'id' => 'callback',
                                ]) ?>
                                <?= Html::a('Оформить заявку', null, [
                                    'onclick' => "
                                                open_modal('proposal');
                                                ga('send', 'event', 'SendProposal', 'OpenProposal', 'OpenProposal');
                                                ga('gipoEc.send', 'event', 'SendProposal', 'OpenProposal', 'OpenProposal');
                                            ",
                                    'class' => 'button button-orange',
                                    'id' => 'proposal',
                                ]) ?>
                            </div>

                            <?= Html::a(
                                Html::img('/images/icons/user.png', ['alt' => 'Личный кабинет', 'title' => 'Личный кабинет']) . '<span>Личный кабинет</span>',
                                (Yii::$app->user->isGuest) ? null : ['dash/info'],
                                [
                                    'class' => 'account button button-big',
                                    'onclick' => (Yii::$app->user->isGuest) ? "open_modal('signin');" : ''
                                ]
                            ) ?>

                            <?php if($cart->items_count > 0) { ?>
                                <div id="basketButton" class="basket button button-orange basket-non-empty">
                                    <?= Html::img('/images/icons/cart.png', [
                                        'alt' => $cart->items_count_string . ' ' . $cart->total_price . ' р',
                                        'title' => $cart->items_count_string . ' ' . $cart->total_price . ' р'
                                    ]) ?>
                                    <span><span class="qty"><?= $cart->items_count_string ?></span> <span class="sum"><?= $cart->total_price ?> р</span></span>
                                </div>
                            <?php } else { ?>
                                <div id="basketButton" class="basket button button-big">
                                    <?= Html::img('/images/icons/cart_empty.png', ['alt' => 'Корзина пуста', 'title' => 'Корзина пуста']) ?>
                                    <span class="empty_cart">Корзина пуста</span>
                                </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <?=$content?>

    <section id="subscribe">
        <div class="wrap">
            <form id="subscribe_user">
                <label for="subscribe_name">
                    <?= Html::img('/images/icons/letter_subscribe.png', [
                        'alt' => 'Подписаться на еженедельное обновление продукции',
                        'title' => 'Подписаться на еженедельное обновление продукции'
                    ]) ?>
                    Подписаться на еженедельное обновление продукции:
                </label>
                <input name="subscribe_user[name]" type="text" id="subscribe_name" placeholder="ФИО">
                <input name="subscribe_user[email]" type="email" id="subscribe_mail" placeholder="Электроная почта">
                <input onClick="sendRegisterFromSubscribe();" type="button" value="Подписаться" class="button button-lightgray">
            </form>
        </div>
    </section>

    <footer id="footer" class="wrap">
        <div class="modules">
            <div class="module catalog">
                <header>
                    <span class="footerHeader">Каталог</span>
                </header>
                <ul>
                    <?php foreach($razdelArray as $razdel) { ?>
                        <li><?= Html::a($razdel['rz_name'], ['catalog/category', 'name' => $razdel['rz_url']]) ?></li>
                    <?php } ?>
                </ul>
            </div>

            <div class="module menu">
                <header>
                    <span class="footerHeader">Меню</span>
                </header>
                <ul>
                    <?php foreach($mainMenu as $page) { ?>
                        <li><noindex><?= Html::a($page['sp_name'], ['page/article', 'name' => $page['sp_url']], ['rel' => 'nofollow']) ?></noindex></li>
                    <?php } ?>
                    <li>
                        <?= Html::a('Производители', ['site/proizvoditeli'])?>
                    </li>
                    <li>
                        <noindex>
                            <?= Html::a('Карта сайта', ['site/map'], ['rel' => 'nofollow']) ?>
                        </noindex>
                    </li>
                </ul>
            </div>

            <div class="module feedback">
                <header>
                    <span class="footerHeader">Обратная связь</span>
                </header>
                <p>
                    <?= Html::a('Оставить отзыв о сайте', null, ['onclick' => "open_modal('review')"]) ?>
                    <noindex>
                        <?= Html::a(
                                Html::img('/images/reviews.png', ['alt' => 'Читать отзывы', 'title' => 'Читать отзывы']),
                                'http://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2508/*http://market.yandex.ru/shop/110733/reviews',
                                [
                                    'target' => '_blank',
                                ]
                            ) ?>
                    </noindex>
                </p>
                <div class="footer-links">
                    <span class="footerHeader">Популярное</span>
                    <?php foreach(Links::getRand(3) as $link) { ?>
                        <?= Html::a(
                            $link['name'],
                            $link['link']
                        ) ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="information">
            <div class="phones">
                <span>
                    <?= Html::a('+7 (495) <div class="call_phone_1">545-44-54</div>', 'tel:+74955454454') ?> заказ по телефону в Москве
                </span>
                <span>
                    <?= Html::a('+7 (812) 425-32-61', 'tel:+78124253261') ?> заказ по телефону в СПБ
                </span>
                <span>
                    <?= Html::a('+7 (800) 555-05-72', 'tel:+78005550572') ?> бесплатный звонок по России
                </span>
                <div class="worktime">
                    <span>Прием заказов по телефону</span>
                    <span>Понедельник - пятница с 9.00 до 18.00</span>
                </div>
            </div>
            <div class="callback">
                <?= Html::a('Заказать звонок', null, [
                    'onclick' => "
                                                open_modal('callback');
                                                ga('send', 'event', 'CallMe', 'OpenCallback', 'OpenCallback');
                                                ga('gipoEc.send', 'event', 'CallMe', 'OpenCallback', 'OpenCallback');
                                            ",
                    'class' => 'button button-gray',
                    'id' => 'callback',
                ]) ?>

                <?= Html::a('Оформить заявку', null, [
                    'onclick' => "
                                                open_modal('proposal');
                                                ga('send', 'event', 'SendProposal', 'OpenProposal', 'OpenProposal');
                                                ga('gipoEc.send', 'event', 'SendProposal', 'OpenProposal', 'OpenProposal'); return !ga;
                                            ",
                    'class' => 'button button-gray',
                    'id' => 'proposal',
                ]) ?>
            </div>
            <p class="offer">
                Обращаем ваше внимание на то, что данный интернет сайт носит исключительно
                информационный характер и ни при каких условиях не является публичной офертой,
                определяемой положениями Статьи 437 Гражданского кодекса РФ.
            </p>
        </div>
        <div class="copyright">
            <p>
                <a href="/">
                    <?= Html::img('/images/logo_footer.png', [
                        'alt' => 'Интернет-гипермаркет Гипострой.ру',
                        'title' => 'Интернет-гипермаркет Гипострой.ру',
                    ]) ?>
                     © 2015 Интернет-магазин строительных и отделочных материалов “Гипострой.ру”</a><br/>
                <noindex>
                    <?= Html::a('Google+', 'https://plus.google.com/118093725446605362791', [
                        'rel' => 'nofollow',
                        'target' => '_blank'
                    ]) ?>
                </noindex>
            </p>
            <div class="support_number">
                <?php if(!Yii::$app->user->isGuest) { ?>
                    Ваш номер: <?= Yii::$app->user->id ?>
                <?php } ?>
            </div>
        </div>
    </footer>

    <div class="modals">

        <!--Оставить заявку-->
        <form class="modal proposal text-center">
            <a href="#" class="cross white"></a>
            <header class="modal-header">
                <span>Отправить заявку</span>
            </header>
            <div class="error-proposal error-modal"></div>
            <div class="modal-body">
                <div class="row">
                    <div class="small-12 columns">
                        <span class="required float-left">*</span>
                        <input name="ProposalForm[name]" type="text" placeholder="Имя" />
                    </div>
                    <div class="small-12 columns">
                        <span class="required float-left">*</span>
                        <input name="ProposalForm[email]" type="text" placeholder="Email">
                    </div>
                    <div class="small-12 columns">
                        <span class="required float-left">*</span>
                        <input name="ProposalForm[phone]" class="mask-phone" type="text" placeholder="+7 (___) ___ __ __">
                    </div>
                    <div class="small-12 columns">
                        <input name="ProposalForm[companyName]" type="text" placeholder="Название компании">
                    </div>
                    <div class="small-12 columns">
                        <input name="ProposalForm[inn]" type="text" placeholder="ИНН">
                    </div>
                    <input name="ProposalForm[attach]" class="proposal-attach" type="hidden" />
                    <div class="small-12 columns">
                        <?= \kato\DropZone::widget([
                            'options' => [
                                'url' => '/ajax/upload',
                                'maxFilesize' => '8',
                                'dictDefaultMessage' => 'Выберите файл или переместите его сюда.',
                                'dictFallbackMessage' => 'Ваш браузер не поддерживает загрузку файлов',
                                'dictFileTooBig' => 'Ошибка загрузки. Максимальный размер файла {{maxFilesize}}МБ.',
                                'dictInvalidFileType' => 'Вы не можете загружать файлы этого типа',
                                'dictCancelUpload' => 'Отменить загрузку',
                                'dictCancelUploadConfirmation' => 'Вы уверены, что хотите отменить загрузку?',
                                'dictRemoveFile' => 'Удалить файл',
                            ],
                            'clientEvents' => [
                                'complete' => "function(file){
                                    $('.dz-message').hide();
                                    $('.proposal-attach').val(file.name);
                                }",
                                'removedfile' => "function(file){alert(file.name + ' is removed')}"
                            ],
                        ]); ?>
                    </div>
                    <input name="_csrf" type="hidden" value="<?= Yii::$app->request->csrfToken ?>"/>
                    <div class="small-12 columns" style="margin-top:10px;">
                        <textarea name="ProposalForm[comment]" cols="30" rows="4" placeholder="Ваш комментарий"></textarea>
                    </div>
                </div>
            </div>
            <hr class="shadow">
            <footer class="modal-footer">
                <a class="button button-blue" onClick="sendProposal(); return false;">Отправить</a>
            </footer>
        </form>
        <!--Оставить заявку END-->

        <!--Заявка отправлена-->
        <div class="modal proposal-accepted text-center">
            <a href="#" class="cross white"></a>
            <header class="modal-header">
                <?= Html::img('/images/check1.png', [
                    'alt' => 'Ваша заявка успешно отправлена!',
                    'title' => 'Ваша заявка успешно отправлена!',
                ]) ?>
                <span class="modal-accept">Ваша заявка успешно отправлена!</span>
            </header>
        </div>
        <!--Заявка отправлена-->

        <!--Отзыв-->
        <form class="modal review text-center">
            <a href="#" class="cross white"></a>
            <header class="modal-header">
                <span>Оставить отзыв о сайте</span>
            </header>
            <div class="error-review error-modal"></div>
            <div class="modal-body">
                <div class="row">
                    <div class="small-12 columns">
                        <span class="required float-left">*</span>
                        <input name="ReviewForm[tema]" type="text"  placeholder="Тема">
                    </div>
                    <div class="small-12 columns">
                        <span class="required float-left">*</span>
                        <textarea name="ReviewForm[comment]" cols="30" rows="4" placeholder="Ваш отзыв"></textarea>
                    </div>
                </div>
            </div>
            <input name="_csrf" type="hidden" value="<?= Yii::$app->request->csrfToken ?>"/>
            <hr class="shadow">
            <footer class="modal-footer">
                <a class="button button-lightgray" onClick="sendReview(); return false;">Отправить</a>
            </footer>
        </form>
        <!--Отзыв END-->

        <!--Отзыв отправлен-->
        <div class="modal review-accepted text-center">
            <a href="#" class="cross white"></a>
            <header class="modal-header">
                <?= Html::img('/images/check1.png') ?>
                <span class="modal-accept">Ваш отзыв принят!</span>
            </header>
        </div>
        <!--Отзыв отправлен END-->

        <!--Обратный звонок-->
        <form class="modal callback">
            <a href="#" class="cross white"></a>
            <header class="modal-header text-center">
                <span>Заказ обратного звонка</span>
            </header>
            <div class="error-callback error-modal"></div>
            <div class="modal-body">
                <div class="row">
                    <div class="small-4 columns">
                        <label for="callback_name">Ваше имя</label>
                    </div>
                    <div class="small-8 columns">
                        <input name="CallbackForm[name]" type="text" id="callback_name" placeholder="Иван Петров">
                    </div>
                </div>
                <div class="row">
                    <div class="small-4 columns">
                        <label for="callback_phone">Ваш телефон</label>
                    </div>
                    <div class="small-8 columns">
                        <input name="CallbackForm[phone]" type="tel" id="callback_phone" placeholder="+7 (___) ___ __ __">
                    </div>
                </div>
                <div class="row">
                    <div class="small-4 columns">
                        <label for="callback_razdel">Консультация по</label>
                    </div>
                    <div class="small-8 columns">
                        <select name="CallbackForm[razdel]" id="callback_razdel" class="chosen-select-modal">
                            <option value="">Выберите раздел</option>
                            <?php foreach($razdelArray as $razdel) { ?>
                                <option><?=$razdel['rz_name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="small-4 columns">
                        <label for="callback_comment">Комментарий</label>
                    </div>
                    <div class="small-8 columns">
                        <textarea name="CallbackForm[comment]" id="callback_comment" cols="30" rows="4" placeholder="Например, удобное время"></textarea>
                    </div>
                </div>
            </div>
            <input name="_csrf" type="hidden" value="<?= Yii::$app->request->csrfToken ?>"/>
            <hr class="shadow">
            <footer class="modal-footer text-center">
                <a class="button button-lightgray" onClick="sendCallback(); return false;">Позвонить мне</a>
            </footer>
        </form>
        <!--Обратный звонок END-->

        <!--(Обратный звонок|Заказ в один клик) уведомление-->
        <div class="modal callback-accepted text-center">
            <a href="#" class="cross white clear-admitad"></a>
            <header class="modal-header">
                <?= Html::img('/images/check1.png') ?>
                <span>Ваша заявка принята!</span>
            </header>
            <div style="display:none;" class="onec_id"></div>
            <div class="modal-body">
                <p>Наш менеджер свяжется с вами в ближайшее время.<br>Пн-Пт с 9.00 до 18.00</p>
            </div>
            <div class="for-admitad"></div>
        </div>
        <!--(Обратный звонок|Заказ в один клик) END-->

        <!-- Авторизация -->
        <form class="modal signin text-center">
            <a href="#" class="cross white"></a>
            <header class="modal-header">
                <span>Вход в личный кабинет</span>
            </header>
            <div class="error-modal error-signin"></div>
            <div class="modal-body">
                <div class="row">
                    <div class="small-12 columns">
                        <input name="SigninForm[email]" type="email" placeholder="Электронная почта">
                    </div>
                    <div class="small-12 columns">
                        <input name="SigninForm[password]" type="password" placeholder="Пароль">
                    </div>
                    <div class="small-5 columns text-left">
                        <input name="_csrf" type="hidden" value="<?= Yii::$app->request->csrfToken ?>"/>
                        <a class="button button-blue button-wide" onClick="signin(); return false;">Войти</a>
                        <br>
                        <a onclick="open_modal('password-recovery');" class="lost_password">Забыли пароль?</a>
                    </div>
                    <div class="small-6 columns text-right">
                        <input name="SinginForm[remember]" id="SinginForm_remember" type="checkbox" checked="checked" class="input-checkbox blue">
                        <label for="SinginForm_remember">Запомнить меня</label>
                    </div>
                </div>
            </div>
            <hr class="shadow">
            <footer class="modal-footer">
                <p>Еще не зарегестрировались?<br>Пройдите быструю регистрацию и получите больше возможностей</p>
                <input type="button" class="button button-lightgray" value="Регистрация" onClick="open_modal('register');">
            </footer>
        </form>
        <!-- Авторизация END -->

        <!--Заказ в 1 клик-->
        <form class="modal oneclickcall">
            <a href="#" class="cross white"></a>
            <header class="modal-header text-center">
                <span>Заказать в 1 клик</span>
            </header>
            <div class="error-modal error-oneclickcall"></div>
            <div class="modal-body">
                <div class="row">
                    <div class="small-12 columns">
                        <input name="OneClickCallForm[name]" type="text" placeholder="Ваше имя">
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <input name="OneClickCallForm[fname]" type="text" placeholder="Ваша фамилия">
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <input name="OneClickCallForm[phone]" class="mask-phone" type="text" placeholder="+7 (___) ___ __ __">
                    </div>
                </div>
                <input name="OneClickCallForm[good_id]" type="hidden" class="good-id-oneclickcall" />
            </div>
            <hr class="shadow">
            <footer class="modal-footer text-center">
                <input type="button" class="button button-blue one-click-call-send" value="Жду звонка"  onClick="sendOneClickCall();">
                <?= Html::img('/images/ajax-loader.gif', [
                    'class' => 'one-click-call-loading'                    
                ]) ?>
            </footer>
        </form>
        <!--Заказ в 1 клик END-->
    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
