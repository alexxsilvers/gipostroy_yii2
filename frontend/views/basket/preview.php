<?php
/** @var $this yii\web\View */
use common\models\Goods;
use yii\helpers\Html;

/** @var $cart \common\models\Cart */
?>

<div id="preCart" data-price="<?= $cart->total_price ?>" data-count="<?= $cart->items_count ?>" data-items="<?= $cart->items_count_string ?>">
    <?php if(sizeof($cart->items) == 0) { ?>
        <div class="preCartEmpty">
            Ваша корзина пуста. Воспользуйтесь каталогом для добавления товаров.
        </div>
    <?php } else { ?>
        <div class="preCartSummary">
            В корзине <?= $cart->items_count_string ?> - <?= $cart->total_price ?> руб.
            <div class="pre-cart-close"></div>
        </div>
        <div class="preCartGoods">
            <?php /* @var $good Goods */ ?>
            <?php foreach($cart->getGoods() as $good) { ?>
            <div class="preCartGood">
                <div class="preCartGoodImage">
                    <noindex>
                        <?= Html::img($good->getImageSrc(), [
                            'alt' => $good->ct_name,
                            'title' => $good->ct_name,
                        ]) ?>
                    </noindex>
                    <div class="preCartGoodName">
                        <?= Html::a(
                            $good->ct_name,
                            ['catalog/item', 'id' => $good->ct_url]
                        ) ?>
                        <span>
                        <strong><?= $cart->items[$good->ct_id]['price'] ?></strong> руб. x <strong><?= $cart->items[$good->ct_id]['quantity'] ?></strong> <?= $good['unit']['name'] ?>
                    </span>
                    </div>
                </div>
                <div class="preCartDelete" data-id="<?= $good->ct_id ?>"></div>
            </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>

<?php if($cart->items_count > 0) { ?>
<div class="preCartButtons">
    <?= Html::a(
        'Перейти в корзину',
        [
            'basket/order'
        ],
        [
            'class' => 'button button-blue',
            'onclick' => "ga('send', 'event', 'test', 'PRE_CART_ORDER');ga('gipoEc.send', 'event', 'test', 'PRE_CART_ORDER');",
        ]
    ) ?>
    <?= Html::a(
        'Оформить заказ',
        [
            'basket/checkout'
        ],
        [
            'class' => 'button button-orange',
            'onClick' => "ga('send', 'event', 'test', 'PRE_CART_CHECKOUT');ga('gipoEc.send', 'event', 'test', 'PRE_CART_CHECKOUT');",
            'style' => 'margin-left:30px;width:164px;'
        ]
    ) ?>
</div>
<?php } ?>