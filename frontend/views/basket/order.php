<?php
/* @var $this yii\web\View */
/* @var $cart Cart */
/* @var $title string */
/* @var $description string */
/* @var $keywords string */

use common\models\Cart;
use common\models\Goods;
use frontend\components\widgets\LeftCatalogWidget;
use frontend\components\widgets\SearchBlockWidget;
use yii\helpers\Html;

$this->title = $title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $description,
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $keywords,
]);
?>


<section id="main" class="full-width">
    <header class="wrap">
        <aside>
            <?= LeftCatalogWidget::widget(['reviews' => false, 'showKompleks' => false, 'show' => '']) ?>
        </aside>
        <div id="content" class="container">
            <?= SearchBlockWidget::widget(); ?>
        </div>
    </header>

    <section class="cart-content wrap">
        <?= $this->render('_order', ['cart' => $cart]) ?>
    </section>