<?php
/* @var $this yii\web\View */
/* @var $cart Cart */
/* @var $good Goods*/

use common\models\Cart;
use common\models\Goods;
use yii\helpers\Html;

?>

<div id="cart-update" class="row collapse">
    <header class="small-12 columns">
        <h3>Товары в вашей корзине</h3>
    </header>

    <?php if(sizeof($cart->messages) > 0 && !$cart->isCouponUsed()) { ?>
        <div class="row" style="margin-bottom: 10px;">
            <div class="small-10 columns">
                <?php foreach($cart->messages as $message) { ?>
                    <div class="basket-success">
                        <?= $message ?>
                    </div>
                <?php } ?>
            </div>
            <div class="small-2 columns"></div>
        </div>
    <?php } ?>

    <div class="small-12 columns panel cart">

        <header class="cart-header">
            <div class="row collapse text-center">
                <div class="small-4 columns">
                    <div class="row">
                        <div class="small-4 columns"><p>Фото</p></div>
                        <div class="small-8 columns text-left"><p>Наименование, артикул</p></div>
                    </div>
                </div>
                <div class="small-8 columns">
                    <div class="row">
                        <div class="small-2 columns"><p>Наличие</p></div>
                        <div class="small-2 columns"><p>Цена</p></div>
                        <div class="small-2 columns"><p>Еденица измерения</p></div>
                        <div class="small-2 columns"><p>Количество</p></div>
                        <div class="small-2 columns end"><p>Цена с учетом количества</p></div>
                    </div>
                </div>
            </div>
        </header>

        <div class="cart-body">
            <?php if(sizeof($cart->items) == 0) { ?>
                <div style="text-align:center;font-size:16px;margin:10px;">
                    Ваша корзина сейчас пуста. Воспользуйтесь каталогом для добавления товаров.
                </div>
            <?php } ?>

            <?php foreach($cart->getGoods() as $good_id => $good) { ?>
                <div class="product row collapse">
                    <div class="small-4 columns">
                        <div class="row">
                            <div class="small-4 columns product-image">
                                <?=  Html::a(
                                    '<noindex>' .
                                    Html::img($good->getImageSrc(), ['class' => 'image', 'alt' => $good->ct_name, 'style' => 'width:120px;height:120px;']) .
                                    '</noindex>',
                                    ['catalog/item', 'id' => $good->ct_url],
                                    []
                                ) ?>
                            </div>
                            <div class="small-8 columns product-name">
                                <?= Html::a($good->ct_name, ['catalog/item', 'id' => $good->ct_url]) ?>
                                <span>Арт. <?= $good->ct_newart ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="small-8 columns text-center">
                        <div class="row">
                            <div class="small-2 columns product-availability">
                                <?= ($good->ct_status == Goods::AVAILABLE) ? 'В наличии' : 'На заказ' ?>
                            </div>

                            <div class="small-2 columns product-price">
                                <?php if($cart->items[$good_id]['original_price'] > $cart->items[$good_id]['price']) { ?>
                                    <span class="old-price-cart" style="margin-top: -10px;">
                                                <?= $cart->items[$good_id]['original_price'] ?>  р.
                                            </span>
                                <?php } ?>
                                <span class="current-price">
                                            <?= $cart->items[$good_id]['price'] ?> р.
                                        </span>
                            </div>


                            <div class="small-2 columns product-unit">
                                <?= $good['unit']['name'] ?>
                            </div>

                            <div class="small-2 columns product-quantity">
                                <div class="qty">
                                    <?= Html::a(
                                        '-',
                                        null,
                                        [
                                            'onclick' => "",
                                            'class' => 'control remove qtyRemove',
                                        ]
                                    ) ?>
                                    <input class="qty-input" type="number" value="<?= $cart->items[$good_id]['quantity'] ?>"
                                           min="<?= $good->restrictions()['min'] ?>"
                                           data-step="<?= $good->restrictions()['step'] ?>"
                                           data-min="<?= $good->restrictions()['min'] ?>"
                                           title="">
                                    <?= Html::a(
                                        '+',
                                        null,
                                        [
                                            'class' => 'control add qtyAdd',
                                            'onclick' => "",
                                        ]
                                    ) ?>
                                </div>
                            </div>

                            <div class="small-2 columns product-sum">
                                <?= $cart->items[$good_id]['all_price'] ?> р.
                            </div>

                            <div class="small-2 columns product-remove">
                                <?= Html::a(
                                    '',
                                    null,
                                    [
                                        'class' => 'cross gray',
                                        'onclick' => "",
                                    ]
                                ) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <footer class="cart-footer">
            <p class="confedential text-center">Мы гарантируем полную конфиденциальность всех введенных вами данных</p>

            <div class="row">
                <div class="small-6 columns text-left">
                    <?= Html::a(
                        'У меня есть дисконтная карта или промо-код',
                        null,
                        [
                            'class' => 'button button-gray coupon_button',
                        ]
                    ) ?>
                </div>
                <div class="small-6 columns"></div>
            </div>
            <div class="row">
                <div class="small-10 columns">
                    <p class="basket-summary">
                        Общей стоимостью: <span><?= $cart->total_price ?> р.</span>
                    </p>
                </div>
                <div class="small-2 columns">
                    <?= Html::a(
                        'Оформить заказ',
                        ['basket/checkout'],
                        [
                            'class' => 'button button-orange button-wide'
                        ]
                    ) ?>
                </div>
            </div>
        </footer>

    </div>
</div>
