<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "rw_reviews".
 *
 * @property integer $id
 * @property integer $good_id
 * @property string $name
 * @property string $email
 * @property string $plus
 * @property string $minus
 * @property string $text
 * @property string $date_review
 * @property integer $rate
 * @property integer $recommend
 */
class Reviews extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rw_reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['good_id', 'name', 'email', 'plus', 'minus', 'text', 'date_review', 'rate', 'recommend'], 'required'],
            [['good_id', 'rate', 'recommend'], 'integer'],
            [['text'], 'string'],
            [['date_review'], 'safe'],
            [['name', 'email', 'plus', 'minus'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'good_id' => 'Good ID',
            'name' => 'Name',
            'email' => 'Email',
            'plus' => 'Plus',
            'minus' => 'Minus',
            'text' => 'Text',
            'date_review' => 'Date Review',
            'rate' => 'Rate',
            'recommend' => 'Recommend',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ReviewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ReviewsQuery(get_called_class());
    }
}
