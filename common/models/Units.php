<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "units".
 *
 * @property integer $id
 * @property string $name
 */
class Units extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'units';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\UnitsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UnitsQuery(get_called_class());
    }
}
