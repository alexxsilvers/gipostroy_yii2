<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "l_links".
 *
 * @property integer $id
 * @property string $link
 * @property string $name
 */
class Links extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'l_links';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link', 'name'], 'required'],
            [['link', 'name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Link',
            'name' => 'Name',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\LinksQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\LinksQuery(get_called_class());
    }


    public static function getRand($num)
    {
        $links = null;
        $count = self::find()->count();
        $idsArr = array();
        for($i=1;$i<=$num;$i++) {
            $idsArr[] = rand(1, (int)$count);
        }

        return self::find()->where(['id' => $idsArr])->asArray()->all();
    }
}
