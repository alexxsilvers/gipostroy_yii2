<?php

namespace common\models;

use common\models\query\RRCacheQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "rr_cache".
 *
 * @property integer $id
 * @property string $type
 * @property integer $cat_id
 * @property integer $item_id
 * @property string $items
 * @property string $update_date
 */
class RRCache extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rr_cache';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'item_id', 'items', 'update_date'], 'required'],
            [['cat_id', 'item_id'], 'integer'],
            [['update_date'], 'safe'],
            [['type'], 'string', 'max' => 50],
            [['items'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'cat_id' => 'Cat ID',
            'item_id' => 'Item ID',
            'items' => 'Items',
            'update_date' => 'Update Date',
        ];
    }

    /**
     * @inheritdoc
     * @return RRCacheQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RRCacheQuery(get_called_class());
    }

    public static function getItemsToCategory($id)
    {
        $db = Yii::$app->db;
        $date = date("Y-m-d");
        $getIds = RRCache::find()->where(['cat_id' => $id])->asArray()->one();
        if(!isset($getIds)) {
            $str = 'http://api.retailrocket.ru/api/1.0/Recomendation/CategoryToItems/53882bd71e994418c8cb27e8/'.$id;
            $source = @file_get_contents($str);
            $db->createCommand()->insert('rr_cache', [
                'type' => 'CategoryToItems',
                'cat_id' => $id,
                'item_id' => 0,
                'items' => $source,
                'update_date' => $date,
            ])->execute();

            $returnArr = Json::decode($source);
        } else {
            if($date == $getIds['update_date']) {
                $returnArr =  Json::decode($getIds['items']);
            } else {
                $str = 'http://api.retailrocket.ru/api/1.0/Recomendation/CategoryToItems/53882bd71e994418c8cb27e8/'.$id;
                $source = @file_get_contents($str);
                $db->createCommand()->update('rr_cache', [
                    'items' => $source,
                    'update_date' => $date,
                ], 'id=:id', [':id'=>$getIds['id']]);

                $returnArr = Json::decode($source);
            }
        }

        return $returnArr;
    }

    public static function getItemsToMain($qty = null)
    {
        $db = Yii::$app->db;
        $date = date("Y-m-d");
        $getIds = RRCache::find()->where(['type' => 'ItemsToMain'])->asArray()->one();
        if(!isset($getIds)) {
            $str = 'http://api.retailrocket.ru/api/1.0/Recomendation/ItemsToMain/53882bd71e994418c8cb27e8/';
            $source = @file_get_contents($str);
            $db->createCommand()->insert('rr_cache', [
                'type' => 'ItemsToMain',
                'cat_id' => 0,
                'item_id' => 0,
                'items' => $source,
                'update_date' => $date,
            ])->execute();

            if(isset($qty)) {
                $returnArr = array_slice(Json::decode($source), 0, $qty);
            } else {
                $returnArr = Json::decode($source);
            }
        } else {
            if($date == $getIds['update_date']) {
                if(isset($qty)) {
                    $returnArr = array_slice(Json::decode($getIds['items']), 0, $qty);
                } else {
                    $returnArr = Json::decode($getIds['items']);
                }
            } else {
                $str = 'http://api.retailrocket.ru/api/1.0/Recomendation/ItemsToMain/53882bd71e994418c8cb27e8/';
                $source = @file_get_contents($str);
                $db->createCommand()->update('rr_cache', [
                    'items' => $source,
                    'update_date' => $date,
                ], 'id=:id', [':id' => $getIds['id']]);

                if(isset($qty)) {
                    $returnArr = array_slice(Json::decode($source), 0, 4);
                } else {
                    $returnArr = Json::decode($source);
                }
            }
        }

        return $returnArr;
    }

    public static function getItemToItems($id)
    {
        $db = Yii::$app->db;
        $date = date("Y-m-d");
        $getIds = RRCache::find()->addSelect(['id', 'items', 'update_date'])->where([
            'item_id' => $id,
            'type' => 'ItemToItems',
        ])->asArray()->one();
        if(!isset($getIds)) {
            $str = 'http://api.retailrocket.ru/api/1.0/Recomendation/ItemToItems/53882bd71e994418c8cb27e8/'.$id;
            $source = @file_get_contents($str);
            $db->createCommand()->insert('rr_cache', [
                'type' => 'ItemToItems',
                'cat_id' => 0,
                'item_id' => $id,
                'items' => $source,
                'update_date' => $date,
            ])->execute();
            $returnArr = Json::decode($source);
        } else {
            if($date == $getIds['update_date']) {
                $returnArr = Json::decode($getIds['items']);
            } else {
                $str = 'http://api.retailrocket.ru/api/1.0/Recomendation/ItemToItems/53882bd71e994418c8cb27e8/'.$id;
                $source = @file_get_contents($str);
                $db->createCommand()->update('rr_cache', [
                    'items' => $source,
                    'update_date' => $date,
                ], 'id=:id', [':id' => $getIds['id']]);

                $returnArr = Json::decode($source);
            }
        }

        return $returnArr;
    }

    public static function getItemToItemAvailable($id)
    {
        $db = Yii::$app->db;
        $date = date("Y-m-d");
        $getIds = RRCache::find()->addSelect(['id', 'items', 'update_date'])->where([
            'item_id' => $id,
            'type' => 'Available'
        ])->asArray()->one();
        if(!isset($getIds)) {
            $str = 'http://api.retailrocket.ru/api/1.0/Recomendation/UpSellItemToItems/53882bd71e994418c8cb27e8/'.$id;
            $source = @file_get_contents($str);
            $db->createCommand()->insert('rr_cache', [
                'type' => 'Available',
                'cat_id' => 0,
                'item_id' => $id,
                'items' => $source,
                'update_date' => $date,
            ])->execute();
            $returnArr = Json::decode($source);
        } else {
            if($date == $getIds['update_date']) {
                $returnArr = Json::decode($getIds['items']);
            } else {
                $str = 'http://api.retailrocket.ru/api/1.0/Recomendation/UpSellItemToItems/53882bd71e994418c8cb27e8/'.$id;
                $source = @file_get_contents($str);
                $db->createCommand()->update('rr_cache', [
                    'items' => $source,
                    'update_date' => $date,
                ], 'id=:id', [':id' => $getIds['id']]);

                $returnArr = Json::decode($source);
            }
        }

        return $returnArr;
    }

    public static function getAccessItems($id)
    {
        $db = Yii::$app->db;
        $date = date("Y-m-d");
        $getIds = RRCache::find()->addSelect(['id', 'items', 'update_date'])->where([
            'item_id' => $id,
            'type' => 'Access'
        ])->asArray()->one();
        if(!isset($getIds)) {
            $str = 'http://api.retailrocket.ru/api/1.0/Recomendation/CrossSellItemToItems/53882bd71e994418c8cb27e8/'.$id;
            $source = @file_get_contents($str);
            $db->createCommand()->insert('rr_cache', [
                'type' => 'Access',
                'cat_id' => 0,
                'item_id' => $id,
                'items' => $source,
                'update_date' => $date,
            ])->execute();
            $returnArr = Json::decode($source);
        } else {
            if($date == $getIds['update_date']) {
                $returnArr = Json::decode($getIds['items']);
            } else {
                $str = 'http://api.retailrocket.ru/api/1.0/Recomendation/CrossSellItemToItems/53882bd71e994418c8cb27e8/'.$id;
                $source = @file_get_contents($str);
                $db->createCommand()->update('rr_cache', [
                    'items' => $source,
                    'update_date' => $date,
                ], 'id=:id', [':id' => $getIds['id']]);

                $returnArr = Json::decode($source);
            }
        }

        return $returnArr;
    }
}
