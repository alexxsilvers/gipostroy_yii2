<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cabt_cable_type".
 *
 * @property integer $cabt_id
 * @property string $cabt_name
 * @property integer $cabt_sorti
 * @property string $cabt_chdate
 * @property integer $cabt_show
 */
class CableType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cabt_cable_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cabt_sorti', 'cabt_show'], 'integer'],
            [['cabt_chdate'], 'safe'],
            [['cabt_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cabt_id' => 'Cabt ID',
            'cabt_name' => 'Cabt Name',
            'cabt_sorti' => 'Cabt Sorti',
            'cabt_chdate' => 'Cabt Chdate',
            'cabt_show' => 'Cabt Show',
        ];
    }

    /**
     * @inheritdoc
     * @return CableTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CableTypeQuery(get_called_class());
    }
}
