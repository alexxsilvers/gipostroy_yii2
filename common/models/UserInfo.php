<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_info".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $fname
 * @property string $name
 * @property string $lname
 * @property string $region
 * @property string $phone
 * @property integer $email_send
 * @property integer $sms_send
 */
class UserInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fname', 'name', 'lname', 'region', 'phone'], 'required'],
            [['user_id', 'email_send', 'sms_send'], 'integer'],
            [['fname', 'lname'], 'string', 'max' => 100],
            [['name', 'region'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'fname' => 'Фамилия',
            'name' => 'Имя',
            'lname' => 'Отчество',
            'region' => 'Регион',
            'phone' => 'Телефон',
            'email_send' => 'Email',
            'sms_send' => 'SMS',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\UserInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\UserInfoQuery(get_called_class());
    }

}
