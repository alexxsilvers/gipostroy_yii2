<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "se_seotext".
 *
 * @property integer $se_id
 * @property integer $serz_id
 * @property integer $seprz_id
 * @property integer $seppprz_id
 * @property integer $semn_id
 * @property string $seotext
 * @property string $se_title
 * @property string $se_description
 * @property string $se_keywords
 */
class ManufacterSeo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'se_seotext';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serz_id', 'seprz_id', 'seppprz_id', 'semn_id', 'seotext', 'se_title', 'se_description', 'se_keywords'], 'required'],
            [['serz_id', 'seprz_id', 'seppprz_id', 'semn_id'], 'integer'],
            [['seotext'], 'string'],
            [['se_title', 'se_description', 'se_keywords'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'se_id' => 'Se ID',
            'serz_id' => 'Serz ID',
            'seprz_id' => 'Seprz ID',
            'seppprz_id' => 'Seppprz ID',
            'semn_id' => 'Semn ID',
            'seotext' => 'Seotext',
            'se_title' => 'Se Title',
            'se_description' => 'Se Description',
            'se_keywords' => 'Se Keywords',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ManufacterSeoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ManufacterSeoQuery(get_called_class());
    }
}
