<?php

namespace common\models;

use common\models\query\GoodsCollectionsQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * This is the model class for table "goods_collections".
 *
 * @property integer $id
 * @property integer $manufacter_id
 * @property string $collection_name
 * @property string $goods
 */
class GoodsCollections extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_collections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacter_id', 'collection_name', 'goods'], 'required'],
            [['manufacter_id'], 'integer'],
            [['goods'], 'string'],
            [['collection_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manufacter_id' => 'Manufacter ID',
            'collection_name' => 'Collection Name',
            'goods' => 'Goods',
        ];
    }

    /**
     * @inheritdoc
     * @return GoodsCollectionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GoodsCollectionsQuery(get_called_class());
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->goods = Json::decode($this->goods);
    }

    public function beforeSave($insert)
    {
        if(!parent::beforeSave($insert)) {
            return false;
        }

        $this->goods = Json::encode($this->goods);
        return true;
    }

}
