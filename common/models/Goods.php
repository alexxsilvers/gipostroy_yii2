<?php

namespace common\models;

use common\models\query\GoodsQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * This is the model class for table "ct_catalog".
 *
 * @property integer $ct_id
 * @property integer $ct_deleted
 * @property string $ct_name
 * @property integer $ct_count_index
 * @property string $ct_cid
 * @property string $ct_url
 * @property string $ct_art
 * @property integer $ct_newart
 * @property string $ct_vendor_code
 * @property string $ct_manufacter_code
 * @property string $ct_garant
 * @property string $ct_text
 * @property integer $ct_mn_id
 * @property integer $ct_price
 * @property integer $ct_newest
 * @property integer $ct_sale
 * @property integer $ct_sale_price
 * @property integer $ct_hit
 * @property integer $ct_laminatclass
 * @property string $ct_laminatolshina
 * @property integer $ct_laminat_imitation
 * @property integer $ct_cable_type
 * @property integer $ct_cable_conductor
 * @property string $ct_cable_cut
 * @property string $ct_lf_id
 * @property integer $ct_sorti
 * @property string $ct_chdate
 * @property integer $ct_show
 * @property integer $ct_noindex
 * @property string $ct_pic
 * @property string $ct_title
 * @property string $ct_keywords
 * @property string $ct_description
 * @property string $ct_upload_time
 * @property string $ct_create_time
 * @property integer $ct_create_admin
 * @property integer $ct_admin_autor
 * @property string $ct_balance
 * @property string $ct_balance_storage
 * @property string $ct_covrolin_width
 * @property integer $ct_covrolin_rezka
 * @property integer $ct_covrolin_shade
 * @property integer $ct_linoleum_type
 * @property string $ct_linoleum_width
 * @property integer $ct_linoleum_rezka
 * @property integer $ct_status
 * @property string $bau_id
 * @property string $ct_tema
 * @property string $ct_pl_collection
 * @property string $ct_min_part
 * @property integer $ct_merc_tarif
 * @property integer $ct_merc_faz
 * @property integer $edm
 * @property string $ct_fl_collection
 * @property integer $ct_fl_collection_id
 * @property double $ct_fl_area
 * @property integer $ct_fl_amount
 * @property double $ct_fl_weight
 * @property double $ct_fl_weight_sht
 * @property double $ct_fl_dlina
 * @property double $ct_fl_price
 * @property integer $ct_fl_min
 * @property integer $ct_fl_min_all
 * @property string $ct_izmer3
 * @property string $ct_fl_razmery
 * @property integer $ct_aut_polus
 * @property double $ct_aut_tok
 * @property double $ct_aut_otkl
 * @property string $ct_aut_type
 * @property double $ct_aut_rasc_toc
 * @property double $ct_aut_rasc
 * @property string $ct_pil_sort
 * @property string $ct_pil_poroda
 * @property double $ct_pil_dlina
 * @property string $ct_pil_razm
 * @property double $ct_pil_weight
 * @property string $ct_pil_dost
 * @property string $ct_lamp_type
 * @property string $ct_lamp_cokol
 * @property string $ct_lamp_colvo
 * @property string $ct_lamp_type_s
 * @property string $ct_lamp_power
 * @property integer $ct_lamp_def
 * @property string $ct_lamp_time
 * @property string $ct_vin_class
 * @property string $ct_vin_zamok
 * @property string $ct_vin_area
 * @property string $ct_vin_colvo
 * @property string $ct_vin_razm
 * @property string $ct_vin_ves
 * @property string $ct_izmer1
 * @property string $ct_izmer2
 * @property string $ct_video
 * @property string $ct_ker_color
 * @property string $ct_ker_type
 * @property string $ct_ker_vid
 * @property string $ct_ker_naz
 * @property string $ct_ker_pov
 * @property string $ct_warning
 * @property integer $ct_complect_id
 * @property double $ct_r_height
 * @property double $ct_r_deep
 * @property double $ct_r_width
 * @property double $ct_p_width
 * @property double $ct_p_height
 * @property double $ct_p_deep
 * @property double $ct_fl_p_weight
 * @property string $char_field1
 * @property string $char_field2
 */
class Goods extends ActiveRecord
{
    const STATUS_SHOW = 1;
    const STATUS_NOT_SHOW = 0;
    const STATUS_DELETED = 1;
    const STATUS_NOT_DELETED = 0;
    const NOT_AVAILABLE = 2;
    const AVAILABLE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ct_catalog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ct_deleted', 'ct_count_index', 'ct_newart', 'ct_mn_id', 'ct_price', 'ct_newest', 'ct_sale', 'ct_sale_price', 'ct_hit', 'ct_laminatclass', 'ct_laminat_imitation', 'ct_cable_type', 'ct_cable_conductor', 'ct_sorti', 'ct_show', 'ct_noindex', 'ct_create_admin', 'ct_admin_autor', 'ct_covrolin_rezka', 'ct_covrolin_shade', 'ct_linoleum_type', 'ct_linoleum_rezka', 'ct_status', 'ct_merc_tarif', 'ct_merc_faz', 'edm', 'ct_fl_collection_id', 'ct_fl_amount', 'ct_fl_min', 'ct_fl_min_all', 'ct_aut_polus', 'ct_lamp_def', 'ct_complect_id'], 'integer'],
            [['ct_count_index', 'ct_cid', 'ct_art', 'ct_sale_price', 'ct_laminatclass', 'ct_laminatolshina', 'ct_laminat_imitation', 'ct_cable_type', 'ct_cable_conductor', 'ct_cable_cut', 'ct_lf_id', 'ct_upload_time', 'ct_create_time', 'ct_create_admin', 'ct_admin_autor', 'ct_covrolin_width', 'ct_covrolin_rezka', 'ct_covrolin_shade', 'ct_linoleum_type', 'ct_linoleum_width', 'ct_linoleum_rezka', 'bau_id', 'ct_tema', 'ct_pl_collection', 'ct_min_part', 'ct_merc_tarif', 'ct_merc_faz', 'ct_fl_collection', 'ct_fl_collection_id', 'ct_fl_area', 'ct_fl_amount', 'ct_fl_weight', 'ct_fl_weight_sht', 'ct_fl_dlina', 'ct_fl_price', 'ct_fl_min', 'ct_fl_min_all', 'ct_izmer3', 'ct_fl_razmery', 'ct_aut_polus', 'ct_aut_tok', 'ct_aut_otkl', 'ct_aut_type', 'ct_aut_rasc_toc', 'ct_aut_rasc', 'ct_pil_sort', 'ct_pil_poroda', 'ct_pil_dlina', 'ct_pil_razm', 'ct_pil_weight', 'ct_pil_dost', 'ct_lamp_type', 'ct_lamp_cokol', 'ct_lamp_colvo', 'ct_lamp_type_s', 'ct_lamp_power', 'ct_lamp_def', 'ct_lamp_time', 'ct_vin_class', 'ct_vin_zamok', 'ct_vin_area', 'ct_vin_colvo', 'ct_vin_razm', 'ct_vin_ves', 'ct_izmer1', 'ct_izmer2', 'ct_video', 'ct_ker_color', 'ct_ker_type', 'ct_ker_vid', 'ct_ker_naz', 'ct_ker_pov', 'ct_warning', 'ct_complect_id', 'ct_r_height', 'ct_r_deep', 'ct_r_width', 'ct_p_width', 'ct_p_height', 'ct_p_deep', 'ct_fl_p_weight', 'char_field1', 'char_field2'], 'required'],
            [['ct_text'], 'string'],
            [['ct_laminatolshina', 'ct_covrolin_width', 'ct_linoleum_width', 'ct_fl_area', 'ct_fl_weight', 'ct_fl_weight_sht', 'ct_fl_dlina', 'ct_fl_price', 'ct_aut_tok', 'ct_aut_otkl', 'ct_aut_rasc_toc', 'ct_aut_rasc', 'ct_pil_dlina', 'ct_pil_weight', 'ct_r_height', 'ct_r_deep', 'ct_r_width', 'ct_p_width', 'ct_p_height', 'ct_p_deep', 'ct_fl_p_weight'], 'number'],
            [['ct_chdate', 'ct_upload_time', 'ct_create_time'], 'safe'],
            [['ct_name', 'ct_url', 'ct_vendor_code', 'ct_manufacter_code', 'ct_pic', 'ct_title', 'ct_keywords', 'ct_description', 'bau_id', 'ct_pil_razm', 'ct_lamp_type', 'ct_lamp_cokol', 'ct_lamp_colvo', 'ct_lamp_type_s', 'ct_lamp_power', 'ct_vin_class', 'ct_vin_zamok', 'ct_vin_area', 'ct_vin_colvo', 'ct_vin_razm', 'ct_vin_ves', 'ct_ker_color', 'ct_ker_type', 'ct_ker_vid', 'ct_ker_naz', 'ct_ker_pov', 'ct_warning', 'char_field1', 'char_field2'], 'string', 'max' => 255],
            [['ct_cid'], 'string', 'max' => 60],
            [['ct_art', 'ct_garant', 'ct_lf_id'], 'string', 'max' => 40],
            [['ct_cable_cut'], 'string', 'max' => 32],
            [['ct_balance', 'ct_balance_storage', 'ct_tema', 'ct_pl_collection'], 'string', 'max' => 50],
            [['ct_min_part'], 'string', 'max' => 100],
            [['ct_fl_collection'], 'string', 'max' => 120],
            [['ct_izmer3'], 'string', 'max' => 10],
            [['ct_fl_razmery', 'ct_izmer1', 'ct_izmer2'], 'string', 'max' => 20],
            [['ct_aut_type'], 'string', 'max' => 5],
            [['ct_pil_sort', 'ct_pil_poroda', 'ct_pil_dost', 'ct_video'], 'string', 'max' => 1024],
            [['ct_lamp_time'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ct_id' => 'Ct ID',
            'ct_deleted' => 'Ct Deleted',
            'ct_name' => 'Ct Name',
            'ct_count_index' => 'Ct Count Index',
            'ct_cid' => 'Ct Cid',
            'ct_url' => 'Ct Url',
            'ct_art' => 'Ct Art',
            'ct_newart' => 'Ct Newart',
            'ct_vendor_code' => 'Ct Vendor Code',
            'ct_manufacter_code' => 'Ct Manufacter Code',
            'ct_garant' => 'Ct Garant',
            'ct_text' => 'Ct Text',
            'ct_mn_id' => 'Ct Mn ID',
            'ct_price' => 'Ct Price',
            'ct_newest' => 'Ct Newest',
            'ct_sale' => 'Ct Sale',
            'ct_sale_price' => 'Ct Sale Price',
            'ct_hit' => 'Ct Hit',
            'ct_laminatclass' => 'Ct Laminatclass',
            'ct_laminatolshina' => 'Ct Laminatolshina',
            'ct_laminat_imitation' => 'Ct Laminat Imitation',
            'ct_cable_type' => 'Ct Cable Type',
            'ct_cable_conductor' => 'Ct Cable Conductor',
            'ct_cable_cut' => 'Ct Cable Cut',
            'ct_lf_id' => 'Ct Lf ID',
            'ct_sorti' => 'Ct Sorti',
            'ct_chdate' => 'Ct Chdate',
            'ct_show' => 'Ct Show',
            'ct_noindex' => 'Ct Noindex',
            'ct_pic' => 'Ct Pic',
            'ct_title' => 'Ct Title',
            'ct_keywords' => 'Ct Keywords',
            'ct_description' => 'Ct Description',
            'ct_upload_time' => 'Ct Upload Time',
            'ct_create_time' => 'Ct Create Time',
            'ct_create_admin' => 'Ct Create Admin',
            'ct_admin_autor' => 'Ct Admin Autor',
            'ct_balance' => 'Ct Balance',
            'ct_balance_storage' => 'Ct Balance Storage',
            'ct_covrolin_width' => 'Ct Covrolin Width',
            'ct_covrolin_rezka' => 'Ct Covrolin Rezka',
            'ct_covrolin_shade' => 'Ct Covrolin Shade',
            'ct_linoleum_type' => 'Ct Linoleum Type',
            'ct_linoleum_width' => 'Ct Linoleum Width',
            'ct_linoleum_rezka' => 'Ct Linoleum Rezka',
            'ct_status' => 'Ct Status',
            'bau_id' => 'Bau ID',
            'ct_tema' => 'Ct Tema',
            'ct_pl_collection' => 'Ct Pl Collection',
            'ct_min_part' => 'Ct Min Part',
            'ct_merc_tarif' => 'Ct Merc Tarif',
            'ct_merc_faz' => 'Ct Merc Faz',
            'edm' => 'Edm',
            'ct_fl_collection' => 'Ct Fl Collection',
            'ct_fl_collection_id' => 'Ct Fl Collection ID',
            'ct_fl_area' => 'Ct Fl Area',
            'ct_fl_amount' => 'Ct Fl Amount',
            'ct_fl_weight' => 'Ct Fl Weight',
            'ct_fl_weight_sht' => 'Ct Fl Weight Sht',
            'ct_fl_dlina' => 'Ct Fl Dlina',
            'ct_fl_price' => 'Ct Fl Price',
            'ct_fl_min' => 'Ct Fl Min',
            'ct_fl_min_all' => 'Ct Fl Min All',
            'ct_izmer3' => 'Ct Izmer3',
            'ct_fl_razmery' => 'Ct Fl Razmery',
            'ct_aut_polus' => 'Ct Aut Polus',
            'ct_aut_tok' => 'Ct Aut Tok',
            'ct_aut_otkl' => 'Ct Aut Otkl',
            'ct_aut_type' => 'Ct Aut Type',
            'ct_aut_rasc_toc' => 'Ct Aut Rasc Toc',
            'ct_aut_rasc' => 'Ct Aut Rasc',
            'ct_pil_sort' => 'Ct Pil Sort',
            'ct_pil_poroda' => 'Ct Pil Poroda',
            'ct_pil_dlina' => 'Ct Pil Dlina',
            'ct_pil_razm' => 'Ct Pil Razm',
            'ct_pil_weight' => 'Ct Pil Weight',
            'ct_pil_dost' => 'Ct Pil Dost',
            'ct_lamp_type' => 'Ct Lamp Type',
            'ct_lamp_cokol' => 'Ct Lamp Cokol',
            'ct_lamp_colvo' => 'Ct Lamp Colvo',
            'ct_lamp_type_s' => 'Ct Lamp Type S',
            'ct_lamp_power' => 'Ct Lamp Power',
            'ct_lamp_def' => 'Ct Lamp Def',
            'ct_lamp_time' => 'Ct Lamp Time',
            'ct_vin_class' => 'Ct Vin Class',
            'ct_vin_zamok' => 'Ct Vin Zamok',
            'ct_vin_area' => 'Ct Vin Area',
            'ct_vin_colvo' => 'Ct Vin Colvo',
            'ct_vin_razm' => 'Ct Vin Razm',
            'ct_vin_ves' => 'Ct Vin Ves',
            'ct_izmer1' => 'Ct Izmer1',
            'ct_izmer2' => 'Ct Izmer2',
            'ct_video' => 'Ct Video',
            'ct_ker_color' => 'Ct Ker Color',
            'ct_ker_type' => 'Ct Ker Type',
            'ct_ker_vid' => 'Ct Ker Vid',
            'ct_ker_naz' => 'Ct Ker Naz',
            'ct_ker_pov' => 'Ct Ker Pov',
            'ct_warning' => 'Ct Warning',
            'ct_complect_id' => 'Ct Complect ID',
            'ct_r_height' => 'Ct R Height',
            'ct_r_deep' => 'Ct R Deep',
            'ct_r_width' => 'Ct R Width',
            'ct_p_width' => 'Ct P Width',
            'ct_p_height' => 'Ct P Height',
            'ct_p_deep' => 'Ct P Deep',
            'ct_fl_p_weight' => 'Ct Fl P Weight',
            'char_field1' => 'Char Field1',
            'char_field2' => 'Char Field2',
        ];
    }

    /**
     * @inheritdoc
     * @return GoodsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GoodsQuery(get_called_class());
    }

    /**
     * Check whether is new good
     * If good`s create time is a last 2 weeks, is a new
     * @return bool
     */
    public function isNewGood()
    {
        if(strtotime($this->ct_create_time) > strtotime(date("Y-m-d", strtotime("-2 week")))) {
            return true;
        }
        return false;
    }

    /**
     * Get src string for good`s image
     * @return string
     */
    public function getImageSrc()
    {
        if(in_array($_SERVER['SERVER_NAME'],array('develop.gipostroy.ru', 'gipo.loc', 'gipo2yii.loc', 'yiishop.loc'))) {
            $src = 'http://www.gipostroy.ru/pdir/catalog/' . $this->ct_pic;
        } elseif(!empty($this->ct_pic) && file_exists($_SERVER['DOCUMENT_ROOT'].'/pdir/catalog/' . $this->ct_pic)) {
            $src = '/pdir/catalog/' . $this->ct_pic;
        } else {
            $src = '/images/nophoto.jpg';
        }

        return $src;
    }

    /**
     * Get balance string for good`s card
     * @param $unit
     * @return string
     */
    public function getGoodBalance($unit)
    {
        if($this->ct_status == self::NOT_AVAILABLE) { //Если статус на наказ
            if(empty($this->ct_balance_storage)) { //Смотрим, есть ли остаток по 1С. Если нет
                return 'Статус товара: на заказ';
            } else { //Если в 1С что то есть, то выводим остаток
                return 'Остаток на складе: ' . $this->ct_balance_storage . ' ' . $unit;
            }
        } else { //Если статус в наличии
            if(!empty($this->ct_balance) || !empty($this->ct_balance_storage)) {
                $summ = $this->ct_balance + $this->ct_balance_storage;
                return 'Остаток на складе: ' . $summ . ' ' . $unit;
            } else {
                return 'Статус товара: в наличии';
            }
        }
    }

    /**
     * Get balance string for goods in catalog
     * @param $unit
     * @return string
     */
    public function getGoodBalanceBlock($unit)
    {
        if($this->ct_status == self::NOT_AVAILABLE) { //Если статус на наказ
            if(empty($this->ct_balance_storage)) { //Смотрим, есть ли остаток по 1С. Если нет
                return '<p class="available">Статус товара: <span>на заказ</span></p>';
            } else { //Если в 1С что то есть, то выводим остаток
                return '<p class="available">Остаток на складе: <span>' . $this->ct_balance_storage . ' ' . $unit . '</span></p>';
            }
        } else { //Если статус в наличии
            if(!empty($this->ct_balance) || !empty($this->ct_balance_storage)) {
                $summ = $this->ct_balance + $this->ct_balance_storage;
                return '<p class="available">Остаток на складе: <span>' . $summ . ' ' . $unit . '</span></p>';
            } else {
                return '<p class="available">Статус товара: <span>в наличии</span></p>';
            }
        }
    }

    /**
     * Get price string for goods in catalog
     * @return string
     */
    public function getGoodPriceBlock()
    {
        $str = '';
        if($this->ct_sale == 1 && !empty($this->ct_sale_price)) {
            $str .= '<span class="old-price" style="margin-left:30px;color:red;text-decoration:line-through;text-shadow:none;">';
            $str .= '    <span style="color:#898989;">' . $this->ct_sale_price . ' р.</span>';
            $str .= '</span>';
            $str .= '<span class="current-price">' . $this->ct_price . ' р.</span>';
        } else {
            $str .= '<span class="current-price" style="margin-top:-1px;padding-top:5px;">';
            $str .= $this->ct_price;
            $str .= ' р.</span>';
        }

        return $str;
    }

    /**
     * Get price string for good`s card
     * @param bool $withCurrency
     * @return string
     */
    public function getGoodPriceInteger($withCurrency = true)
    {
        $price = $this->ct_price;
        if($this->ct_sale == 1 && !empty($this->ct_sale_price)) {
            $price = $this->ct_sale_price;
        }

        $ret = $price;
        if($withCurrency) {
            $ret .= ' р.';
        }
        return $ret;
    }

    /**
     * Get array of goods charakters
     * @param null $manufacter
     * @param null $section
     * @param null $subsection
     * @return array
     */
    public function getCharakters($manufacter = null, $section = null, $subsection = null)
    {
        $section = is_null($section) ? $this->section : $section;
        $subsection = is_null($subsection) ? $this->subsection : $subsection;
        $manufacter = is_null($manufacter) ? $this->manufacter : $manufacter;
        $charaktersList = [];
        $charaktersArr = [];

        if(empty($subsection['pprz_charakters'])) {
            if(!empty($section['prz_charakters'])) {
                $charaktersList = unserialize(base64_decode($section['prz_charakters']));
            }
        } else {
            $charaktersList = unserialize(base64_decode($subsection['pprz_charakters']));
        }

        if(!empty($manufacter['mn_name'])) {
            $charaktersArr['Производитель'] = $manufacter['mn_name'];
        }

        if(!empty($manufacter['mn_country'])) {
            $charaktersArr['Страна-производитель'] = $manufacter['mn_country'];
        }

        if(!empty($this->ct_garant)) {
            $charaktersArr['Гарантия производителя'] = $this->ct_garant;
        }

        if(!empty($this->ct_manufacter_code)) {
            $charaktersArr['Код производителя'] = $this->ct_manufacter_code;
        }

        //some charakters
        if(sizeof($charaktersList) > 0) {
            foreach($charaktersList as $charField => $charInfo) {
                if(!empty($this->$charField) && $charInfo['show'] == 1) {
                    $charaktersArr[$charInfo['name']] = $this->$charField;
                    if(!empty($charInfo['edm'])) {
                        $charaktersArr[$charInfo['name']] .= ' ' . $charInfo['edm'];
                    }
                }
            }
        }

        if(!empty($this->ct_r_width) && !empty($this->ct_r_height) && !empty($this->ct_r_deep)) {
            $charaktersArr['Размеры'] = $this->ct_r_width . 'x' . $this->ct_r_height . 'x' . $this->ct_r_deep . ' мм.';
        }

        if(!empty($this->ct_fl_weight)) {
            $charaktersArr['Вес'] = $this->ct_fl_weight . ' кг.';
        }

        if(!empty($this->ct_fl_price)) {
            $charaktersArr['Цена указана за'] = $this->ct_fl_price . ' ' . $this->ct_izmer1;
        }

        return $charaktersArr;
    }

    /**
     * Get goods fot tabs in the good card
     * @return array
     * @throws \Exception
     */
    public function getGoodsForTabs()
    {
        $allIds = [];
        $tabs = [
            'first_block' => [
                'activeTab' => 'shop-recommended',
                'tabs' => [

                ],
            ],
            'second_block' => [
                'activeTab' => 'shop-watched',
                'tabs' => [

                ],
            ],
        ];

        $collectionIds = $this->getCollectionGoodsIds(); // Товары из одной коллекции (только id)
        //$allIds = $this->populateIdsToArray($collectionIds, $allIds);

        //Просмотренные пользователем товары (только id)
        $viewedIds = $this->getViewedGoodsIds();
        $allIds = $this->populateIdsToArray($viewedIds, $allIds);

        //Другие товары этой категории (сразу товары)
        $additionalGoods = $this->getAdditionalGoods();

        //Вам так же может быть интересно
        $recommendedIds = $this->getRecommendedIds();
        $allIds = $this->populateIdsToArray($recommendedIds, $allIds);

        //Похожие товары в наличии
        $availableIds = $this->getAvailableIds();
        $allIds = $this->populateIdsToArray($availableIds, $allIds);

        //С этим товаром покупают
        $accessIds = $this->getAccessIds();
        $allIds = $this->populateIdsToArray($accessIds, $allIds);

        $allGoods = self::getDb()->cache(function($db) use ($allIds) {
            return self::find()
                ->addSelect(Yii::$app->params['good_fields'])
                ->andWhere(['and', ['in', 'ct_id', $allIds]])
                ->show()
                ->noDeleted()
                ->indexBy('ct_id')
                ->all();
        }, 600);

        if(!empty($collectionIds)) {
            foreach($collectionIds as $key => $collectionId) {
                if(isset($allGoods[$collectionId])) {
                    $collectionIds[$key] = $allGoods[$collectionId];
                } else {
                    unset($collectionIds[$key]);
                }
            }
            $tabs['second_block']['tabs']['collectionTab']['name'] = 'Еще товары в данной коллекции';
            $tabs['second_block']['tabs']['collectionTab']['id'] = 'shop-collection';
            $tabs['second_block']['tabs']['collectionTab']['goods'] = $collectionIds;
        }

        if(!empty($viewedIds)) {
            foreach($viewedIds as $key => $viewedId) {
                if(isset($allGoods[$viewedId])) {
                    $viewedIds[$key] = $allGoods[$viewedId];
                } else {
                    unset($viewedIds[$key]);
                }
            }
            $tabs['second_block']['tabs']['viewedTab']['name'] = 'Вы смотрели';
            $tabs['second_block']['tabs']['viewedTab']['id'] = 'shop-watched';
            $tabs['second_block']['tabs']['viewedTab']['goods'] = $viewedIds;
        }

        if(!empty($additionalGoods)) {
            $tabs['second_block']['tabs']['additionalTab']['name'] = 'Другие товары этой категории';
            $tabs['second_block']['tabs']['additionalTab']['id'] = 'shop-additional';
            $tabs['second_block']['tabs']['additionalTab']['goods'] = $additionalGoods;
        }

        if(!empty($recommendedIds)) {
            foreach($recommendedIds as $key => $recommendedId) {
                if(isset($allGoods[$recommendedId])) {
                    $recommendedIds[$key] = $allGoods[$recommendedId];
                } else {
                    unset($recommendedIds[$key]);
                }
            }
            $tabs['first_block']['tabs']['recommendedTab']['name'] = 'Вам также может быть интересно';
            $tabs['first_block']['tabs']['recommendedTab']['id'] = 'shop-recommended';
            $tabs['first_block']['tabs']['recommendedTab']['goods'] = $recommendedIds;
        }

        if($this->ct_status == self::NOT_AVAILABLE && !empty($availableIds)) {
            foreach($availableIds as $key => $availableId) {
                if(isset($allGoods[$availableId])) {
                    $availableIds[$key] = $allGoods[$availableId];
                } else {
                    unset($availableIds[$key]);
                }
            }
            $tabs['first_block']['tabs']['availableTab']['name'] = 'Похожие товары в наличии';
            $tabs['first_block']['tabs']['availableTab']['id'] = 'shop-available';
            $tabs['first_block']['tabs']['availableTab']['goods'] = $availableIds;
        }

        if(!empty($accessIds)) {
            foreach($accessIds as $key => $accessId) {
                if(isset($allGoods[$accessId])) {
                    $accessIds[$key] = $allGoods[$accessId];
                } else {
                    unset($accessIds[$key]);
                }
            }
            $tabs['first_block']['tabs']['accessTab']['name'] = 'С этим товаром покупают';
            $tabs['first_block']['tabs']['accessTab']['id'] = 'shop-access';
            $tabs['first_block']['tabs']['accessTab']['goods'] = $accessIds;
        }

        return $tabs;
    }

    protected function populateIdsToArray($ids, $populateArray)
    {
        if(!empty($ids)) {
            foreach($ids as $id) {
                if(!in_array($id, $populateArray)) {
                    $populateArray[] = $id;
                }
            }
        }

        return $populateArray;
    }

    protected function getAccessIds()
    {
        return array_slice(RRCache::getAccessItems($this->ct_id), 0, 15);
    }

    protected function getAvailableIds()
    {
        return array_slice(RRCache::getItemToItemAvailable($this->ct_id), 0, 15);
    }

    protected function getRecommendedIds()
    {
        return array_slice(RRCache::getItemToItems($this->ct_id), 0, 15);
    }

    protected function getAdditionalGoods()
    {
        $sectionId = $this->section['prz_id'];
        $ids = Goods::getDb()->cache(function($db) use ($sectionId) {
            return Goods::find()
                ->addSelect(Yii::$app->params['good_fields'])
                ->joinWith([
                    'sectionGoods' => function ($query) use ($sectionId) {
                        $query->andWhere([
                            'cat_id' => $sectionId
                        ]);
                    }
                ])
                ->show()
                ->noDeleted()
                ->indexBy('ct_id')
                ->limit(40)
                ->all();
        }, 600);

        shuffle($ids);
        return array_slice($ids, -20);
    }

    protected function getViewedGoodsIds()
    {
        $ids = [];
        $goodId = $this->ct_id;
        if(Yii::$app->user->isGuest) {
            if(Yii::$app->session->has('viewed_goods')) {
                $ids = Yii::$app->session->get('viewed_goods');
                unset($ids[array_search($goodId, $ids)]);
            }
        } else {
            $ids = UserViewedGoods::getDb()->cache(function($db){
                return UserViewedGoods::find()->addSelect('good_id')->where([
                    'user_id' => Yii::$app->user->id,
                ])->asArray()->indexBy('good_id')->column();
            }, 600);
        }

        return $ids;
    }

    protected function getCollectionGoodsIds()
    {
        $manufacterId = $this->ct_mn_id;
        $collection = $this->ct_fl_collection;
        $goodId = $this->ct_id;

        $ids = GoodsCollections::getDb()->cache(function($db) use ($manufacterId, $collection){
            return GoodsCollections::find()->where([
                'manufacter_id' => $manufacterId,
                'collection_name' => $collection
            ])->asArray()->one();
        });

        $ids = Json::decode($ids['goods']);
        if(!empty($ids)) {
            unset($ids[array_search($goodId, $ids)]);
        }

        return $ids;
    }

    public function getGoodShipping()
    {
        if($this->ct_status == 1) {
            return 'Возможная дата доставки - <strong>' . self::isHappyDay(date("Y-m-d H:i"), 1) . '</strong>';
        } elseif($this->ct_status == 2) {
            return 'Возможная дата доставки - <strong>по согласованию</strong>';
        } else {
            return 'Возможная дата доставки - <strong>по согласованию</strong>';
        }
    }

    public function calculateDelivery($section = null, $subsection = null)
    {
        $width = $this->ct_r_width;
        $deep = $this->ct_r_deep;
        $height = $this->ct_r_height;
        $weight = $this->ct_fl_weight;

        $section = is_null($section) ? $this->section['prz_id'] : $section;
        $subsection = is_null($subsection) ? $this->subsection['pprz_id'] : $subsection;

        if(in_array($section, [46,45,47,15,143,144,154,168])) {
            return 'Доставка по Москве от 1000 р.';
        } elseif(in_array($section, [194,213,259])) {
            return 'Бесплатная доставка по Москве';
        } elseif(in_array($section, [225,224,253,220,261])) {
            return 'Доставка по Москве от 500 р.';
        } elseif($section == 177 && $this->ct_mn_id != 631 && $this->ct_price >= 10000) {
            return 'Бесплатная доставка по Москве';
        } elseif($section == 212 && $this->ct_mn_id != 614 && $this->ct_price >= 4900) {
            return 'Бесплатная доставка по Москве';
        } elseif(in_array($subsection, [380,385])) {
            return 'Бесплатная доставка по Москве';
        } elseif($subsection == 316) {
            return 'Доставка по Москве от 500 р.';
        } else {
            if(!empty($width) && !empty($height) && !empty($deep) && !empty($weight)) {
                $volume = round(($width/1000)*($deep/1000)*($height/1000), 2);
                $valuesFromDb = ShipmentValues::find()->asArray()->all();
                $volumePrice = 0;
                $weightPrice = 0;
                $dimensionPrice = 0;

                $tmpArrForDimensions = array($width/1000, $height/1000, $deep/1000);
                $maxDimension = max($tmpArrForDimensions);

                $valSize = sizeof($valuesFromDb);
                for($i = 0; $i < $valSize; $i++) {
                    if($volume>$valuesFromDb[$i]['volume'] && $volume<=$valuesFromDb[$i+1]['volume']) {
                        $volumePrice = $valuesFromDb[$i+1]['price'];
                    }

                    if($weight>$valuesFromDb[$i]['weight'] && $weight<=$valuesFromDb[$i+1]['weight']) {
                        $weightPrice = $valuesFromDb[$i+1]['price'];
                    }

                    if($maxDimension>$valuesFromDb[$i]['dimensions'] && $maxDimension<=$valuesFromDb[$i+1]['dimensions']) {
                        $dimensionPrice = $valuesFromDb[$i+1]['price'];
                    }
                }

                if($volumePrice==0) {
                    $volumePrice = $valuesFromDb[0]['price'];
                }
                if($weightPrice==0) {
                    $weightPrice = $valuesFromDb[0]['price'];
                }
                if($dimensionPrice==0 && $maxDimension > 6) {
                    $weightPrice = $valuesFromDb[7]['price'];
                }

                $deliveryPrice = ($volumePrice>=$weightPrice) ? $volumePrice : $weightPrice;
                if($deliveryPrice < $dimensionPrice) {
                    $deliveryPrice = $dimensionPrice;
                }

                return 'Доставка по москве '.$deliveryPrice.' р.';
            } else {
                return 'Доставка по Москве от 300 р.';
            }
        }
    }

    public static function getPageSize()
    {
        $pageSize = Yii::$app->request->get('psize');
        if(isset($pageSize) && is_numeric($pageSize)) {
            $pSize = $pageSize;
        } elseif(isset($pageSize) && $pageSize === 'all') {
            $pSize = 1000;
        } else {
            $pSize = 24;
        }

        return $pSize;
    }

    public static function isHappyDay($date, $nextDay, $catIds = array())
    {
        $san = array(224,225,253,261,220); //Для этих 2 уровней делаем всегда +1 день.
        $returnDay = null;
        if(date("H", strtotime($date))<1 || date("H", strtotime($date))>14) {
            $nextDay++;
        }

        $happyArr2014 = array('2014-01-01','2014-01-02','2014-01-03','2014-01-06','2014-01-07','2014-01-08','2014-02-24',
            '2014-03-07','2014-03-10','2014-04-30','2014-05-01','2014-05-02','2014-05-08','2014-05-09','2014-06-11',
            '2014-06-12','2014-06-13','2014-11-03','2014-11-04','2014-12-31','2015-05-01','2015-05-02','2015-05-03',
            '2015-05-04','2015-05-09','2015-05-10','2015-05-11','2015-06-12','2015-11-04');
        if(array_intersect($catIds, $san)) {
            for($i=$nextDay;$i<20;$i++) {
                $ii = $i+1;
                $find = date("Y-m-d", strtotime($date.' + '.$i.' day'));
                $findNext = date("Y-m-d", strtotime($date.' + '.$ii.' day'));
                if(!in_array(date("w", strtotime($find)),array(0,6)) && !in_array(date("w", strtotime($findNext)),array(0,6)) && !in_array($find,$happyArr2014)) {
                    return $returnDay = Yii::$app->formatter->asDate($findNext);
                }
            }
        } else {
            for($i=$nextDay;$i<20;$i++) {
                $find = date("Y-m-d", strtotime($date.' + '.$i.' day'));
                if(!in_array(date("w", strtotime($find)),array(0,6)) && !in_array($find,$happyArr2014)) {
                    return $returnDay = Yii::$app->formatter->asDate($find);
                }
            }
        }
    }

    public static function getDisplayGoods()
    {
        $acceptedValues = ['list', 'plate'];
        $displayGoodsCookie = (isset($_COOKIE['display_goods_gipostroy'])) ? $_COOKIE['display_goods_gipostroy'] : null;
        if($displayGoodsCookie === null) {
            setcookie('display_goods_gipostroy', 'plate', time()+604800, '/');
            return 'plate';
        } else {
            if(in_array($displayGoodsCookie, $acceptedValues)) {
                return $displayGoodsCookie;
            } else {
                return 'plate';
            }
        }
    }

    public function getGoodsCompareBlock($categories = null, $compareIds)
    {
        $thisSections = array_keys($this['sectionGoods']);
        $str = '';
        if($categories) {
            if(array_intersect($thisSections, $categories)) {
                $str .= '<div class="compare-cat">';
                if(is_array($compareIds) && in_array($this->ct_id, $compareIds)) {
                    $str .= '<span class="compare-cat-remove comp' . $this->ct_id . '" data-id="' . $this->ct_id . '"><span>Убрать из сравнения</span></span>';
                } else {
                    $str .= '<span class="compare-cat-add comp' . $this->ct_id . '" data-id="' . $this->ct_id . '"><span>Добавить к сравнению</span></span>';
                }
                $str .= '</div>';
            } else {
                $str .= '<div class="compare-cat-cap"></div>';
            }
        } else {
            $str .= '<div class="compare-cat">';
            if(is_array($compareIds) && in_array($this->ct_id, $compareIds)) {
                $str .= '<span class="compare-cat-remove comp' . $this->ct_id . '" data-id="' . $this->ct_id . '"><span>Убрать из сравнения</span></span>';
            } else {
                $str .= '<span class="compare-cat-add comp' . $this->ct_id . '" data-id="' . $this->ct_id . '"><span>Добавить к сравнению</span></span>';
            }
            $str .= '</div>';
        }

        return $str;
    }

    public function getGoodBasketButton()
    {
        return Html::a(
            'В корзину ' . Html::img('/images/icons/cart_small.png'),
            '#',
            [
                'class' => 'button button-orange add_to_cart',
                'data-id' => $this->ct_id,
            ]
        );
    }

    public function getTitle()
    {
        return $this->ct_name . ' купить в Москве';
    }

    public function getDescription()
    {
        return 'Купить недорого ' . $this->ct_name . ' в Москве и Московской области, лучшая цена, интернет-магазин «Гипострой.ру»';
    }

    public function getKeywords()
    {
        return '';
    }

    public function restrictions()
    {
        $catArr = $this['section'];
        //Тут определяем ограничания для различных видов товаров на количество
        $min = 1; // Минимальное количество, которое можно положить
        $step = 1;// Минимальный шаг, с которым можно класть товары в корзину
        $disabled = false; // Деактивировано ли поле для ввода количества

        if($catArr['prz_id'] == 138) { // Правила для электрозвонков и т.д.
            if(!empty($this->ct_fl_amount)) {
                $min = $this->ct_fl_amount;
                $step = $this->ct_fl_amount;
                $disabled = true;
            }
        }

        if($catArr['prz_id'] == 127) { // Правила для кабельной продукции
            //Производителя VOLTEX кладем только бухтами
            if($this->ct_mn_id != 670 && $this->ct_cable_cut <= 10 && !empty($this->ct_cable_cut)) {
                //Для кабеля сечением меньше 10 мм
                $min = 100;
                $disabled = true;
            }
        }

        if($catArr['prz_id'] == 50 && $this->ct_mn_id == 137){ // Правила для Плинтуса
            $min = 2.4;
            $step = 2.4;
            $disabled = true;
        }

        if($catArr['prz_id'] == 45) { // Для ковролина
            if($this->ct_covrolin_rezka == 2) { //Если ковролин не режется
                if(!empty($this->ct_fl_dlina) && $this->ct_covrolin_width != '0.0') {
                    $disabled = true;
                    $min = $step = round($this->ct_fl_dlina * $this->ct_covrolin_width, 2, PHP_ROUND_HALF_UP);
                } else {
                    $disabled = true;
                    $min = 10;
                    $step = 1;
                }
            } elseif($this->ct_covrolin_rezka == 1) { //Если ковролин режется
                $disabled = true;
                $min = 10;
                $step= 1;
            }
        }

        if($catArr['prz_id'] == 47) { //Для линолеума
            if($this->ct_linoleum_rezka == 2) { //Если линолеум не режется
                if(!empty($this->ct_fl_dlina) && $this->ct_linoleum_width != '0.0') {
                    $disabled = true;
                    $min = $step = round($this->ct_fl_dlina * $this->ct_linoleum_width, 2, PHP_ROUND_HALF_UP);
                } else {
                    $disabled = true;
                    $min = 10;
                    $step = 1;
                }
            } elseif($this->ct_linoleum_rezka == 1) { //Если линолеума режется
                $disabled = true;
                $min = 10;
                $step= 1;
            }
        }

        if($catArr['prz_id'] == 46) { // Для ламината
            if(!empty($this->ct_fl_area)) {
                $area = round($this->ct_fl_area, 2, PHP_ROUND_HALF_UP);
                for($i=1;$i<20;$i++) {
                    if($min < 10) {
                        $min = round($area*$i, 2, PHP_ROUND_HALF_UP);
                    }
                }
                $disabled = true;
                $step = $area;
            } else {
                $disabled = true;
                $min = 10;
                $step = 1;
            }
        }

        if($catArr['prz_id'] == 49) { // Правила для паркетной доски
            if(!empty($this->ct_fl_area)) {
                $area = round($this->ct_fl_area, 2, PHP_ROUND_HALF_UP);
                for($i=1;$i<20;$i++) {
                    if($min < 10) {
                        $min = round($area*$i, 2, PHP_ROUND_HALF_UP);
                    }
                }
                $disabled = true;
                $step = $area;
            } else {
                $disabled = true;
                $min = 10;
                $step = 1;
            }
        }

        if($catArr['prz_id'] == 154) { // Виниловое покрытие
            if(!empty($this->ct_vin_area)) {
                $area = round($this->ct_fl_area, 2, PHP_ROUND_HALF_UP);
                for($i=1;$i<20;$i++) {
                    if($min < 10) {
                        $min = round($area*$i, 2, PHP_ROUND_HALF_UP);
                    }
                }
                $disabled = true;
                $step = $area;
            } else {
                $disabled = true;
                $min = 10;
                $step = 1;
            }
        }

        if($catArr['prz_id'] == 142) { // Пробковое покрытие
            if(!empty($this->ct_fl_area)) {
                $area = round($this->ct_fl_area, 2, PHP_ROUND_HALF_UP);
                for($i=1;$i<20;$i++) {
                    if($min < 10) {
                        $min = round($area*$i, 2, PHP_ROUND_HALF_UP);
                    }
                }
                $disabled = true;
                $step = $area;
            } else {
                $disabled = true;
                $min = 10;
                $step = 1;
            }
        }

        return [
            'min'=>$min,
            'step'=>$step,
            'disabled'=>$disabled,
        ];
    }

    public function getSection()
    {
        return $this->hasOne(Podrazdel::className(), ['prz_id' => 'cat_id'])
            ->viaTable('good_cat', ['good_id' => 'ct_id']);
    }

    public function getSubsection()
    {
        return $this->hasOne(Podrazdel3lvl::className(), ['pprz_id' => 'subcat_id'])
            ->viaTable('good_subcat', ['good_id' => 'ct_id']);
    }

    public function getSectionGoods()
    {
        return $this->hasMany(GoodCat::className(), [
            'good_id' => 'ct_id'
        ])->indexBy('cat_id');
    }

    public function getSubsectionGoods()
    {
        return $this->hasMany(GoodSubcat::className(), [
            'good_id' => 'ct_id'
        ])->indexBy('subcat_id');
    }

    public function getUnit()
    {
        return $this->hasOne(Units::className(), [
            'id' => 'edm'
        ]);
    }

    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), [
            'good_id' => 'ct_id'
        ])->asArray();
    }

    public function getMorePhotos()
    {
        return $this->hasMany(MorePhotos::className(), [
            'good_id' => 'ct_id'
        ])->asArray();
    }

    public function getManufacter()
    {
        return $this->hasOne(Manufacter::className(), [
            'mn_id' => 'ct_mn_id',
        ])->addSelect([
            'mn_id',
            'mn_name',
            'mn_country',
            'mn_url',
            'mn_picture'
        ])->where([
            'mn_show' => Manufacter::IS_SHOW
        ]);
    }
}
