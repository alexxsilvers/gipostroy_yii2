<?php

namespace common\models;

use common\models\query\UserShipmentQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_shipment".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $fname
 * @property string $name
 * @property string $lname
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $apartment
 * @property string $floor
 * @property string $addr_string
 * @property string $comment
 * @property integer $addr_def
 */
class UserShipment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_shipment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'city', 'street', 'house', 'apartment', 'floor', 'addr_string'], 'required'],
            [['user_id', 'addr_def'], 'integer'],
            [['comment', 'addr_def'], 'safe'],
            [['fname', 'name', 'lname', 'house'], 'string', 'max' => 100],
            [['city', 'street', 'addr_string'], 'string', 'max' => 255],
            [['apartment', 'floor'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'fname' => 'Фамилия получателя',
            'name' => 'Имя получателя',
            'lname' => 'Отчество получателя',
            'city' => 'Город',
            'street' => 'Улица',
            'house' => 'Номер дома',
            'apartment' => 'Квартира или офис',
            'addr_string' => 'Адрес полностью',
            'floor' => 'Этаж',
            'comment' => 'Комментарий',
            'addr_def' => 'Использовать по умолчанию',
        ];
    }

    /**
     * @inheritdoc
     * @return UserShipmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserShipmentQuery(get_called_class());
    }

    public function beforeValidate()
    {
        if(parent::beforeValidate()) {
            $this->addr_string = ' г. ' . $this->city . ', ул. ' . $this->street . ', д. ' . $this->house . ', кв. ' . $this->apartment . ', ' . $this->floor . ' этаж';
            $this->user_id = Yii::$app->user->id;
        }
    }
}
