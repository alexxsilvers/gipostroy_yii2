<?php

namespace common\models\query;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Razdel]].
 *
 * @see \common\models\Razdel
 */
class RazdelQuery extends ActiveQuery
{
    public function show()
    {
        $this->andWhere('[[rz_show]]=1');
        return $this;
    }

    public function notSale()
    {
        $this->andWhere('[[rz_id]]!=25');
        return $this;
    }

    public function notNY()
    {
        $this->andWhere('[[rz_id]]!=21');
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\models\Razdel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Razdel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}