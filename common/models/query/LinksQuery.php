<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\Links]].
 *
 * @see \common\models\Links
 */
class LinksQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Links[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Links|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}