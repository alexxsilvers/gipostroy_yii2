<?php

namespace common\models\query;
use common\models\Goods;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Goods]].
 *
 * @see \common\models\Goods
 */
class GoodsQuery extends ActiveQuery
{
    public function sectionFiltersHandle($sectionId)
    {
        if(!empty(Yii::$app->params['section_filters'][$sectionId])) {
            $filters = Yii::$app->params['section_filters'][$sectionId];
            foreach ($filters as $field => $filter) {
                if(is_array(Yii::$app->request->get($filter['param']))) {
                    $this->andFilterWhere([
                        $field => Yii::$app->request->get($filter['param'])
                    ]);
                }
                $getParameters[$filter['param']] = $field;
            }
        }

        if(Yii::$app->request->get('minPrice') > 0) {
            $this->andWhere('ct_price >= :minPrice', [':minPrice' => Yii::$app->request->get('minPrice')]);
        }

        if(Yii::$app->request->get('maxPrice') > 0) {
            $this->andWhere('ct_price <= :maxPrice', [':maxPrice' => Yii::$app->request->get('maxPrice')]);
        }

        if(is_array(Yii::$app->request->get('manufacter'))) {
            $this->andFilterWhere([
                'ct_mn_id' => Yii::$app->request->get('manufacter')
            ]);
        }

        return $this;
    }

    public function subsectionFiltersHandle($subsectionId)
    {
        if(!empty(Yii::$app->params['subsection_filters'][$subsectionId])) {
            $filters = Yii::$app->params['subsection_filters'][$subsectionId];
            foreach ($filters as $field => $filter) {
                if(is_array(Yii::$app->request->get($filter['param']))) {
                    $this->andFilterWhere([
                        $field => Yii::$app->request->get($filter['param'])
                    ]);
                }
                $getParameters[$filter['param']] = $field;
            }
        }

        if(Yii::$app->request->get('minPrice') > 0) {
            $this->andWhere('ct_price >= :minPrice', [':minPrice' => Yii::$app->request->get('minPrice')]);
        }

        if(Yii::$app->request->get('maxPrice') > 0) {
            $this->andWhere('ct_price <= :maxPrice', [':maxPrice' => Yii::$app->request->get('maxPrice')]);
        }

        if(is_array(Yii::$app->request->get('manufacter'))) {
            $this->andFilterWhere([
                'ct_mn_id' => Yii::$app->request->get('manufacter')
            ]);
        }

        return $this;
    }

    public function show()
    {
        $this->andWhere(['ct_show' => Goods::STATUS_SHOW]);
        return $this;
    }

    public function noDeleted()
    {
        $this->andWhere(['ct_deleted' => Goods::STATUS_NOT_DELETED]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\models\Goods[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Goods|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}