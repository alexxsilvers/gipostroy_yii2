<?php

namespace common\models\query;
use common\models\Podrazdel3lvl;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Podrazdel3lvl]].
 *
 * @see \common\models\Podrazdel3lvl
 */
class Podrazdel3lvlQuery extends ActiveQuery
{
    public function show()
    {
        $this->andWhere([
            'pprz_show' => Podrazdel3lvl::IS_SHOW
        ]);
        return $this;
    }

    public function hasChars()
    {
        $this->andWhere("[[pprz_charakters]]IS NOT NULL");
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\models\Podrazdel3lvl[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Podrazdel3lvl|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}