<?php

namespace common\models\query;
use yii\db\ActiveQuery;
use common\models\Podrazdel;

/**
 * This is the ActiveQuery class for [[\common\models\Podrazdel]].
 *
 * @see \common\models\Podrazdel
 */
class PodrazdelQuery extends ActiveQuery
{
    public function show()
    {
        $this->andWhere([
            'prz_show' => Podrazdel::IS_SHOW
        ]);
        return $this;
    }

    public function hasChars()
    {
        $this->andWhere("[[prz_charakters]]IS NOT NULL");
        return $this;
    }

    /**
     * @inheritdoc
     * @return \common\models\Podrazdel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Podrazdel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}