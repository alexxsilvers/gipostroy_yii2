<?php

namespace common\models\query;
use common\models\Sitepages;

/**
 * This is the ActiveQuery class for [[Sitepages]].
 *
 * @see Sitepages
 */
class SitepagesQuery extends \yii\db\ActiveQuery
{
    public function show()
    {
        $this->andWhere('[[sp_show]]=1');
        return $this;
    }

    /**
     * @inheritdoc
     * @return Sitepages[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Sitepages|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}