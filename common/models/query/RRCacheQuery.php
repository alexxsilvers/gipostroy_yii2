<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\RRCache]].
 *
 * @see \common\models\RRCache
 */
class RRCacheQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\RRCache[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\RRCache|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}