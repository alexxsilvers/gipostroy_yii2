<?php

namespace common\models;

use common\models\query\SitepagesQuery;
use Yii;

/**
 * This is the model class for table "sp_sitepages".
 *
 * @property integer $sp_id
 * @property string $sp_name
 * @property string $sp_text
 * @property string $sp_url
 * @property integer $sp_show
 * @property integer $sp_sorti
 * @property string $sp_title
 * @property string $sp_keywords
 * @property string $sp_description
 */
class Sitepages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sp_sitepages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sp_text'], 'string'],
            [['sp_show', 'sp_sorti'], 'integer'],
            [['sp_name', 'sp_url', 'sp_title', 'sp_keywords', 'sp_description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sp_id' => Yii::t('app', 'Sp ID'),
            'sp_name' => Yii::t('app', 'Sp Name'),
            'sp_text' => Yii::t('app', 'Sp Text'),
            'sp_url' => Yii::t('app', 'Sp Url'),
            'sp_show' => Yii::t('app', 'Sp Show'),
            'sp_sorti' => Yii::t('app', 'Sp Sorti'),
            'sp_title' => Yii::t('app', 'Sp Title'),
            'sp_keywords' => Yii::t('app', 'Sp Keywords'),
            'sp_description' => Yii::t('app', 'Sp Description'),
        ];
    }

    /**
     * @inheritdoc
     * @return SitepagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SitepagesQuery(get_called_class());
    }
}
