<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "orct_orderscatalog".
 *
 * @property integer $id
 * @property integer $or_id
 * @property integer $ct_id
 * @property double $colvo
 * @property integer $price
 */
class OrderItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orct_orderscatalog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['or_id', 'ct_id', 'price'], 'integer'],
            [['colvo', 'price'], 'required'],
            [['colvo', 'price'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'or_id' => 'Номер заказа',
            'ct_id' => 'ID товара',
            'colvo' => 'Количество',
            'price' => 'Цена',
        ];
    }

    public function getGood()
    {
        return $this->hasOne(Goods::className(), ['ct_id' => 'ct_id']);
    }
}
