<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "shipment_values".
 *
 * @property integer $id
 * @property integer $weight
 * @property double $volume
 * @property double $dimensions
 * @property integer $price
 */
class ShipmentValues extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipment_values';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['weight', 'volume', 'dimensions', 'price'], 'required'],
            [['weight', 'price'], 'integer'],
            [['volume', 'dimensions'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'weight' => 'Weight',
            'volume' => 'Volume',
            'dimensions' => 'Dimensions',
            'price' => 'Price',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\ShipmentValuesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ShipmentValuesQuery(get_called_class());
    }
}
