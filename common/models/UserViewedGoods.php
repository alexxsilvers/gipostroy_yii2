<?php

namespace common\models;

use common\models\query\UserViewedGoodsQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "user_viewed_goods".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $good_id
 * @property string $view_time
 */
class UserViewedGoods extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_viewed_goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'good_id', 'view_time'], 'required'],
            [['user_id', 'good_id'], 'integer'],
            [['view_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'good_id' => 'Good ID',
            'view_time' => 'View Time',
        ];
    }

    /**
     * @inheritdoc
     * @return UserViewedGoodsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserViewedGoodsQuery(get_called_class());
    }

    public function getGood()
    {
        return $this->hasOne(Goods::className(), [
            'ct_id' => 'good_id'
        ]);
    }

    public function beforeSave($insert)
    {
        $this->view_time = new Expression('NOW()');
        return parent::beforeSave($insert);
    }
}
