<?php

namespace common\models;

use common\models\query\Podrazdel3lvlQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "pprz_podrazdel3lvl".
 *
 * @property integer $pprz_id
 * @property string $pprz_cid
 * @property string $pprz_name
 * @property string $pprz_pic
 * @property integer $pprz_prz_id
 * @property string $pprz_text
 * @property string $pprz_title
 * @property string $pprz_keywords
 * @property string $pprz_description
 * @property string $pprz_url
 * @property integer $pprz_sorti
 * @property string $pprz_chdate
 * @property integer $pprz_show
 * @property string $pprz_google
 * @property string $pprz_charakters
 * @property integer $pprz_ym
 * @property integer $pprz_gm
 */
class Podrazdel3lvl extends ActiveRecord
{
    const IS_SHOW = 1;
    const IS_NOT_SHOW = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pprz_podrazdel3lvl';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pprz_cid', 'pprz_google'], 'required'],
            [['pprz_prz_id', 'pprz_sorti', 'pprz_show', 'pprz_ym', 'pprz_gm'], 'integer'],
            [['pprz_text', 'pprz_charakters'], 'string'],
            [['pprz_chdate'], 'safe'],
            [['pprz_cid', 'pprz_name', 'pprz_pic', 'pprz_title', 'pprz_keywords', 'pprz_description', 'pprz_url', 'pprz_google'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pprz_id' => Yii::t('front', 'Pprz ID'),
            'pprz_cid' => Yii::t('front', 'Pprz Cid'),
            'pprz_name' => Yii::t('front', 'Pprz Name'),
            'pprz_pic' => Yii::t('front', 'Pprz Pic'),
            'pprz_prz_id' => Yii::t('front', 'Pprz Prz ID'),
            'pprz_text' => Yii::t('front', 'Pprz Text'),
            'pprz_title' => Yii::t('front', 'Pprz Title'),
            'pprz_keywords' => Yii::t('front', 'Pprz Keywords'),
            'pprz_description' => Yii::t('front', 'Pprz Description'),
            'pprz_url' => Yii::t('front', 'Pprz Url'),
            'pprz_sorti' => Yii::t('front', 'Pprz Sorti'),
            'pprz_chdate' => Yii::t('front', 'Pprz Chdate'),
            'pprz_show' => Yii::t('front', 'Pprz Show'),
            'pprz_google' => Yii::t('front', 'Pprz Google'),
            'pprz_charakters' => Yii::t('front', 'Pprz Charakters'),
            'pprz_ym' => Yii::t('front', 'Pprz Ym'),
            'pprz_gm' => Yii::t('front', 'Pprz Gm'),
        ];
    }

    /**
     * @inheritdoc
     * @return Podrazdel3lvlQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new Podrazdel3lvlQuery(get_called_class());
    }

    /**
     * Relation with section
     * @return static
     */
    public function getSection()
    {
        return $this->hasOne(Podrazdel::className(), [
            'prz_id' => 'pprz_prz_id'
        ])->addSelect([
            'prz_url', 'prz_name', 'prz_pic', 'prz_id', 'prz_rz_id'
        ]);
    }

    /**
     * Relation with seo tags
     * @return static
     */
    public function getSeoTags()
    {
        return $this->hasMany(Seo::className(), [
            'subcat_id' => 'pprz_id'
        ])->andWhere(['entity_type' => 'tag'])->addSelect(['id', 'url', 'type', 'cat_id', 'subcat_id']);
    }

    /**
     * Relation with seo pages
     * @return static
     */
    public function getSeo4lvl()
    {
        return $this->hasMany(Seo::className(), [
            'subcat_id' => 'pprz_id'
        ])->andWhere(['entity_type' => 'level4'])->addSelect(['id', 'url', 'type', 'cat_id', 'subcat_id', 'pic']);
    }

    public function getSeo4lvlBlock($items)
    {
        $outputStr = '';
        if(sizeof($items) > 0) {
            $outputStr .= '<div class="seo-categories">';
            foreach($items as $item) {
                if(!empty($item['pic'])) {
                    $outputStr .= '<div class="seo-category">';
                    $outputStr .= Html::a(
                        Html::img('/images/seo/' . $item->pic) . '<span>' . $item->type . '</span>',
                        ['seo/subsectionseo', 'name' => $this->pprz_url, 'pageName' => $item->url]
                    );
                    $outputStr .= '</div>';
                }
            }
            $outputStr .= '<div class="clear"></div>';
            $outputStr .= '</div>';
        }

        return $outputStr;
    }

    public function getSeoTagsBlock($tags)
    {
        $outputStr = '';
        if(sizeof($tags) > 0) {
            $outputStr .= '<div class="tags">';
            foreach($tags as $tag) {
                $outputStr .= Html::a(
                    $tag->type,
                    ['seo/subsectiontag', 'section' => $this->pprz_url, 'tag' => $tag->url]
                );
            }
            $outputStr .= '</div>';
        }

        return $outputStr;
    }

    public function getTitle()
    {
        if(!empty($this->pprz_title)) {
            return $this->pprz_title;
        } else {
            return $this->pprz_name . ' купить в Москве и МО, лучшая цена';
        }
    }

    public function getDescription()
    {
        if(!empty($this->pprz_description)) {
            return $this->pprz_description;
        } else {
            return 'Купить недорого ' . $this->pprz_name . ' в Москве и Московской области с доставкой, лучшая цена, интернет-магазин «Гипострой.ру» ☎ +7 (495) 545-44-547';
        }
    }

    public function getKeywords()
    {
        return $this->pprz_keywords;
    }

    public function getH1()
    {
        $output = '<h1>';
        if(mb_strlen($this->pprz_name, 'utf-8')<10) {
            $output .= $this->pprz_name . ' в интернет-магазине Гипострой.ру';
        } else {
            $output .= $this->pprz_name;
        }
        $output .= '</h1>';

        return $output;
    }

    public function getHeader()
    {
        return $this->pprz_name;
    }

    public function getText()
    {
        $output = '';
        if(isset($this->pprz_text[10])) {
            if(preg_match('#<h1>#i', $this->pprz_text)) {
                $output .= strip_tags($this->pprz_text, Yii::$app->params['allowed_tags']);
                $output .= '<hr/>';
            } else {
                $output .= $this->getH1();
                $output .= strip_tags($this->pprz_text, Yii::$app->params['allowed_tags']);
                $output .= '<hr/>';
            }
        } else {
            $output .=  $this->getH1();
        }

        return $output;
    }
}
