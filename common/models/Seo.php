<?php

namespace common\models;

use common\models\query\SeoQuery;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "seo_text".
 *
 * @property integer $id
 * @property integer $cat_id
 * @property integer $subcat_id
 * @property string $type
 * @property string $url
 * @property string $text
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $header
 * @property string $pic
 * @property string $entity_type
 * @property string $condition
 */
class Seo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seo_text';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat_id', 'subcat_id'], 'integer'],
            [['type', 'url', 'text', 'title', 'description', 'keywords', 'header', 'pic', 'entity_type', 'condition'], 'required'],
            [['text'], 'string'],
            [['type', 'url', 'title', 'description', 'keywords', 'header', 'pic', 'entity_type', 'condition'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cat_id' => 'Cat ID',
            'subcat_id' => 'Subcat ID',
            'type' => 'Type',
            'url' => 'Url',
            'text' => 'Text',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'header' => 'Header',
            'pic' => 'Pic',
            'entity_type' => 'Тип',
            'condition' => 'Условие',
        ];
    }

    /**
     * @inheritdoc
     * @return SeoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SeoQuery(get_called_class());
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->condition = Json::decode($this->condition);
    }

    public function getSection()
    {
        return $this->hasOne(Podrazdel::className(), [
            'prz_id' => 'cat_id'
        ]);
    }

    public function getSubsection()
    {
        return $this->hasOne(Podrazdel3lvl::className(), [
            'pprz_id' => 'subcat_id'
        ]);
    }

    public function getText()
    {
        if(!isset($this->text[50])) {
            return '<h1>' . $this->header . '</h1>';
        }

        return $this->text;
    }
}
