<?php

namespace common\models;

use common\models\query\GoodSubcatQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "good_subcat".
 *
 * @property integer $id
 * @property integer $subcat_id
 * @property integer $good_id
 * @property integer $manufacter_id
 */
class GoodSubcat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'good_subcat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subcat_id', 'good_id', 'manufacter_id'], 'required'],
            [['subcat_id', 'good_id', 'manufacter_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subcat_id' => 'Subcat ID',
            'good_id' => 'Good ID',
            'manufacter_id' => 'Manufacter ID',
        ];
    }

    /**
     * @inheritdoc
     * @return GoodSubcatQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GoodSubcatQuery(get_called_class());
    }

    public function getSubsection()
    {
        return $this->hasOne(Podrazdel3lvl::className(), [
            'pprz_id' => 'subcat_id'
        ])->addSelect([
            'pprz_id', 'pprz_name', 'pprz_url', 'pprz_pic', 'pprz_prz_id'
        ])->andWhere([
            'pprz_show' => Podrazdel3lvl::IS_SHOW
        ])->addOrderBy([
            'pprz_sorti' => SORT_ASC
        ]);
    }
}
