<?php

namespace common\models;

use common\models\query\PodrazdelQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "prz_podrazdel".
 *
 * @property integer $prz_id
 * @property string $prz_name
 * @property string $prz_cid
 * @property integer $prz_rz_id
 * @property string $prz_pic
 * @property string $prz_text
 * @property integer $prz_sorti
 * @property integer $prz_show
 * @property string $prz_title
 * @property string $prz_keywords
 * @property string $prz_description
 * @property string $prz_url
 * @property string $prz_google
 * @property string $prz_yandex
 * @property integer $column_number
 * @property string $prz_charakters
 * @property integer $prz_ym
 * @property integer $prz_gm
 * @property string $prz_vin
 */
class Podrazdel extends ActiveRecord
{
    const IS_SHOW = 1;
    const IS_NOT_SHOW = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prz_podrazdel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prz_cid', 'prz_google', 'prz_yandex', 'prz_vin'], 'required'],
            [['prz_rz_id', 'prz_sorti', 'prz_show', 'column_number', 'prz_ym', 'prz_gm'], 'integer'],
            [['prz_text', 'prz_charakters'], 'string'],
            [['prz_name', 'prz_cid', 'prz_pic', 'prz_title', 'prz_keywords', 'prz_description', 'prz_url', 'prz_google', 'prz_yandex', 'prz_vin'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'prz_id' => Yii::t('front', 'Prz ID'),
            'prz_name' => Yii::t('front', 'Prz Name'),
            'prz_cid' => Yii::t('front', 'Prz Cid'),
            'prz_rz_id' => Yii::t('front', 'Prz Rz ID'),
            'prz_pic' => Yii::t('front', 'Prz Pic'),
            'prz_text' => Yii::t('front', 'Prz Text'),
            'prz_sorti' => Yii::t('front', 'Prz Sorti'),
            'prz_show' => Yii::t('front', 'Prz Show'),
            'prz_title' => Yii::t('front', 'Prz Title'),
            'prz_keywords' => Yii::t('front', 'Prz Keywords'),
            'prz_description' => Yii::t('front', 'Prz Description'),
            'prz_url' => Yii::t('front', 'Prz Url'),
            'prz_google' => Yii::t('front', 'Prz Google'),
            'prz_yandex' => Yii::t('front', 'Prz Yandex'),
            'column_number' => Yii::t('front', 'Column Number'),
            'prz_charakters' => Yii::t('front', 'Prz Charakters'),
            'prz_ym' => Yii::t('front', 'Prz Ym'),
            'prz_gm' => Yii::t('front', 'Prz Gm'),
            'prz_vin' => Yii::t('front', 'Prz Vin'),
        ];
    }

    /**
     * @inheritdoc
     * @return PodrazdelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PodrazdelQuery(get_called_class());
    }

    /**
     * Relation with category in which where is this section
     * @return static
     */
    public function getCategory()
    {
        return $this->hasOne(Razdel::className(), [
            'rz_id' => 'prz_rz_id'
        ])->addSelect([
            'rz_url', 'rz_name', 'rz_pic', 'rz_id'
        ]);
    }

    /**
     * Relation with child subsections
     * @return static
     */
    public function getSubsections()
    {
        return $this->hasMany(Podrazdel3lvl::className(), [
            'pprz_prz_id' => 'prz_id'
        ])->addSelect([
            'pprz_id', 'pprz_name', 'pprz_url', 'pprz_pic', 'pprz_prz_id'
        ])->andWhere([
            'pprz_show' => Podrazdel3lvl::IS_SHOW
        ])->addOrderBy([
            'pprz_sorti' => SORT_ASC
        ]);
    }

    /**
     * Relation with seo tags
     * @return static
     */
    public function getSeoTags()
    {
        return $this->hasMany(Seo::className(), [
            'cat_id' => 'prz_id'
        ])->andWhere(['subcat_id' => 0, 'entity_type' => 'tag'])->addSelect(['id', 'url', 'type', 'cat_id']);
    }

    /**
     * Relation with seo pages
     * @return static
     */
    public function getSeo3lvl()
    {
        return $this->hasMany(Seo::className(), [
            'cat_id' => 'prz_id'
        ])->andWhere(['subcat_id' => 0, 'entity_type' => 'level3'])
            ->addSelect(['id', 'url', 'type', 'cat_id', 'pic']);
    }

    public function getSeo3lvlBlock($items)
    {
        $outputStr = '';
        if(sizeof($items) > 0) {
            $outputStr .= '<div class="seo-categories">';
            foreach($items as $item) {
                if(!empty($item['pic'])) {
                    $outputStr .= '<div class="seo-category">';
                    $outputStr .= Html::a(
                        Html::img('/images/seo/' . $item->pic) . '<span>' . $item->type . '</span>',
                        ['seo/sectionseo', 'name' => $this->prz_url, 'pageName' => $item->url]
                    );
                    $outputStr .= '</div>';
                }
            }
            $outputStr .= '<div class="clear"></div>';
            $outputStr .= '</div>';
        }

        return $outputStr;
    }

    public function getSeoTagsBlock($tags)
    {
        $outputStr = '';
        if(sizeof($tags) > 0) {
            $outputStr .= '<div class="tags">';
            foreach($tags as $tag) {
                $outputStr .= Html::a(
                    $tag->type,
                    ['seo/sectiontag', 'section' => $this->prz_url, 'tag' => $tag->url]
                );
            }
            $outputStr .= '</div>';
        }

        return $outputStr;
    }

    public function getTitle()
    {
        if(!empty($this->prz_title)) {
            return $this->prz_title;
        } else {
            return $this->prz_name . ' купить в Москве и МО, лучшая цена';
        }
    }

    public function getDescription()
    {
        if(!empty($this->prz_description)) {
            return $this->prz_description;
        } else {
            return 'Купить недорого ' . $this->prz_name . ' в Москве и Московской области с доставкой, лучшая цена, интернет-магазин «Гипострой.ру» ☎ +7 (495) 545-44-547';
        }
    }

    public function getKeywords()
    {
        return $this->prz_keywords;
    }

    public function getH1()
    {
        $output = '<h1>';
        if(mb_strlen($this->prz_name, 'utf-8')<10) {
            $output .= $this->prz_name . ' в интернет-магазине Гипострой.ру';
        } else {
            $output .= $this->prz_name;
        }
        $output .= '</h1>';

        return $output;
    }

    public function getHeader()
    {
        return $this->prz_name;
    }

    public function getText()
    {
        $output = '';
        if(isset($this->prz_text[10])) {
            if(preg_match('#<h1>#i', $this->prz_text)) {
                $output .= strip_tags($this->prz_text, Yii::$app->params['allowed_tags']);
                $output .= '<hr/>';
            } else {
                $output .= $this->getH1();
                $output .= strip_tags($this->prz_text, Yii::$app->params['allowed_tags']);
                $output .= '<hr/>';
            }
        } else {
            $output .=  $this->getH1();
        }

        return $output;
    }
}
