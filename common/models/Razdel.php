<?php

namespace common\models;

use common\models\query\RazdelQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "rz_razdel".
 *
 * @property integer $rz_id
 * @property string $rz_name
 * @property string $rz_cid
 * @property string $rz_url
 * @property string $rz_text
 * @property integer $rz_sorti
 * @property string $rz_chdate
 * @property integer $rz_show
 * @property string $rz_title
 * @property string $rz_keywords
 * @property string $rz_description
 * @property string $rz_pic
 * @property string $rz_icon
 * @property integer $rz_ym
 * @property integer $rz_gm
 */
class Razdel extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rz_razdel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rz_cid', 'rz_icon'], 'required'],
            [['rz_text'], 'string'],
            [['rz_sorti', 'rz_show', 'rz_ym', 'rz_gm'], 'integer'],
            [['rz_chdate'], 'safe'],
            [['rz_name', 'rz_cid', 'rz_url', 'rz_title', 'rz_keywords', 'rz_description', 'rz_pic', 'rz_icon'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rz_id' => Yii::t('front', 'Rz ID'),
            'rz_name' => Yii::t('front', 'Rz Name'),
            'rz_cid' => Yii::t('front', 'Rz Cid'),
            'rz_url' => Yii::t('front', 'Rz Url'),
            'rz_text' => Yii::t('front', 'Rz Text'),
            'rz_sorti' => Yii::t('front', 'Rz Sorti'),
            'rz_chdate' => Yii::t('front', 'Rz Chdate'),
            'rz_show' => Yii::t('front', 'Rz Show'),
            'rz_title' => Yii::t('front', 'Rz Title'),
            'rz_keywords' => Yii::t('front', 'Rz Keywords'),
            'rz_description' => Yii::t('front', 'Rz Description'),
            'rz_pic' => Yii::t('front', 'Rz Pic'),
            'rz_icon' => Yii::t('front', 'Rz Icon'),
            'rz_ym' => Yii::t('front', 'Rz Ym'),
            'rz_gm' => Yii::t('front', 'Rz Gm'),
        ];
    }

    /**
     * @inheritdoc
     * @return RazdelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RazdelQuery(get_called_class());
    }

    public static function getRing($catId)
    {
        $categoriesArray = self::find()->addSelect(['rz_id', 'rz_name', 'rz_url'])
            ->show()->orderBy('rz_sorti ASC')->asArray()->indexBy('rz_id')->all();

        return [
            'prev' => self::array_key_relative($categoriesArray, $catId, 'prev'),
            'next' => self::array_key_relative($categoriesArray, $catId, 'next'),
        ];
    }

    public static function array_key_relative($array, $current_key, $offset)
    {
        $keys = array_keys($array);
        $current_key_index = array_search($current_key, $keys);

        if($offset == 'prev') {
            if(isset($keys[$current_key_index - 1])) {
                return $array[$keys[$current_key_index - 1]];
            } else {
                return $array[array_pop($keys)];
            }
        } else {
            if(isset($keys[$current_key_index + 1])) {
                return $array[$keys[$current_key_index + 1]];
            } else {
                return $array[array_shift($keys)];
            }
        }
    }


    public function getTitle()
    {
        if(!empty($this->rz_title)) {
            return $this->rz_title;
        } else {
            return $this->rz_name . ' купить в Москве и МО, лучшая цена';
        }
    }

    public function getDescription()
    {
        if(!empty($this->rz_description)) {
            return $this->rz_description;
        } else {
            return 'Купить недорого ' . $this->rz_name . ' в Москве и Московской области с доставкой, лучшая цена, интернет-магазин «Гипострой.ру» ☎ +7 (495) 545-44-547';
        }
    }

    public function getKeywords()
    {
        return $this->rz_keywords;
    }

    public function getH1()
    {
        $output = '<h1>';
        if(mb_strlen($this->rz_name, 'utf-8')<10) {
            $output .= $this->rz_name . ' в интернет-магазине Гипострой.ру';
        } else {
            $output .= $this->rz_name;
        }
        $output .= '</h1>';

        return $output;
    }

    public function getHeader()
    {
        return $this->rz_name;
    }

    public function getText()
    {
        $output = '';
        if(isset($this->rz_text[10])) {
            if(preg_match('#<h1>#i', $this->rz_text)) {
                $output .= strip_tags($this->rz_text, Yii::$app->params['allowed_tags']);
                $output .= '<hr/>';
            } else {
                $output .= $this->getH1();
                $output .= strip_tags($this->rz_text, Yii::$app->params['allowed_tags']);
                $output .= '<hr/>';
            }
        } else {
            $output .=  $this->getH1();
        }

        return $output;
    }

    public function getSections()
    {
        return $this->hasMany(Podrazdel::className(), [
            'prz_rz_id' => 'rz_id'
        ])->addSelect([
            'prz_id', 'prz_name', 'prz_url', 'prz_pic', 'prz_rz_id', 'prz_charakters'
        ])->andWhere([
            'prz_show' => Podrazdel::IS_SHOW,
        ])->indexBy('prz_id');
    }
}
