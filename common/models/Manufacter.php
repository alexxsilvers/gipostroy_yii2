<?php

namespace common\models;

use common\models\query\ManufacterQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "mn_manufacter".
 *
 * @property integer $mn_id
 * @property string $mn_name
 * @property string $mn_url
 * @property string $mn_text
 * @property string $mn_country
 * @property string $mn_picture
 * @property integer $mn_sorti
 * @property string $mn_chdate
 * @property integer $mn_show
 * @property string $mn_title
 * @property string $mn_description
 * @property string $mn_keywords
 */
class Manufacter extends ActiveRecord
{
    const IS_SHOW = 1;
    const IS_NOT_SHOW = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mn_manufacter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mn_text'], 'string'],
            [['mn_country', 'mn_title', 'mn_description', 'mn_keywords'], 'required'],
            [['mn_sorti', 'mn_show'], 'integer'],
            [['mn_chdate'], 'safe'],
            [['mn_name', 'mn_url', 'mn_picture', 'mn_title', 'mn_description', 'mn_keywords'], 'string', 'max' => 255],
            [['mn_country'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mn_id' => 'Mn ID',
            'mn_name' => 'Mn Name',
            'mn_url' => 'Mn Url',
            'mn_text' => 'Mn Text',
            'mn_country' => 'Mn Country',
            'mn_picture' => 'Mn Picture',
            'mn_sorti' => 'Mn Sorti',
            'mn_chdate' => 'Mn Chdate',
            'mn_show' => 'Mn Show',
            'mn_title' => 'Mn Title',
            'mn_description' => 'Mn Description',
            'mn_keywords' => 'Mn Keywords',
        ];
    }

    /**
     * @inheritdoc
     * @return ManufacterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ManufacterQuery(get_called_class());
    }


    public function getSeo()
    {
        return $this->hasMany(ManufacterSeo::className(), [
            'semn_id' => 'mn_id'
        ]);
    }


    public function getImageSrc()
    {
        if(in_array($_SERVER['SERVER_NAME'],array('develop.gipostroy.ru','gipo.loc','gipo2yii.loc','yiishop.loc'))) {
            $src = 'http://www.gipostroy.ru/pdir/' . $this->mn_picture;
        } elseif(!empty($this->mn_picture) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/pdir/' . $this->mn_picture)) {
            $src = '/pdir/' . $this->mn_picture;
        } else {
            $src = '/images/nophoto.jpg';
        }

        return $src;
    }


    public function getManufactersPageTitle()
    {
        return empty($this->mn_title) ? $this->mn_name : $this->mn_title;
    }

    public function getManufactersPageDescription()
    {
        return $this->mn_description;
    }

    public function getManufactersPageKeywords()
    {
        return $this->mn_keywords;
    }

    public function getManufactersPageText()
    {
        return strip_tags($this->mn_text, Yii::$app->params['allowed_tags']);
    }

    public function getManufactersPageHeader()
    {
        if(preg_match('#<h1>#i', $this->mn_text)) {
            return '<span class="myCustomHeader">' . $this->mn_name . '</span>';
        } else {
            return '<h1>' . $this->mn_name . '</h1>';
        }
    }


    /**
     * Возвращаем title для страниц производителей в разделах
     * @param Podrazdel $section
     * @param Podrazdel3lvl $subsection
     * @return string
     */
    public function getCatalogManufacterTitle($section = null, $subsection = null)
    {
        if(!is_null($section)) {
            $title = '';
            foreach($this['seo'] as $seo) {
                if($seo->seprz_id == $section->prz_id && $seo->seppprz_id == 0) {
                    if(!empty($seo->se_title)) {
                        $title = $seo->se_title;
                    }
                }
            }

            return !empty($title) ? $title : $section->prz_name . ' ' . $this->mn_name . ' купить в Москве и МО, лучшая цена';
        }

        if(!is_null($subsection)) {
            $title = '';
            foreach($this['seo'] as $seo) {
                if($seo->seppprz_id == $subsection->pprz_id) {
                    if(!empty($seo->se_title)) {
                        $title = $seo->se_title;
                    }
                }
            }

            return !empty($title) ? $title : $subsection->pprz_name . ' ' . $this->mn_name . ' купить в Москве и МО, лучшая цена';
        }

        return $this->mn_title;
    }

    /**
     * Возвращаем description для страниц производителей в разделах
     * @param Podrazdel $section
     * @param Podrazdel3lvl $subsection
     * @return string
     */
    public function getCatalogManufacterDescription($section = null, $subsection = null)
    {
        if(!is_null($section)) {
            $description = '';
            foreach($this['seo'] as $seo) {
                if($seo->seprz_id == $section->prz_id && $seo->seppprz_id == 0) {
                    if(!empty($seo->se_description)) {
                        $description = $seo->se_description;
                    }
                }
            }

            return !empty($description) ? $description : 'Купить недорого ' . $section->prz_name . ' ' . $this->mn_name
                . ' в Москве и Московской области с доставкой, лучшая цена, интернет-магазин «Гипострой.ру» ☎ +7 (495) 545-44-547';
        }

        if(!is_null($subsection)) {
            $description = '';
            foreach($this['seo'] as $seo) {
                if($seo->seppprz_id == $subsection->pprz_id) {
                    if(!empty($seo->se_description)) {
                        $description = $seo->se_description;
                    }
                }
            }

            return !empty($description) ? $description : 'Купить недорого ' . $subsection->pprz_name . ' ' . $this->mn_name
                . ' в Москве и Московской области с доставкой, лучшая цена, интернет-магазин «Гипострой.ру» ☎ +7 (495) 545-44-547';
        }

        return $this->mn_description;
    }

    /**
     * Возвращаем keywords для страниц производителей в разделах
     * @param Podrazdel $section
     * @param Podrazdel3lvl $subsection
     * @return string
     */
    public function getCatalogManufacterKeywords($section = null, $subsection = null)
    {
        if(!is_null($section)) {
            $keywords = '';
            foreach($this['seo'] as $seo) {
                if($seo->seprz_id == $section->prz_id && $seo->seppprz_id == 0) {
                    if(!empty($seo->se_keywords)) {
                        $keywords = $seo->se_keywords;
                    }
                }
            }

            return !empty($keywords) ? $keywords : '';
        }

        if(!is_null($subsection)) {
            $keywords = '';
            foreach($this['seo'] as $seo) {
                if($seo->seppprz_id == $subsection->pprz_id) {
                    if(!empty($seo->se_keywords)) {
                        $keywords = $seo->se_keywords;
                    }
                }
            }

            return !empty($keywords) ? $keywords : '';
        }

        return $this->mn_description;
    }

    /**
     * Возвращаем заголовок для страниц производителей в разделах
     * @param Podrazdel $section
     * @param Podrazdel3lvl $subsection
     * @return string
     */
    public function getCatalogManufacterHeader($section = null, $subsection = null)
    {
        if(!is_null($section)) {
            return $section->prz_name . ' ' . $this->mn_name;
        }

        if(!is_null($subsection)) {
            return $subsection->pprz_name . ' ' . $this->mn_name;
        }

        return $this->mn_name;
    }

    /**
     * Возвращаем H1 для страниц производителей в разделах
     * @param Podrazdel $section
     * @param Podrazdel3lvl $subsection
     * @return string
     */
    public function getCatalogManufacterH1($section = null, $subsection = null)
    {
        if(!is_null($section)) {
            return '<h1>' . $section->prz_name . ' ' . $this->mn_name . '</h1>';
        }

        if(!is_null($subsection)) {
            return '<h1>' . $subsection->pprz_name . ' ' . $this->mn_name . '</h1>';
        }

        return '<h1>' . $this->mn_name . '</h1>';
    }

    /**
     * Возвращаем текст для страниц производителей в разделах
     * @param Podrazdel $section
     * @param Podrazdel3lvl $subsection
     * @return string
     */
    public function getCatalogManufacterText($section = null, $subsection = null)
    {
        if(!is_null($section)) {
            $seoText = '';
            foreach($this['seo'] as $seo) {
                if($seo->seprz_id == $section->prz_id && $seo->seppprz_id == 0) {
                    $seoText = $seo->seotext;
                }
            }

            if(!empty($seoText)) {
                if(preg_match('#<h1>#i', $seoText)) {
                    return strip_tags($seoText, Yii::$app->params['allowed_tags']) . '<hr/>';
                } else {
                    return $this->getCatalogManufacterH1($section) . strip_tags($seoText, Yii::$app->params['allowed_tags']) . '<hr/>';
                }
            } else {
                return $this->getCatalogManufacterH1($section);
            }
        }

        if(!is_null($subsection)) {
            $seoText = '';
            foreach($this['seo'] as $seo) {
                if($seo->seppprz_id == $subsection->pprz_id) {
                    $seoText = $seo->seotext;
                }
            }

            if(!empty($seoText)) {
                if(preg_match('#<h1>#i', $seoText)) {
                    return strip_tags($seoText, Yii::$app->params['allowed_tags']) . '<hr/>';
                } else {
                    return $this->getCatalogManufacterH1(null, $subsection) . strip_tags($seoText, Yii::$app->params['allowed_tags']) . '<hr/>';
                }
            } else {
                return $this->getCatalogManufacterH1(null, $subsection);
            }
        }

        return $this->mn_name;
    }
}
