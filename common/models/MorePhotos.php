<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "more_photos".
 *
 * @property integer $id
 * @property integer $good_id
 * @property string $photo
 */
class MorePhotos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'more_photos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['good_id', 'photo'], 'required'],
            [['good_id'], 'integer'],
            [['photo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'good_id' => 'Good ID',
            'photo' => 'Photo',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\MorePhotosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\MorePhotosQuery(get_called_class());
    }
}
