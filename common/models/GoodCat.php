<?php

namespace common\models;

use common\models\query\GoodCatQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "good_cat".
 *
 * @property integer $id
 * @property integer $cat_id
 * @property integer $good_id
 * @property integer $manufacter_id
 */
class GoodCat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'good_cat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat_id', 'good_id', 'manufacter_id'], 'required'],
            [['cat_id', 'good_id', 'manufacter_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cat_id' => 'Cat ID',
            'good_id' => 'Good ID',
            'manufacter_id' => 'Manufacter ID',
        ];
    }

    /**
     * @inheritdoc
     * @return GoodCatQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GoodCatQuery(get_called_class());
    }

    public function getSection()
    {
        return $this->hasOne(Podrazdel::className(), [
            'prz_id' => 'cat_id'
        ])->addSelect([
            'prz_id', 'prz_name', 'prz_url', 'prz_pic', 'prz_rz_id'
        ])->andWhere([
            'prz_show' => Podrazdel3lvl::IS_SHOW
        ])->addOrderBy([
            'prz_sorti' => SORT_ASC
        ]);
    }
}
