<?php

namespace common\models;

use common\models\query\OrdersQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "or_orders".
 *
 * @property integer $or_id
 * @property string $first_confirm
 * @property integer $first_confirm_send
 * @property string $second_confirm
 * @property integer $second_confirm_send
 * @property string $onec_id
 * @property integer $or_coupon_id
 * @property string $or_ordertime
 * @property integer $or_manager_id
 * @property string $or_manager_time
 * @property integer $or_ordersum
 * @property integer $or_order_type
 * @property integer $or_delivery_type
 * @property string $or_pick_point_address
 * @property integer $or_ship_addr
 * @property string $or_notice
 * @property string $or_notice_time
 * @property string $or_companyname
 * @property string $or_inn
 * @property string $or_kpp
 * @property string $or_legalAdres
 * @property string $or_firstname
 * @property string $or_lname
 * @property string $or_surname
 * @property string $or_email
 * @property string $or_phone
 * @property string $or_deliveryAdres
 * @property string $or_dopinfo
 * @property integer $unload
 * @property integer $or_loaded
 * @property integer $vkredit
 * @property integer $tiu
 * @property integer $tiu_id
 * @property integer $oneclickcall
 * @property integer $from_mobile
 * @property integer $or_user_id
 * @property integer $or_subscribed
 * @property integer $or_comments_unloaded
 * @property string $or_td_pickup_address
 * @property integer $or_td_pickup_address_id
 * @property integer $or_td_delivery_type
 * @property integer $or_td_price
 * @property double $or_td_rko
 */
class Orders extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'or_orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_confirm', 'second_confirm', 'or_ordertime', 'or_manager_time', 'or_notice_time'], 'safe'],
            [['first_confirm_send', 'second_confirm_send', 'or_coupon_id', 'or_manager_id', 'or_ordersum', 'or_order_type', 'or_delivery_type', 'or_ship_addr', 'unload', 'or_loaded', 'vkredit', 'tiu', 'tiu_id', 'oneclickcall', 'from_mobile', 'or_user_id', 'or_subscribed', 'or_comments_unloaded', 'or_td_pickup_address_id', 'or_td_delivery_type', 'or_td_price'], 'integer'],
            [['onec_id', 'or_ordertime', 'or_ordersum', 'or_order_type', 'or_delivery_type'], 'required'],
            [['or_notice'], 'string'],
            [['or_td_rko'], 'number'],
            [['onec_id'], 'string', 'max' => 24],
            [['or_pick_point_address', 'or_companyname', 'or_inn', 'or_kpp', 'or_legalAdres', 'or_firstname', 'or_lname', 'or_surname', 'or_email', 'or_phone', 'or_deliveryAdres', 'or_td_pickup_address'], 'string', 'max' => 255],
            [['or_dopinfo'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'or_id' => 'Or ID',
            'first_confirm' => 'First Confirm',
            'first_confirm_send' => 'First Confirm Send',
            'second_confirm' => 'Second Confirm',
            'second_confirm_send' => 'Second Confirm Send',
            'onec_id' => 'Onec ID',
            'or_coupon_id' => 'Or Coupon ID',
            'or_ordertime' => 'Or Ordertime',
            'or_manager_id' => 'Or Manager ID',
            'or_manager_time' => 'Or Manager Time',
            'or_ordersum' => 'Or Ordersum',
            'or_order_type' => 'Or Order Type',
            'or_delivery_type' => 'Or Delivery Type',
            'or_pick_point_address' => 'Or Pick Point Address',
            'or_ship_addr' => 'Or Ship Addr',
            'or_notice' => 'Or Notice',
            'or_notice_time' => 'Or Notice Time',
            'or_companyname' => 'Or Companyname',
            'or_inn' => 'Or Inn',
            'or_kpp' => 'Or Kpp',
            'or_legalAdres' => 'Or Legal Adres',
            'or_firstname' => 'Or Firstname',
            'or_lname' => 'Or Lname',
            'or_surname' => 'Or Surname',
            'or_email' => 'Or Email',
            'or_phone' => 'Or Phone',
            'or_deliveryAdres' => 'Or Delivery Adres',
            'or_dopinfo' => 'Or Dopinfo',
            'unload' => 'Unload',
            'or_loaded' => 'Or Loaded',
            'vkredit' => 'Vkredit',
            'tiu' => 'Tiu',
            'tiu_id' => 'Tiu ID',
            'oneclickcall' => 'Oneclickcall',
            'from_mobile' => 'From Mobile',
            'or_user_id' => 'Or User ID',
            'or_subscribed' => 'Or Subscribed',
            'or_comments_unloaded' => 'Or Comments Unloaded',
            'or_td_pickup_address' => 'Or Td Pickup Address',
            'or_td_pickup_address_id' => 'Or Td Pickup Address ID',
            'or_td_delivery_type' => 'Or Td Delivery Type',
            'or_td_price' => 'Or Td Price',
            'or_td_rko' => 'Or Td Rko',
        ];
    }

    /**
     * @inheritdoc
     * @return OrdersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrdersQuery(get_called_class());
    }

    public function getPositions()
    {
        return $this->hasMany(OrderItem::className(), ['or_id' => 'or_id'])->indexBy('ct_id');
    }

    /**
     * Берем последний ид для 1С из базы
     */
    protected function OneCid()
    {
        $id = self::find()->addSelect('onec_id')->orderBy(['or_id' => SORT_DESC])->asArray()->one();
        $onecId = (int)str_replace('CT-', '', $id['onec_id']);
        return 'CT-'.++$onecId;
    }

    public function beforeValidate()
    {
        $this->onec_id = $this->OneCid();
        $this->or_ordertime = date("Y-m-d H:i:s", strtotime("+2 hour")); //Время оформления заказа
        $this->first_confirm = $this->acceptHoursForFirst(); // Первое уведомление в админке
        $this->second_confirm = $this->acceptHoursForSecond(); // Второе уведомление в админке

        if(!Yii::$app->user->isGuest) {
            $this->or_user_id = Yii::$app->user->id;
        }

        return parent::beforeValidate();
    }

    protected function acceptHoursForSecond()
    {
        $returnTime = '';
        $tmp = explode(' ', $this->or_ordertime);
        $tmpTime = explode(':', $tmp[1]);
        $nowHour = $tmpTime[0];
        $nowMin = $tmpTime[1];
        $nowDay = date("w");

        $acceptHoursForSecond = array('08','09','10','11','12','13','14');
        $accHoursForSecond = array('15','16','17','18','19','20','21','22','23');
        $nightHoursForSecond = array('00','01','02','03','04','05','06','07');
        //Формируем дату второго напоминания
        if(in_array($nowHour, $acceptHoursForSecond) && !in_array($nowDay,array(6,0))) {
            if($nowHour==14) {
                if($nowMin <= 20) {
                    $returnTime = date("Y-m-d H:i:s", strtotime("+118 min")); //Время второго напоминания
                } else {
                    $returnTime = date("Y-m-d", strtotime("+1 day")) . ' 09:10:00';
                }
            } else {
                $returnTime = date("Y-m-d H:i:s", strtotime("+118 min")); //Время второго напоминания
            }
        } elseif(in_array($nowHour,$accHoursForSecond) && !in_array($nowDay,array(6,0))) {
            $returnTime = date("Y-m-d", strtotime("+1 day")) . ' 09:10:00';
        } elseif(in_array($nowHour, $nightHoursForSecond) && !in_array($nowDay,array(6,0))){
            $returnTime = date("Y-m-d") . ' 09:10:00';
        } else {
            if(in_array($nowDay,array(5,6,0))) { //Смотрим, если заказ сделан в пятницу или выходные
                if($nowDay==5) {
                    $returnTime = date("Y-m-d", strtotime("+3 day")).' 10:00:00';
                } elseif($nowDay==6) {
                    $returnTime = date("Y-m-d", strtotime("+2 day")).' 10:00:00';
                } elseif($nowDay==0) {
                    $returnTime = date("Y-m-d", strtotime("+1 day")).' 10:00:00';
                }
            } else {
                $returnTime = date("Y-m-d", strtotime("+1 day")).' 10:00:00';
            }
        }

        return $returnTime;
    }

    protected function acceptHoursForFirst()
    {
        $returnTime = '';
        $tmp = explode(' ', $this->or_ordertime);
        $tmpTime = explode(':', $tmp[1]);
        $nowHour = $tmpTime[0];
        $nowMin = $tmpTime[1];
        $nowDay = date("w");

        $acceptHoursForFirst = array('08','09','10','11','12','13','14','15','16');
        $accHoursForFirst = array('00','01','02','03','04','05','06','07');


        //Формируем дату первого напоминания
        if(in_array($nowHour, $acceptHoursForFirst) && !in_array($nowDay,array(6,0))) {
            if ($nowHour == 16) {
                if($nowMin <= 30) {
                    $returnTime = date("Y-m-d H:i:s", strtotime("+8 min")); //Время первой напоминалки
                } else {
                    $returnTime = date("Y-m-d", strtotime("+1 day")) . ' 09:10:00';
                }
            } else {
                $returnTime = date("Y-m-d H:i:s", strtotime("+8 min")); //Время первой напоминалки
            }
        } elseif(in_array($nowHour,$accHoursForFirst) && !in_array($nowDay,array(6,0))){
            $returnTime = date("Y-m-d").' 09:10:00';
        } else { //Если заказ оформили после 16:30, преносим уведомление на следующий день, учитывая день недели
            if(in_array($nowDay,array(5,6,0))) { //Смотрим, если заказ сделан в пятницу или выходные
                if($nowDay==5) {
                    $returnTime = date("Y-m-d", strtotime("+3 day")).' 09:10:00';
                } elseif($nowDay==6) {
                    $returnTime = date("Y-m-d", strtotime("+2 day")).' 09:10:00';
                } elseif($nowDay==0) {
                    $returnTime = date("Y-m-d", strtotime("+1 day")).' 09:10:00';
                }
            } else {
                $returnTime = date("Y-m-d", strtotime("+1 day")).' 09:10:00';
            }
        }

        return $returnTime;
    }
}
