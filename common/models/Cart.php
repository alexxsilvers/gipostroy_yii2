<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Json;

/**
 * This is the model class for table "cart".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $create_time
 * @property string $update_time
 * @property string $items_json
 * @property integer $items_count
 * @property integer $total_price
 * @property [] $items
 * @property string $items_count_string
 * @property [] $messages
 */
class Cart extends ActiveRecord
{
    protected static $cart;
    protected $goods;
    protected $coupon = null;
    public $items_count_string;
    public $items = [];
    public $messages = [];



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['create_time', 'update_time'], 'safe'],
            [['items_json'], 'string'],
            [['total_price', 'items_count'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'create_time' => 'Время создания',
            'update_time' => 'Время обновления',
            'items_json' => 'Товары',
            'items_count' => 'Кол-во товаров',
            'total_price' => 'Сумма',
        ];
    }

    public function beforeSave($insert)
    {
        if(!parent::beforeSave($insert)) {
            return false;
        }
        $this->items_json = Json::encode($this->items);
        if($insert || !isset($this->oldAttributes['items_count']) || $this->oldAttributes['items_count'] == 0) {
            $this->create_time = new Expression('NOW()');
        }
        return true;
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->items = Json::decode($this->items_json);
        $this->items_count_string = Yii::t('front', '{n, plural, =1{# товар} one{# товар} few{# товара} many{# товаров} other{# товара}}', ['n' => $this->items_count]);
    }

    /**
     * @param bool $create
     * @return Cart
     */
    public static function getCart($create = false)
    {
        if(is_null(self::$cart) && isset($_COOKIE['gipostroy_cart'])) {
            self::$cart = self::findOne(['id' => self::getCartId($_COOKIE['gipostroy_cart'])]);
        }
        if(is_null(self::$cart) && !Yii::$app->user->isGuest) {
            self::$cart = self::findOne(['user_id' => Yii::$app->user->id]);
        }
        if(is_null(self::$cart) && $create === true) {
            $cart = new static;
            $cart->user_id = !Yii::$app->user->isGuest ? Yii::$app->user->id : 0;
            if($cart->save()) {
                self::$cart = $cart;
                setcookie('gipostroy_cart', self::setCartId($cart->id), time()+604800, '/');
            }
        }

        return self::$cart;
    }


    /**
     * @return bool
     */
    public function reCalc()
    {
        $totalPrice = 0;
        $itemsCount = 0;
        $goods = Goods::find()->where(['in', 'ct_id', array_keys($this->items)])->with(['unit', 'section', 'subsection'])->all();
        $catArr = GoodCat::find()->addSelect(['cat_id'])->where(['in', 'good_id', array_keys($this->items)])->column();
        $subcatArr = GoodSubcat::find()->addSelect(['subcat_id'])->where(['in', 'good_id', array_keys($this->items)])->column();
        $items = [];
        $this->goods = [];

        $catActions = [
            13 => [
                'cat' => [168],
                'subcat' => [],
                'discount' => 10,
                'type' => 'percent',
                'message' => 'При покупке сухих смесей скидка на весь инструмент 10%',
            ],
        ];
        $subcatActions = [
            218 => [
                'cat' => [],
                'subcat' => [219,220,221,222,224,227,258,259],
                'discount' => 10,
                'type' => 'percent',
                'message' => 'При покупке смесителя скидка на все аксессуары 10%',
            ],
        ];

        /* @var $good Goods */
        foreach($goods as $good) {
            $itemsCount++;
            $originalPrice = round($good->getGoodPriceInteger(false), 0, PHP_ROUND_HALF_UP);
            $goodPrice = $originalPrice;

            if(isset($catActions[$good['section']['prz_id']])) { // 2 уровень
                $action = $catActions[$good['section']['prz_id']];
                if((!empty($action['cat']) && array_intersect($action['cat'], $catArr)) || (!empty($action['subcat']) && array_intersect($action['subcat'], $subcatArr))) {
                    $this->messages[$good['section']['prz_id']] = $action['message'];
                    if($action['type'] == 'percent') {
                        $goodPrice = round($originalPrice - ($originalPrice * ($action['discount'] / 100)), 0, PHP_ROUND_HALF_UP);
                    }
                    if($action['type'] == 'price') {
                        if($originalPrice > $action['discount']) {
                            $goodPrice = round($originalPrice - round($action['discount'] / $this->items[$good->ct_id]['quantity'], 0, PHP_ROUND_HALF_UP), 0, PHP_ROUND_HALF_UP);
                        }
                    }
                }
            }

            if(isset($subcatActions[$good['subsection']['pprz_id']])) { // 3 уровень
                $action = $subcatActions[$good['subsection']['pprz_id']];
                if((!empty($action['cat']) && array_intersect($action['cat'], $catArr)) || (!empty($action['subcat']) && array_intersect($action['subcat'], $subcatArr))) {
                    $this->messages[$good['subsection']['pprz_id']] = $action['message'];
                    if($action['type'] == 'percent') {
                        $goodPrice = round($originalPrice - ($originalPrice * ($action['discount'] / 100)), 0, PHP_ROUND_HALF_UP);
                    }
                    if($action['type'] == 'price') {
                        if($originalPrice > $action['discount']) {
                            $goodPrice = round($originalPrice - round($action['discount'] / $this->items[$good->ct_id]['quantity'], 0, PHP_ROUND_HALF_UP), 0, PHP_ROUND_HALF_UP);
                        }
                    }
                }
            }

            $allPrice = round($this->items[$good->ct_id]['quantity'] * $goodPrice, 0, PHP_ROUND_HALF_UP);
            $items[$good->ct_id] = $this->items[$good->ct_id];
            $items[$good->ct_id]['original_price'] = $originalPrice;
            $items[$good->ct_id]['price'] = $goodPrice;
            $items[$good->ct_id]['all_price'] = $allPrice;
            $totalPrice += $allPrice;

            $this->goods[$good->ct_id] = $good;
        }


        $this->total_price = round($totalPrice, 0);
        $this->items_count = $itemsCount;
        $this->items_count_string = Yii::t('front', '{n, plural, =1{# товар} one{# товар} few{# товара} many{# товаров} other{# товара}}', ['n' => $this->items_count]);
        $this->items = $items;
        return $this->save();
    }

    public function getGoods()
    {
        return $this->goods;
    }

    private static function getCartId($cookie)
    {
        return substr($cookie, -1);
    }

    private static function setCartId($id)
    {
        $hash = md5($id . time());
        return substr($hash, 0, strlen($hash)-1) . $id;
    }

    public function isCouponUsed()
    {
        if(is_null($this->coupon)) {
            return false;
        }

        return true;
    }
}
