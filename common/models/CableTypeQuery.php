<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[CableType]].
 *
 * @see CableType
 */
class CableTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CableType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CableType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}