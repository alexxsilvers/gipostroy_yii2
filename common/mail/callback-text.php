<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

/* @var $model frontend\models\CallbackForm */
?>
Имя <b><?= Html::encode($model->name) ?></b>,\n
Номер телефона <b><?= Html::encode($model->phone) ?></b>\n

<?php if(!empty($model->razdel)) { ?>
, Вопрос по теме <b><?= Html::encode($model->razdel) ?></b>\n
<?php } ?>

<?php if(!empty($model->comment)) { ?>
, Комментарий <b><?= Html::encode($model->comment) ?></b>\n
<?php } ?>
