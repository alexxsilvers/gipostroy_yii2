<?php
/* @var $this yii\web\View */
use yii\helpers\Html;

/* @var $model frontend\models\ProposalForm */
?>
Имя <b><?= Html::encode($model->name) ?></b>,\n
Email <b><?= Html::encode($model->email) ?></b>,\n
Номер телефона <b><?= Html::encode($model->phone) ?></b>\n

<?php if(!empty($model->companyName)) { ?>
, Компания <b>"<?= Html::encode($model->companyName) ?>"</b>\n
<?php } ?>

<?php if(!empty($model->inn)) { ?>
, ИНН <b><?= Html::encode($model->inn) ?></b>\n
<?php } ?>

<?php if(!empty($model->comment)) { ?>
, Комментарий <b><?= Html::encode($model->comment) ?></b>
<?php } ?>