<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProposalForm */
$this->title = 'Новая заявка с сайта';
?>
<div class="password-reset">
    <p>Имя <b><?= Html::encode($model->name) ?></b></p>
    <p>Email <b><?= Html::encode($model->email) ?></b></p>
    <p>Телефон <b><?= Html::encode($model->phone) ?></b></p>

    <?php if(!empty($model->companyName)) { ?>
        <p>Компания <b>"<?= Html::encode($model->companyName) ?>"</b></p>
    <?php } ?>

    <?php if(!empty($model->inn)) { ?>
        <p>ИНН <b><?= Html::encode($model->inn) ?></b></p>
    <?php } ?>

    <?php if(!empty($model->comment)) { ?>
        <p>Комментарий <b><?= Html::encode($model->comment) ?></b></p>
    <?php } ?>
</div>
