<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\CallbackForm */
$this->title = 'Новый запрос на обратный звонок';
?>
<div>
    <p>Имя <b><?= Html::encode($model->name) ?></b></p>
    <p>Телефон <b><?= Html::encode($model->phone) ?></b></p>

    <?php if(!empty($model->razdel)) { ?>
        <p>Вопрос по теме <b><?= Html::encode($model->razdel) ?></b></p>
    <?php } ?>

    <?php if(!empty($model->comment)) { ?>
        <p>Комментарий <b><?= Html::encode($model->comment) ?></b></p>
    <?php } ?>
</div>
