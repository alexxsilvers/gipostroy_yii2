<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\ReviewForm */
$this->title = 'Новая заявка с сайта';
?>
<div class="password-reset">
    <p>Тема <b><?= Html::encode($model->tema) ?></b></p>
    <p>Email <b><?= Html::encode($model->comment) ?></b></p>
</div>
