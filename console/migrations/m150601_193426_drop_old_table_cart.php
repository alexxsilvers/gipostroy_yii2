<?php

use yii\db\Schema;
use yii\db\Migration;

class m150601_193426_drop_old_table_cart extends Migration
{
    public function safeUp()
    {
        $this->dropTable('cart');
    }

    public function safeDown()
    {
        echo "m150601_193426_drop_old_table_cart cannot be reverted.\n";

        return false;
    }
}
