<?php

use yii\db\Schema;
use yii\db\Migration;

class m150626_195605_add_new_double_index_to_good_cat_and_good_subcat_tables extends Migration
{
    public function safeUp()
    {
        $this->createIndex('good_cat_index', 'good_cat', ['good_id', 'cat_id']);
        $this->createIndex('good_cat_index', 'good_subcat', ['good_id', 'subcat_id']);
    }
    
    public function safeDown()
    {
        $this->dropIndex('good_cat_index', 'good_cat');
        $this->dropIndex('good_cat_index', 'good_subcat');
    }
}
