<?php

use yii\db\Schema;
use yii\db\Migration;

class m150626_193243_add_index_to_good_cat_and_good_subcat_tables extends Migration
{    
    public function safeUp()
    {
        $this->createIndex('good', 'good_cat', 'good_id');
        $this->createIndex('good', 'good_subcat', 'good_id');
    }
    
    public function safeDown()
    {
        $this->dropIndex('good', 'good_cat');
        $this->dropIndex('good', 'good_subcat');
    }
}
