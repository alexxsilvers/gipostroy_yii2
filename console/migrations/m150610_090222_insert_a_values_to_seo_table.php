<?php

use yii\db\Schema;
use yii\db\Migration;

class m150610_090222_insert_a_values_to_seo_table extends Migration
{
    public function safeUp()
    {
        $this->update('seo_text', ['entity_type' => 'level4'], ['id' => 1]);
        $this->update('seo_text', ['entity_type' => 'level4'], ['id' => 2]);

        $this->update('seo_text', ['entity_type' => 'level4'], ['id' => 3]);
        $this->update('seo_text', ['entity_type' => 'level4'], ['id' => 4]);

        $this->update('seo_text', ['entity_type' => 'level3'], ['id' => 5]);
        $this->update('seo_text', ['entity_type' => 'level3'], ['id' => 6]);
        $this->update('seo_text', ['entity_type' => 'level3'], ['id' => 7]);
        $this->update('seo_text', ['entity_type' => 'level3'], ['id' => 8]);
        $this->update('seo_text', ['entity_type' => 'level3'], ['id' => 9]);

        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 10]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 11]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 12]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 13]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 14]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 15]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 16]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 17]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 18]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 19]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 20]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 21]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 22]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 23]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 24]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 25]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 26]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 27]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 28]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 29]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 30]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 31]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 32]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 33]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 34]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 35]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 36]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 37]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 38]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 39]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 40]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 41]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 42]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 43]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 44]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 45]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 46]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 47]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 48]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 49]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 50]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 51]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 52]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 53]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 54]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 55]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 56]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 57]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 58]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 59]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 60]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 61]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 62]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 63]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 64]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 65]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 66]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 67]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 68]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 69]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 70]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 71]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 72]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 73]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 74]);
        $this->update('seo_text', ['entity_type' => 'tag'], ['id' => 75]);
    }
    
    public function safeDown()
    {
        $this->update('seo_text', ['entity_type' => null]);
    }
}
