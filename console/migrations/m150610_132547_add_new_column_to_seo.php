<?php

use yii\db\Schema;
use yii\db\Migration;

class m150610_132547_add_new_column_to_seo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('seo_text', 'condition', 'string');
    }

    public function safeDown()
    {
        $this->dropColumn('seo_text', 'condition');
    }
}
