<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_075417_add_a_doska_values_to_seo_table extends Migration
{
    public function safeUp()
    {
        $this->insert('seo_text', [
            'cat_id' => 74,
            'subcat_id' => 0,
            'type' => 'Доска обрезная 25 мм.',
            'url' => '25',
            'title' => 'Купить доску обрезную 25 мм. по лучшим ценам. Компания "Гипострой.ру"',
            'description' => 'Доска обрезная 25 мм. в интернет магазине Гипострой',
            'keywords' => 'Доска обрезная 25 мм.',
            'header' => 'Доска обрезная 25 мм.',
            'entity_type' => 'level3',
            'condition' => '{"ct_vin_class":"25"}',
        ]);
        $this->insert('seo_text', [
            'cat_id' => 74,
            'subcat_id' => 0,
            'type' => 'Доска обрезная 30 мм.',
            'url' => '30',
            'title' => 'Купить доску обрезную 30 мм. по лучшим ценам. Компания "Гипострой.ру"',
            'description' => 'Доска обрезная 30 мм. в интернет магазине Гипострой',
            'keywords' => 'Доска обрезная 30 мм.',
            'header' => 'Доска обрезная 30 мм.',
            'entity_type' => 'level3',
            'condition' => '{"ct_vin_class":"30"}',
        ]);
        $this->insert('seo_text', [
            'cat_id' => 74,
            'subcat_id' => 0,
            'type' => 'Доска обрезная 40 мм.',
            'url' => '40',
            'title' => 'Купить доску обрезную 40 мм. по лучшим ценам. Компания "Гипострой.ру"',
            'description' => 'Доска обрезная 40 мм. в интернет магазине Гипострой',
            'keywords' => 'Доска обрезная 40 мм.',
            'header' => 'Доска обрезная 40 мм.',
            'entity_type' => 'level3',
            'condition' => '{"ct_vin_class":"40"}',
        ]);
        $this->insert('seo_text', [
            'cat_id' => 74,
            'subcat_id' => 0,
            'type' => 'Доска обрезная 50 мм.',
            'url' => '50',
            'title' => 'Купить доску обрезную 50 мм. по лучшим ценам. Компания "Гипострой.ру"',
            'description' => 'Доска обрезная 50 мм. в интернет магазине Гипострой',
            'keywords' => 'Доска обрезная 50 мм.',
            'header' => 'Доска обрезная 50 мм.',
            'entity_type' => 'level3',
            'condition' => '{"ct_vin_class":"50"}',
        ]);
    }
    
    public function safeDown()
    {
        return false;
    }
}
