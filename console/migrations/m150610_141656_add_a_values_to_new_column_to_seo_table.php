<?php

use yii\db\Schema;
use yii\db\Migration;

class m150610_141656_add_a_values_to_new_column_to_seo_table extends Migration
{
    public function safeUp()
    {
        $this->update('seo_text', ['condition' => '{"ct_vin_class":"Колодезный"}'], ['id' => 1]);
        $this->update('seo_text', ['condition' => '{"ct_vin_class":"Скважинный"}'], ['id' => 2]);

        $this->update('seo_text', ['condition' => 'level4'], ['id' => 3]);
        $this->update('seo_text', ['condition' => 'level4'], ['id' => 4]);

        $this->update('seo_text', ['condition' => '{"ct_ker_naz":"Бытовой"}'], ['id' => 5]);
        $this->update('seo_text', ['condition' => '{"ct_ker_naz":"Детский"}'], ['id' => 6]);
        $this->update('seo_text', ['condition' => '{"ct_ker_naz":"Офисный"}'], ['id' => 7]);
        $this->update('seo_text', ['condition' => '{"ct_ker_naz":"Коммерческий"}'], ['id' => 8]);
        $this->update('seo_text', ['condition' => '{"ct_ker_naz":"Выставочный"}'], ['id' => 9]);

        $this->update('seo_text', ['condition' => '{"ct_pil_razm":"Да"}'], ['id' => 10]);
        $this->update('seo_text', ['condition' => '["and",[">=","ct_vin_class",300],["!=","ct_vin_class",""]]'], ['id' => 11]);
        $this->update('seo_text', ['condition' => '{"char_field1":"Да"}'], ['id' => 12]);
        $this->update('seo_text', ['condition' => '{"ct_lamp_power":"Да"}'], ['id' => 13]);
        $this->update('seo_text', ['condition' => '{"ct_lamp_power":"Да"}'], ['id' => 14]);
        $this->update('seo_text', ['condition' => '{"ct_vin_area":"Да"}'], ['id' => 15]);
        $this->update('seo_text', ['condition' => '{"ct_ker_pov":"На стену"}'], ['id' => 16]);
        $this->update('seo_text', ['condition' => '{"mn_name":"Чехия"}'], ['id' => 17]);
        $this->update('seo_text', ['condition' => '{"mn_name":"Россия"}'], ['id' => 18]);
        $this->update('seo_text', ['condition' => '{"mn_name":"Германия"}'], ['id' => 19]);
        $this->update('seo_text', ['condition' => '{"ct_pil_sort":"Бесконтактный"}'], ['id' => 20]);
        $this->update('seo_text', ['condition' => '{"ct_lamp_cokol":"Да"}'], ['id' => 21]);
        $this->update('seo_text', ['condition' => '{"ct_lamp_type":"Да"}'], ['id' => 22]);
        $this->update('seo_text', ['condition' => '{"mn_name":"Болгария"}'], ['id' => 23]);
        $this->update('seo_text', ['condition' => '{"char_field2":"Да"}'], ['id' => 24]);
        $this->update('seo_text', ['condition' => '{"ct_ker_pov":"На борт ванны"}'], ['id' => 25]);
        $this->update('seo_text', ['condition' => '{"ct_vin_razm":"Для ванны"}'], ['id' => 26]);
        $this->update('seo_text', ['condition' => '{"ct_vin_razm":"Для ванны"}'], ['id' => 27]);
        $this->update('seo_text', ['condition' => '{"ct_vin_razm":"Для ванны"}'], ['id' => 28]);
        $this->update('seo_text', ['condition' => '{"ct_vin_colvo":"Поворотный"}'], ['id' => 29]);
        $this->update('seo_text', ['condition' => '["and",["<=","ct_vin_class",300],["!=","ct_vin_class",""]]'], ['id' => 30]);
        $this->update('seo_text', ['condition' => '["and",[">","ct_vin_zamok",200],["!=","ct_vin_zamok",""]]'], ['id' => 31]);
        $this->update('seo_text', ['condition' => '["and",[">","ct_vin_zamok",200],["!=","ct_vin_zamok",""]]'], ['id' => 32]);
        $this->update('seo_text', ['condition' => '{"mn_name":"Китай"}'], ['id' => 33]);
        $this->update('seo_text', ['condition' => '{"ct_pil_poroda":"Золото"}'], ['id' => 34]);
        $this->update('seo_text', ['condition' => '{"ct_pil_poroda":"Матовый хром"}'], ['id' => 35]);
        $this->update('seo_text', ['condition' => '{"ct_pil_poroda":"Черный"}'], ['id' => 36]);
        $this->update('seo_text', ['condition' => '{"ct_pil_poroda":"Бронза"}'], ['id' => 37]);
        $this->update('seo_text', ['condition' => '{"ct_pil_poroda":"Бежевый"}'], ['id' => 38]);
        $this->update('seo_text', ['condition' => '{"ct_pil_poroda":"Белый"}'], ['id' => 39]);
        $this->update('seo_text', ['condition' => '{"ct_pil_poroda":"Медь"}'], ['id' => 40]);
        $this->update('seo_text', ['condition' => '["in","ct_pil_poroda",["Матовый черный","Матовый хром"]]'], ['id' => 41]);
        $this->update('seo_text', ['condition' => '{"ct_lamp_type_s":"Да"}'], ['id' => 42]);
        $this->update('seo_text', ['condition' => '{"ct_ker_pov":"Бесцветный"}'], ['id' => 43]);
        $this->update('seo_text', ['condition' => '{"ct_lamp_cokol":"Да"}'], ['id' => 44]);
        $this->update('seo_text', ['condition' => '{"ct_ker_naz":"Глянцевый"}'], ['id' => 45]);
        $this->update('seo_text', ['condition' => '{"ct_vin_zamok":"Лак для наружных работ"}'], ['id' => 46]);
        $this->update('seo_text', ['condition' => '{"ct_vin_class":"Лак для террасы"}'], ['id' => 47]);
        $this->update('seo_text', ['condition' => '{"ct_pil_razm":"Да"}'], ['id' => 48]);
        $this->update('seo_text', ['condition' => '{"ct_vin_class":"Лак для пола"}'], ['id' => 49]);
        $this->update('seo_text', ['condition' => '{"ct_vin_class":"Лак для мебели"}'], ['id' => 50]);
        $this->update('seo_text', ['condition' => '{"ct_vin_class":"Лак для печей и каминов"}'], ['id' => 51]);
        $this->update('seo_text', ['condition' => '{"ct_lamp_power":"Лак для камня"}'], ['id' => 52]);
        $this->update('seo_text', ['condition' => '{"ct_lamp_power":"Лак для металла"}'], ['id' => 53]);
        $this->update('seo_text', ['condition' => '{"ct_mn_id":"74"}'], ['id' => 54]);
        $this->update('seo_text', ['condition' => '["and",["!=","ct_vin_class",""],["!=","ct_lamp_power",""]]'], ['id' => 55]);
        $this->update('seo_text', ['condition' => '{"mn_name":"Россия"}'], ['id' => 56]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 57]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 58]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 59]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 60]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 61]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 62]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 63]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 64]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 65]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 66]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 67]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 68]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 69]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 70]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 71]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 72]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 73]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 74]);
        $this->update('seo_text', ['condition' => 'tag'], ['id' => 75]);
    }

    public function safeDown()
    {
        $this->update('seo_text', ['condition' => null]);
    }
}
