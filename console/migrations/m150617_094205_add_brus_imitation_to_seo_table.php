<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_094205_add_brus_imitation_to_seo_table extends Migration
{
    public function safeUp()
    {
        $this->insert('seo_text', [
            'cat_id' => 76,
            'subcat_id' => 0,
            'type' => 'Имитация бруса 140 мм.',
            'url' => '140',
            'title' => 'Купить имитацию бруса 140 мм. по лучшим ценам. Компания "Гипострой.ру"',
            'description' => 'Имитация бруса 140 мм. в интернет магазине Гипострой',
            'keywords' => 'Имитация бруса 140 мм. в интернет магазине Гипострой',
            'header' => 'Имитация бруса 140 мм.',
            'entity_type' => 'level3',
            'condition' => '{"ct_vin_class":"140"}',
        ]);
        $this->insert('seo_text', [
            'cat_id' => 76,
            'subcat_id' => 0,
            'type' => 'Имитация бруса 185 мм.',
            'url' => '185',
            'title' => 'Купить имитацию бруса 185 мм. по лучшим ценам. Компания "Гипострой.ру"',
            'description' => 'Имитация бруса 185 мм. в интернет магазине Гипострой',
            'keywords' => 'Имитация бруса 185 мм. в интернет магазине Гипострой',
            'header' => 'Имитация бруса 185 мм.',
            'entity_type' => 'level3',
            'condition' => '{"ct_vin_class":"185"}',
        ]);
    }
    
    public function safeDown()
    {
        return false;
    }
}
