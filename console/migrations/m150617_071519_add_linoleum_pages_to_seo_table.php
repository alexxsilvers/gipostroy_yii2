<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_071519_add_linoleum_pages_to_seo_table extends Migration
{
    public function safeUp()
    {
        $this->insert('seo_text', [
            'cat_id' => 47,
            'subcat_id' => 0,
            'type' => 'Бытовой',
            'url' => 'bitovoyy',
            'title' => 'Линолеум бытовой',
            'description' => 'Линолеум бытовой',
            'keywords' => 'Линолеум бытовой',
            'header' => 'Линолеум бытовой',
            'pic' => 'bitovoy2.jpg',
            'entity_type' => 'level3',
            'condition' => '{"ct_linoleum_type":"1"}',
        ]);
        $this->insert('seo_text', [
            'cat_id' => 47,
            'subcat_id' => 0,
            'type' => 'Полукоммерческий',
            'url' => 'polucommercheskiy',
            'title' => 'Линолеум Полукоммерческий',
            'description' => 'Линолеум полукоммерческий',
            'keywords' => 'Линолеум полукоммерческий',
            'header' => 'Линолеум полукоммерческий',
            'pic' => 'polukomercheskiy.jpg',
            'entity_type' => 'level3',
            'condition' => '{"ct_linoleum_type":"2"}',
        ]);
        $this->insert('seo_text', [
            'cat_id' => 47,
            'subcat_id' => 0,
            'type' => 'Коммерческий-гомогенный',
            'url' => 'gomogenniy',
            'title' => 'Линолеум коммерческий-гомогенный',
            'description' => 'Линолеум коммерческий-гомогенный',
            'keywords' => 'Линолеум коммерческий-гомогенный',
            'header' => 'Линолеум коммерческий-гомогенный',
            'pic' => 'gomogenniy.jpg',
            'entity_type' => 'level3',
            'condition' => '{"ct_linoleum_type":"3"}',
        ]);
        $this->insert('seo_text', [
            'cat_id' => 47,
            'subcat_id' => 0,
            'type' => 'Коммерческий-гетерогенный',
            'url' => 'geterogenniy',
            'title' => 'Линолеум коммерческий-гетерогенный',
            'description' => 'Линолеум коммерческий-гетерогенный',
            'keywords' => 'Линолеум коммерческий-гетерогенный',
            'header' => 'Линолеум коммерческий-гетерогенный',
            'pic' => 'geterogenniy.jpg',
            'entity_type' => 'level3',
            'condition' => '{"ct_linoleum_type":"4"}',
        ]);
    }
    
    public function safeDown()
    {
        return false;
    }
}
