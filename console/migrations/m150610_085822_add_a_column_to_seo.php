<?php

use yii\db\Schema;
use yii\db\Migration;

class m150610_085822_add_a_column_to_seo extends Migration
{
    public function safeUp()
    {
        $this->addColumn('seo_text', 'entity_type', 'string');
    }

    public function safeDown()
    {
        $this->dropColumn('seo_text', 'entity_type');
    }
}
