<?php

use yii\db\Schema;
use yii\db\Migration;

class m150626_200756_add_index_to_more_photos_table extends Migration
{
    public function safeUp()
    {
        $this->createIndex('good_id_index', 'more_photos', 'good_id');
    }
    
    public function safeDown()
    {
        $this->dropIndex('good_id_index', 'more_photos');
    }
}
