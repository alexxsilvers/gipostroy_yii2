<?php

use yii\db\Schema;
use yii\db\Migration;

class m150626_185305_add_index_to_rr_cache extends Migration
{
    public function safeUp()
    {
        $this->createIndex('good_type_index', 'rr_cache', ['item_id', 'type']);
    }
    
    public function safeDown()
    {
        $this->dropIndex('good_type_index', 'rr_cache');
    }
}
