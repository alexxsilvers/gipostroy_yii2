<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_114754_fasad_paneli_seo_table extends Migration
{
    public function safeUp()
    {
        $this->update('ct_catalog', [
            'ct_ker_pov' => 'Да'
        ], ['and', ['in', 'ct_id', [111047, 111043, 111045, 111048, 111046]]]);
        $this->update('seo_text', [
            'condition' => '{"ct_ker_pov":""}',
        ], ['id' => 3]);
        $this->update('seo_text', [
            'condition' => '{"ct_ker_pov":"Да"}',
        ], ['id' => 4]);

        $this->update('good_subcat', [
            'subcat_id' => 265,
        ], ['good_id' => 111047]);
        $this->update('good_subcat', [
            'subcat_id' => 265,
        ], ['good_id' => 111043]);
        $this->update('good_subcat', [
            'subcat_id' => 265,
        ], ['good_id' => 111045]);
        $this->update('good_subcat', [
            'subcat_id' => 265,
        ], ['good_id' => 111048]);
        $this->update('good_subcat', [
            'subcat_id' => 265,
        ], ['good_id' => 111046]);
    }
    
    public function safeDown()
    {
        $this->update('ct_catalog', [
            'ct_ker_pov' => ''
        ], ['and', ['in', 'ct_id', [111047, 111043, 111045, 111048, 111046]]]);
        $this->update('seo_text', [
            'condition' => '',
        ], ['id' => 3]);
        $this->update('seo_text', [
            'condition' => '',
        ], ['id' => 4]);
    }
}
