<?php

use yii\db\Schema;
use yii\db\Migration;

class m150626_201635_add_index_to_reviews_table extends Migration
{
    public function safeUp()
    {
        $this->createIndex('good_id_index', 'rw_reviews', 'good_id');
    }
    
    public function safeDown()
    {
        $this->dropIndex('good_id_index', 'rw_reviews');
    }
}
