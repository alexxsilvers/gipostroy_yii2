<?php

use yii\db\Schema;
use yii\db\Migration;

class m150702_065209_add_index_to_rr_cache extends Migration
{
    public function safeUp()
    {
        $this->createIndex('type_index_c', 'rr_cache', 'type');
    }

    public function safeDown()
    {
        $this->dropIndex('type_index_c', 'rr_cache');
    }
}
