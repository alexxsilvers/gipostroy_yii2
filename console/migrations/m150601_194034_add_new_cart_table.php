<?php

use yii\db\Schema;
use yii\db\Migration;

class m150601_194034_add_new_cart_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable(
            'cart',
            [
                'id' => 'BIGINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
                'user_id' => 'INT UNSIGNED DEFAULT \'0\'',
                'create_time' => 'TIMESTAMP NULL',
                'update_time' => 'TIMESTAMP NULL',
                'items_json' => 'TEXT',
                'items_count' => 'INT DEFAULT \'0\'',
                'total_price' => 'INT DEFAULT \'0\''
            ],
            $tableOptions
        );
    }
    
    public function safeDown()
    {
        $this->dropTable('cart');
    }
}
