<?php

use yii\db\Schema;
use yii\db\Migration;

class m150616_123218_add_a_new_records_to_seo_text_table extends Migration
{
    public function safeUp()
    {
        $this->insert('seo_text', [
            'cat_id' => 225,
            'subcat_id' => 302,
            'type' => 'Сиденье',
            'url' => 'sidenie',
            'title' => 'Сиденья для унитазов',
            'description' => 'Сиденья для унитазов',
            'keywords' => 'Сиденья для унитазов',
            'header' => 'Сиденье',
            'pic' => 'sidenie.jpg',
            'entity_type' => 'level4',
            'condition' => '{"ct_tema":"Сиденье"}',
        ]);
        $this->insert('seo_text', [
            'cat_id' => 225,
            'subcat_id' => 302,
            'type' => 'Чаша',
            'url' => 'chasha',
            'title' => 'Чаши для унитазов',
            'description' => 'Чаши для унитазов',
            'keywords' => 'Чаши для унитазов',
            'header' => 'Чаша',
            'pic' => 'chasha.jpg',
            'entity_type' => 'level4',
            'condition' => '{"ct_tema":"Чаша"}',
        ]);
        $this->insert('seo_text', [
            'cat_id' => 225,
            'subcat_id' => 302,
            'type' => 'Бачок',
            'url' => 'bachok',
            'title' => 'Бачки для унитазов',
            'description' => 'Бачки для унитазов',
            'keywords' => 'Бачки для унитазов',
            'header' => 'Бачок',
            'pic' => 'bachok.jpg',
            'entity_type' => 'level4',
            'condition' => '{"ct_tema":"Бачок"}',
        ]);
        $this->insert('seo_text', [
            'cat_id' => 225,
            'subcat_id' => 302,
            'type' => 'Спинка к сиденью',
            'url' => 'spinka-k-sideniu',
            'title' => 'Спинки к сиденью для унитазов',
            'description' => 'Спинки к сиденью для унитазов',
            'keywords' => 'Спинки к сиденью для унитазов',
            'header' => 'Спинка к сиденью',
            'pic' => 'spinka.jpg',
            'entity_type' => 'level4',
            'condition' => '{"ct_tema":"Спинка к сиденью"}',
        ]);
    }
    
    public function safeDown()
    {
        return false;
    }
}
