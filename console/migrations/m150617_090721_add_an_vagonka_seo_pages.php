<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_090721_add_an_vagonka_seo_pages extends Migration
{
    public function safeUp()
    {
        $this->insert('seo_text', [
            'cat_id' => 72,
            'subcat_id' => 0,
            'type' => 'Класс А',
            'url' => 'A',
            'title' => 'Купить вагонку A класса по лучшим ценам. Компания "Гипострой.ру"',
            'description' => 'Вагонка A класса в интернет магазине Гипострой',
            'keywords' => 'Вагонка A класса в интернет магазине Гипострой',
            'header' => 'Класс А',
            'entity_type' => 'level3',
            'condition' => '{"ct_pil_sort":"А"}',
        ]);
        $this->insert('seo_text', [
            'cat_id' => 72,
            'subcat_id' => 0,
            'type' => 'Класс B',
            'url' => 'B',
            'title' => 'Купить вагонку B класса по лучшим ценам. Компания "Гипострой.ру"',
            'description' => 'Вагонка B класса в интернет магазине Гипострой',
            'keywords' => 'Вагонка B класса в интернет магазине Гипострой',
            'header' => 'Класс B',
            'entity_type' => 'level3',
            'condition' => '{"ct_pil_sort":"В"}',
        ]);
        $this->insert('seo_text', [
            'cat_id' => 72,
            'subcat_id' => 0,
            'type' => 'Класс C',
            'url' => 'C',
            'title' => 'Купить вагонку C класса по лучшим ценам. Компания "Гипострой.ру"',
            'description' => 'Вагонка C класса в интернет магазине Гипострой',
            'keywords' => 'Вагонка C класса в интернет магазине Гипострой',
            'header' => 'Класс C',
            'entity_type' => 'level3',
            'condition' => '{"ct_pil_sort":"С"}',
        ]);
    }
    
    public function safeDown()
    {
        return false;
    }
}
